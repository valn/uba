-- Ejericio 1:
-- null :: [a] -> Bool
-- Devuelve True <=> la lista está vacía

-- head :: [a] -> a
-- Devuelve el primer elemento de la lista

-- tail :: [a] -> [a]
-- Devuelve una lista de longitud: len(lista_pasada_como_parámetro) - 1, y con los mismos elementos que lista_pasada_como_parámetro salvo el primer elemento

-- init :: [a] -> [a]
-- Devuelve una lista de longitud: len(lista_pasada_como_parámetro) - 1, y con los mismos elementos que la lista_pasada_como_parámetro salvo el último elemento

-- last :: [a] -> a
-- Devuelve el último elemento de la lista

-- take :: Int -> [a] -> [a]
-- Devuelve una lista (sublista) con los primeros número_pasado_como_parámetro elementos (manteniendo el órden original de lista_pasada_como_parámetro)

-- drop :: Int -> [a] -> [a]
-- TL;DR: devuelve el complemento de take

-- (++) :: [a] -> [a] -> [a]
-- Devuelve la concatenación de dos listas

-- concat :: [[a]] -> [a]
-- Devuelve la concatenación de todas las listas pasadas en la lista como parámetro

-- (!!) :: [a] -> Int -> a
-- Devuelve el elemento en la iésima (el int que pasamos) posición

-- elem :: a -> [a] -> Bool
-- Devuelve True <=> el elemento (pasado como parámetro) está en la lista (pasada como parámetro)

--Ejericio 2:
valorAbsoluto :: Float -> Float
valorAbsoluto x | x >= 0 = x
                | otherwise = -x

-- Ej de notación lambda con dos parámetros
esDivisible :: Int -> Int -> Bool
esDivisible = \x y -> mod x y == 0

bisiesto :: Int -> Bool
bisiesto x | esDivisible x 4 && not (esDivisible x 100) = True
           | esDivisible x 4 && esDivisible x 100 && esDivisible x 400 = True
           | otherwise = False

factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial n-1

menorDivisor :: Int -> Int
menorDivisor n = menorDivisorDesde n 2

-- No se puede usar la notación lambda junto a guardas. Es necesario utilizar if-then-else.
menorDivisorDesde :: Int -> Int -> Int
menorDivisorDesde = \n m -> if esDivisible n m then m else menorDivisorDesde n (m+1)

esPrimo :: Int -> Bool
esPrimo = \n -> n == menorDivisor n

cantDivisoresPrimos :: Int -> Int
cantDivisoresPrimos n = cantDivisoresPrimosHasta n n

cantDivisoresPrimosHasta :: Int -> Int -> Int
cantDivisoresPrimosHasta n m | m == 1 = 0
                             | esDivisible n m && esPrimo m = 1 + cantDivisoresPrimosHasta n (m-1)
                             | otherwise = cantDivisoresPrimosHasta n (m-1)

-- Ej 3
--data Maybe a = Nothing | Just a
--data Either a b = Left a | Right b
-- (!) Maybe y Either se encuentran en el preludio.

-- Definir la función inverso :: Float → Maybe Float que dado un número devuelve su inverso multiplicativo si está definido, o Nothing en caso contrario.
-- Nota: el inverso multiplicativo de x es 1/x. El inverso multiplicativo de 0 no está definido pues 1/0 es indefinido.
inverso :: Float -> Maybe Float
inverso 0 = Nothing
inverso x = Just (1/x)

-- Definir la función aEntero :: Either Int Bool → Int que convierte a entero una expresión que puede ser booleana o entera. En el caso de los booleanos, el entero que corresponde es 0 para False y 1 para True.
aEntero :: Either Int Bool -> Int
aEntero (Left int) = int
aEntero (Right bool) = if bool == True then 1 else 0

-- Ej 4
limpiar :: String -> String -> String
limpiar [p] palabra = quitar p palabra
limpiar (p:ps) palabra = limpiar ps (quitar p palabra)

quitar :: Char -> String -> String
--quitar _ [] = []
--quitar char (p:ps) = if char == p then quitar char ps else p:quitar char ps
-- Lo hice quick and dirty como me salió, ahora trato de utilizar lo que vimos en clase
quitar char = recr (\p ps result -> if char == p then quitar char ps else p:result) []

-- Tuve que recrear recr pues no existe en el preludio.
recr :: (a -> [a] -> b -> b) -> b -> [a] -> b
recr f z [] = z
recr f z (x : xs) = f x xs (recr f z xs)

promedio :: [Float] -> Float
promedio lista = foldl (\ac x -> x / fromIntegral (length lista) + ac) 0 lista

difPromedio :: [Float] -> [Float]
difPromedio lista = map (\ x -> x - promedio lista) lista

todosIguales :: [Int] -> Bool
todosIguales [] = True
todosIguales [x] = True
todosIguales (x1:x2:xs) = x1 == x2 && todosIguales xs

-- insertar :: Ord a => a -> AB a -> AB a
-- insertar x Nil = Bin Nil x Nil
-- insertar x (Bin izq y der) | x < y = Bin (insertar x izq) y der
--                            | x > y = Bin izq y (insertar x der)
--                            | otherwise = Bin izq y der -- Devuelve el árbol original sin cambios

-- Ej 5
data AB a = Nil | Bin (AB a) a (AB a) -- deriving Show
-- deriving Show es necesario para poder realizar:
-- ghci> negacionAB (Bin Nil True Nil)
-- Bin Nil False Nil
-- Por ejemplo, caso contrario tira error

vacioAB :: AB a -> Bool
vacioAB Nil = True
vacioAB _ = False

negacionAB :: AB Bool -> AB Bool
negacionAB Nil = Nil
negacionAB (Bin izq nodo der) = Bin (negacionAB izq) (not nodo) (negacionAB der)

productoAB :: AB Int -> Int
productoAB Nil = 0
productoAB (Bin Nil nodo Nil) = nodo
productoAB (Bin Nil nodo der) = nodo * productoAB der
productoAB (Bin izq nodo Nil) = nodo * productoAB izq
productoAB (Bin izq nodo der) = productoAB der * nodo * productoAB izq
