import Data.Maybe(fromJust)
import Data.List(findIndex)

{- Elementos de la SECD -}
type Código = [Ins]                     -- Código : lista de instrucciones
data Valor  = VClosure [Ins] Env        -- Valores : booleanos y clausuras
            | VBool Bool
  deriving Show
type Env    = [Valor]                   -- Entornos : lista de valores
type Pila   = [Valor]                   -- Pilas : lista de valores
type Dump   = [([Ins], Pila, Env)]      -- Dumps

{- Instrucciones de la SECD -}
data Ins = ILDB Bool                      -- LDB(b) : carga el booleano b en la pila
         | IMKCLO Código                  -- MKCLO(l) : carga una clausura con código l en la pila
         | ILD Int                        -- LD(n) : carga el n-ésimo valor del entorno en la pila
         | IAP                            -- AP : invoca a una función
         | IRET                           -- RET : retorna de una función
         | ITEST Código Código            -- TEST(l1, l2) : ejecuta l1 o l2 según el tope de la pila
  deriving Show

{- Términos del CL -}
type Id = String
data Term = TVar Id
          | TAbs Id Term
          | TApp Term Term
          | TFalse
          | TTrue
          | TIf Term Term Term

{- Esta es la función que hay que completar: toma un término del CL y devuelve la lista de instrucciones de la SECD correspondiente -}
compile :: [Id] -> Term -> [Ins]
compile lst (TVar x)    = [ILD (fromJust (findIndex (== x) lst))]
compile lst (TAbs x m)  = [IMKCLO (compile (x : lst) m ++ [IRET])]
compile lst (TApp m n)  = compile lst m ++ compile lst n ++ [IAP]
compile lst TFalse      = [ILDB False]
compile lst TTrue       = [ILDB True]
compile lst (TIf m n o) = compile lst m ++ [ITEST (compile lst n) (compile lst o)]

{- Lo que viene a partir de ahora sólo es relevante para ejecutar el intérprete de la SECD -}

{- Estado de la SECD -}
data State = State [Ins] Pila Env Dump  -- Un estado de la máquina es una 4-upla
  deriving Show

{- Ejecutar una instrucción de la SECD -}
step :: State -> Maybe State
step (State (ILDB b : is)       pila                              env dump)                         = Just $ State is         (VBool b : pila)          env           dump
step (State (IMKCLO is1 : is2)  pila                              env dump)                         = Just $ State is2        (VClosure is1 env : pila) env           dump
step (State (ILD i : is)        pila                              env dump)                         = Just $ State is         (env !! i : pila)         env           dump
step (State (IAP : is)          (val : VClosure is1 env1 : pila)  env dump)                         = Just $ State is1        []                        (val : env1)  ((is, pila, env) : dump)
step (State (IRET : _)          (val : _)                         _   ((is1, pila1, env1) : dump))  = Just $ State is1        (val : pila1)             env1          dump
step (State (ITEST ct cf : is)  (VBool True : pila)               env dump)                         = Just $ State (ct ++ is) pila                      env           dump
step (State (ITEST ct cf : is)  (VBool False : pila)              env dump)                         = Just $ State (cf ++ is) pila                      env           dump
step _                                                                                              = Nothing

{- Ejecutar la SECD -}
run :: State -> State
run state =
  case step state of
    Nothing     -> state
    Just state' -> run state'

{- Compilar un término y ejecutar su código resultante en la SECD -}
runTerm :: Term -> Valor
runTerm t = resultado (run $ State (compile [] t) [] [] [])

resultado :: State -> Valor
resultado (State _ (r:_) _ _) = r

{- Algunos términos de ejemplo para probar el compilador CL a SECD y el intérprete SECD -}
-- (\x. x) True
t1 = TApp (TAbs "x" (TVar "x")) TTrue
-- (\z. z) (\x. \y. y) True False
t2 = TApp (TApp (TApp (TAbs "z" (TVar "z")) (TAbs "x" (TAbs "y" (TVar "y")))) TTrue) TFalse
-- \z. if (\x. if x then False else True) z then False else True
t3 = TAbs "z" (TIf (TApp (TAbs "x" (TIf (TVar "x") TFalse TTrue)) (TVar "z")) TFalse TTrue)
-- (\z. if (\x. if x then False else True) z then False else True) True
t4 = TApp t3 TTrue