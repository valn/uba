> [!WARNING] Disclaimer
> Hice este resúmen basándome principalmente en las diapositivas/apuntes teóricos de **Pablo Barenbaum** y **Alejandro Díaz-Caro**, sumado a conclusiones que fui sacando durante la cursada. Este apunte está hecho por mí, y para mí; pensado para ser leido por mí, entendido por mí.
> No es un lugar fiable de estudio ni mucho menos. Puede contener $orrorez$, pues tiene mi toque de conclusiones (y humor) que pueden estar erradas, aún así decidí ponerlo público, pues capaz a alguien le sirve.
>  
> Puede ser que no profundice en temas que considere medio elementales, y en otros profundice de más pues los consideré más relevante al momento de redactar el documento.
>  
> Con esto dicho, invito la lectura a la persona aventurera que lo desee.
>  
> $\approx @valnrms$
## Programación funcional, Expresiones, Tipos y polimorfismo, Modelo de cómputo (Capítulo 0)
### Programación funcional 101 (Capítulo 0: parte I)
La programación funcional consiste en definir funciones y aplicarlas para procesar información.

Solemos decir que "**las funciones son funciones** (parciales), **posta**." (como en matemáticas!!1):
 - **Aplicar** una **función** **no tiene efectos secundarios**.
 - **Transparencia referencial**: el **valor del output** corresponde **únicamente del input**.
 - Las **estructuras de datos** son **inmutables**.

Las funciones son ***first-class citizen***, es decir, **son como cualquier otro tipo de dato**:
 - Se pueden pasar como parámetro.
 - Se pueden devolver como resultado.
 - Pueden formar partes de estructuras de datos.

**Un programa funcional está dado por un conjunto de ecuaciones.** *(más información sobre esto en la parte IV de modelo de cómputo)*

#### Funciones de orden superior
Son funciones que cumplen **alguna** de estas dos condiciones:
1. **Toman** una o más **funciones como argumento**.
2. **Devuelven** una **función** como resultado.


Un ejemplo clave se relaciona con la notiación lambda ($\lambda$) en Haskell.
Supongamos la siguiente función $add$ $x$ $=$ $($$\text{\\ }y \to x + y)$
$add$ es una función de orden superior. Fijate que devuelve una función como resultado. ¡Devuelve una Lambda ($\lambda$)!

Nótese que al devolver funciones, permitimos algo muy fuerte de la programación funcional llamado **curryficación**. La curryficación nos permite realizar aplicaciones parciales a funciones. Por ejemplo,
```haskell
add :: Int -> Int -> Int
add n m = n + m

-- Puedo hacer
next = add 1

-- Ahora hacer next {Int} me da el número siguiente a {Int}.
next 0 -- Devuelve 1
next 1 -- Devuelve 2
       -- ...
```

Más ejemplos míticos: $curry$, $uncurry$, $map$, $(\text{{.}})$, etc...

Por ejemplo, $map$ toma una función.
```haskell
map :: (a -> b) -> [a] -> [b]
map f [] = []
map f (x : xs) = f x : map f xs
```

### Expresiones (Capítulo 0: parte II)
Tenemos **expresiones**. Son secuencias de **símbolos**, por ejemplo, $carlos$.
$carlos$, es una expresión. Así como $carlos$ es una expresión, tenemos más expresiones.

Por ejemplo: $True$, $False$, $argentinaCampeónDelMundo$, $[]$, $(:)$, $plp$, $miau$, $0$, $1337$, $curry$, $x$, $y$, $funciónMágicaQueResuelveTodosLosProblemas$...

Las **expresiones** se pueden caracterizar de **diferentes maneras**:
 - **Constructor**: $True$, $False$, $[]$, $(:)$, $0$, $1337$
 - **Variable**: $dobleHuevo$, $plp$, $miau$, $curry$, $x$, $y$ $funciónMágicaQueResuelveTodosLosProblemas$
 - **Aplicación**:
	 - $dobleHuevo$ $siemprePá$
	 - $curry$ $x$ $y$
	 - $funciónMágicaQueResuelveTodosLosProblemas$ $problemaMuyMuyMuyFeo$
	 - $miau$ $meow$

Hay más caracterizaciones pero estas son **fundamentales**.

### Tipos y polimorfismo (Capítulo 0: parte III)
Un **tipo** es una **especificación** del **invariante** de un dato o de una función.
El tipo de una función expresa un **contrato**.

Si la expresión tiene más de un tipo, decimos que hay **polimorfismo**.
Ejemplitos: $id :: a \to a$, $[] :: [a]$, $(:) :: a \to [a] \to [a]$, $fst :: (a, b) \to a$, $snd :: (a, b) \to b$

### Modelo de cómputo (prog. func.) (Capítulo 0: parte IV)
Dada una expresión, se computa su *valor*, usando las ecuaciones.
![[Pasted image 20240719050821.png|center|350]]
No toda expresión bien tipada tiene valor. Por ejemplo: $1/0$.
Decimos que este tipo de expresiones se **indefinen**, o tienden a $\bot$ *"bottom"*.

Un programa funcional está dado por un conjunto de ecuaciones. Más precisamente, por un conjunto de **ecuaciones orientadas**.
*Se le llama ecuaciones orientadas pues las ecuaciones responden a una cierta orientación: del lado izquierdo está lo que se define, y del lado derecho la definición (o expresión que produce el valor).*

Una ecuación $e1 = e2$ se interpreta desde dos puntos de vista:
1. **Denotacional**: Declara que $e1$ y $e2$ tienen el mismo significado.
	- *"**Denotan** lo mismo".*
2. **Operacional**: Computar el valor de $e1$ se reduce a computar el valor de $e2$.
	- *"**Operan** de la misma forma".*

El lado izquiero de una ecuación no puede ser cualquier cosa. $ladoIzquierdo = \dots$
¿Me comprás si te digo $1 + n = sumaleUno$ $n$?, ¿no verdad?
¿Cómo que decir $sumaleUno$ $n$ $= n + 1$ es más natural?, ¿no?

El lado izquierdo de una ecuación no es una expresión arbitraria. Debe ser una función aplicada a patrones.

Un patrón puede ser:
1. Variable: $n$, $x$, $xs$
2. Comodín: $\_$.
3. Constructor aplicado a patrones: $x:xs$, $0$, $Nil$

**En nuestro ejemplo, $n$ es un patrón pues es una variable**.

Y no debe contener variables repetidas. *(Escribía $divisiónEntera$ $n$ $n$ $= div$ $n$ $n$, ¿te imaginás?, ¿cuál $n$ es cuál?, ni idea, incomprensible)*

| ![[Pasted image 20240728053030.png\|center\|320]] | ![[Pasted image 20240728053048.png\|center\|320]] |
| :-----------------------------------------------: | :-----------------------------------------------: |

---

## Esquemas de recursión y Tipos de datos inductivos (Capítulo 1)
### Esquemas de recursión (Capítulo 1: parte I)

### Listas
Cuando hacemos recursión, hay ciertos patrones que suelen repetirse. Es por ello que contamos con ciertas funciones que abstraen ciertos esquemas de recursión.

- **Recursión estructural** *(foldr)*:
	- El caso base devuelve un valor fijo $z$.
	- El caso recursivo se escribe usando (cero, una, o muchas veces) $x$, y la llamada recursiva (en $foldr$ es `(foldr f z xs)`, en $map$ es `map f xs`). No se utiliza ni $xs$, ni otros llamados recursivos.
```haskell
foldr :: (a -> b -> b) -> b -> [a] -> b
foldr f z [] = z
foldr f z (x : xs) = f x (foldr f z xs)

-- Ejemplito de map usando foldr:
map :: (a -> b) -> [a] -> [b]
map f = foldr (\x rec -> f x : rec) []
```

![[Pasted image 20240719181852.png|center|425]]
Existe también $foldr1$. Es igual a $foldr$, solo que **no** toma un caso base $z$. El caso base es la instancia con un solo elemento. Por ejemplo en listas sería $x$ sería el caso base cuando la lista es $[x]$.
```haskell
foldr1 :: (a -> a -> a) -> [a] -> a
foldr1 _ [x] = x
foldr1 f (x : xs) = f x (foldr1 f xs)
```

- **Recursión primitiva** *(recr)*:
	- Casi igual a la estructural; tiene de diferencia que podés usar el $xs$.
	- Recursión primitiva $\implies$ Recursión estructural *(la 2da es un caso particular de la 1ra)* 
```haskell
recr :: (a -> [a] -> b -> b) -> b -> [a] -> b
recr f z [] = z
recr f z (x : xs) = f x xs (recr f z xs)

-- Ejemplito de trim usando recr:
trim :: String -> String
trim = recr (\x xs rec -> if x == ' ' then rec else x : xs) []
```

- **Recursión iterativa** *(foldl)*:
	- El caso base devuelve el acumulador $acc$.
	- El caso recursivo invoca inmediatamente a $(f$ $acc$ $x)$. Onda, vas trabajando con el acumulador.
```haskell
foldl :: (b -> a -> b) -> b -> [a] -> b
foldl f acc [] = acc
foldl f acc (x : xs) = foldl f (f acc x) xs

-- Ejemplito de reverse usando foldl
reverse :: [a] -> [a]
reverse = foldl (\acc x -> x : acc) []
```
![[Pasted image 20240719184305.png|center|425]]
Al igual que en $foldr$, existe $foldl1$. Sigue la misma idea refente al caso base.
### Relación entre $foldr$ y $foldl$:
No es casualidad los nombres. Estas funciones están estrechamente relacionadas.
![[Pasted image 20240719184422.png|center|450]]
```haskell
f :: a -> b -> b
z :: b
xs :: [a]

foldr f z xs = foldl (flip f) z (reverse xs)
```

### Otras estructuras
**Puede ser interesante leer la parte II antes de entrar acá.**
Cuando queremos hacer recursión estructural sobre estructuras de datos con más de un constructor *(sean/n recursivo/s, o no)*, es importante tener en cuenta que vamos a tener tanta cantidad de casos, como constructores tenga nuestro tipo de datos.
![[Pasted image 20240719191719.png|center|450]]
```haskell
-- Ejemplo típico:
data AB a = Nil | Bin (AB a) a (AB a) -- Árbol binario

-- Definamos foldAB para abstraer la recursión estructural en el tipo de dato de árbol binario
foldAB :: (b -> a -> b -> b) -> b -> AB a -> b
foldAB cBin cNil Nil = cNil
foldAB cBin cNil (Bin i r d) = cBin (foldAB cBin cNil i) r (foldAB cBin cNil d)

-- Y ya que estamos hacemos map para el árbol binario
mapAB :: (a -> b) -> AB a -> AB b
mapAB f = foldAB (\recIzq raíz recDer -> Bin recIzq (f raíz) recDer) Nil
```

### Tipos de datos inductivos (Capítulo 1: parte II)
Tenemos algunos tipos de datos primitivos: $Char$, $Int$, $Float$, $a \to b$, $(a,b)$, $[a]$, $String$.
Y podemos definir los nuestros propios.
```haskell
-- Cómo se construye: data Type = <declaración de constructores>

-- Ejemplo
data Dia = Dom | Lun | Mar | Mie | Jue | Vie | Sab
```
$Dom$, $Lun$, $\dots$, $Sab$, son **contructores** *(simples)*. Todos ellos son *(los únicos constructores)* del tipo $Dia$.

Ahora, un constructor también puede tener parámetros.
```haskell
data Vehiculo = Auto String Int | Moto String String Float

Auto :: String -> Int -> Vehiculo
Moto :: String -> String -> Float -> Vehiculo
```
En este caso $Auto$ y $Moto$ son los únicos dos constructores del tipo de dato $Vehiculo$.
$Auto$ toma dos parámetros, un $String$ y un $Int$; mientras que $Moto$ toma un $String$, un $String$, y un $Float$.

Y también tenemos **tipos de datos recursivos**.
```haskell
data Nat = Zero | Succ Nat

Zero :: Nat
Succ :: Nat -> Nat
```
$Zero$ es un constructor simple, mientras que $Succ$ es un constructor que *"toma"* algo de tipo $Nat$, esto lo hace recursivo.
Las funciones sobre tipos de datos con constructores recursivos normalmente se definen por recursión.
```haskell
doble :: Nat -> Nat
doble Zero = Zero
doble (Succ n) = Succ (Succ (doble n))
```

Podemos definir estructuras de datos infinitas:
```haskell
infinito :: Nat
infinito = Succ infinito
```

![[Pasted image 20240719191433.png|center|600]]

```haskell
-- Ya que estoy defino Polinomio también
data Polinomio a = X
                | Cte a
                | Suma (Polinomio a) (Polinomio a)
                | Prod (Polinomio a) (Polinomio a) 

foldPoli :: b -> (a -> b) -> (b -> b -> b) -> (b -> b -> b) -> Polinomio a -> b
foldPoli z _ _ _ X = z
foldPoli _ fCte _ _ (Cte m) = fCte m
foldPoli z fCte fSuma fProd (Suma p q) = fSuma (foldPoli z fCte fSuma fProd p) (foldPoli z fCte fSuma fProd q)
foldPoli z fCte fSuma fProd (Prod p q) = fProd (foldPoli z fCte fSuma fProd p) (foldPoli z fCte fSuma fProd q)

evaluar :: Num a => a -> Polinomio a -> a
evaluar n = foldPoli n id (+) (*)
```

---

## Razonamiento ecuacional, Inducción estructural, Extensionalidad, Isomorfismo de tipos (Capítulo 2)
Dijimos en un primer momento que los programas en el paradigma funcional son un conjunto de ecuaciones orientadas.
Al ser ecuaciones es razonable plantearse, ¿es posible demostrar que ciertas expresiones son equivalentes?, y si se pudiese, ¿para qué nos interesaría?

Podríamos, por ejemplo, demostrar que un algoritmo es correcto, o realizar optimizaciones a un programa.
### Razonamiento ecuacional (Capítulo 2: parte I)
Para razonar sobre equivalencia de expresiones vamos a asumir:
1. Que trabajamos con **estructuras de datos finitas** *(técnicamente: con **tipos de datos inductivos**)*
2. Que trabajamos con **funciones totales**.
	- Las ecuaciones deben cubrir todos los casos.
	- La recursión siempre debe terminar.
3. Que el programa **no depende del orden** de las ecuaciones.

### Inducción estructural (Capítulo 2: parte II)
###### Principio de inducción estructural:
Sea $P$ una propiedad acerca de las expresiones tipo $T$ tal que:
 - $P$ vale sobre todos los constructores base de $T$,
 - $P$ vale sobre todos los constructores recursivos de $T$, asumiendo como hipótesis inductiva que vale para los parámetros de tipo $T$,
entonces $\forall x :: T.$ $P(x)$.

En criollo: Para probar $P$ sobre todas las instancias de un tipo $T$, basta probar $P$ para cada uno de los constructores *(asumiendo la H.I. para los constructores recursivos)*.

- Principio de **inducción sobre booleanos**:
	- Si $P(True)$ y $P(False)$ entonces $\forall x :: Bool.$ $P(x)$
	- En criollo: si es un bool podés separar en casos $True$ y $False$.
	- Ejemplito:
		- Para probar $\forall x :: Bool.$ $not$ $(not$ $x) = x$, basta probar
		 $not$ $(not$ $True) = True$ $\land$ $not$ $(not$ $False) = False$.
- Principio de **inducción sobre pares**:
	- Si $\forall x :: a.$ $\forall y :: b.$ $P(x,y)$ entonces $\forall p :: (a,b).$ $P(p)$.
	- En criollo: si probás que vale para una tupla cualquiera $(x,y)$ tal que $x :: a$, $y :: b$, entonces vale para todo par/tupla $p :: (a,b)$.
- Principio de **inducción sobre naturales**:
	- No requiere mucha presentación. Es el mismo que el de Álgebra I.
	- Si $P(Zero)$ y $\forall n :: Nat.$ $P(n) \implies P(Suc$ $n)$, entonces $\forall n :: Nat.$ $P(n)$.
	- A $P(Zero)$ se lo conoce como **caso base**.
	- Al antecedente del $\forall n$ *(es decir, $P(n)$)*, se lo conoce como **hipótesis inductiva**. Al consecuente como tesis inductiva.
	- Tenés que probar el caso base, y que, asumiendo la **H.I.**, podés concluir la **T.I.**.

### Extensionalidad (Capítulo 2: parte III)
Se deriva a partir del principio de inducción estructural.

- Principio de **extensionalidad para pares**:
	- Si $p :: (a, b)$, entonces $\exists x :: a.$ $\exists y :: b.$ $p = (x, y)$.
	- En criollo: si tenés un par/tupla $p :: (a,b)$, entonces existen $x :: a$, $y :: b$ que lo forman (al par/tupla).
- Principio de **extensionalidad para sumas**:
	- Parecido al de booleanos.
	- Si $e :: Either$ $a$ $b$, entonces:
		- o bien $\exists x :: a.$ $e = Left$ $x$
		- o bien $\exists y :: b.$ $e = Right$ $y$

Al preguntarnos sobre la igualdad de expresiones tenemos dos posturas.
- Decimos que dos valores son iguales:
	- **==intensional==mente** sii están definidos de la misma manera.
	- **==extensional==mente** sii son indistinguibles al observarlos.

- Principio de **extensionalidad funcional**:
	- Si $(\forall x :: a.$ $f$ $x = g$ $x)$ entonces $f = g$.
	- En criollo: para probar que dos funciones son iguales, basta probar que son iguales punto a punto.

Algunas propiedades sacadas de la práctica:
```haskell
F :: a -> b
G :: a -> b
Y :: b
Z :: a
```
Estas dos reglas son casos particulares del principio de extensionalidad de funciones (más o menos):
$$F = G \Longleftrightarrow \forall x::a. F\text{ }x = G\text{ }x$$
$$F = \text{\\}x \to Y \Longleftrightarrow \forall x::a . F\text{ }x = Y$$
$$\text{Regla } \beta \text{: } (\text{\\}x \to Y)\text{ }Z = Y \text{ (reemplazando x por Z)}$$$$\text{Regla } \eta \text{: } \text{\\} x \to F \text{ } x = F$$
### Isomorfismo de tipos (Capítulo 2: parte IV)
Decimos que dos tipos de datos $A$ y $B$ son **isomorfos** si:
1. Hay una función $f :: A \to B$ total.
2. Hay una función $g :: B \to A$ total.
3. Se puede demostrar que $g$ $.$ $f = id :: A \to A$.
4. Se puede demostrar que $f$ $.$ $g = id :: B \to B$.

Escribimos $A \simeq B$ para indicar que $A$ y $B$ son isomorfos.

---

## Sistemas deductivos (Capítulo 3)
Antes de empezar a hablar de sistemas deductivos es importante entender las **fórmulas** son un **conjunto inductivo**.

##### Fórmulas (gramática de las fórmulas en lógica proposicional)
1. cualquier variable proposicional es una fórmula.
2. si $\tau$ es una fórmula, ($\neg\tau$) es una fórmula.
3. si $\tau$ y $\sigma$ son fórmulas, $(\tau \land \sigma)$ es una fórmula.
4. si $\tau$ y $\sigma$ son fórmulas, $(\tau \lor \sigma)$ es una fórmula.
5. si $\tau$ y $\sigma$ son fórmulas, $(\tau \Rightarrow \sigma)$ es una fórmula.
6. si $\tau$ y $\sigma$ son fórmulas, $(\tau \Leftrightarrow \sigma)$ es una fórmula.

Al ser un conjunto inductivo, viene provisto de:
 - Esquema de prueba para probar propiedades sobre ellos **(inducción estructural)**.
 - Esquema de recursión para definir funciones sobre el conjunto **(recursión estructural)**.

##### Valuación
- Una valuación es una función $v : \mathcal{V} \Rightarrow \{V, F\}$ que asigna valores de verdad a las variables proposicionales.
- Una valuación satisface una proposición $\tau$ si $v \models \tau$ donde:
  - $v \models P$ sii $v(P) = V$
  - $v \models \neg\tau$ sii $v \not\models \tau$ (i.e. no $v \models \tau$)
  - $v \models \tau \lor \sigma$ sii $v \models \tau$ o $v \models \sigma$
  - $v \models \tau \land \sigma$ sii $v \models \tau$ y $v \models \sigma$
  - $v \models \tau \Rightarrow \sigma$ sii $v \not\models \tau$ o $v \models \sigma$
  - $v \models \tau \Leftrightarrow \sigma$ sii $(v \models \tau$ sii $v \models \sigma)$

##### Equivalencia lógica y tipos de fórmulas
Dadas fórmulas $\tau$ y $\sigma$:
- $\tau$ es lógicamente equivalente a $\sigma$ cuando $v \models \tau$ sii $v \models \sigma$ para toda valuación $v$.

Una fórmula $\tau$ es:
- una tautología si $v \models \tau$ para toda valuación $v$.
- satisfactible si existe una valuación $v$ tal que $v \models \tau$.
- insatisfactible si no es satisfactible.

Un conjunto de fórmulas $\Gamma$ es:
- satisfactible si existe una valuación $v$ tal que para todo $\tau \in \Gamma$, se tiene $v \models \tau$.
- insatisfactible si no es satisfactible.

**Teorema:** Una fórmula $\tau$ es una tautología sii $\neg \tau$ es insatisfactible.

##### Sistema deductivo basado en reglas de prueba
###### Secuente
$$\tau_1, \tau_2, ..., \tau_n \vdash \sigma$$
Denota que a partir de asumir que el conjunto de fórmulas $\{\tau_1, \tau_2, ..., \tau_n\}$ son tautologías, podemos obtener una prueba de la validez de $\sigma$.

###### Reglas de prueba
$$\frac{\Gamma_1 \vdash \tau_1 \quad \cdots \quad \Gamma_n \vdash \tau_n}{\Gamma \vdash \sigma} \text{ nombre\_de\_la\_regla}$$

Permiten deducir un secuente (conclusión) a partir de ciertos otros (premisas).

$$\Gamma_1 \vdash \tau_1 \cdots \Gamma_n \vdash \tau_n \textit{: premisas}$$
$$\Gamma \vdash \sigma \textit{: conclusión}$$
###### Pruebas
- Aplicando reglas de prueba a premisas y conclusiones obtenidas previamente.
- Un secuente es **válido** si podemos construir una prueba.

###### Importancia de la elección de las reglas
- Deben permitir construir solo pruebas que constituyan una argumentación válida.
  - Deberían impedir probar secuentes tales como $P, Q \vdash P \land \neg Q$.
- Deberían permitir inferir todas las fórmulas que se desprenden de las premisas.

Llamamos **teorema** a toda fórmula lógica $\tau$ tal que el secuente $\vdash \tau$ es válido.

###### Lógica clásica vs constructiva / intuicionista
Existe la lógica clásica y la intuicionista.
Las reglas *vistas en clase* de la **lógica clásica** son: $PBC$ (proof by contradiction), $LEM$ (law of excluded middle), y $\neg \neg_{e}$ (double negation elimination).
El resto que vimos son de lógica intuicionista.
Las tres reglas *vistas en clase* de lógica clásica son equivalentes. Se puede probar que $$PBC \implies LEM \implies \neg \neg_{e} \implies PBC$$
*Aclaración: cuando digo "reglas de lógica clásica", estoy pensando en $\{ \textit{reglas de lógica clásica} \} \setminus \{ \textit{reglas de lógica intuicionista} \}$, es sabido que las reglas de la lógica intuicionista están contenidas en la reglas de la lógica clásica.*

### Corrección y completitud
Responde a la pregunta: ¿cuál es la relación entre la sintaxis y la semántica?

**Sintaxis:** Conjunto de fórmulas $\tau$ tal que $\neg \tau$ es un secuente válido.
**Semántica:** Conjunto de fórmulas $\tau$ tal que $v \models \tau$ , para toda valuación $v$ *(i.e. tautologías)*.

#### Corrección
$\vdash \tau$ secuente válido implica que $\tau$ es tautología.
- En criollo: $\tau$ tiene una prueba implica que $\tau$ es tautología
- Generalizada: $\sigma_{1}, \sigma_{2}, ..., \sigma_{n} \vdash \tau$ secuente válido implica que $\sigma_{1}, \sigma_{2}, ..., \sigma_{n} \models \tau$

Una idea para la demo de **corrección** es hacer inducción en la estructura de la prueba, procedemos analizando por casos la última regla aplicada en la prueba.
Caso base, la última regla fue un axioma.
Casos recursivos, el resto de reglas $\land_{i}, \land_{e_{1}}, etc\dots$.
#### Completitud
$\tau$ tautología implica que $\vdash \tau$ es secuente válido.
- En criollo: $\tau$ tautología implica que $\tau$ tiene una prueba.
- Generalizada: $\sigma_{1}, \sigma_{2}, ..., \sigma_{n} \models \tau$ implica que $\sigma_{1}, \sigma_{2}, ..., \sigma_{n} \vdash \tau$ es secuente válido

Una idea para la demo de **completitud** es usar por el contrarrecíproco: si $P \Rightarrow Q$ vale que $\neg Q \Rightarrow \neg P$.
Luego, probar estos dos lemas:
1. Si $\Gamma \not \vdash \tau$ , entonces $\Gamma \cup \{ \neg \tau \}$ es consistente. *(Sale por el contrarrecíproco también)*
2. Si $\Gamma$ es consistente, entonces tiene modelo *(i.e. $\Gamma$ es satisfactible)*. *(Más jodido)*

Con esos dos lemitas sale, pues supongamos $\Gamma \not \vdash \tau$
$\Rightarrow$ $\Gamma \cup \{ \neg \tau \}$ es consistente *(por Lema 1)*
$\Rightarrow$ $\Gamma \cup \{ \neg \tau \}$ tiene modelo *(por Lema 2)*
$\Rightarrow$ $\exists v$ tal que $\forall \sigma \in  \Gamma \cup \{ \neg \tau \}, v \models \sigma$ (def. de tener modelo)
$\Rightarrow$ $\exists v$ tal que $v \not\models \tau$ y $\forall \sigma \in  \Gamma, v \models \sigma$ (def. de $\models$)
$\Rightarrow$ $\Gamma \not \models \tau$ (def. de consecuencia semántica)
#### Consecuencia semántica
Sean $\tau_1, \tau_2, \ldots, \tau_n, \sigma$ fórmulas de la lógica proposicional.
$\tau_1, \tau_2, \ldots, \tau_n \models \sigma \quad \Longleftrightarrow \quad \forall v \text{ valuación} \, ( (v \models \tau_1 \land v \models \tau_2 \land \cdots \land v \models \tau_n) \implies (v \models \sigma) ).$

En criollo: Vale que $\tau_1, \tau_2, \ldots, \tau_n \models \sigma$ $\Longleftrightarrow$ (para toda valuación $v$ tal que $v$ satisface a todas las hipótesis ($v \models \tau_{i}$ para toda $i \in 1, \dots n$) $\implies$ $v \models \sigma$).

#### Definiciones extra
Sea $\Gamma = \tau_1, \tau_2, \ldots, \tau_n$ un conjunto de hipótesis.
Se dice que $\Gamma$ es **consistente** si $\Gamma \not\vdash \bot$
 - En criollo: $\Gamma$ es consistente si no se puede derivar una contradicción a partir de él.

Decimos que $\Gamma$ tiene un **modelo** *(o es satisfactible)* si existe una valuación $v$ tal que $v \models \tau$, para toda $\tau \in \Gamma$.

Decimos que $\Gamma$ es **consistente maximal** si
 - $\Gamma$ es consistente.
 - Si $\Gamma \subseteq$ $\Gamma'$ y $\Gamma'$ es consistente, entonces $\Gamma' = \Gamma$.
   $\equiv$ $\Gamma \subset$ $\Gamma'$, entonces $\Gamma'$ es inconsistente.

#### Comentarios
**Afirmación**
"$\sigma_{1}, \sigma_{2}, ..., \sigma_{n} \vdash \tau$ es secuente válido" significa que hay una prueba de $\tau$ bajo las hipótesis $\sigma_{1}, \sigma_{2}, ..., \sigma_{n}$.
$\sigma_{1}, \sigma_{2}, ..., \sigma_{n} \models \tau$ significa que para toda valuación $v$ tal que $v \models \sigma_{i}$, para toda $i \in 1 \dots n$, vale que $v \models \tau$.

**Negación**
"$\sigma_{1}, \sigma_{2}, ..., \sigma_{n} \vdash \tau$ **no** es secuente válido" significa que no hay una prueba de $\tau$ bajo las hipótesis $\sigma_{1}, \sigma_{2}, ..., \sigma_{n}$.
$\sigma_{1}, \sigma_{2}, ..., \sigma_{n} \not\models \tau$ significa que existe valuación $v$ tal que $v \models \sigma_{i}$, para toda $i \in 1 \dots n$, pero $v \not\models \tau$.

---

## Cálculo-$\lambda$ (Capítulos 4, 5, 6)
- Para esta sección recomiendo ir directamente [al apunte de cálculo-λ de Jano](https://gitlab.com/valn/uba/-/blob/main/Paradigmas%20de%20Programaci%C3%B3n/Final/Te%C3%B3ricas/4,%205,%206%20(Apunte%20de%20Jano)%20-%20C%C3%A1lculo%20lambda.pdf?ref_type=heads).

Voy a resolver algunos ejercicios del apunte de cálculo-$\lambda$ de Jano.

###### Ejercicio 1.3
Faltarían dos reglas de congruencia.
$$\begin{array} \\
{\text{if }M\text{ then }O\text{ else }P} & {\rightarrow} & \text{if }M\text{ then }O'\text{ else }P & {(\text{if}_{c_{t}})} \\
{\text{if }M\text{ then }O\text{ else }P} & {\rightarrow} & \text{if }M\text{ then }O\text{ else }P' & {(\text{if}_{c_{f}})}
\end{array}$$
Servirían capaz para optimizar programas, pero es un tema, suponete que la rama *True* reduce a una función $\lambda$ y la rama *False* a un $\mathbb{Bool}$, te podés meter en quilombo al pedo.
A su vez, un *if* jamás va a ser un valor, por lo que si tenés que ejecutarlo *(estoy pensando en evaluación lazy)*, sí o sí vas a tener que hacer $(\text{if}_{c})$.
Conclusión, no te aportan nada.

###### Ejercicio 1.4
Aclaración: la regla $\alpha$ equivalencia la usamos siempre en clase, pero no la llamamos como tal nunca (salvo una vez que la mencionó Gabi).
$$\begin{array} \\
(\lambda x. \lambda x. x)\textbf{tt}\textbf{ff} & \overset{\alpha \text{ equivalencia}}{\rightarrow} & (\lambda x. \lambda y. y)\textbf{tt}\textbf{ff} \\
(\lambda x. \lambda y. y)\textbf{tt}\textbf{ff} & \overset{\beta \text{ reducción}}{\rightarrow} (\lambda y. y)\{ x := \textbf{tt} \}\mathbf{ff} \equiv (\lambda y. y)\mathbf{ff} & \overset{\beta \text{ reducción}}{\rightarrow} & y\{ y := \mathbf{ff} \} \equiv \mathbf{ff}
\end{array}
$$
Dejo el resto a ustedes, este ejercicio no es muy interesante.

###### Ejercicio 1.5
Recordemos la gramática de $M$.
$$\begin{array} \\
{M} & {::=} & {x} & {|} & {\lambda x. M} & {|} & {MM} & {|} & {tt} & {|} & {ff} & {|} & {\text{if }M\text{ then }M\text{ else }M}

\end{array}
$$
**Casos base:**
$$
\begin{array} \\
FV(x) = \{ x \} \\
FV(tt) = FV(ff) = \emptyset
\end{array}
$$

**Casos recursivos:**
$$\begin{array} \\
FV(\lambda x. M) = FV(M) \setminus \{ x \} \\
FV(MN) = FV(M) \cup FV(N) \\
FV(\text{if }M_{1}\text{ then }M_{2}\text{ else }M_{3}) = FV(M_{1}) \cup FV(M_{2}) \cup FV(M_{3})
\end{array}$$

###### Ejercicio 1.11
1. Idea de la demo para el Lema 1:
   Sé que $\rightarrow_{R}$ satisface la *propiedad del diamante*. Quiero ver que $\rightarrow^{*}_{R}$ lo satisface.
   Pensemos un poco. Sé que si $M$ $\rightarrow_{R}$ $N_{1}$ y $M$ $\rightarrow_{R}$ $N_{2}$, entonces existe $O$ tal que $N_{1}$ $\rightarrow_{R}$ $O$ y $N_{2}$ $\rightarrow_{R}$ $O$ (definición de satisfacer la *propiedad del diamante*).
   Ahora, esta relación $\rightarrow_{R}$ es en un paso. Mientras que $\rightarrow^{*}_{R}$ puede ser en $0,1,\dots n$ pasos *(finitos)*.
   Nada, sabés que uno lo hace en un paso, queres ver que el otro lo haga en una cierta cantidad finita de pasos entre $0$ y $n$. Asumo que ya ves por donde voy, tomá cantidad de pasos $= 1$ y listo.
   Luego como $\rightarrow^{*}_{R}$ cumple la *propiedad del diamante*, entonces $\rightarrow_{R}$ es *Church-Rosser*.
2. Idea de la demo para el Lema 2:
   Sé que $\rightarrow_{R}$ es *Church-Rosser*. Es decir, que $\rightarrow^{*}_{R}$ satisface la *propiedad del diamante*.
   Quiero ver que $\rightarrow_{R}$ tiene *formas normales únicas*, es decir que si $M$ $\rightarrow^{*}_{R}$ $N_{1}$ y $M$ $\rightarrow^{*}_{R}$ $N_{2}$, entonces $N_{1} = N_{2}$. Bueno, sé que si $M$ $\rightarrow^{*}_{R}$ $N_{1}$ y $M$ $\rightarrow^{*}_{R}$ $N_{2}$, entonces $N_{1}$ $\rightarrow^{*}_{R}$ $O$ y $N_{2}$ $\rightarrow^{*}_{R}$ $O$ (pues $\rightarrow^{*}_{R}$ cumple la propiedad del diamante), luego $O = O$.

###### Ejercicio 1.20
Reglas de reducción **(CBN)**:
$$\begin{array} \\
{(\lambda x. M)N} & {\rightarrow} & M\{ x:=N \} & {(\beta \text{ reducción})} \\
{\text{if tt}\text{ then }M\text{ else }N} & {\rightarrow} & M & {(\text{if}_{t}}) \\
{\text{if ff}\text{ then }M\text{ else }M} & {\rightarrow} & N & {(\text{if}_{f})}
\end{array}
$$
Reglas de congruencia **(CBN)**:
- Si $M \rightarrow M'$, entonces
$$
\begin{array} \\
{MN} & {\rightarrow} & M'N & {(\text{app})} \\
{\text{if }M\text{ then }O\text{ else }P} & {\rightarrow} & \text{if }M'\text{ then }O\text{ else }P & {(\text{if}_{c})}
\end{array}
$$

###### Ejercicio 1.23
Sea $V$ un valor.
Reglas de reducción **(CBV)**: 
$$\begin{array} \\
{(\lambda x. M)V} & {\rightarrow} & M\{ x:=V \} & {(\beta \text{ reducción})} \\
{\text{if tt}\text{ then }M\text{ else }N} & {\rightarrow} & M & {(\text{if}_{t}}) \\
{\text{if ff}\text{ then }M\text{ else }M} & {\rightarrow} & N & {(\text{if}_{f})}
\end{array}
$$
Reglas de congruencia **(CBV)**:
- Si $M \rightarrow M'$, entonces
$$
\begin{array} \\
{MN} & {\rightarrow} & M'N & {(\text{app}_{l})} \\
{VM} & {\rightarrow} & VM' & {(\text{app}_{r})} \\
{\text{if }M\text{ then }O\text{ else }P} & {\rightarrow} & \text{if }M'\text{ then }O\text{ else }P & {(\text{if}_{c})}
\end{array}
$$

###### Ejercicio 1.27
No es muy interesante, me lo salteo.

###### Ejercicio 1.28
El término $\lambda x: \tau. x x$ es tipable únicamente en **Sistema F** *(al menos dentro del foco de la materia)*.

###### Ejercicio 1.30
Quiero probar que, si $\Gamma \vdash M : \tau$ y $M \to N$, entonces $\Gamma \vdash N : \tau$.
Jano propone demostrar el **lema de sustitución**, y luego usarlo para demostrar el teorema por inducción en la definición de $\to$.

Probemos primero el lema de sustitución.
**Lema:** Si $\Gamma, x : \tau \vdash M : \sigma$ y $\Gamma \vdash N : \tau$, entonces $\Gamma \vdash M\{ x := N \} : \sigma$.
Jano propone demostrar este lema por inducción en $M$. Hagámoslo por inducción en $M$.

Recordemos nuevamente la gramática de $M$.
$$\begin{array} \\
{M} & {::=} & {x} & {|} & {\lambda x. M} & {|} & {MM} & {|} & {tt} & {|} & {ff} & {|} & {\text{if }M\text{ then }M\text{ else }M}
\end{array}
$$

**Casos base:**
**Caso $M = y$ con $y \neq x$:**
Tenemos $\Gamma, x: \tau \vdash  y : \sigma$ y $\Gamma \vdash N : \tau$
Luego, por la propiedad de $fortalecimiento$ o $strengthening$ *(Guía 4: Ej. 11, item 2)*, vale que $\Gamma \vdash y\{x := N\} : \sigma$ (o lo que es lo mismo, $\Gamma \vdash y : \sigma$).

**Caso $M = x$ con $\sigma = \tau$:**
Tenemos $\Gamma, x: \tau \vdash  x : \tau$ y $\Gamma \vdash N : \tau$
Luego, vale que $\Gamma \vdash x\{x := N\} : \tau$ (o lo que es lo mismo, $\Gamma \vdash N : \tau$).

*También los casos $tt$ y $ff$ son casos base, pero son triviales.*

**Pasos inductivos:**
Ahora nos toca probarlo para todos los otros constructores de $M$-términos. Siendo nuestra hipótesis inductiva, que vale para cualquier subtérmino.

**Caso $M = \lambda x. M'$:**
Sé que $\Gamma \vdash N : \tau$, y que $\Gamma, x : \tau \vdash \lambda y.M' : \rho \to \sigma$,
y quiero concluir que $\Gamma \vdash \lambda y.M'\{x := N\} : \rho \to \sigma$.

Usando $\to_{i}$ en $\Gamma, x : \tau \vdash \lambda y.M' : \rho \to \sigma$
$$
\frac{\Gamma, x : \tau, y : \rho \vdash M' : \sigma}{\Gamma, x : \tau \vdash \lambda y.M' : \rho \to \sigma} \to_{i}
$$
Obtengo $\Gamma, x : \tau, y : \rho \vdash M' : \sigma$
Y sé que, por **HI** aplicada al subtérmino $M'$ que la derivación  $\Gamma, y : \rho \vdash M'\{ x:=N \} : \sigma$ es cierta.
Entonces, usando nuevamente $\to_{i}$ pero en $\Gamma, y : \rho \vdash M'\{ x:=N \} : \sigma$ obtengo
$$
\frac{\Gamma, y : \rho \vdash M'\{ x:=N \} : \sigma}{\Gamma \vdash \lambda y.M'\{x := N\} : \rho \to \sigma} \to_{i}
$$
como se quería probar.

Ahora sería necesario realizar lo mismo, pero para todo el resto de constructores de la gramática de $M$.

Bueno, pero esto era para el lema de sustitución, todavía no demostramos el teorema.
Jano dijo de hacer inducción sobre la definición de $\to$.
Pensemos cómo sería eso. Sabemos que hice $M \to N$, es porque utilicé alguna regla. Podemos hacer inducción en las reglas utilizadas :).

Repasemos las reglas.
Reglas de resolución:
$$\begin{array} \\
{(\lambda x. M)N} & {\rightarrow} & M\{ x:=N \} & {(\beta \text{ reducción})} \\
{\text{if tt}\text{ then }M\text{ else }N} & {\rightarrow} & M & {(\text{if}_{t}}) \\
{\text{if ff}\text{ then }M\text{ else }M} & {\rightarrow} & N & {(\text{if}_{f})}
\end{array}
$$
Reglas de congruencia (si $M \to N$ entonces):
$$
\begin{array} \\
{MO} & {\rightarrow} & NO & {(\text{app}_{l})} \\
{OM} & {\rightarrow} & ON & {(\text{app}_{r})} \\
{\lambda x. M} & {\rightarrow} & (\lambda x. N) & {(\text{fun})} \\
{\text{if }M\text{ then }O\text{ else }P} & {\rightarrow} & \text{if }N\text{ then }O\text{ else }P & {(\text{if}_{c})}
\end{array}
$$

Supongo $M \to N$ usando la regla $\beta$ reducción.
Entonces $M = (\lambda x. M')N'$ y $N = M'\{ x := N' \}$

Sé que $\Gamma \vdash (\lambda x. M')N' : \tau$, quiero probar que $\Gamma \vdash M'\{ x := N' \} : \tau$.
De $\Gamma \vdash (\lambda x. M')N' : \tau$ puedo sacar que:

$$
\frac{\Gamma \vdash (\lambda x. M') : \sigma \to \tau \text{ }\text{ }\text{ }\text{ }\text{ }\text{ }\text{ }\text{ } \Gamma \vdash N' : \sigma }{\Gamma \vdash (\lambda x. M')N' : \tau} \to_{e}
$$
Y de $\Gamma \vdash (\lambda x. M') : \sigma \to \tau$ *(arriba a la izquierda en el árbol)* puedo sacar que:
$$
\frac{\Gamma, x: \sigma \vdash M' : \tau}{\Gamma \vdash (\lambda x. M') : \sigma \to \tau} \to_{i}
$$
Finalmente, tengo $\Gamma, x: \sigma \vdash M' : \tau$ y $\Gamma \vdash N' : \sigma$.
Luego, por el **lema de sustitución**, sé que vale que $\Gamma \vdash M'\{ x:=N' \} : \tau$, que es lo que quería probar.

Igual que antes, me faltaría hacer el resto de casos para todas las reglas de reducción $\to$ :).

###### Ejercicio 1.33
Nos piden extender la gramática, semántica operacional, y reglas de tipado, del cálculo lambda para pares.
**Gramática:**
$$
\begin{array} \\
M & ::= & \dots & | & (M,M) & | & fst(M) & | & snd(M)
\end{array}
$$

Podemos definir también los valores si es que nos interesa.
$$
\begin{array} \\
V & ::= & \dots & | & (V,V)
\end{array}
$$

Me voy a meter con la semántica al final. Vayamos antes con las **reglas de tipado**.
$$
\dfrac{
\begin{array} \\
\Gamma \vdash M : \sigma & \Gamma \vdash N : \tau
\end{array}
}{
\Gamma \vdash (M,N) : \sigma \times \tau
} \text{ }par
$$
$$
\begin{array} \\
\dfrac{
\Gamma \vdash M : \sigma \times \tau
}{
\Gamma \vdash fst(M) : \sigma
} \text{ }par_{fst}
&
\dfrac{
\Gamma \vdash M : \sigma \times \tau
}{
\Gamma \vdash snd(M) : \tau
} \text{ }par_{snd}
\end{array}
$$

Para la **semántica operacional** propongo:
$$
\begin{array} \\
\dfrac{
}{
fst((V, W)) \to V
}
&
\dfrac{
}{
snd((V, W)) \to W
}
\end{array}
$$
$$
\begin{array} \\
\dfrac{
M \to M'
}{
(M, N) \to (M', N)
}
&
\dfrac{
M \to M'
}{
(V, M) \to (V, M')
}
\end{array}
$$
$$
\begin{array} \\
\dfrac{
M \to M'
}{
fst(M) \to fst(M')
}
&
\dfrac{
M \to M'
}{
snd(M) \to snd(M')
}
\end{array}
$$
Nótese que agregar este conjunto de reglas no quita el **determinismo** (esto igual habría que demostrarlo formalmente) en la semántica operacional (para un término $M$ cualquiera, hay una única regla que lo reduce). Seguro sirven para **CBV**, yo justifico (creo) que sirven también para **CBN**, pues formalmente la única diferencia es radica en que en **CBN** pasamos los argumentos a las $\lambda$s sin que sean necesariamente valores; fijate como está definida la semántica operacional en el ejercicio 1.20 sino.
A su vez, me faltaría ponerles nombre a las reglas, pero soy medio queso con eso.

###### Ejercicio 2.2
**Item 1:** no se puede tipar.
**Item 2:**
Sea $I_{1} = \lambda x: \sigma_{1}. x$ y $I_{2} = \lambda x: \sigma_{2}.x$.
Determino $\sigma_{1} = \sigma_{2} \to \sigma_{2}$.
$$
\dfrac{\begin{array} \\
\dfrac{\dfrac{}{x : \sigma_{2} \to \sigma_{2} \vdash x: \sigma_{2} \to \sigma_{2}}ax_{v}}{\vdash \lambda x: \sigma_{2} \to \sigma_{2}. x : (\sigma_{2} \to \sigma_{2}) \to \sigma_{2} \to \sigma_{2}} \to_{i} & \dfrac{\dfrac{}{x : \sigma_{2} \vdash x : \sigma_{2}}ax_{v}}{\vdash \lambda x: \sigma_{2}.x : \sigma_{2} \to \sigma_{2}}\to_{i}
\end{array}}{\vdash I_{1}I_{2} : \sigma_{2} \to \sigma_{2}} \to_{e}
$$
###### Ejercicio 2.5
Lo defino de manera recursiva. Los primeros dos son los casos base, el resto son los casos recursivos.
$$\begin{array} \\
FV(x) = \{ x \} \\
FV(\text{Bool}) = \emptyset \\
FV(\tau_{1} \to \tau_{2}) = FV(\tau_{1}) \cup FV(\tau_{2}) \\
FV(\forall X. \tau) = FV(\tau) \setminus \{ X \}
\end{array}$$

###### Ejercicio 2.6
Este es un re choclo de escribí acá les cuento como es la idea.
Querés mostar que esta regla:
$$
\frac{
\begin{array} \\
\Gamma \vdash N : \tau & \Gamma,x : \tau \vdash M : \sigma
\end{array}
}{\Gamma \vdash \text{let }x = N \text{ in } M : \sigma} \text{ let}
$$
se puede obtener desde la derivación de $(\lambda x. M) N$.
Planteate $(\lambda x. M) N$ y andá aplicando reglas hasta llegar que necesitas las mismas premisas que para la regla **let**.
Nos estamos basando fuertemente en que $\text{let }x = N \text{ in } M$ reduce exáctamente igual que $(\lambda x. M) N$.

###### Ejercicio 2.10
El primer item es una rama del árbol del 2do así que lo salteo, deberías tomar $e = [X]$ y el tipo de la $\lambda$ es $\forall X. [X \to X]$.
Segundo item:
$$
\small{}
\dfrac{
\dfrac{\begin{array} \\
{\dfrac{
\dfrac{
\dfrac{}{x: [X] \vdash x : [X]} ax_{v}
}{
\vdash \lambda x: [X]. x : [X \to X]
} \to_{i}
}{
\vdash \lambda x : [X]. x : \forall X.[X \to X]
} \forall_{i}} & {
\dfrac{
\begin{array} \\
\dfrac{\dfrac{}{\Gamma \vdash i : \forall X.[X \to X]}ax_{v}}{\Gamma \vdash i : [(X \to X) \to X \to X]} \forall_{e} &
\dfrac{\dfrac{}{\Gamma \vdash i : \forall X.[X \to X]} ax_{v}}{\Gamma \vdash i : [X \to X]}\forall_{e}
\end{array}
}
{\Gamma \equiv i:\forall X. [X \to X] \vdash ii : [X \to X]} \to_{e}
}
\end{array}}{
\vdash \text{let } i = \lambda x : [X] . x\text{ in }ii : [X \to X]
} \text{let}
}{
\vdash \text{let } i = \lambda x : [X] . x\text{ in }ii : \forall X. [X \to X]
} \forall_{i}
$$
Notar que en la rama de la derecha, cuando aplicamos los dos $\forall_{_{e}}$, se podría pensar en el reemplazo $[(X \to X) \to X \to X]\{  Y:= (X\to X) \}$, y luego te queda $\Gamma \vdash i : \forall Y.[Y \to Y]$, le dejo $X$ por gusto, pero si la variable está ligada a un cuantificador, me da igual si se llama $X$ o $PEPITO$.
En mi caso yo tomo $[(X \to X) \to X \to X]\{  X:= (X\to X) \}$, pero es medio raro de leer.

Los otros dos items tienen truquito.

El tercer item nos pide tipar $(\lambda f:e. ff) (\lambda x : e'. x)$, para algún $e,e'$.
Si sacamos los tipos y vemos como reduce, vemos que me termina quedando la identidad, por lo que el tipo debería ser $\forall X . [X \to X]$.
Por lo que vimos antes *(ej anterior)*, $e' = [X]$ es una buena opción. El tema es $e$. Voy a necesitar que sea un esquema cuantificado, sino esto no va a salir. El problema es que si digo que $e = \forall X . [X \to X]$, me voy a trabar más adelante en una rama del árbol de derivación.
Por lo que así como está, no es tipable.

Puedo pensar un término *análogo*, $\text{let } f = \lambda x : [X] . x\text{ in }ff : \forall X . [X \to X]$, que es literalmente el item 2.
Ojo, $(\lambda f:e. ff) (\lambda x : e'. x)$ no es el mismo término que $\text{let } f = \lambda x : [X] . x\text{ in }ff : \forall X . [X \to X]$, digo que es *análogo* en cierto grado pues sé que $\text{let } x = N \text{ in }M$ reduce exáctamente igual que $(\lambda x. M)N$.

El item 4 directamente no un término tipable de cálculo lambda, al igual que $(\lambda x. xx)$. No existe término análogo $\text{let}$ que lo salve.

###### Ejercicio 2.19
$(\mu f. \lambda x. fx)0 \to (\lambda x . (\mu f. \lambda x. fx)x)0 \to (\mu f. \lambda x. fx)0$
No.

###### Ejercicio 2.20
Recuerdo el factorial original.
$Fact_{\mu} = \mu f. \lambda n. \text{if } isZero(n) \text{ then } \underline{1}\text{ else } n \times (f \text{ } pred(n))$

Recuerdo lo que dice el apunte sobre **FIX** sin $\mu$.
$Y = \lambda f. (\lambda x. f(xx))(\lambda x.f(xx))$
Sea $F$ una función cualquiera, $YF \to (\lambda x. F(xx))(\lambda x.F(xx)) \to F(YF)$. Ergo, $YF$ es un punto fijo de $F$.

Ahora vamos con lo que pide el ejercicio.
Tomo $F = \lambda f. \lambda n. \text{if } isZero(x) \text{ then } \underline{1}\text{ else } n \times (f \text{ } pred(n))$.
Luego, $Fact_{\not\mu} = YF = Y(\lambda f. \lambda n. \text{if } isZero(n) \text{ then } \underline{1}\text{ else } n \times (f \text{ } pred(n)))$.

###### Ejercicio 2.21
No es muy interesante.

###### Ejercicio 3.10
Bien, gracias. Sale haciendo inducción en estructural en los términos, o sea, inducción estructural en $M$. Planteate la gramática (está escrita más arriba) y resolvelo. Tu **HI** es que vale que para todos los subtérminos que pueda contener $M$, si estos son cerrados irreducibles y bien tipados, entonces es un valor.

###### Ejercicio 3.11
Fijate que Jano en la hoja siguiente lo hizo para $pred$. Es la misma idea pero para el resto de términos.
El $error$ se propaga, al igual que el $\bot$.

###### Ejercicio 3.15
Lo mismo que el ejercicio de arriba pero con $\mu$.

Y hasta acá llegué, los últimos ejercicios me quedé medio *¿?*, si me sobra tiempo los hago.

###### Extras
Quiero hablar un poquito del punto fijo $\mu$ en semántica denotacional.
Tengo $[[\mu x:\tau. M]]_v = \text{FIX}(V^{[[\tau]]} \mapsto [[M]]_{v,x=V})$. Que para mí es chino básico.
El punto fijo del factorial es: (es un galerazo, ahora lo explico)
$FIX(f \in \mathbb{N} \cup \{ \bot \} \to \mathbb{N} \cup \{ \bot \} \mapsto n \in \mathbb{N} \cup \{ \bot \} \mapsto \left\{ \begin{array}{lcc} 1 & \text{si } n = 0 \\ n \times f(n-1) & \text{sino} \end{array} \right)$
**¿Cómo razonamos esto?**
Recordemos que el punto fijo de $\lambda f:\tau. M$ es $\mu f:\tau. M$. Esto es, por definición.

*"¿Y ESTO QUÉ QUIERE DECIR 😭😭?"*
Veamos primero la definición matemática.
**Def:** $x$ es punto fijo de una función $f$ sii. $f(x) = x$.
Por ejemplo, la función identidad $id(x) = x$ tiene infinitos puntos fijos.
La función $f(x) = x + x$ tiene uno solo, con $x = 0$ pues $f(0) = 0$ y no hay otro $x$ que lo cumpla.
Y $g(x) = x + 1$ no tiene ningún $x$ que sea punto fijo.

Volviendo al cálculo-$\lambda$
Quiere decir que, si le paso $\mu f:\tau. M$ a $\lambda f:\tau. M$, obtengo nuevamente $\mu f:\tau. M$.
O sea, $M\{ f := \mu f:\tau. M \} = \mu f:\tau. M$ análogo en matemáticas, $M(\mu \dots M) = \mu\dots M$
Ahora, esta es una definición, nada más.

Cuestión, suponete que hago $[[Fact_{\mu}]]$ con $Fact_{\mu} = \mu f. \lambda n. \text{if } isZero(n) \text{ then } \underline{1}\text{ else } n \times (f \text{ } pred(n))$.
Tenemos que pensar qué es $V^{[[\tau]]}$, es un elemento del tipo de $f$, por lo que tenemos $f \in \mathbb{N} \cup \{ \bot \} \to \mathbb{N} \cup \{ \bot \}$.
Ahora nos queda $[[M]]_{v,x=V}$. Chequeate que es buscar la semántica denotacional de una lambda. Siguiendo la idea de arriba nos queda $n \in \mathbb{N} \cup \{ \bot \} \mapsto \left\{ \begin{array}{lcc} 1 & \text{si } n = 0 \\ n \times f(n-1) & \text{sino} \end{array} \right.$.
El $n \in \mathbb{N} \cup \{ \bot \}$ sale de $V^{[[\tau]]}$, y lo otro de la recursión del subtérmino.

Luego buscamos un punto fijo, agarramos el más chico que sería $\bot$, esto nos va a dar $FIX(n \in \mathbb{N} \cup \{ \bot \} \mapsto \left\{ \begin{array}{lcc} 1 & \text{si } n = 0 \\ \bot & \text{sino} \end{array} \right)$ que no es la función constantemente $\bot$ pues si $n = 0$, nos da $1$.
En cambio si le pasamos el factorial como $f$, es decir, $Fact_{\mu}$, nos va a dar el punto fijo (es decir, $Fact_{\mu}$ para cualquier n). Nos quedaría: $FIX(n \in \mathbb{N} \cup \{ \bot \} \mapsto \left\{ \begin{array}{lcc} 1 & \text{si } n = 0 \\ n \times Fact_{\mu}(n-1) & \text{sino} \end{array} \right)$
Que si lo pensás un cacho, es literalmente la definición matemática de factorial. Antes $f$ era genérica, ahora es el factorial.

---

## Compilación, inferencia de tipos, y máquinas abstractas (Capítulo 7)
En la sección anterior vimos cálculo-$\lambda$, la base de la programación funcional. Algo interesante que se menciona allí es el cálculo-$\lambda$ **tipado**, a su vez, vimos como inferir el tipo de un programa mediante árboles de deducción.
Ahora, uno podría decir "muy lindo todo, pero yo quiero hacer esto en la compu..."; para eso existe esta sección. Veremos por encima que es un compilador, otra forma de inferencia de tipos, y dónde corre todo esto, una máquina abstracta.
Cuando termines con esta sección debería poder agarrar, por ejemplo, Haskell y crear tu propio "lenguaje" con base en cálculo lambda que corre en tu propia máquina abstracta hecha en Haskell.

Si estás leyendo esto en mi [GitLab](https://gitlab.com/valn/uba), en la sección *Útil*, hay una [implementación de un compilador](https://gitlab.com/valn/uba/-/blob/main/Paradigmas%20de%20Programaci%C3%B3n/Final/%C3%9Atil/M%C3%A1quina%20abstracta%20SECD.hs?ref_type=heads) de cálculo-$\lambda$ hecho en Haskell que traduce a máquina abstracta SECD :).
### Compilación (Capítulo 7: parte I)
**Def. (compilador):** Un compilador es un programa que traduce programas.
- **Entrada:** programa escrito en un **lenguaje fuente**.
- **Salida:** programa escrito en un **lenguaje objeto**.

Este proceso de traducción debe **preservar la semántica**. Es decir, mantener el **significado** *(que nos interesa)*.

Como motivación, queremos **traducir** de **alto nivel** *(C, Haskell, Python, Lisp, Erlang...)* a un lenguaje de **bajo nivel** como *Assembly*; aún así uno podría tener un compilador de C a Python y sería perfectamente válido.

Las faces de un compilador son:
![[Pasted image 20240723172232.png|center|450]]

### Inferencia de tipos (Capítulo 7: parte II)
###### Entrada en calor
Clarifiquemos un poco la notación.
Tenemos términos **sin anotaciones**
`U ::= x | λx. U | U U | True | False | if U then U else U`

y términos **con anotaciones**
`M ::= x | λx : τ. M | M M | True | False | if M then M else M`

Notamos $erase(M)$ al término sin anotaciones de tipos que resulta de borrar las anotaciones de tipos de $M$.
Ejemplo: `erase((λx : Bool. x) True) = (λx. x) True`

Decimos que un término $U$ sin anotaciones de tipos es **tipable** sii existen:
- un contexto de tipado $\Gamma$
- un término con anotaciones de tipos $M$
- un tipo $\tau$

tales que `erase(M) = U y Γ |- M : τ`.

Queremos un **algoritmo de inferencia de tipos** que dado un término $U$ me determine si es tipable, y si lo es, que me halle un contexto $\Gamma$, un término $M$, y un tipo $\tau$ tales que `erase(M) = U y Γ |- M : τ`.

Sin embargo, antes de adentrarnos en ese lugar, tenemos que aprender sobre el **algoritmo de unificación**...
Y antes de aprender sobre el algoritmo de unificación, tenemos que aprender sobre **unificación**, es decir, qué es **unificar**.

##### Unificación
Introduzcamos una noción, la de incógnita, tal que una incógnita se denota como $?n$ para algún $n \in \mathbb{N^+}$.

Supongamos que queremos determinar el tipo de $f$, y sabemos que $f$ devuelve obligatoriamente un $\mathbb{Bool}$. Podemos decir que `f : ?1 -> Bool`.
Y a su vez sabemos que $f$ debe tomar un $\mathbb{N}$, entonces podemos decir que `f : Nat -> ?2`.
*Guiño de notación a semántica denotacional (pues usé $\mathbb{Bool}$ y $\mathbb{N}$)*

Estas dos **ecuaciones** son independientes. Razonablemente uno podría pensar ahora que $f$ debe ser de tipo `f : Nat -> Bool`.
La idea de la unificación se basa en **manipular tipos parcialmente conocidos**. Para esto, se incorporan incógnitas *(representadas como ?1, ?2, ?3, etc.)* a los tipos.

En esta sección trabajaremos únicamente con **dos** ecuaciones de tipos *(salvo por las definiciones)*, después se generalizará.

Para resolver estas incógnitas, es necesario resolver ecuaciones entre tipos. Por ejemplo:
1. $(?1 \to Bool)$ $\overset{?}{=}$ $((Bool \to Bool) \to$ $?2)$
   Tiene solución: $?1 := (Bool \to Bool)$ y $?2 := Bool$.
2. $(?1 \to$ $?1) \overset{?}{=} ((Bool \to Bool) \to$ $?2)$
   Tiene solución: $?1 := (Bool \to Bool)$ y $?2 := (Bool \to Bool)$.
3. $(?1 \to Bool) \overset{?}{=}$ $?1$
   No tiene solución.

Este proceso de resolución de ecuaciones entre tipos con incógnitas va ser fundamental para el **algoritmo de unificación** y **algoritmo de inferencia de tipos** *(ambos los veremos más adelante, pero en este capítulo, no te emocionés tanto)*.

Pensemos un poco en la gramática de los tipos ahora que agregamos las incógnitas...
Supongamos fijado un conjunto finito de **constructores de tipos**:
**Constantes**: $Bool, Nat, \dots$
**Unarios**: $(Lista$ $\bullet$), $(Maybe$ $\bullet)$
**Binarios**: $(\bullet$ $\to$ $\bullet)$, $(\bullet$ $\times$ $\bullet)$, $(Either \text{ } \bullet \text{ } \bullet)$
*Y la lista sigue...*

Su gramática queremos que se defina usando incógnitas $?n$ y constructores, es decir:
$\tau \text{ } ::= \text{ }?n \text{ } | \text{ } C(\tau_{1},\dots,\tau_{n})$

Llamamos **sustitución** a una función que a cada incógnita le asocia un tipo.

Notamos: $S = \{{?k_{1} := \tau_{1},\dots,?k_{n} := \tau_{n}} \}$ a la sustitución $S$.
$S(k_{i}) = \tau_{i}$ para cada $1 \leq i \leq n$, y $S(w) = w$ para el resto.
En criollo, si $k_{i}$ pertence a $S$, te devuelve $\tau_{i}$; si no pertenece te devuelve lo mismo que le diste.

Un **problema de unificación** es un conjunto finito $E$ de ecuaciones entre tipos que pueden involucrar incógnitas, $E = \{ \tau_1 \overset{?}{=} \sigma_1, \tau_2 \overset{?}{=} \sigma_2, \ldots, \tau_n \overset{?}{=} \sigma_n \}$.
*O sea, es el conjunto de ecuaciones que queremos despejar; en el ejemplo de $f$ sería $E = \{ (?1$ $\to Bool) \overset{?}{=} (Nat \to$ $?2) \}$*

Y un **unificador** para $E$ es una sustitución $S$ tal que: $S(\tau_{1}) = S(\sigma_{1}), \dots, S(\tau_{n}) = S(\sigma_{n})$.
*En el ejemplo de $f$ sería $S(?1 \to Bool) = S(Nat \to$ $?2)$.
Notar que acá funcaría $S = \{ ?1 := Nat, ?2 := Bool \}$ como sustitución. Ya veremos cómo encuentra ese $S$, que no panda el cúnico.*

Supongamos que contamos con el siguiente problema de unificación: $E = \{ (?1 \to Bool) \overset{?}{=}$ $?2 \}$
Uno acá podría tirar varios $S = \{ algo \}$, es por ello que metemos una noción más. La noción del **unificador más general** *(MGU)*

Formalmente se dice que: una susitución $S_{A}$ es más general una sustitución $S_{B}$ si existe una sustitución $S_{C}$ tal que: $S_{B} = S_{C} \circ S_{A}$. Es decir, $S_{B}$ se obtiene instanciando variables de $S_{A}$.

Pero yo sé que a vos no te gusta la formalidad, así que te lo hago tipo `r/ExplainLikeImFive`.

Vos querés encontrar una sustitución para el unificador $E$, pero querés la más general, ¿a qué te suena?, a que no querés pedir de más, querés la sustitución *"más débil"* por así decirlo.

Ponele, si yo le pido a las incógnitas que: $?1 := Int$ y que $?2 := (Int \to Bool)$ es medio un montón, ¿de dónde saqué que $?1$ tenga que ser de tipo $Int$? es un recontra galerazo.
Vamos a querer unificar de forma tal que valga la igualdad $S(\tau_{i}) = S(\sigma_{i})$, pero pidiendo lo menos posible.

*"Bueno, bueno, pero decime cómo lo hago che..."*
Más adelante vamos a ver (literalmente en la siguiente sección), por ahora quedate con que vine yo y te dije que el **MGU** es $S = \{ ?2 := (?1 \to Bool) \}$, podés intentar pensar el motivo, pero no te quemes porque en 5 minutos vas a tener todo el material para poder deducirlo.

Uno podría preguntarse si $S = \{ ?1 :=$ $?3, ?2 := (?3 \to Bool) \}$ es también un **MGU** válido. La respuesta es que no, por la definición de **MGU**.
Tomá:
- $S_{A} = \{ ?2 := (?1 \to Bool) \}$
- $S_{B} = \{ ?1 :=$ $?3, ?2 := (?3 \to Bool) \}$
- $S_{C} = \{ ?1 :=$ $?3 \}$
  Te va a quedar que $S_{A}$ es más general que $S_{B}$ pues $S_{B} = S_{C} \circ S_{A}$.

##### Algoritmo de unificación (de Martelli–Montanari)
Ahora que estás familiarizado con el concepto de unificación de términos (o tipos, porque los tipos son términos). Planteamos el siguiente algoritmo dado un **problema de unificación** $E$ (conjunto de ecuaciones a despejar):
- Mientras $E \neq \emptyset$, se aplica sucesivamente alguna de las reglas que veremos a continuación.
	- La regla nos puede devolver un nuevo problema de unificación $E'$, o una $falla$.
- Si la regla nos devuelve una $falla$, el problema de unificación no tiene solución.
- De lo contrario la regla es de la forma $E \to_{S} E'$.
  La resolución del problema $E$ se reduce a resolver otro problema $E'$, aplicando la sustitución $S$.

Cuestión, que hay dos posibilidades.
1. $E = E_{0} \to_{S_{1}} E_{1} \to_{S_{2}} E_{2} \to_{S_{3}} \dots \to_{S_{n}} E_{n} \to_{S_{n+1}} falla$
   El problema de unificación $E$ no tiene solución
2. $E = E_{0} \to_{S_{1}} E_{1} \to_{S_{2}} E_{2} \to_{S_{3}} \dots \to_{S_{n}} E_{n} = \emptyset$
   El problema de unificación $E$ tiene solución.

Este algoritmo está demostrado que es **correcto**. Es decir:
- Termina para cualquier $E$.
- Si $E$ tiene solución, la encuentra.
	- Se puede recontruir la solución con $S = S_{n} \circ S_{n-1}$ $\circ$ $\dots$ $\circ$ $S_{2} \circ S_{1}$.
- Si $E$ no tiene solución, llega a una $falla$.

Y se denota $mgu(E)$ al unificador más general de $E$, si existe.
**Reglas:**
![[Pasted image 20240723202405.png|center|450]]
##### Algoritmo $\mathbb{W}$ de inferencia de tipos (finalmente)
El algoritmo $\mathbb{W}$ recibe un término $U$ sin anotaciones de tipos y procede recursivamente sobre la estructura de $U$:
- Puede fallar, indicando que $U$ no es tipable.
- Si tiene éxito, devuelve una tripla $(\Gamma, M, \tau)$, donde $erase(M) = U$ y $\Gamma \vdash M : \tau$ es válido.

Escribimos $\mathbb{W}(U) \rightsquigarrow \Gamma \vdash M : \tau$ para indicar que el algoritmo de inferencia tiene éxito cuando se le pasa $U$ como entrada y devuelve una tripla $(\Gamma, M, \tau)$.

Las reglas del algoritmo $\mathbb{W}$ son:
1. Para constantes booleanas:
   $$\frac{}{\mathbb{W}(True) \rightsquigarrow \emptyset \vdash True : Bool}$$
   $$\frac{}{\mathbb{W}(False) \rightsquigarrow \emptyset \vdash False : Bool}$$

2. Para variables:
   $$\frac{}{\mathbb{W}(x) \rightsquigarrow x : ?k \vdash x : ?k}$$
   donde $?k$ es una incógnita fresca.

3. Para condicionales:
   $$\frac{\mathbb{W}(U_1) \rightsquigarrow \Gamma_1 \vdash M_1 : \tau_1 \quad \mathbb{W}(U_2) \rightsquigarrow \Gamma_2 \vdash M_2 : \tau_2 \quad \mathbb{W}(U_3) \rightsquigarrow \Gamma_3 \vdash M_3 : \tau_3}{\mathbb{W}(\text{if } U_1 \text{ then } U_2 \text{ else } U_3) \rightsquigarrow S(\Gamma_1) \cup S(\Gamma_2) \cup S(\Gamma_3) \vdash S(\text{if } M_1 \text{ then } M_2 \text{ else } M_3) : S(\tau_2)}$$
   donde $S = mgu(\{\tau_1 \overset{?}{=} Bool, \tau_2 \overset{?}{=} \tau_3\} \cup \{\Gamma_i(x) \overset{?}{=} \Gamma_j(x) \text{ } | \text{ } i,j \in \{1,2,3\}, \text{ } x \in \Gamma_i \cap \Gamma_j\})$.

4. Para aplicaciones:
   $$\frac{\mathbb{W}(U) \rightsquigarrow \Gamma_1 \vdash M : \tau \quad \mathbb{W}(V) \rightsquigarrow \Gamma_2 \vdash N : \sigma}{\mathbb{W}(U V) \rightsquigarrow S(\Gamma_1) \cup S(\Gamma_2) \vdash S(M N) : S(?k)}$$
   donde $?k$ es una incógnita fresca y $S = mgu(\{\tau \overset{?}{=} \sigma \to ?k\} \cup \{\Gamma_1(x) \overset{?}{=} \Gamma_2(x) : x \in \Gamma_1 \cap \Gamma_2\})$.

5. Para abstracciones:
   $$\frac{\mathbb{W}(U) \rightsquigarrow \Gamma \vdash M : \tau}{\mathbb{W}(\lambda x. U) \rightsquigarrow \Gamma \ominus \{x\} \vdash \lambda x : \sigma. M : \sigma \to \tau}$$
   donde $\sigma = \begin{cases} \Gamma(x) & \text{si } x \in \Gamma \\ \text{una incógnita fresca } ?k & \text{si no} \end{cases}$.

6. Para naturales:
   $$\frac{}{\mathbb{W}(zero) \rightsquigarrow \emptyset \vdash zero : Nat}$$
   $$\frac{\mathbb{W}(U) \rightsquigarrow \Gamma \vdash M : \sigma}{\mathbb{W}(succ(U)) \rightsquigarrow S(\Gamma) \vdash S(succ(M)) : Nat}$$
   $$\frac{\mathbb{W}(U) \rightsquigarrow \Gamma \vdash M : \sigma}{\mathbb{W}(pred(U)) \rightsquigarrow S(\Gamma) \vdash S(pred(M)) : Nat}$$
   $$\frac{\mathbb{W}(U) \rightsquigarrow \Gamma \vdash M : \sigma}{\mathbb{W}(izZero(U)) \rightsquigarrow S(\Gamma) \vdash S(isZero(M)) : Bool}$$
   donde $S = mgu(\{\sigma \overset{?}{=} Nat\})$ en todos los casos.

6. Para $\mu$ (recursión):
   $$\frac{\mathbb{W}(U) \rightsquigarrow \Gamma \vdash M : \sigma}{\mathbb{W}(\mu x. U) \rightsquigarrow S(\Gamma) \ominus \{x\} \vdash S(\mu x. M) : S(\sigma)}$$
   donde $S = mgu(\{\sigma \overset{?}{=} \tau\})$ y $\tau = \begin{cases} \Gamma(x) & \text{si } x \in \Gamma \\ \text{una incógnita fresca } ?k & \text{si no} \end{cases}$.

El algoritmo $\mathbb{W}$ es correcto:
1. Si $U$ no es tipable, $\mathbb{W}(U)$ falla al resolver alguna unificación.
2. Si $U$ es tipable, $\mathbb{W}(U) \rightsquigarrow \Gamma \vdash M : \tau$, donde $erase(M) = U$ y $\Gamma \vdash M : \tau$ es un juicio válido.

Además, $\Gamma \vdash M : \tau$ es el juicio de tipado más general posible. Es decir, si $\Gamma' \vdash M' : \tau'$ es un juicio válido y $erase(M') = U$, existe una sustitución $S$ tal que:
- $\Gamma' \supseteq S(\Gamma)$
- $M' = S(M)$
- $\tau' = S(\tau)$

**Tip práctico**: generalmente para un ejercicio de inferencia de tipos con el algoritmo $\mathbb{W}$ está bueno armarte el árbol de subtérminos, después agarrás y vas resolviendo $\mathbb{W}(\text{hoja})$ para toda las hojas, y así para arriba hasta llegar a la raíz.
Así vas recontruyendo para arriba la solución evitás confundirte y marearte.

### Máquinas abstractas (Capítulo 7: parte III)
Bueno, ya tenemos una base sólida para crear nuestro propio lenguaje. ¡Pero no tenemos dónde correrlo!, de eso se trata esta parte del capítulo :).
Si cursaste alguna materia de Sistemas (por ejemplo, Sistemas Digitales), seguramente este capítulo te sea más ameno.

Imaginemos una máquina con tres instrucciones *(como las que brinda la arquitectura de un procesador, por ejemplo)*:
$$LDI(n) \text{ | }ADD \text{ | }MUL$$
Un programa $\ell$ *(lista de instrucciones)* opera sobre una pila $\pi$ *(ponele la memoria)*:
$$
\begin{array} \\
\begin{array}{|c|c|}
\hline LDI(n) : \ell & \pi \\

\hline \end{array} \longrightarrow

\begin{array}{|c|c|}
\hline \ell & n : \pi \\
\hline
\end{array}
\\
\begin{array}{|c|c|}
\hline ADD : \ell & m : n : \pi \\

\hline \end{array} \longrightarrow

\begin{array}{|c|c|}
\hline \ell & (n + m) : \pi \\
\hline
\end{array}
\\
\begin{array}{|c|c|}
\hline MUL : \ell & m : n : \pi \\

\hline \end{array} \longrightarrow

\begin{array}{|c|c|}
\hline \ell & (n * m) : \pi \\
\hline
\end{array}
\end{array}
$$
Consideremos la siguiente gramática de expresiones aritméticas:
$$E ::= n \text{ } | \text{ } E + E \text{ } | \text{ } E * E$$
Una expresión se puede **compilar** a una lista de instrucciones:
$$\mathcal{C}\{ n \} = LDI(n)$$
$$\mathcal{C}\{ E_{1} + E_{2}\} = \mathcal{C}\{ E_{1} \};\mathcal{C}\{ E_{2} \};ADD$$
$$\mathcal{C}\{ E_{1} * E_{2}\} = \mathcal{C}\{ E_{1} \};\mathcal{C}\{ E_{2} \};MUL$$
###### Ejemplito de compilación
![[Pasted image 20240724004754.png|center|400]]
Bueno pero este fue un ejemplo medio de juguete. Hagamos algo con cálculo-$\lambda$.

Queremos definir un compilador tal que:
- Lenguaje fuente: términos del cálculo-$\lambda$ con booleanos.
- Lenguaje objeto: código para la [máquina abstracta SECD](https://en.wikipedia.org/wiki/SECD_machine).

El compilador implementa la estrategia **call-by-value**.
La evaluación de una aplicación $MN$ es de izquierda a derecha.

Una **máquina abstracta** es una abstracción de la arquitectura real.
Es sencillo traducir su código a un lenguaje de bajo nivel.

##### La máquina SECD
![[Pasted image 20240724010812.png|center|400]]
###### Ejemplo de ejecución en la máquina abstracta SECD
![[Pasted image 20240724011014.png|center|400]]

###### Compilación del cálculo-$\lambda$ a la máquina SECD
![[Pasted image 20240724011106.png|center|400]]
###### Ejemplo de compilación
![[Pasted image 20240724011139.png|center|400]]

**Corrección del compilador (teorema)**
Sea $M$ un término cerrado del cálculo-$\lambda$ y v un valor booleano.
$M \to^{*} v$ es equivalente a
$$
\begin{array}{|c|c|c|c|}
\hline C_{[]}\{M\} & [ ] & [ ] & [ ] \\

\hline \end{array} \longrightarrow^*

\begin{array}{|c|c|c|c|}
\hline [ ] & v : \pi & e & d \\
\hline
\end{array}
$$
para ciertos $\pi, e, d$.

---

## Lógica de primer orden (Capítulo 8)
Bueno, ya vimos mucho del paradigma funcional. Este capítulo es el 101 que nos va a servir para poder pasar al paradigma lógico. Cubriremos la noción de lógica de primer orden, tema que, va más allá de la programación lógica.

Veníamos trabajando con **lógica proposicional** *(ahora vas y recordás el capítulo 3)*, esta lógica nos permite razonar sobre ==proposiciones==, ejemplo: $P \lor \neg P$ con $P$ proposición.

Ahora vamos a trabajar con **lógica de primer orden**, ==permite **razonar** acerca de **elementos** sobre los que se **predica**==, ejemplo: $\forall X. (P(X) \implies \neg P(término(X)))$.
Extiende a la lógica proposicional con **términos** y **cuantificadores**.

*"Daaa Valen, ¿qué es un término?..."*
Ya vamos con la gramática, tranqui palanqui. Dejame explayar un poco mejor el motivo de estudiar **LPO** en este curso. Aunque actualmente hay banda de motivos, voy a restringirme a lo que nos interesa, en **AED** tuviste que haber visto especificación, la idea es que queremos que los programas sean similares a ese mundo, onda copiar y pegar la especificación en código y que la compu lo resuelva.

Venimos laburando con **programación declarativa** hace rato **"define el ¿qué? y no el ¿cómo?"**, seguro lo escuchaste 80 veces. Bueno, la idea de la programación declarativa, es que **los programas se asemejen lo más posible a las especificaciones**. A su vez, **dentro** de la programación declarativa, encuentra el **paradigma funcional** y el **paradigma lógico**.
El funcional ya lo vimos, ahora **vamos con el lógico**.
*Psst..., si te gusta todo eso de programación declarativa, chequeate [NixOS](https://nixos.org/), un S.O. GNU/Linux que es declarativo en lugar de caótico e imperativo :).*

Volviendo a lo nuestro. Estamos parados en la programación lógica, queremos que el usuario *(programador)* escriba una fórmula como por ejemplo $\exists X. P(X)$, y el sistema busca satisfacer o refutar la fórmula.
En caso de lograr satisfacerla, el sistema produce una salida que verifica la propiedad P buscada.

##### Sintaxis de la lógica de primer orden
Un **lenguaje de primer orden** $\mathcal{L}$ está dado por:
1. Un conjunto de **símbolos de <span style="color:rgb(0, 0, 255); font-weight: bold;">función</span>** $\mathcal{F} = \{ f,g,h,\dots \}$
   Cada símbolo de función tiene asociada una aridad $(\geq 0)$.
2. Un conjunto de **símbolos de <span style="color:rgb(255, 0, 0); font-weight: bold;">predicado</span>** $\mathcal{P} = \{ P,Q,R,\dots \}$.
   Cada símbolo de predicado tiene asociada una aridad $(\geq 0)$.

Suponemos fijado un lenguaje de primer orden $\mathcal{L}$, y un conjunto infinito numerable de **varaibles** $\mathcal{X} = \{ X,Y,Z,\dots \}$.

El conjunto $\mathcal{T}$ de <span style="color:rgb(0, 0, 255)">términos</span> se define por la siguiente gramática:
$$t ::= X \text{ } | \text{ } f(t_{1},\dots,t_{n})$$
donde:
- $X$ denota una variable.
- $f$ denota un símbolo de función de aridad $n$.

Es decir, <mark style="background: #ADCCFFA6;">los términos son o bien variables, o bien funciones</mark> de aridad $0,1,2,\dots,n$. Y <mark style="background: #ADCCFFA6;">adentro de las funciones van otros términos</mark> (o sea: o variables, o funciones).

![[Pasted image 20240724174401.png|center|500]]

Bueno, yo dije que esto era una extensión de la lógica proposicional (prefiero decir que la lógica proposicional es un caso particular de LPO, pero bueno). Cuestión, en el capítulo 3 vimos la **gramática de las fórmulas** en lógica proposicional.
Extendámosla para lógica de primer orden.

![[Pasted image 20240724180649.png|center|500]]

**Ejemplos de fórmulas**
![[Pasted image 20240724180746.png|center|400]]
![[Pasted image 20240724181722.png|center|550]]
Notar el renombramiento de $Z$ a $Z'$ para evitar la captura de variables.

##### Deducción natural
Sí, otra vez; **it's back**, it's back in full force.
Vamos a extender lo que vimos de lógica proposicional a lógica de primer orden, y una de las cosas que vimos fue deducción natural.

Se mantiene que
- Un **contexto** $\Gamma$ es un conjunto finito de fórmulas.
- Un **secuente** es de la forma $\Gamma \vdash \sigma$.

Todas las reglas de deducción natural proposicional siguen vigentes, se agregan reglas: la introducción y eleminación del $\forall$ y $\exists$.

**Introducción del $\forall$**
$$\frac{\Gamma \vdash \sigma \text{ }\text{ }\text{ }\text{ }\text{ }\text{ }\text{ }\text{ }\text{ }\text{ } X \not\in \text{fv}(\Gamma)}{\Gamma \vdash \sigma \{ X := t \}} \forall_{I}$$
**Eliminación del $\forall$**
$$\frac{\Gamma \vdash \forall X. \sigma}{\Gamma \vdash \sigma \{ X := t \}} \forall_{E}$$

**Introducción del $\exists$**
$$\frac{\Gamma \vdash \sigma \{ X := t \}}{\Gamma \vdash \exists X. \sigma} \exists_{I}$$
**Eliminación del $\exists$**
$$\frac{\Gamma \vdash \exists X. \sigma \text{ }\text{ }\text{ }\text{ }\text{ }\text{ } \Gamma, \sigma \vdash \tau \text{ }\text{ }\text{ }\text{ }\text{ }\text{ } X \not\in \text{fv}(\Gamma, \tau)}{\Gamma \vdash \sigma} \exists_{E}$$

##### Semántica de la lógica de primer orden
Bueno, hasta acá era todo muy color de rosas. Esta es la parte más densa del capítulo; vamos a meterle semántica a la cosa.
Definamos primero qué es una **estructura de primer orden**.

Una **estructura de primer orden** es un par $\mathcal{M} = (M, I)$ donde:
- $M$ es un conjunto **no vacío**, llamado <mark style="background: #D2B3FFA6; font-style: italic;">universo</mark>.
- $I$ es una <mark style="background: #BBFABBA6; font-style: italic;">función</mark> que a le da una <mark style="background: #BBFABBA6; font-style: italic;">interpretación a cada símbolo</mark>.
- Para cada símbolo de <span style="color:rgb(0, 0, 255)">función f</span> de aridad $n$:
$$I(f) : M^{n} \to M$$
- Para cada símbolo de <span style="color:rgb(255, 0, 0)">predicado</span> <span style="color:rgb(255, 0, 0); font-weight: bold;">P</span> de aridad $n$:
$$I(P) \subseteq M^{n}$$

| ![[Pasted image 20240724191330.png\|center\|320]] | ![[Pasted image 20240724191343.png\|center\|320]] |
| ------------------------------------------------- | :-----------------------------------------------: |

**Interpretación de términos**
![[Pasted image 20240724191401.png|center|400]]

**Interpretación de fórmulas**
![[Pasted image 20240724191420.png|center|400]]

**Validez y satisfactibilidad**
Decimos que una fórmula $\sigma$ es:
![[Pasted image 20240724191449.png|center|600]]

![[Pasted image 20240724191514.png|center|500]]

**Modelos**
![[Pasted image 20240724191548.png|center|500]]

**Corrección y completitud** *(sí, como en el capítulo 3)*

| ![[Pasted image 20240728053652.png\|center\|200]] | ![[Pasted image 20240728053712.png\|center\|200]] | ![[Pasted image 20240728053738.png\|center\|200]] |
| :-----------------------------------------------: | :-----------------------------------------------: | :-----------------------------------------------: |

![[Pasted image 20240724191803.png|center|600]]

**El problema de la decisión**
Querríamos un algoritmo que resuelva el siguiente problema:
- Entrada: una fórmula $\sigma$.
- Salida: un booleano que indica si $\sigma$ es válida.

**No** es posible dar un algoritmo que cumpla dicha especificación.

##### Unificación de términos de lógica de primer orden
Anteriormente dije que los tipos no eran más que términos. El algoritmo de unificación para tipos es literalmente el mismo que para términos. Con un leve cambio de notación nos quedan las reglas:
![[Pasted image 20240724183507.png|center|600]]

---

## Resolución lógica (Capítulo 9)
En el capítulo anterior... *(flashbacks de LPO)*...
Ahora vamos a ver **resolución lógica**. ¿Cuál es la motivación para aprenderla?, bueno, de **Prolog**, el lenguaje de **paradigma lógico** que vamos a utilizar.

Veamos para motivarnos cómo funciona **Prolog**. A este progrma:
```prolog
padre(cronos, zeus).
padre(zeus, atenea).
padre(zeus, hefesto).

padre(zeus, ares). abuelo(X, Y) :- padre(X, Z), padre(Z, Y).
```

Se le pueden realizar, por ejemplo, las siguientes **consultas**:
```prolog
?- padre(zeus, atenea).
>> true.

?- padre(zeus, cronos).
>> false.

?- abuelo(X, atenea).
>> X = cronos.

?- abuelo(X, zeus).
>> false.

?- abuelo(cronos, X).
>> X = atenea ;
>> X = hefesto ;
>> X = ares.

?- abuelo(X, Y).
>> X = cronos, Y = atenea ;
>> X = cronos, Y = hefesto ;
>> X = cronos, Y = ares.
```

Prolog opera con **términos de primer orden**:
$$X \text{ }\text{ }\text{ }\text{ } Y \text{ }\text{ }\text{ }\text{ } succ(succ(zero)) \text{ }\text{ }\text{ }\text{ } bin(I, R, D) \text{ }\text{ }\text{ }\text{ } \dots$$
Las **fórmulas atómicas** son de la forma $pred(t_{1},\dots,t_{n})$
```prolog
padre(zeus, atenea)
suma(zero, X, X)
```


Un programa es un conjunto de **reglas**. Cada regla es de la forma:
$$\sigma \text{ }\text{ } \text{:-} \text{ }\text{ }\text{ } \tau_{1}, \dots, \tau_{n}$$
donde $\sigma,\tau_{1},\dots,\tau_{n}$ son fórmulas atómicas.
```prolog
abuelo(X, Y) :- padre(X, Z), padre(Z, Y).
```


Las reglas en las que $n = 0$ se llaman hechos y se escriben: $\sigma$.
```prolog
padre(zeus, ares).
```

Las reglas tienen la siguiente interpretación lógica:
$$\forall X_{1} \dots \forall X_{k}. ((\tau_{1} \land \dots \land \tau_{n}) \implies \sigma)$$
donde $X_{1},\dots,X_{k}$ son todas las variables libres de las fórmulas.
```prolog
∀X. ∀Y. ∀Z. ((padre(X, Z) ∧ padre(Z, Y)) ⇒ abuelo(X, Y))
```


Una **consulta** es de la forma:
$$\text{?-} \text{ }\text{ } \sigma_{1}, \dots, \sigma_{n}$$
```prolog
?- abuelo(X, ares).
```

Las consultas tienen la siguiente interpretación lógica:
$$\exists_{1} \dots \exists_{k}. (\sigma_{1} \land \dots \land \sigma_{n})$$
donde $X_{1}, \dots, X_{k}$ son todas las variables libres de las fórmulas.


**El entorno de Prolog** busca demostrar la fórmula $\tau$ de la consulta.
En realidad **busca refutar $\neg \tau$**.
**La búsqueda de la refutación se basa en el método de resolución.**
(¡Chan!, ahora sabés por qué vemos esto)

Veámoslo primero para *lógica proposicional* para entender los conceptos (más fácil), después para *lógica de primer orden* (más complicado).

#### Método de resolución (para lógica proposicional)
Pensemos **qué queremos** primero. Queremos un algoritmo tal que la
- Entrada sea: una formula $\sigma$ de la lógica proposicional.
- Salida sea: un booleano que indica si $\sigma$ es válida.

##### Idea
1. Escribir $\neg\sigma$ como un conjunto $\mathcal{C}$ de **cláusulas**. (Pasar a _forma clausal_).
2. Buscar una **refutación** de $\mathcal{C}$. Una refutación de $\mathcal{C}$ es una derivación de $\mathcal{C} \vdash \bot$.
	* Si se encuentra una refutación de $\mathcal{C}$:
		- Vale $\neg\sigma\vdash\bot$. Es decir, $\neg\sigma$ es insatisfactible/contradicción.
		- Luego vale $\vdash\sigma$. Es decir, $\sigma$ es valida/tautología.
	* Si no se encuentra una refutación de $\mathcal{C}$:
		- No vale $\neg\sigma\vdash\bot$. Es decir, $\sigma$ es satisfactible.
		- Luego no vale $\vdash\sigma$. Es decir, $\sigma$ no es válida.

Veamos como hacer lo primero (obtener el conjunto de **cláusulas** $\mathcal{C}$, *i.e. pasar a forma clausal*)
##### Pasaje a forma clausal
Una fórmula se pasa a forma clausal aplicando las siguientes reglas. Todas las reglas transforman la fórmula en otra equivalente.

**Paso 1.** Deshacrese del conectivo "$\Rightarrow$":
$$\sigma\Rightarrow\tau\quad\rightarrow\quad\neg\sigma\vee\tau$$
La fórmula resultante solo usa los conectivos $\{\neg,\vee,\wedge\}$.

**Paso 2.** Empujar el conectivo "$\neg$" hacia adentro:
$$\begin{array}{ccc}\neg(\sigma\wedge\tau)&\rightarrow&\neg\sigma\vee\neg\tau\\ \neg(\sigma\vee\tau)&\rightarrow&\neg\sigma\wedge\neg\tau\\ \neg\neg\sigma&\rightarrow&\sigma\end{array}$$
La fórmula resultante esta en **forma normal negada (NNF)**:

$$\sigma_{\rm nnf}\ ::=\ \text{P}\ |\ \neg\text{P}\ |\ \sigma_{\rm nnf}\wedge\sigma_{\rm nnf}\ |\ \sigma_{\rm nnf}\vee\sigma_{\rm nnf}$$

**Paso 3.** Distribuir $\lor$ sobre $\land$:
$$\begin{array}{ccc}\sigma\vee(\tau\wedge\rho)&\rightarrow&(\sigma\vee\tau)\wedge(\sigma\vee\rho)\\ (\sigma\wedge\tau)\vee\rho&\rightarrow&(\sigma\vee\rho)\wedge(\tau\vee\rho)\end{array}$$

La fórmula resultante esta en **forma normal conjuntiva (CNF)**. Una fórmula en **CNF** es conjunción de disyunciones de literales (asumiendo que permitimos asociar libremente $\wedge$ y $\vee$):
$$\begin{array}{l l l l}{{\mathrm{Fórmulas~en~CNF}}}&{{\sigma_{\mathrm{cnf}}}}&{{::=}}&{{\left(\kappa_{1}\wedge\kappa_{2}\wedge\ldots\wedge\kappa_{n}\right)}}\\ {{\mathrm{Cláusulas}}}&{{\kappa}}&{{::=}}&{{\left(\ell_{1}\lor\ell_{2}\vee\ldots\lor\ell_{m}\right)}}\\ {{\mathrm{Literales}}}&{{\ell}}&{{::=}}&{{{\bf P}\mid\neg{\bf P}}}\end{array}$$

**Paso 4.** Final:
Por último, usando el hecho de que la disyunción ($\vee$) es:
$$\begin{array}{ll}\mbox{Asociativa}&\sigma\vee(\tau\vee\rho)\iff(\sigma\vee\tau)\vee\rho\\ \mbox{Conmutativa}&\sigma\vee\tau\iff\tau\vee\sigma\\ \mbox{Idempotente}&\sigma\vee\sigma\iff\sigma\end{array}$$
notamos una cláusula (disyunción de literales) como un conjunto:
$$(\ell_{1}\vee\ell_{2}\vee\ldots\vee\ell_{n})\quad\mbox{se nota}\quad\{\ell_{1},\ell_{2},\ldots,\ell_{n}\}$$

Análogamente, usando el hecho de que la conjunción ($\wedge$) es asociativa, conmutativa, e idempotente, notamos una conjunción de cláusulas como un conjunto:
$$(\kappa_{1}\wedge\kappa_{2}\wedge\ldots\wedge\kappa_{n})\quad\mbox{se nota}\quad\{\kappa_{1},\kappa_{2},\ldots,\kappa_{n}\}$$

Completamos el primer paso. Veamos ahora como buscamos una **refutación**.
Una vez obtenido un conjunto de cláusulas $\mathcal{C}=\{\kappa_{1},\ldots,\kappa_{n}\}$, se busca una **refutación**, es decir, una demostración de $\mathcal{C}\vdash\bot$.

El método de refutación se basa en la siguiente regla de deducción:

$$\frac{\color{red}\text{P}\color{black}\vee\color{blue}\ell_{1}\vee\ldots\lor\ell_{n}\qquad\color{black}\neg\color{red}\text{P}\color{black}\vee\color{green}\ell_{1}^{\prime}\vee\ldots\vee\ell_{m}^{\prime}}{\color{blue}\ell_{1}\vee\ldots\vee\ell_{n}\color{black}\vee\color{green}\ell_{1}^{\prime}\vee\ldots\vee\ell_{m}^{\prime}} \text{ Regla de resolución}$$

Escrita con notación de clásulas:
$$\frac{\{ \color{red}\text{P}\color{black},\color{blue}\ell_{1},\ldots,\ell_{n} \color{black}\} \qquad \{ \neg\color{red}\text{P}\color{black},\color{green}\ell_{1}^{\prime},\ldots,\ell_{m}^{\prime} \color{black}\}}{ \{ \color{blue}\ell_{1}\color{black},\ldots,\color{blue}\ell_{n}\color{black},\color{green}\ell_{1}^{\prime}\color{black},\ldots,\color{green}\ell_{m}^{\prime}\color{black}\}} \text{ Regla de resolución}$$
La conclusión se llama la **resolvente** de las premisas.

##### Algoritmo de refutación
- Entrada: un conjunto de cláusulas $\mathcal{C}_{0}=\{\kappa_{1},\ldots,\kappa_{n}\}$.
- Salida: <span style="color:rgb(0, 0, 255)">SAT</span>/<span style="color:rgb(0, 0, 255)">INSAT</span> indicando si $\mathcal{C}_{0}$ es insatisfactible $(\mathcal{C}_{0}\vdash\bot)$.

Sea $\mathcal{C}:=\mathcal{C}_{0}$. Repetir mientras sea posible:
1. Si $\{\}\in\mathcal{C}$, devolver <span style="color:rgb(0, 0, 255)">INSAT</span>.
2. Elegir dos cláusulas $\kappa,\kappa^{\prime}\in\mathcal{C}$, tales que:
	- $\kappa=\{\text{P},\ell_{1},\ldots,\ell_{n}\}$ 
	- $\kappa^{\prime}=\{-\text{P},\ell_{1}^{\prime},\ldots,\ell_{m}^{\prime}\}$
	  La resolvente $\rho=\{\ell_{1},\ldots,\ell_{n},\ell_{1}^{\prime},\ldots,\ell_{m}^{\prime}\}$ no está en $\mathcal{C}$.
	  Si no es posible, devolver <span style="color:rgb(0, 0, 255)">SAT</span>.
1. Tomar $\mathcal{C} := \mathcal{C} \cup \{ \rho \}$ y volver al paso <span style="color:rgb(0, 0, 180)">1</span>.

###### Corrección del pasaje a forma clausal (teorema)
Dada una fórmula $\sigma$:
1. El pasaje a forma clausal termina.
2. El conjunto de cláusulas $\mathcal{C}$ obtenido es equivalente a $\sigma$.
   Es decir, $\vdash \sigma \Longleftrightarrow \mathcal{C}$.

###### Corrección del algoritmo de refutación (teorema)
Dado un conjunto de cláusulas $\mathcal{C}_{0}$:
1. El algoritmo de refutación termina.
2. El algoritmo retorna <span style="color:rgb(0, 0, 255)">INSAT</span> si y solo si $\mathcal{C}_{0} \vdash \bot$.

##### Método de resolución (para lógica de primer orden)
Bueno, ahora que sabemos sobre cómo funciona esto el lógica proposicional, podemos encarar lo mismo, pero en lógica de primer orden, veamos...
Acá la milanesa tiene más vueltas, principalmente en algunos casos, **nuestro algoritmo se puede colgar** 😔.

Seguimos queriendo *casi* lo mismo, un algoritmo tal que la
- Entrada sea: una formula $\sigma$ de la lógica de primer rden.
- Salida sea: un booleano que indica si $\sigma$ es válida.

**Si $\sigma$ es válida, el método siempre termina.**
**Si $\sigma$ no es válida, el método puede no terminar.**

##### Idea (procedimiento de semi-decisión)
1. Escribir $\neg \sigma$ como conjunto $\mathcal{C}$ de **cláusulas**.
2. Buscar una **refutación** de $\mathcal{C}$.
   <span style="color:rgb(0, 0, 255)">Si existe alguna refutación, el método encuentra alguna.</span>
   <span style="color:rgb(255, 0, 0)">Si no existe una refutación, el método puede "colgarse".</span>

Veamos cómo pasar una fórmula a forma clausal, pero en **LPO**. Los pasos son _parecidos_, pero con cambios importantes.

**Paso 1.** Deshacerse del conectivo "$\Rightarrow$":
$$\sigma\Rightarrow\tau\quad\rightarrow\quad\neg\sigma\vee\tau$$
La fórmula resultante solo usa los conectivos $\{\neg,\vee,\wedge,\color{magenta}\forall\color{black},\color{magenta}\exists\color{black}\}$.

**Paso 2.** Empujar el conectivo "$\neg$" hacia adentro:
$$\begin{array}{ccc}\neg(\sigma\wedge\tau)&\rightarrow&\neg\sigma\vee\neg\tau\\ \neg(\sigma\vee\tau)&\rightarrow&\neg\sigma\wedge\neg\tau\\ \neg\neg\sigma&\rightarrow&\sigma\\ \color{magenta}\neg\forall X. \sigma&\color{magenta}\rightarrow&\color{magenta}\exists X. \neg\sigma\\ \color{magenta}\neg\exists X. \sigma&\color{magenta}\rightarrow&\color{magenta}\forall X. \neg\sigma\end{array}$$
La fórmula resultante esta en **forma normal negada (NNF)**.

$$
\sigma_{\rm nnf}\ ::=\ \color{magenta}\text{P}(t_{1,\dots,t_{n}})\color{black}\ |\ \color{magenta}\neg\text{P}(t_{1,\dots,t_{n}})\color{black}\ |\ \sigma_{\rm nnf}\wedge\sigma_{\rm nnf}\ |\ \sigma_{\rm nnf}\vee\sigma_{\rm nnf}\ |\ \color{magenta}\forall X.\sigma_{nnf} \color{black}\ |\ \color{magenta}\exists X. \sigma_{nnf}$$

**Paso 3.** Extraer los cuantificadores ($\forall,\exists$) hacia afuera.
Se asume siempre que $\text{X} \not\in \text{fv}(\tau)$:
$$\begin{array}{l l l}
    \left(\forall \text{X}.\, \sigma \right) \wedge \tau \longrightarrow \forall \text{X}.\left(\sigma \wedge \tau \right) & \tau \wedge \left(\forall \text{X}.\, \sigma \right) \longrightarrow \forall \text{X}.\left(\tau \wedge \sigma \right) \\
    \left(\forall \text{X}.\, \sigma \right) \lor \tau \longrightarrow \forall \text{X}.\left(\sigma \lor \tau \right) & \tau \vee \left(\forall \text{X}.\, \sigma \right) \longrightarrow \forall \text{X}.\left(\tau \lor \sigma \right) \\
    \left(\exists \text{X}.\, \sigma \right) \wedge \tau \longrightarrow \exists \text{X}.\left(\sigma \wedge \tau \right) & \tau \wedge \left(\exists \text{X}.\, \sigma \right) \longrightarrow \exists \text{X}.\left(\tau \wedge \sigma \right) \\
    \left(\exists \text{X}.\, \sigma \right) \lor \tau \longrightarrow \exists \text{X}.\left(\sigma \lor \tau \right) & \tau \vee \left(\exists \text{X}.\, \sigma \right) \longrightarrow \exists \text{X}.\left(\tau \lor \sigma \right)
\end{array}
$$
Todas las reglas transforman la fórmula en otra equivalente.

La fórmula resultante está en **forma normal prenexa**:
$$\sigma_{\text{pre}} ::= \mathcal{Q}_{1}\text{X}_{1}.\,\mathcal{Q}_{2}\text{X}_{2}.\,\ldots.\,\mathcal{Q}_{n}\text{X}_{n}.\,\tau$$
donde cada $\mathcal{Q}_{i}$ es un cuantificador $\{ \forall, \exists \}$ y $\tau$ representa una fórmula en **forma normal negada (NNF)** libre de cuantificadores.

**(!) Paso 4.** Deshacerse de los $\exists$, a este paso se lo conoce como **Skolemnización** *(técnica de Herbrand y Skolem)*:
$$\begin{array}{l l l}
    {\forall \text{X}\text{. } \exists \text{Y. } \text{P}(\text{X},\text{Y})} & {\text{es sat.}} & {sii} & {\forall \text{X}\text{. } \text{P}(\text{X},\text{f}(\text{X}))} & {\text{es sat.}} \\
    {\forall \text{X}_{1}\text{X}_{2}\text{. } \exists \text{Y. } \text{P}(\text{X}_{1},\text{X}_{2},\text{Y})} & {\text{es sat.}} & {sii} & {\forall \text{X}_{1}\text{X}_{2}\text{. } \text{P}(\text{X}_{1},\text{X}_{2},\text{f}(\text{X}_{1},\text{X}_{2}))} & {\text{es sat.}} \\
    {}&{}&{.}&{}&{} \\
    {}&{}&{.}&{}&{} \\
    {}&{}&{.}&{}&{} \\
    {\forall \overset{\rightarrow}{\text{X}}\text{. } \exists \text{Y. } \text{P}(\overset{\rightarrow}{\text{X}},\text{Y})} & {\text{es sat.}} & {sii} & {\forall \text{X}\text{. } \text{P}(\overset{\rightarrow}{\text{X}},\text{f}(\overset{\rightarrow}{\text{X}}))} & {\text{es sat.}}
\end{array}
$$
El lado izquierdo es una fórmula en el lenguaje $\mathcal{L}$.
El lado derecho es una fórmula el lenguaje $\mathcal{L} \cup \{ \text{f} \}$.

Caso particular cuando $|\overset{\rightarrow}{\text{X}}| = 0$
$$\begin{array}{l l l}
    {\exists\text{Y. } \text{P(Y)}} & {\text{es sat.}} & {sii} & {\text{P(c)}} & {\text{es sat.}} \\
\end{array}$$
El lenguaje se extiende con una nueva constante $\text{c}$.

**(!)** La Skolemización **preserva la satisfactibilidad**.
Pero **no** siempre **produce fórmulas equivalentes**.
Es decir, <span style="color:rgb(255, 0, 0); font-weight: bold;">no preserva la validez</span>.
![[Pasted image 20240725185200.png|center|450]]

*"Entonces, ¿cómo hacemos?"*
Dada una fórmula en **forma normal prenexa**, se aplica la regla:
$$\forall\text{X}_{1}.\ldots\forall\text{X}_{n}.\exists\text{Y}.\sigma\quad\rightarrow\quad\forall\text{X}_{1}.\ldots\forall\text{X}_{n}.\sigma\{\text{Y}:=\text{f}(\text{X}_{1},\ldots,\text{X}_{n})\}$$
donde $\text{f}$ es un símbolo de función nuevo de aridad $n\geq0$.

La fórmula resultante está en <span style="color:rgb(0, 0, 255); font-weight: bold;">forma normal de Skolem</span>:
$$\sigma_{\text{Sk}}::=\forall\text{X}_{1}\text{X}_{2}\ldots\text{X}_{n}.\tau$$
donde $\tau$ representa una formula en **forma normal negada (NNF)** libre de cuantificadores.

**(!) Paso 5.** Dada una fórmula en **forma normal de Skolem**:
$$
\begin{array} \\
\forall\text{X}_{1},\text{X}_{2},\dots,\text{X}_{n}. \tau & (\tau \text{ libre de cuantificadores})
\end{array}$$
se pasa $\tau$ a forma normal conjuntiva usando las reglas ya vistas:
$$\begin{array}{ccc}\sigma\vee(\tau\wedge\rho)&\rightarrow&(\sigma\vee\tau)\wedge(\sigma\vee\rho)\\ (\sigma\wedge\tau)\vee\rho&\rightarrow&(\sigma\vee\rho)\wedge(\tau\vee\rho)\end{array}$$

El resultado es una fórmula de la forma:
$$\forall\text{X}_{1}\ldots\text{X}_{n}.\left(\begin{array}{c}{{(\ell_{1}^{(1)}\lor\ldots\lor\ell_{m_{1}}^{(1)})}}\\ {{\land\ (\ell_{1}^{(2)}\lor\ldots\lor\ell_{m_{2}}^{(2)})}}\\ {{\cdots}}\\ {{\land\ (\ell_{1}^{(k)}\lor\ldots\lor\ell_{m_{k}}^{(k)})}}\end{array}\right)$$


**(!) Paso 6.** Empujar los cuantificadores universales hacia adentro:
$$\forall\text{X}_{1}\ldots\text{X}_{n}.\left(\begin{array}{c}(\ell_{1}^{(1)}\vee\ldots\vee\ell_{m_{1}}^{(1)})\\ \wedge\ (\ell_{1}^{(2)}\vee\ldots\vee\ell_{m_{2}}^{(2)})\\ \ldots\\ \wedge\ (\ell_{1}^{(k)}\vee\ldots\vee\ell_{m_{k}}^{(k)})\end{array}\right)\rightarrow\left(\begin{array}{c}\forall\text{X}_{1}\ldots\text{X}_{n}.(\ell_{1}^{(1)}\vee\ldots\vee\ell_{m_{1}}^{(1)})\\ \wedge\ \forall\text{X}_{1}\ldots\text{X}_{n}.(\ell_{1}^{(2)}\vee\ldots\vee\ell_{m_{2}}^{(2)})\\ \dots \\ \wedge\ \forall\text{X}_{1}\ldots\text{X}_{n}.(\ell_{1}^{(k)}\vee\ldots\vee\ell_{m_{k}}^{(k)})\end{array}\right)$$
Por último, la **forma clausal** es:
$$\left\{\begin{array}{l l}{\{l_{1}^{(1)},\ldots,l_{m_{1}}^{(1)}\},}\\ {\{l_{1}^{(2)},\ldots,l_{m_{2}}^{(2)}\},}\\ {\vdots}\\ {\{l_{1}^{(k)},\ldots,l_{m_{k}}^{(k)}\}}\end{array}\right\}$$

##### Resumen (de pasaje a forma clausal para LPO):
1. Reescribir $\Rightarrow$ usando $\neg$ y $\lor$.
2. Pasar a f.n. negada, empujando $\neg$ hacia adentro.
3. Pasar a f.n. prenexa, extrayendo $\forall, \exists$ hacia afuera.
4. Pasar a f.n. de Skolem, **Skolemizando los existenciales**.
5. Pasar a f.n. conjuntiva, distribuyendo $\land$ sobre $\lor$.
6. Empujar los cuantificadores hacia adentro de las conjunciones.
Cada paso produce una fórmula equivalente, excepto la Skolemización que solo preserva satisfactibilidad.
![[Pasted image 20240725191333.png|center|400]]
##### Refutación en lógica de primer orden
Bueno, habíamos dicho que la estrategia era la misma. Teníamos nuestra fórmula $\sigma$, la negamos y nos quedamos con $\neg \sigma$, después, la pasamos a un conjunto $\mathcal{C}$ de cláusulas *(forma clausal)*.
Veamos ahora cómo funciona el método de refutación *(general)*.

Uno en un primer momento podría pensar que podríamos la regla que vimos para lógica proposicional...
![[Pasted image 20240725192149.png|center|400]]
Sin embargo, esa idea no funciona pues:
![[Pasted image 20240725192311.png|center|400]]

Ahora sí, veamos la regla **correcta**.
![[Pasted image 20240725192352.png|center|400]]

Y referente al algoritmo de refutación, este se adapta sin mayores cambios. Se usa la nueva regla de resolución para calcular la resolvente.

![[Pasted image 20240725192514.png|center|400]]

Podríamos también considerar si se puede aplicar una regla de resolución que sea binaria. Sin embargo,
![[Pasted image 20240725192602.png|center|400]]

##### Corrección del método de resolución de primer orden (teoremas)

| ![[Pasted image 20240725192629.png\|center\|315]] | ![[Pasted image 20240725192708.png\|center\|315]] |
| ------------------------------------------------- | ------------------------------------------------- |

Y antes dijimos que este método podría no terminar. Acá hay un ejemplo donde no termina.
![[Pasted image 20240725192741.png|center|400]]

---

## Resolución SLD y Prolog (Capítulo 10)
En el capítulo anterior vimos el método de resolución general, sin embargo, este tiene varios problemas. Uno de ellos es la complejidad, más allá del cálculo, puede uno darse cuenta que se va bastante para arriba.
Requiere de un criterio de búsqueda *(elegir dos cláusulas)*
Y de un criterio de selección *(elegir un subconjunto de literales de cada cláusula)*
La cantidad de opciones es exponencial en el tamaño del problema.
Además,
- cada paso agrega una nueva cláusula.
- en cada paso se deben resolver ecuaciones de unificación.
- el método requiere usar BFS (para evitar irse por una rama que no termine / para que sea completo).

Y peor aún, no es determinístico, ¿cómo elegimos esas cláusulas, y ese subconjunto de literales?...


Es por ello que planteamos una alternativa a la resolución general, llamada resolución SLD.
Esta resolución tiene ventajas y desventajas, de eso se trata este capítulo, a su vez, es el método de resolución que usa Prolog con ligeras modificaciones (de base SLD no es determinístico).

### Resolución SLD (Capítulo 10: parte I)
La resolución SLD es un *tradeoff*: menor generalidad a cambio de mayor eficiencia.

**Menor generalidad**
No se puede aplicar sobre fórmulas de primer orden arbitrarias. Solo se puede aplicar sobre cláusulas de Horn.

**Mayor eficiencia**
Se reducen las opciones de búsqueda/selección.

###### Cláusulas de Horn
Recordemos que una **cláusula** es un **conjunto de literales**:
$$\{\ell_{1},\ldots,\ell_{n}\}$$

donde cada literal es una fórmula atómica posiblemente negada:
$$\ell::=\underbrace{\color{blue}\text{P}(\mathbf{t}_{1},\ldots,\mathbf{t}_{n})}_{\text{Literal \color{blue}positivo}}\mid\underbrace{\color{red}\neg\text{P}(\mathbf{t}_{1},\ldots,\mathbf{t}_{n})}_{\text{Literal \color{red}negativo}}$$

**Definición (Cláusulas de Horn 🐐)**
Las cláusulas son de los siguientes tipos, **dependiendo del número de literales positivos** que contengan:
$$
\begin{array}{c|c|c}
\ &\color{blue}\#\text{positivos}&\color{red}\#\text{negativos}\\
\hline\text{Cláusula objetivo}&\color{blue}\mathbf{0}& \color{red}*\\
\text{Cláusula de definición}&\color{blue}\mathbf{1}& \color{red}*\\
\text{Cláusula de Horn}&\color{blue}\leq1& \color{red}*\\
\end{array}
$$
**Ejemplo**
$$
\underbrace{
\underbrace{\{\text{\color{blue}P(x)}\}\quad\{\text{\color{blue}P(x)},\color{red}\neg\text{Q(Y,X)}\color{black},\color{red}\neg\text{R(Y)}\color{black}\}}_{\text{Cláusulas de definición}}\quad\underbrace{\{\}\quad\{\color{red}\neg\text{P(X)}\color{black},\color{red}\neg\text{Q(Y,X)}\color{black}\}}_{\text{Cláusulas objetivo}}}_{\text{Cláusulas de Horn}}\
$$

###### Regla de resolución SLD
La regla de **resolución SLD** involucra siempre a una cláusula de **definición** y una cláusula **objetivo**.
![[Pasted image 20240727013354.png|center|450]]
Es un caso particular de la regla de resolución general.
La **selección es binaria** *(elige un literal de cada cláusula)*.
La resolvente es una nueva cláusula **objetivo**.

**Dato:** hay fórmuas que no se pueden escribir como **cláusulas de Horn**, por ejemplo $P \lor Q$.

###### Derivaciones SLD
Una derivación SLD comienza con $n \geq 0$ cláusulas de **definición** y una cláusula **objetivo**.
$$
\begin{array} \\
\color{blue}D_{1} & \dots & \color{blue}D_{n} & \color{red}G_{1}
\end{array}
$$
En cada paso:
- Se elige una cláusula **definición** $\color{blue}D_{j}$ con $1 \leq j \leq n$.
- Se aplica la regla de resolución SLD sobre $\color{blue}D_{j}$ y $\color{red}G_{i}$.
- La resolvente es una nueva cláusula **objetivo** $\color{red}G_{i+1}$.

![[Pasted image 20240727014126.png|center|400]]

Conclusión, se nos **simplifica** la **búsqueda** y **selección**.

**Búsqueda:**
- Se limita a elegir $\color{blue}C_{i}$ como una de las $n$ cláusulas $\color{blue}D_{1}\color{black}, \dots , \color{blue}D_{n}$.
- La cláusula objetivo $\color{red}G_{i}$ está fijada, no hay alternativas.

**Selección:**
- Se limita a elegir uno de los literales <span style="color:rgb(255, 0, 0)">negativos</span> de $G_{i}$.
- La cláusula de definición $\color{blue}C_{i}$ tiene un único literal <span style="color:rgb(0, 0, 255)">positivo</span>.

![[Pasted image 20240727014507.png|center|500]]

**Completitud del método de resolución SLD**
El método de resolución **es completo para cláusulas de Horn**. Más precisamente, si $\color{blue}D_{1}\color{black}, \cdots, \color{blue}D_{n}$ son cláusulas de **definición** y $\color{red}G$ una cláusula **objetivo**:
Si $\{  \color{blue}D_{1}\color{black}, \dots, \color{blue}D_{n}\color{black}, \color{red}G \color{black}\}$ es insatisfactible, existe una refutación SLD. **(Teorema)**

### Prolog (Capítulo 10: parte II)
Un programa en Prolog es un conjunto de cláusulas de definición.
Una consulta en Prolog es una cláusula objetivo.
La notación cambia ligeramente.

![[Pasted image 20240727023104.png|center|450]]

La ejecución se basa en la regla de resolución SLD.
El orden delos literales de cláusula objetivo es relevante, pues, para hacerlo determinístico, Prolog siempre elige el **primer literal** de la cláusula *(criterio de selección)*.
"De izquierda a derecha".

Prolog busca sucesivamente todas las refutaciones haciendo DFS.
Toma las reglas en **orden de aparición** *(criterio de búsqueda)*.
"De arriba para abajo".

Ejemplo de árbol de refutación en Prolog con
```prolog
fruta(manzana).
dulce(manzana).
fruta(limón).
dulce(pera).
fruta(pera).
meGusta(helado).

meGusta(X) :- fruta(X), dulce(X).
```

![[Pasted image 20240727023558.png|center|400]]

La exploración DFS es <span style="color:rgb(255, 0, 0); font-weight: bold;">incompleta</span>.
Puede provocar que Prolog nunca encuentre refutaciones posibles.
```prolog
esMaravilloso(X) :- esMaravilloso(suc(X)).
esMaravilloso(cero).
```
Es por ello que el orden de las reglas se torna relevante. La exploración BFS es completa pero **muy** costosa.

A su vez, Prolog al unificar, no usa la regla *occurs-check*.
![[Pasted image 20240727024006.png|center|400]]
Esto puede provocar que  Prolog encuentre una "refutación" incorrecta.
Por ejemplo, $X$ unifica con $f(X)$, lo que es <span style="color:rgb(255, 0, 0); font-weight: bold;">incorrecto</span>.
```prolog
esElSucesor(X, suc(X)).

% Encuentra una refutación {Y := X, X := (X)}
esElSucesor(Y, Y).
```
En muchos contextos, la regla *occurs-check* es innecesaria.
Lo que sí, perdemos corrección.

###### Aspectos extralógicos de Prolog
Más allá del motor lógico que fue de lo que estuvimos hablando arriba, Prolog cuenta con un motor extralógico. Tenés el operador de corte **!**, **not**, etc...
Dejo unas imagenes de la teórica para no dejar esto vacío.

**Operador de corte (!) "cut"**

| ![[Pasted image 20240727024535.png\|center\|300]] | ![[Pasted image 20240727024546.png\|center\|300]] |
| :-----------------------------------------------: | :-----------------------------------------------: |
| ![[Pasted image 20240727024611.png\|center\|300]] | ![[Pasted image 20240727024655.png\|center\|300]] |
| ![[Pasted image 20240727024624.png\|center\|150]] |                                                   |

**Negación por falla (not)**

| ![[Pasted image 20240727024746.png\|center\|300]] | ![[Pasted image 20240727024811.png\|center\|300]] |
| ------------------------------------------------- | ------------------------------------------------- |

---

## Programación orientada a objetos (Capítulo 11)
Bueno, finalmente el último capítulo. Sobre este no hay teórica, así que voy a escribir todo lo que me acuerde. Seguramente lo complemente con partes prácticas.
En particular, usamos **Smalltalk** para trabajar con el paradigma de objetos. A su vez, usamos *Pharo* como entorno de ejecución.

¿Qué es un programa en la programación orientada a objetos?
- Es un software como modelo computable de la realidad para comprender y resolver problemas.
- Cómputo basado en **objetos** enviándose **mensajes**.
- Evitar soluciones procedurales **delegando responsabilidades** en objetos.

El conjunto de **mensajes** a los que un objeto sabe responder, define su comportamiento (qué hacen) y sus responsabilidades (qué deben saber hacer).

Es decir, en el paradigma de objetos, **todo es un objeto**.
Los **objetos se comunican** entre si **enviándose mensajes**.
**Si** un objeto **no sabe responder** al mensaje, **le pregunta al de más arriba** (estructura de clases).

Veamos más a fondo eso de los mensajes.

```smalltalk
2 pesos.
December first, 1985.
'hola mundo' indexOf: $o startingAt: 6.
```

El primer mensaje es **unario**, se le está enviando el mensaje `pesos` al objeto `2`.

El segundo mensaje es **binario**. ¿Cómo funciona te preguntarás?
Bueno, primero tenemos que entender cómo se maneja el orden de la resolución de mensajes.
Primero se resuelven los mensajes **unarios**, luego los **binarios**, y finalmente los **keywords**, aún no vimos qué son los keywords, pero tenía que mencionarlo.
Cuestión, en `December first, 1985.` tenemos dos mensajes, el primero, que es unario, es `December first`, el segundo es `,`, este mensaje se le envía al objeto `December first` con `1985` como colaborador/argumento (que también es un objeto!!).

El tercer mensaje es para desglosar. Acá apareció el concepto de **keywords**.
Vayamos primero a identificar el **objeto receptor** del mensaje
el objeto `'hola mundo'` del mensaje `indexOf: $o startingAt: 6`, este mensaje cuenta con dos **keywords**: `indexOf:` y `startingAt:`, y los colaboradores/argumentos son `$o` y `6`.

También, los mensajes se leen de izquierda a derecha, y de arriba para abajo; priorizando paréntesis. La precedencia de paréntesis es distinta a la que solemos usar, así que tratar las cosas con cuidado.
Por ejemplo, `20 + 3 * 5 = (20 + 3) * 5 != 20 + (3 * 5)` en *Smalltalk*. Para nosotros debería ser `20 + 3 * 5 = 20 + (3 * 5)` en lugar de lo de arriba.

¿Qué saben hacer los objetos?
Colaborar entre sí mediante el envío de mensajes. A esta secuencia de colaboraciones la llamamos método, y definen el CÓMO (antes definimos el qué).
- Los métodos son objetos
- La ejecución de un método también es un objeto.
- Ambos se pueden inspeccionar dentro del entorno de programación.

**Comunicación entre objetos en Smalltalk**
- Es **dirigida**: hay un emisor y un recepto.
- Es **sincrónica**: se espera a la respuesta del mensaje.
- **Siempre** hay respuesta: si no se explicita, se retorna `self`.
- **El receptor no conoce al emisor**: siempre resonde igual sin importar el emisor (salvo que el emisor se envíe como colaborador del mensaje)

¿Cómo es la **sintaxis** de Smalltalk?
```smalltalk
"Comentarios".
| var1 var2 ...|. "inicializar variables"
[:arg1 :arg2 | | var1 var2 | expresión]. "closures, como 'lambdas'"
expresión1. expresión2. expresión3. "'ejecutar' expresiones"
objeto mensaje. "mandar mensaje a objeto"
objeto msj1; msj2. "mandar msj1 y msj2 al mismo objeto (objeto)"
var := expresión. "declarar una variable"
^ expresión. "devolver (return) una expresión"
```

Literales
```smalltalk
123.
123.4.
$c.
'texto'.
#símbolo.
#(123 123.3 $a ábc#abc).
```

Palabras reservadas
```smalltalk
self.
super.
nil.
true.
false.
thisContext.
```

**Closures**
Permiten **representar** un **conjunto de colaboraciones**. En definitiva, es **segmento de código** al cual no me importa ponerle un nombre. Su sintaxis la vimos más arriba, veamos un ejemplo.
```smalltalk
| result |
result := [ :x :y |
            | v |
            v := x.
            v * x + y
          ] value: 3 value: 5.
```
Toma dos argumentos, `x` e `y` e inicializa una variable local `v`, luego la declara con el valor de `x` y ejecuta la siguiente expresión `v * x + y`; al no haber `^`, retorna la expresión de la última línea, en este caso el objeto que resulta de la expresión `v * x + y`.

Los closures se ligan al contexto de ejecución donde son creados, tanto las variables como el return.

**Colecciones**
Hay un montón, algunas conocidas son:
- Bag (multiconjunto)
- Set (conjunto)
- Array (arreglo)
- OrderedCollection (Lista)
- SortedCollection (Lista ordenada)
- Dictionary (hash)

Los mensajes `#with: with: ...` son una forma de crear colecciones. Por ejemplo
```smalltalk
"Se le puede agregar elementos."
Bag with: 1 with: 2 with: 4.
Bag withAll: #(1 2 4).

"No se le puede agregar elementos."
(Array with: 1 with: 2 with: 4).
#(1 2 4).
```
Dependiendo de la colección va a reponderte una cosa u otra a los mensajes que le mandes. Por ejemplo `add:` no es lo mismo para lo de arriba que para lo de abajo. Lo de abajo es un arreglo, al igual que `#(1 2 4)`.

Mensajes comunes a colecciones
- `add:` agrega un elemento.
- `at:` devuelve el elemento en una posición.
- `at:put:` agrega un elemento a una posición.
- `includes:` responde si un elemento pertenece o no.
- `includesKey:` responde si una clave pertence o no.
- `do:` evalúa un bloque con cada elemento de la colección.
- `keysAndValuesDo:` evalúa un bloque con cada par clave-valor.
- `keysDo:` evalúa un bloque con cada clave.
- `select:` devuelve los elementos de una colección que cumplen un predicado (filter de funcional).
- `reject:` la negación del select:
- `collect:` devuelve una colección que es resultado de aplicarle un bloque a cada elemento de la colección original (map de funcional).
- `detect:` devuelve el primer elemento que cumple un predicado.
- `detect:ifNone:` como `detect:`, pero permite ejecutar un bloque si no se encuentra ningún elemento.
- `reduce:` toma un bloque de dos o más parámetros de entrada y hace fold de los elementos de izquierda a derecha (foldl de funcional).

Bueno, vayamos a resolver ejercicios.
![[Pasted image 20240728013250.png|center|500]]
Creo que se entiende con los colores.

![[Pasted image 20240728014252.png|center|500]]
```smalltalk
[:x | x + 1] value: 2 "3"
[|x| x := 10. x + 12] value "22"
[:x :y | |z| z := x + y] value: 1 value: 2 "3"
[:x :y | x + 1] value: 1 "error - falta un colaborador"
[:x | [:y | x + 1]] value: 2 "un closure que toma un argumento y devuelve constantemente 3"
[[:x | x + 1]] value "un closure [:x | x + 1]"
[:x :y :z | x + y + z] valueWithArguments: #(1 2 3) "6"
[ |z| z := 10. [:x | x + z]] value value: 10 "20 pues el primer value te da [:x | x + 10] (en realidad no reemplaza z por 10 sino que queda en el contexto)"
```

![[Pasted image 20240728015306.png|center|500]]
**Resolución de este ejercicio sacada de [honi](https://github.com/honi/uba-plp/tree/main/pr%C3%A1cticas/p09).**
```smalltalk
#(1 2 3 4) collect: [:x | x * 2]. "devuelve #(2 4 6 8). - Es un `map` de funcional."

#(1 2 3 4) select: [:x | (x mod: 2) = 0 ]. "devuelve #(2 4). - Es un `filter` de funcional."

#(1 2 3 4) inject: 0 into: [:x :y | x + y ]. "devuelve 10. - Es un `foldl` de funcional. El argumento `inject:` es el valor inicial, `into:` es el bloque que se ejecuta por cada par de elementos."

#(1 2 3 4) reduce: [:x :y | x + y ]. "devuelve 10. - Es un `foldl1` de funcional. Falla si la colección está vacía."

#(1 2 3 4) reduceRight: [:x :y | x + y ]. "devuelve 10. - Es un `foldr1` de funcional. Falla si la colección está vacía."

| L1 L2 |
L1 := OrderedCollection withAll: #(1 2 3 4).
L2 := OrderedCollection new.
L1 do: [:x | L2 add: x + 1].
L2.
"devuelve an OrderedCollection(2 3 4 5). - Agrega todos los elementos de `L1` en `L2` sumándoles `1`."
```

![[Pasted image 20240728015857.png|center|500]]
**Créditos nuevamente a [honi](https://github.com/honi/uba-plp/tree/main/pr%C3%A1cticas/p09).**
*a)* Dentro del método vale `x = 4`. Cuando evaluamos el valor de `aBlock`, `x > 5` es falso entonces se evalúa el valor del argumento `ifFalse:`, puntualmente el bloque `[z := z - x. 5]`. Como `z = 10`, este bloque hace `z := 10 - 4` y luego retorna el número `5`, que se asigna a la variable `y`. La variable `z` ahora vale `6`. Finalmente se ejecuta `y := 5 + 6`, y luego se retorna el valor de `y` que es `11`.

*b)* Crea un objeto de la clase `Message` que codifica el mensaje para el selector `foo` con argumento `5`.
Podemos enviar este mensaje de la siguiente forma:
```smalltalk
(Message selector: #foo: argument: 5) sendTo: obj.
```

*c)* Dentro del método value `x = 10`. Cuando evaluamos el valor de `aBlock`, `x > 5` es verdadero entonces se evalúa el valor del argumento `ifTrue:`, puntualmente el bloque `[z := z + x. ^0]`. Este bloque es interesante porque más allá de la cuentita que hace con `z`, después retorna: `^0`. El símbolo `^` es un return explícito del método, **no** del bloque. Entonces el resultado que obtenemos es `0` ya que no se ejecuta el código que viene después del bloque.

![[Pasted image 20240728023904.png|center|500]]
![[Pasted image 20240728023916.png|center|475]]
Método curry:
```smalltalk
curry
	"Currifica."
	^ [:x | [:y | self value: x value: y]]
```
Método flip:
```smalltalk
flip
	"Flipea."
	^ [:x :y | self value: y value: x]
```
Método timesRepeat:
```smalltalk
timesRepeat: aBlock
	"Ejecuta un bloque self veces."
	1 to: self do: [ aBlock ]
```

![[Pasted image 20240728025115.png|center|500]]
```smalltalk
generarBloqueInfinito: aBlock
	| bloque | "inicializa la variable bloque"
	bloque := [:i | { i . [:j | bloque value: j + 1]} ]. "define bloque tal que, toma un i y te devuelve un arreglo {i (bloque i+1)} por así decirlo"
	^bloque value: 1. "caso base, i vale 1"
```

![[Pasted image 20240728030315.png|center|500]]
*I.* Queda ligada al objeto **anObject**, es decir, a la instancia anObject de la clase *OneClass*.
*II.* Al objeto **anObject**, la diferencia es que en lugar de ir a buscar el método de instancia que implementa el mensaje **aMessage** en OneClass, va a ir buscar en los métodos de instancia de la clase que está arriba de OneClass.
*III.* Decir `super == self` es medio un montón. La idea es que es "análogo" usar uno u otro sii. el método de instancia que implementa el mensaje (en este caso *aMessage*) no está implementado en la clase que le corresponde a esa instancia (en este caso *OneClass*).
Ergo, lo va a ir a buscar siempre al menos una clase más arriba, por lo que skipearte buscarlo de entrada con `super` o mandarte a buscarlo con `self` para terminar subiendo una clase y hacer lo mismo que `super`, da lo mismo.

Resuelvo el ejercicio de objetos del segundo parcial, del primer cuatrimestre de 2024.

| ![[Pasted image 20240728211424.png\|center\|320]] | ![[Pasted image 20240728211435.png\|center\|320]] |
| :-----------------------------------------------: | :-----------------------------------------------: |

*a)*

| Objeto                      | Mensaje    | Colaboradores                      | Ubicación      | Respuesta                   | ¿Qué estoy resolviendo? $\times_{*}$                                                                                                                |
| --------------------------- | ---------- | ---------------------------------- | -------------- | --------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| `A`                         | `new`      |                                    | `Behavior`     | `anA`                       | `A new`                                                                                                                                             |
| `B`                         | `new`      |                                    | `Behavior`     | `aB`                        | `B new`                                                                                                                                             |
| `C`                         | `new`      |                                    | `Behavior`     | `aC`                        | `C new`                                                                                                                                             |
| `anA`                       | `a:`, `b:` | `aB`, `aC`                         | `A`            | `3` por $\times_{2}$        | `anA a: aB b: aC`                                                                                                                                   |
| `aC`                        | `c`        |                                    | `C`            | `[self a: super c b: self]` | La parte de `(y c)` en `x a: (y c) b: self`                                                                                                         |
| `aB`                        | `a:`, `b:` | `[self a: super c b: self]`, `anA` | `B`            | `3` por $\times_{2}$        | `x a: (y c) b: self` donde`(y c)` es `[self a: super c b: self]` y `self` es `anA`                                                                  |
| `anA`                       | `c`        |                                    | `A`            | `2`                         | `y c` en `y c + x value` donde `y` es `anA` y `x` es `[self a: super c b: self]`                                                                    |
| `[self a: super c b: self]` | `value`    |                                    | `C`            | `1` por $\times_{1}$        | `[self a: super c b: self] value` donde `self` y `super` están ligadas al objeto `aC` y `super` va a buscar el mensaje que le mandes a la clase `B` |
| `aC`                        | `c`        |                                    | `B`            | `1`                         | `super c` en`[self a: super c b: self]`                                                                                                             |
| `aC`                        | `a:`, `b:` | `1`, `aC`                          | `C`            | `1` $\times_{1}$            | `[self a: super c b: self]` que "se puede leer" como `[aC a: 1 b: aC]` ponele                                                                       |
| `2`                         | `+`        | `1`                                | `SmallInteger` | `3` $\times_{2}$            | `y c + x value`                                                                                                                                     |
$\times_{*}$ no es necesario, pero lo pongo por claridad.

*b)*
```smalltalk
Integer << divisores

| res |
res := OrderedCollection new.
1 to: self do: [ :d |
	self \\ d = 0 ifTrue: [res add: d]
].

^res
```

---
