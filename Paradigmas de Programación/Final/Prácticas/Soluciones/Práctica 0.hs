import Data.Foldable (Foldable(fold))
-- Ejericio 1:
-- null :: [a] -> Bool
-- Devuelve True <=> la lista está vacía

-- head :: [a] -> a
-- Devuelve el primer elemento de la lista

-- tail :: [a] -> [a]
-- Devuelve una lista de longitud: len(lista_pasada_como_parámetro) - 1, y con los mismos elementos que lista_pasada_como_parámetro salvo el primer elemento

-- init :: [a] -> [a]
-- Devuelve una lista de longitud: len(lista_pasada_como_parámetro) - 1, y con los mismos elementos que la lista_pasada_como_parámetro salvo el último elemento

-- last :: [a] -> a
-- Devuelve el último elemento de la lista

-- take :: Int -> [a] -> [a]
-- Devuelve una lista (sublista) con los primeros número_pasado_como_parámetro elementos (manteniendo el órden original de lista_pasada_como_parámetro)

-- drop :: Int -> [a] -> [a]
-- TL;DR: devuelve el complemento de take

-- (++) :: [a] -> [a] -> [a]
-- Devuelve la concatenación de dos listas

-- concat :: [[a]] -> [a]
-- Devuelve la concatenación de todas las listas pasadas en la lista como parámetro

-- (!!) :: [a] -> Int -> a
-- Devuelve el elemento en la iésima (el int que pasamos) posición

-- elem :: a -> [a] -> Bool
-- Devuelve True <=> el elemento (pasado como parámetro) está en la lista (pasada como parámetro)

--Ejericio 2:
valorAbsoluto :: Float -> Float
valorAbsoluto x | x >= 0 = x
                | otherwise = -x

esDivisible :: Int -> Int -> Bool
esDivisible x y = mod x y == 0

bisiesto :: Int -> Bool
bisiesto x | esDivisible x 4 && not (esDivisible x 100) = True
           | esDivisible x 4 && esDivisible x 100 && esDivisible x 400 = True
           | otherwise = False

factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial n-1

menorDivisor :: Int -> Int
menorDivisor n = menorDivisorDesde n 2

-- No se puede usar la notación lambda junto a guardas. Es necesario utilizar if-then-else.
menorDivisorDesde :: Int -> Int -> Int
menorDivisorDesde n m = if esDivisible n m then m else menorDivisorDesde n (m+1)

esPrimo :: Int -> Bool
esPrimo n = n == menorDivisor n

cantDivisoresPrimosHasta :: Int -> Int -> Int
cantDivisoresPrimosHasta n m | m == 1 = 0
                             | esDivisible n m && esPrimo m = 1 + cantDivisoresPrimosHasta n (m-1)
                             | otherwise = cantDivisoresPrimosHasta n (m-1)

cantDivisoresPrimos :: Int -> Int
cantDivisoresPrimos n = cantDivisoresPrimosHasta n n

-- Ej 3
--data Maybe a = Nothing | Just a
--data Either a b = Left a | Right b
-- (!) Maybe y Either se encuentran en el preludio.

-- Definir la función inverso :: Float → Maybe Float que dado un número devuelve su inverso multiplicativo si está definido, o Nothing en caso contrario.
-- Nota: el inverso multiplicativo de x es 1/x. El inverso multiplicativo de 0 no está definido pues 1/0 es indefinido.
inverso :: Float -> Maybe Float
inverso 0 = Nothing
inverso x = Just (1/x)

-- Definir la función aEntero :: Either Int Bool → Int que convierte a entero una expresión que puede ser booleana o entera. En el caso de los booleanos, el entero que corresponde es 0 para False y 1 para True.
aEntero :: Either Int Bool -> Int
aEntero (Left int) = int
aEntero (Right bool) = if bool then 1 else 0

-- Ej 4
limpiar :: String -> String -> String
limpiar chars = foldr (\x rec -> if x `elem` chars then rec else x:rec) []

promedio :: [Float] -> Float
promedio xs = foldr (\x rec -> x / fromIntegral (length xs) + rec) 0 xs
-- Con foldl: promedio xs = foldl (\acc x -> x / fromIntegral (length xs) + acc) 0 xs

difPromedio :: [Float] -> [Float]
difPromedio xs = map (\x -> x - promedio xs) xs

todosIguales :: [Int] -> Bool
todosIguales [] = True
todosIguales [x] = True
todosIguales (x1:x2:xs) = x1 == x2 && todosIguales xs

-- Ej 5
data AB a = Nil | Bin (AB a) a (AB a)

foldAB :: (b -> a -> b -> b) -> b -> AB a -> b
foldAB _ cNil Nil = cNil
foldAB cBin cNil (Bin i r d) = cBin (foldAB cBin cNil i) r (foldAB cBin cNil d)

vacioAB :: AB a -> Bool
vacioAB Nil = True
vacioAB _ = False

negacionAB :: AB Bool -> AB Bool
negacionAB = foldAB (\recIzq r recDer -> Bin recIzq (not r) recDer) Nil

-- Medio como que productoAB Nil evalua a 1, y podrías argumentar que querés que te de 0; eso se arreglar agregando ese casito con pattern matching 
productoAB :: AB Int -> Int
productoAB = foldAB (\recIzq r recDer -> recIzq * r * recDer) 1
