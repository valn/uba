-- Ej 1:

-- max2 (x, y) :: (Float, Float) -> Float
-- normaVectorial (x, y) :: (Float, Float) -> Float
-- subtract :: Float -> Float -> Float
-- predecesor :: Float -> Float
-- evaluarEnCero :: (Float -> a) -> a
-- dosVeces debe tomar una función (a -> a) pues puede componer su respuesta i.e. hacer f(f(algo de tipo a))
-- entonces:
-- dosVeces :: (a -> a) -> a -> a
-- requiere una función de a en a, un valor a para la primer evaluación y devuelve algo de tipo a pues estoy aplicando f
-- flipAll:
-- Sé q map toma una función (a -> b) y se la aplica a cada elemento de una lista
-- el tipo de map es map (a -> b) -> [a] -> [b]
-- y flip es una función que toma una función (a' -> b' -> c), algo de tipo b, algo de tipo a y devuelve algo de tipo c
-- flip :: (a -> b -> c) -> b -> a -> c
-- La forma de resolver estos dos es con remplásos sintácticos. Si quieren que los explique me hacen un ping en los grupos
-- flipAll :: [a -> b -> c] -> [b -> a -> c]
-- flipRaro :: b -> (a -> b -> c) -> a -> c

-- Las funciones NO currificadas son (junto a su currificación y tipo)
-- max2 :: Float -> Float -> Float
-- max2 x y = max2 (x, y)
-- normaVectorial Float -> Float -> Float
-- normaVectorial x y = normaVectorial (x, y)

-- Ej 2:
curry :: ((a, b) -> c) -> a -> b -> c
curry f x y = f (x, y)

uncurry :: (a -> b -> c) -> (a, b) -> c
uncurry f (x, y) = f x y

-- item 3: TBD

-- Ej 3:

sumFoldr :: (Num a) => [a] -> a
sumFoldr = foldr (+) 0

elemFoldr :: (Eq a) => a -> [a] -> Bool
elemFoldr elem = foldr (\x res -> x == elem || res) False

ppFoldr :: [a] -> [a] -> [a]
ppFoldr = flip (foldr (:))

filterFoldr :: (a -> Bool) -> [a] -> [a]
filterFoldr predicado = foldr (\x res -> if predicado x then x : res else res) []

mapFoldr :: (a -> b) -> [a] -> [b]
mapFoldr f = foldr (\x res -> f x : res) []

mejorSegún :: (a -> a -> Bool) -> [a] -> a
mejorSegún comp = foldr1 (\x res -> if comp x res then x else res)

sumasParciales :: (Num a) => [a] -> [a]
sumasParciales = reverse . foldl (\ac x -> (x + (if length ac >= 1 then head ac else 0)) : ac) []

sumaAlt :: (Num a) => [a] -> a
sumaAlt = foldr (-) 0

sumaAlt' :: (Num a) => [a] -> a
sumaAlt' = (*) (-1) . sumaAlt

-- Ejercicio 5

-- Ninguna de las dos utiliza recursión estructural, pues ambas refieren a la cola de la lista.

-- Ejercicio 6

recr :: (a -> [a] -> b -> b) -> b -> [a] -> b
recr _ z [] = z
recr f z (x : xs) = f x xs (recr f z xs)

sacarUna :: (Eq a) => a -> [a] -> [a]
sacarUna elem = recr (\x xs res -> if x == elem then xs else x : res) []

-- Al hacer los replasos sintácticos, Haskell va del comienzo ([1,2,3], el comienzo sería el 1) al final
-- Una vez encuentra el elemento, debe concatenar la respuesta parcial con la lista sin su primer elemento, i.e., xs
-- en foldr no puedo acceder al xs

insertarOrdenado :: (Ord a) => a -> [a] -> [a]
insertarOrdenado elem = recr (\x xs res -> if elem <= x then elem : x : xs else x : res) [elem]

-- Ejercicio 8

mapPares :: (a -> b -> c) -> [(a, b)] -> [c]
mapPares f = map (\x -> f (fst x) (snd x))

-- Me aprovecho de la evaluación parcial y currificación
{-

  armarPares = foldr f z (x:xs) = f x res ys = if null ys then [] else (x, head ys) : foldr f z xs tail ys
  el caso base es const [] pues cuando termine la recursión me va a quedar: foldr f (const []) [] ys = const [] ys
  si pusiera únicamente la lista vacía no tiparía pues [] ys no tiene sentido

  caso en que la recursión termine antes con xs, llego a (x, head ys), por lo que no vuelvo a llamar a res

  * f toma tres argumentos, la cabeza de xs (x), el resultado parcial (res = (foldr f z xs)), y ys
-}
armarPares :: [a] -> [b] -> [(a, b)]
armarPares = foldr (\x res ys -> if null ys then [] else (x, head ys) : res (tail ys)) (const [])

-- armarPares' :: [a] -> [b] -> [(a,b)]
-- armarPares' [] _ = []
-- armarPares' _ [] = []
-- armarPares' (x:xs) (y:ys) = [(x,y)] ++ armarPares' xs ys

mapDoble :: (a -> b -> c) -> [a] -> [b] -> [c]
mapDoble f = foldr (\x rec ys -> if null ys then [] else f x (head ys) : rec (tail ys)) (const [])

-- Ejercicio 10

generateFrom :: ([a] -> Bool) -> ([a] -> a) -> [a] -> [a]
generateFrom stop next xs
  | stop xs = init xs
  | otherwise = generateFrom stop next (xs ++ [next xs])

generate :: ([a] -> Bool) -> ([a] -> a) -> [a]
generate stop next = generateFrom stop next []

generateBase :: ([a] -> Bool) -> a -> (a -> a) -> [a]
generateBase pred z f = generate pred (\xs -> if null xs then z else f (last xs))

factoriales :: Int -> [Int]
factoriales n = generate (\xs -> length xs > n) (\xs -> if null xs then 1 else last xs * (length xs + 1))

iterateN :: Int -> (a -> a) -> a -> [a]
iterateN n f x = generateBase (\xs -> length xs > n) x f

generateFrom' :: ([a] -> Bool) -> ([a] -> a) -> [a] -> [a]
generateFrom' stop next xs = last (takeWhile (not . stop) (iterate (\ys -> ys ++ [next ys]) xs))

-- Ejercicio 11

foldNat :: (Integer -> b -> b) -> b -> Integer -> b
foldNat _ z 0 = z
foldNat f z n = f n (foldNat f z (n - 1))

potencia :: Integer -> Integer -> Integer
potencia base = foldNat (\n rec -> base * rec) 1

-- Ejercicio 13

data AB a = Nil | Bin (AB a) a (AB a)

foldAB :: (b -> a -> b -> b) -> b -> AB a -> b
foldAB _ z Nil = z
foldAB fBin z (Bin izq nodo der) = fBin (foldAB fBin z izq) nodo (foldAB fBin z der)

recAB :: (AB a -> a -> AB a -> b -> b -> b) -> b -> AB a -> b
recAB _ z Nil = z
recAB fBin z (Bin izq nodo der) = fBin izq nodo der (recAB fBin z izq) (recAB fBin z der)

esNil :: AB a -> Bool
esNil arbol = case arbol of
  Nil -> True
  Bin _ _ _ -> False

altura :: AB a -> Int
altura = foldAB (\recIzq nodo recDer -> 1 + max recIzq recDer) 0

cantNodos :: AB a -> Int
cantNodos = foldAB (\recIzq nodo recDer -> 1 + recIzq + recDer) 0

-- Función auxiliar que (creo) recomienda el ejercicio
recAB1 :: (AB a -> a -> AB a -> a -> a -> a) -> AB a -> a
recAB1 _ (Bin Nil nodo Nil) = nodo
recAB1 fBin (Bin izq nodo der) = fBin izq nodo der (recAB1 fBin izq) (recAB1 fBin der)

-- Función que pedía el ejercicio
mejorSegúnAB :: (a -> a -> Bool) -> AB a -> a
mejorSegúnAB comp = recAB1 (\izq nodo der recIzq recDer ->
        case (esNil izq, esNil der) of
          (True, True) -> nodo
          (True, False) -> if comp nodo recDer then nodo else recDer
          (False, True) -> if comp nodo recIzq then nodo else recIzq
          (False, False) -> if comp nodo recIzq && comp nodo recDer then nodo else (if comp recIzq recDer then recIzq else recDer)
    )

raiz :: AB a -> a
raiz (Bin _ nodo _) = nodo

máx :: Ord a => AB a -> a
máx = mejorSegúnAB (>)

mín :: Ord a => AB a -> a
mín = mejorSegúnAB (<)

esABB :: Ord a => AB a -> Bool
esABB = recAB (\izq nodo der recIzq recDer ->
        case (esNil izq, esNil der) of
          (True, True) -> True
          (True, False) -> if caminoVálidoDer nodo der then recDer else False -- Ídem: caminoVálidoDer nodo der && recDer
          (False, True) -> if caminoVálidoIzq nodo izq then recIzq else False
          (False, False) -> if caminoVálidoIzq nodo izq && caminoVálidoDer nodo der then (recIzq && recDer) else False
    )
    True
  where
    caminoVálidoIzq nodo árbol = nodo >= raiz árbol && nodo >= máx árbol
    caminoVálidoDer nodo árbol = nodo < raiz árbol && nodo < mín árbol
