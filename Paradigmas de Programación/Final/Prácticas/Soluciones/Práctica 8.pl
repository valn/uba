padre(juan, carlos).
padre(juan, luis).
padre(carlos, daniel).
padre(carlos, diego).
padre(luis, pablo).
padre(luis, manuel).
padre(luis, ramiro).
abuelo(X,Y) :- padre(X,Z), padre(Z,Y).

% Ejercicio 1.

% Item a
% {¬abuelo(X,manuel)} con {abuelo(X,Y), ¬padre(X,Z), ¬padre(Z,Y)} (Y := manuel) da {¬padre(X,Z), ¬padre(Z,manuel)}, luego
% Prolog unifica {¬padre(X,Z), ¬padre(Z,manuel)} con {padre(juan, carlos)} (X := juan; Z:= carlos), luego queda
% {¬padre(carlos,manuel)}, esto falla porque no es un hecho en la base de conocimiento. Hace backtracking hasta la última decisión que tomó, luego
% unifica {¬padre(X,Z), ¬padre(Z,manuel)} con {padre(juan, luis)} (X := juan; Z := luis), luego queda padre(luis,manuel)}
% (Duda: está bien dicho esto?/  cuál de las dos es?) Esto ya es un hecho por lo que Prolog se fija en su base de conocimiento / Prolog sigue tratando de unificar hasta llegar al hecho correspondiente en su base de conocimiento padre(luis,manuel), por lo que devuelve true, componiendo los reemplazos de variables para devolver X := juan como respuesta.
% si pido más respuesta, prolog va a revisar el resto del árbol y devolverá false (pues no hay más rtas válidas) 

% Item b
% A partir del predicado binario padre, definir en Prolog los predicados binarios: hijo, hermano y descendiente.
hijo(X,Y) :- padre(Y,X).
hermano(X,Y) :- padre(Z,X), padre(Z,Y).
descendiente(X,Y) :- padre(Y,X).
descendiente(X,Y) :- padre(X,Z), padre(Z,Y).

% Item d
% abuelo(juan,Nieto).

% Item e
% hermano(pablo,Hermano)., luego usás ;.

%ancestro(X, X).
%ancestro(X, Y) :- ancestro(Z, Y), padre(X, Z)

% Devuelve X = juan. Pues unifica {¬ancestro(juan, X)} con {ancestro(X, X)}
% y queda {X := juan}.

ancestro(X, Y) :- padre(Y,X).
ancestro(X, Y) :- abuelo(Y,X).

natural(0).
natural(suc(X)) :- natural(X).
menorOIgual(X,X) :- natural(X).
menorOIgual(X, suc(Y)) :- menorOIgual(X, Y).

juntar([],L2,L2).
juntar([Elem|L1],L2,[Elem|L3]) :- juntar(L1,L2,L3).

% last(?L, ?U).
ultimo(L,U) :- append(_,[U],L).

reverse([], []).
reverse([Elem|L], L1) :- reverse(L, L2), append(L2, [Elem], L1).

prefijo(P, L) :- append(P, _, L).

sufijo(S, L) :- append(_, S, L).

sublista([], _).
sublista(S, L) :- prefijo(P, L), sufijo(S, P), S \= [].

pertenece(X, L) :- sublista([X], L), !.

aplanar([], []).
aplanar([Ls1|Ls2], L) :- is_list(Ls1), aplanar(Ls1, L1), aplanar(Ls2, L2), append(L1, L2, L).
aplanar([X|Ls], [X|L]) :- not(is_list(X)), aplanar(Ls, L).

%palindromo(+L, ?L1)
palindromo(L, L1) :- reverse(L, R), append(L,R,L1).

%iesimo(?I, +L, -X)
iesimo(0, [X|_], X).
iesimo(I, [_|YS], X) :- iesimo(I1, YS, X), I is I1 + 1.

%interseccion(+L1, +L2, -L3)
interseccion(L1, L2, L3) :- interseccionConDuplicados(L1, L2, L4), sacarDuplicados(L4, L3), !.
interseccionConDuplicados([], _, []).
interseccionConDuplicados([X|L1], L2, [X|L3]) :- member(X, L2), interseccionConDuplicados(L1, L2, L3).
interseccionConDuplicados([X|L1], L2, L3) :- not(member(X, L2)), interseccionConDuplicados(L1, L2, L3).

%partir(N, L, L1, L2)
partir(N, L, L1, L2) :- length(L1, N), append(L1, L2, L).
% Todos son reversibles. length(?L, ?N), Y append(?L1,?L2,L).

%borrar(+ListaOriginal, +X, -ListaSinXs)
borrar([], _, []).
borrar([X|L], X, Res) :- borrar(L, X, Res).
borrar([Z|L], X, [Z|Res]) :- Z \= X, borrar(L, X, Res).

%sacarDuplicados(+L1, -L2)
sacarDuplicados([], []).
sacarDuplicados([X|L1], L2) :- member(X,L1), sacarDuplicados(L1, L2).
sacarDuplicados([X|L1], [X|L2]) :- not(member(X, L1)), sacarDuplicados(L1, L2).

%permutacion(+L1, ?L2)
insertar(X, L1, L2) :- append(I, D, L1), append(I, [X|D], L2).

permutacion([], []).
permutacion([X|L1], L2) :- permutacion(L1, PermuSinX), insertar(X, PermuSinX, L2).

%reparto(+L, +N, -LListas)
reparto(L, 1, [L]).
reparto(L, N, [L1|LListas]) :- N >= 1, append(L1, L2, L), N1 is N - 1, reparto(L2, N1, LListas).

%repartoSinVacias(+L, -LListas)
repartoSinVacias(L, [L]).
repartoSinVacias(L, [L1|LListas]) :-
	append(L1, L2, L),
	length(L1, Length1), Length1 > 0,
	length(L2, Length2), Length2 > 0,
	repartoSinVacias(L2, LListas).

elementosTomadosEnOrden(_, 0, []).
elementosTomadosEnOrden([X|L], N, [X|Elems]) :- N >= 1, length([X|Elems], N), N1 is N - 1, elementosTomadosEnOrden(L, N1, Elems).
elementosTomadosEnOrden([_|L], N, Elems) :- N >= 1, length(Elems, N), elementosTomadosEnOrden(L, N, Elems).

desde(X,X).
desde(X,Y) :- N is X+1, desde(N,Y).

desde2(X,X).
desde2(X, Y) :- var(Y), N is X + 1, desde2(N, Y).
desde2(X, Y) :- nonvar(Y), Y > X.

intercalar([], L2, L2).
intercalar(L1, [], L1).
intercalar([X|L1], [Y|L2], [X,Y|L3]) :- intercalar(L1, L2, L3).


vacio(nil).

raiz(bin(_, Raiz, _), Raiz).

altura(nil, 0).
altura(bin(Izq, _, Der), Altura) :- altura(Izq, AlturaIzq), altura(Der, AlturaDer), Altura is max(AlturaIzq, AlturaDer) + 1.

cantNodos(nil, 0).
cantNodos(bin(Izq, _, Der), CantidadDeNodos) :- cantNodos(Izq, NodosIzq), cantNodos(Der, NodosDer), CantidadDeNodos is NodosIzq + NodosDer + 1.

%inorder(+AB, -L)
inorder(nil, []).
inorder(bin(Izq, Raiz, Der), InOrder) :- inorder(Izq, InOrderIzq), inorder(Der, InOrderDer), append(InOrderIzq, [Raiz|InOrderDer], InOrder).

arbolConInorder([], nil).
arbolConInorder(InOrder, bin(Izq, Raiz, Der)) :- append(InOrderIzq, [Raiz|InOrderDer], InOrder), arbolConInorder(InOrderIzq, Izq), arbolConInorder(InOrderDer, Der).

aBB(nil).
aBB(bin(nil, _, nil)).
aBB(bin(nil, Raiz, Der)) :- raiz(Der, R), Raiz < R, aBB(Der).
aBB(bin(Izq, Raiz, nil)) :- raiz(Izq, L), L < Raiz, aBB(Izq).
aBB(bin(izq, Raiz, Der)) :- raiz(Izq, L), raiz(Der, R), L < Raiz, Raiz < R, aBB(Izq), aBB(Der).

%aBBInsertar(+X, +T1, -T2).
aBBInsertar(X, T1, T2) :- inorder(T1, InOrder), insertar(X, InOrder, InOrderConX), arbolConInorder(InOrderConX, T2), aBB(T2).
%T2 es reversible

frutal(frutilla).
frutal(banana).
frutal(manzana).
cremoso(banana).
cremoso(americana).
cremoso(frutilla).
cremoso(dulceDeLeche).

leGusta(X) :- frutal(X), cremoso(X).
cucurucho(X,Y) :- leGusta(X), !, leGusta(Y).

esPar(N) :- desde(0, N), 0 is N mod 2.
esNumero(N) :- N = 0.

% 18.I: Existe Y : P(Y) and (not Q(Y))
% 18.II: Existe Y : ((Existe Y : (not Q(Y))) and P(Y))
% 18.III:
predExisteUnicaX(X) :- esPar(X), not((Z \= X, esPar(Z))).


%% No es de la práctica, si esto está commiteado es porque me olvidé de borrarlo
%noEsPrimo(1) :- !.
%noEsPrimo(P) :- P2 is P-1, between(2, P2, D), P mod D =:= 0, !.
%
%sublistaMasLargaDePrimos(L,P) :- sublistaDePrimosDeLong(L,P,Long), not((sublistaDePrimosDeLong(L,_,Long2), Long2 > Long)).
%
%sublistaDePrimosDeLong(L,P,Long) :- sublista(L,P), soloPrimos(P), length(P,Long).
%
%sublista(_,[]).
%sublista(L,S) :- append(P,_,L), append(_,S,P), S \= [].
%
%soloPrimos(L) :- not((member(X,L), not(esPrimo(X)))).
%
%% esPrimo(+P)
%esPrimo(P) :- P \= 1, P2 is P-1, not((between(2,P2,D), mod(P,D) =:= 0)).
%
%genPrimos(P) :- desde(2,P), esPrimo(P).
%
%prefijoHasta(X, L, Prefijo) :- append(Prefijo, [X | _], L).
%
%preorder(nil, []).
%preorder(bin(I, R, D), [R | L]) :- preorder(I, LI), preorder(D, LD), append(LI, LD, L).
%
%%?- listaDeÁrboles(L).
%%L = [];														
%%L = [bin(nil,_,nil)]; ---> [1]
%%L = [bin(nil,_,nil), bin(nil,_,nil)]; ---> [1,1]
%%L = [bin(nil,_, bin(nil,_,nil))];  ---> [2]
%%L = [bin(bin(nil _,nil),_,nil)];   ---> [2]
%%L = [bin(nil,_,nil), bin(nil,_,nil), bin(nil,_,nil)]; --> [1,1,1]
%%---> [1,2]
%%---> [1,2]
%%---> [2,1]
%%---> [2,1]
%%---> [3]
%
%listaDeArboles(L) :- desde(0,S), listaAcotadaDeArboles(S,L).
%
%listaAcotadaDeArboles(0,[]).
%listaAcotadaDeArboles(S,[X|XS]) :- between(1,S,Na), 
%		arbolDeN(Na,X), S2 is S-Na, 
%		listaAcotadaDeArboles(S2,XS).
%
%
%arbolDeN(0,nil).
%arbolDeN(N,bin(I,_,D)) :- N > 0, N2 is N-1, paresQueSuman(N2,NI,ND), arbolDeN(NI,I), arbolDeN(ND,D).
%
%paresQueSuman(S,X,Y) :- between(0,S,X), Y is S-X.
%
%
%tamArbol(0,nil).
%tamArbol(N,bin(I,_,D)) :- tamArbol(NI,I), tamArbol(ND,D), N is 1+NI+ND.
%
%calcularTamanios(AS,TS) :- maplist(tamArbol,TS,AS).
%
%
%
%
%
%
%% corteMasParejo(+L,-L1,-L2)
%corteMasParejo(L,L1,L2) :- unCorte(L,L1,L2,D), not((unCorte(L,_,_,D2), D2 < D)).
%
%unCorte(L,L1,L2,D) :- append(L1,L2,L), sumlist(L1,S1), sumlist(L2,S2), D is abs(S1-S2).
%
%
%
%esTriangulo(tri(A,B,C)) :- A < B+C, B < A+C, C < B+A.
%
%
%perimetro(tri(A,B,C),P) :- ground(tri(A,B,C)), esTriangulo(tri(A,B,C)), P is A+B+C.
%perimetro(tri(A,B,C),P) :- not(ground(tri(A,B,C))), armarTriplas(P,A,B,C), esTriangulo(tri(A,B,C)).
%
%
%
%armarTriplas(P,A,B,C) :- desde2(3,P), between(0,P,A), S is P-A, between(0,S,B), C is S-B.
%
%
%
%triangulos(T) :- perimetro(T,_).
%
%%matrices(+LS, -L)
%matrices(LS, L) :- longitudDeListaMasLarga(LS, Longitud), between(1, Longitud, N), generadorMatriz(N, N, M1), sublista(LS, M2), permutacion(M2, L), L = M1.
%
%%generadorMatrizCuadrada(+N, -L)
%generadorMatriz(_, 0, []).
%generadorMatriz(N, M, Matriz) :- N > 1, M >= 1, Matriz = [Fila|MatrizSinUnaFila], length(Fila, N), M2 is M - 1, generadorMatriz(N, M2, MatrizSinUnaFila).
%
%%generadorMatrizCuadrada(+l, -Longitud)
%longitudDeListaMasLarga(L, Longitud) :- member(Lista, L), length(Lista, Longitud), not((member(Lista2, L), length(Lista2, Longitud2), Longitud2 > Longitud)).
%