#import "@preview/ilm:1.2.1": *

#set text(lang: "es")

#show: ilm.with(
  title: [Ingeniería de Software],
  author: "@valnrms",
  date: datetime.today(),
  date-format: "Fecha de compilación: [day padding:zero] / [month padding:zero] / [year repr:full].",
  abstract: [
    Resumen (teórico) de la materia Ingeniería de Software de la licenciatura en Ciencias de la Computación de la Universidad Nacional de Buenos Aires. \

    Profesor: #link("https://ar.linkedin.com/in/hernanwilkinson")[Hernan Wilkinson]. \
    Página web con la información de la materia: #link("https://www.isw2.com.ar/")[Ingeniería de Software].

  ],
  preface: [
    #align(center + horizon)[
      Este texto asume que el lector está estudiando la licenciatura en Ciencias de la Computación de la Universidad Nacional de Buenos Aires. \
    ]
  ],
)


= Introducción al software y al desarrollo de software
Capaz por lo que muchos entraron a la carrera. La rama de ingeniería. Venís de cursar varias materias, siendo estas principalmente de la rama de algoritmos, y posiblemente un poquito de la rama de sistemas. \
En esta materia se va a intentar ver la disciplina desde otra perspectiva. Arranquemos con algunas definiciones.

== Software
_¿Qué es el software?:_ ¿es lo que corre en la compu?, ¿es el código que programamos?, ¿son los amigos que hacemos en el camino? \

Historicamente, el software, se relacionó con las instrucciones escritas en lenguaje ensamblador que se ejecutaban en una computadora. Es decir, el software era una *secuencia de instrcciones*, que dado un input, producía un output.

Para la materia vamos a optar por una definición más moderna. Entendemos al software como un *#underline("modelo computable") de un #underline("dominio de problema") de la #underline("realidad")*.

#align(center)[
  #rect[
    *Modelo Computable:* representación de aquello que se está modelando, en particular que puede ser ejecutado en una máquina de Turing $->$ Formal, a-contextual. \
    Que sea computable nos da una característica esencial: no solo especifica el qué sino que además implementa el cómo. \
    *Dominio de Problema:* un recorte de la realidad que nos interesa para el negocio que estamos modelando. \
    *Realidad:* es todo aquello que podemos percibir, tocar, hablar sobre, etc...
  ]
  #image("./images/software.svg", width: 80%)
]

=== ¿Qué es un buen software?

Bueno. Acabamos de decir que el software es un modelo computable de un dominio de problema de la realidad. Es correcto preguntarse ahora, ¿qué es un buen software? \
Para eso debemos responder la pregunta, ¿qué es un buen modelo computable? Que sea computable, ya sabemos (puede ser ejecutado en una máquina de Turing). \
Veamos qué quiere decir ser un buen modelo. \

#align(center)[
  #rect[
    Un buen modelo es aquel que responde de manera positiva a los siguientes ejes:
    #list(
      [*Funcional:* qué tan buena es la representación del dominio.],
      [*Descriptivo:* qué tan bien está descripto el modelo, que tan "entendible es".],
      [*Implementativo:* cómo "ejecuta" en el ambiente técnico.]
      )
  ]
]

==== Eje funcional
Un modelo es bueno cuando *ejecuta* en el *tiempo esperado*, usando los *recursos definidos* como necesarios:
- Performance.
- Espacio.
- Escalabilidad.
- Todo lo relacionado con _requerimientos no funcionales_.
- Es la parte *"detallista"* del desarrollo.

==== Eje descriptivo
Un modelo es bueno cuando *se lo puede "entender"* (y aprender de él) y por lo tanto *"cambiar"*:
- Importantísimo usar buenos nombres.
- Importantísimo usar mismo lenguaje que el del dominio de problema.
- El código debe ser "lindo".
- Es la parte *“artística”* del desarrollo.

==== Eje implementativo
Un modelo es bueno cuando *puede representar correctamente toda observación de aquello que modela*:
- Si aparece algo nuevo en el dominio, debe aparecer algo nuevo en el modelo (no modificarlo).
- Si se modifica algo del dominio, solo se debe modificar su representación en el modelo.
- Relación 1:1 dominio-modelo _(isomorfismo)_.
- Es la parte *"observacional"* del desarrollo.

=== Entendimiento del modelo (ejemplo)
Supongamos que tenemos un software que