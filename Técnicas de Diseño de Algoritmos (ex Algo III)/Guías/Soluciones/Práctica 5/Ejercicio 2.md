Diseñar un algoritmo eficiente que, dado un digrafo $G$ con pesos no negativos, dos vértices $s$ y $t$ y una cota $c$, determine una arista de peso máximo de entre aquellas que se encuentran en algún recorrido de $s$ a $t$ cuyo peso (del recorrido, no de la arista) sea a lo sumo $c$. **Demostrar** que el algoritmo propuesto es correcto.

**Aclaración:** Mi idea con este ejercicio es formalizar conceptos en lo que a las demostraciones refiere. La idea es poderlo utilizar como una guía para poder realizar demostraciones.

* **Notación:**
	* $w_{G}(u,v)$ con $u$, $v$ vértices es una función que devuelve el costo del camino mínimo entre dos vértices $u$, $v$ cualesquiera en el (di)grafo $G$.
	* $w_{G}(P)$ con $P$ recorrido (análogo camino) es una función que devuelve el costo del recorrido (análogo camino) $P$ en el (di)grafo $G$.
	* $c_{G}(u,v)$ con $u$, $v$ vértices es una función que devuelve el costo de la arista $(u,v)$ en el (di)grafo $G$.

* **Algoritmo:**
	* **Introducción:**
	  El ejercicio nos pide dos cosas.
	  Primero armar un algoritmo que nos permita encontrar la arista $e = (u,v)$ más grande de un digrafo $G$, tal que esa arista pertenezca a un camino $P = {s,\dots,u,v,\dots,t}$. y que $w_{G}(P) \leq c$ para un $c$ dado.
	  
	  Luego, nos pide demostrar que el algoritmo que planteamos es correcto.
	  A su vez es importante notar que el enunciado dice *"determine una arista de peso máximo de entre aquellas que se encuentran en algún recorrido de s a t"*, por lo que no es incoherente asumir que existe al menos un camino entre $s$ y $t$.
		
	  El jugo del ejercicio está en el segundo item. Es lo que caracteriza a los científicos en computación, el saber realizar demostraciones.
		
	  Es importante tener en cuenta eso *(que nos va a interesar realizar una demostración)* a la hora de escribir el algoritmo. Es preferible separar nuestro algoritmo en pequeños pasos sencillos de demostrar (o mejor aún, **ya demostrados**) para facilitarnos la tarea de demostrar.
		
	  Si logramos **descomponer el algoritmo en problemas sencillos**, vamos a poder deconstruir nuestra demostración en pequeños **lemas**. Estos **lemas** los vamos a demostrar, y así construir la demostración de nuestro algoritmo.
	  
	* **Idea:**
		1. Corremos Dijkstra sobre el digrafo $G$ en el vértice destacado s. Nos da $w_{G}(s,u)$ $\forall u \in G(V)$.
		   Complejidad: $\theta(n + m \cdot \log n)$
		2. Armamos $G^{t}$.
		   Complejidad: $\theta(n + m)$
		3. Corremos Dijkstra sobre el digrafo $G^{t}$ en el vértice destacado t. Nos da $w_{G^{t}}(t, v)$ $\forall v \in G^{t}(V)$, pero podemos pensar que nos devuelve $w_{G}(v, t)$ $\forall v \in G(V)$.
		   Complejidad: $\theta(n + m \cdot \log n)$
		4. Hacemos **un ciclo sencillo** que nos agrega en una lista todas las aristas $e = (u,v)$ que cumplen la condición $w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t) \leq c$.
		   Complejidad: $\theta(m)$
		5. Hacemos **otro ciclo sencillo** que busca la arista $e = (u,v)$ con costo máximo en la lista que armamos en el paso anterior.
		   Complejidad: $\theta(m)$
		6. Devolvemos la arista $e$.

		    Notar que en los últimos dos pasos se podrían haber realizado en un sólo ciclo. No estoy buscando optimizar el problema, estoy tratando de encontrar el approach más **naive** posible para poder facilitar mi demostración. Mientras más simples sean los pasos de mi algoritmo, más sencilla será la demostración.
		
* **Demostración:**
	* Para que valga la demostración, me voy a aprovechar que considero demostrado el algoritmo de Dijkstra. Como a su vez que transponer un digrafo $G$ y realizar Dijkstra (en $G^{t}$) desde un vértice cualquiera $v$, me devuelve la distancia sobre cualquier vértice de $G$ a el vértice $v$.
	  Con esto sabido me queda demostrar mis cíclos, asumiendo que puedo conseguir la función $w_{G}$. Tengo que pasar del mundo de los algoritmos al mundo de la matemática.
    
		Quiero probar el paso número $4$. Sé que un algoritmo puede agarrar una arista y tomar una decisión sobre ella, sin embargo, tengo que demostrar que la decisión que estoy tomando es útil para lo que quiero obtener como respuesta. **Planteo dos lemas**.
      
    **Lema I:** $P$ es camino mínimo desde $s$ hasta $t$ en $G$ con la arista $e = (u,v)$ $\iff$ $w_{G}(P) = w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t)$

    Este lema me será útil para demostrar el lema de más abajo. Para que se entienda el desarrollo, primero se planteó el segundo lema, en el proceso de demostrarlo uno debe darse cuenta que puede llegar a necesitar de otros lemas para su demostración. Este es uno de esos casos, y este es el lema que uno debe apoyarse.

    * $\implies$) $P$ es camino mínimo desde $s$ hasta $t$ en $G$ con la arista $e = (u,v)$ $\implies$ $w_{G}(P) = w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t)$
      Como $P$ es un camino podemos descomponerlo tal que $P = P_{1} \cup P_{2} \cup P_{3}$.
      Sea:
      $P_{1} = \{\text{camino mínimo de s a u}\}$
      $P_{2} = \{u,v\}$
      $P_{3} = \{\text{camino mínimo de v a t}\}$
      
      Como $P$ es un camino mínimo desde $s$ hasta $t$, el costo total del camino $P$ es la suma de los pesos de los subcaminos, luego:
      $w_{G}(P) = w_{G}(P_{1}) + w_{G}(P_{2}) + w_{G}(P_{3})$
       
      Dado que $P_{1}$ es el camino mínimo desde $s$ hasta $u$ pues los caminos mínimos cumplen la propiedad de subestructura óptima, entonces $w_{G}(P_{1}) = w_{G}(s,u)$
       
      Dado que $P_{2}$ es la arista $(u,v)$, es cierto que $w_{G}(P_{2}) = c_{G}(u,v)$
       
      Dado que $P_{3}$ es el camino mínimo desde $v$ hasta $t$ pues los caminos mínimos cumplen la propiedad de subestructura óptima, entonces $w_{G}(P_{3}) = w_{G}(v,t)$
       
      Finalmente, $w_{G}(P) = w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t)$
      $\square$
      
    * $\impliedby$) $w_{G}(P) = w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t)$ $\implies$ $P$ es camino mínimo desde $s$ hasta $t$ en $G$ con la arista $e = (u,v)$
      Sabemos que existe un camino $P$ tal que $w_{G}(P) = w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t)$. Es decir, que el peso total de $P$ está dado por la suma del costo del camino mínimo desde $s$ hasta $u$, el costo de la arista $(u,v)$, y el costo del camino mínimo desde $v$ hasta $t$.
      Queremos demostrar que $P$ es un camino mínimo con la arista $(u,v)$ desde $s$ hasta $t$ en $G$.
      
      Vamos por el absurdo.
      Supongamos que P no es camino mínimo desde $s$ hasta $t$ en $G$ con la arista $e = (u,v)$. Eso quiere decir que existe otro camino $Q$ desde $s$ a $t$ con la arista $(u,v)$ con un costo menor al de $P$.
      Es decir, este otro camino $Q$ es el verdadero camino mínimo.
      Bajo esa premisa debería valer que $w_{G}(Q) < w_{G}(P)$.
      Veamos por qué esto es falso...
      
      Utilizando la $\implies$ del lema, como $Q$ es camino mínimo sé que $w_{G}(Q) = w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t)$.
      
      Ahora, quiero ver que $w_{G}(Q) < w_{G}(P)$, o lo que es lo mismo
      $w_{G}(Q) = w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t) < w_{G}(Q) = w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t)$
      Y esto vale si y sólo si $0 < 0$. Lo que es absurdo.
      $\square$
      
    **Lema II:** la arista $(u,v)$ forma parte de un camino $P$ en $G$ que va de $s$ hasta $t$ con costo $\leq c$ $\iff$ $w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t) \leq c$
      
    * $\implies$) la arista $(u,v)$ forma parte de un camino $P$ en $G$ que va de $s$ hasta $t$ con costo $\leq c$ $\implies$ $w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t) \leq c$
      Sale por de manera directa pues tengo la arista $(u,v)$ que sé que pertenece a un camino $P$ en $G$ que va de $s$ hasta $t$ con costo $\leq c$.
        
      También sé que existe $P'$ que va desde $s$ hasta $t$ en $G$, que es algún camino (capaz es $=$ a $P$, capaz es $\neq$ a $P$; no lo sé a priori) que tiene a la arista $(u,v)$, pero $P'$ tiene la particularidad que es el **camino mínimo**. Entonces sé que (por la $\implies$ del Lema I) $w_{G}(P') = w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t)$.
      Ahora, como $P'$ es el camino mínimo sé que $w_{G}(P') \leq w_{G}(P)$. Y sé que $w_{G}(P) \leq c$ por el antecedente. Luego $w_{G}(P') \leq c$, o lo que es lo mismo $w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t) \leq c$.
      $\square$
      
    * $\impliedby$) $w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t) \leq c$ $\implies$ la arista $(u,v)$ forma parte de un camino $P$ en $G$ que va de $s$ hasta $t$ con costo $\leq c$
      También sale directamente, pues supongamos que tengo un camino $P$ de $s$ hasta $t$ que tiene costo $w_{G}(P)$. Aún no sé si es $\leq c$.
      Construyamos $P$ de tal manera que, sabiendo el antecedente, podamos concluir que $w_{G}(P) \leq c$.
      Sea $P = \{\text{camino mínimo de s y u}\} \cup (u,v) \cup \{\text{camino mínimo de v y t}\}$. Luego $w_{G}(P) = w_{G}(\{\text{camino mínimo de s y u}\}) + c_{G}(u,v) + w_{G}(\{\text{camino mínimo de v y t}\})$.
      Por definición de $w_{G}$ sé que $w_{G}(\{\text{camino mínimo de s y u}\}) = w_{G}(s,u)$ y $w_{G}(\{\text{camino mínimo de s y t}\}) = w_{G}(v,t)$.
      Entonces $w_{G}(P) = w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t) \leq c$. Encontré el camino que cumple el consecuente. Es $P$.
      $\square$
      
    Con este último lema demostrado, me faltaría probar que el ciclo del paso número $5$ me da efectivamente el máximo. Sin embargo, sabemos tomar el máximo de una lista.
    El pseudocódigo sería algo como esto. Supongamos que la lista $L$ es la que contiene todas las aristas $(u,v)$ tal que $w_{G}(s,u) + c_{G}(u,v) + w_{G}(v,t) \leq c$.
    ```
      maximo <- -inf
      e <- ⊥
      
      for each arista in L:
        if c(arista) > maximo then
          maximo <- c(arista)
          e <- arista
        endif
      endfor
      
      return e
    ```
    Estoy asumiendo este ciclo como demostrado, pues únicamente busca un máximo. Es decir $e = (u,v) \quad \text{tal que} \quad (u,v) \in L \quad \text{y} \quad c(u,v) \geq c(x,y) \quad \forall (x,y) \in L$.

    Después de leer todo esto, uno podría preguntarse: ¿dónde usaste que vale Dijkstra?, ¿y lo de $G^{t}$?
    Aunque no lo usé explícitamente en mis demostraciones, estoy usando que puedo conseguir esa función $w_{G}$, no es un galerazo, sino que la obtengo aplicando dos veces Dijkstra como expliqué en la idea del algoritmo.
* Notas:
	* La demostración del punto $3$ de mi algoritmo sale por el absurdo o inducción sobre el tamaño del camino mínimo.
	* La demostración del punto $5$ de mi algoritmo sale por inducción sobre las iteraciones del ciclo.