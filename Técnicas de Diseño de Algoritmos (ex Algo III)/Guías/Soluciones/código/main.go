package main

import (
	"fmt"

	"gitlab.com/valn/uba/TDA/p1/ej12"
)

func main() {
	n := 3
	k := 4
	s := 6

	fmt.Println(ej12.Distinguibles(n, s, k))
}
