package ej7

import "math"

func MGN2(P *[]int) int {
	n := len(*P)

	// Inicializamos la matriz de memoria (filas x columnas)
	memoria := make([][]int, n+1)
	for i := 0; i <= n; i++ {
		memoria[i] = make([]int, n+1)
		for j := 0; j <= n; j++ {
			memoria[i][j] = -math.MaxInt32
		}
	}

	memoria[0][0] = 0

	for j := 1; j <= n; j++ { // Columnas
		for i := 0; i <= n; i++ { // Filas
			// Caso compro asteroide
			comprado := -math.MaxInt32
			if i-1 >= 0 { // Evitamos index out of range
				comprado = memoria[i-1][j-1] - (*P)[j-1]
			}

			// Caso no hago nada
			nada := memoria[i][j-1]

			// Caso vendo asteroide
			vendido := -math.MaxInt32
			if i+1 <= n { // Evitamos index out of range
				vendido = memoria[i+1][j-1] + (*P)[j-1]
			}

			memoria[i][j] = max(comprado, vendido, nada)
		}
	}

	// Me quedé con 0 asteroides en el día n
	return memoria[0][n]

}
