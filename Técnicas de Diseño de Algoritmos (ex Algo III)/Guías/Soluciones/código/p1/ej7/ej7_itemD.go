package ej7

import "math"

// Complejidad temporal y espacial: n^2 tq' n es la cantidad de días (longitud de P)

func MGN(P *[]int, memoria *[][]int, c int, j int) int {
	if c < 0 || c > j {
		return -math.MaxInt32
	}

	if j == 0 && c == 0 {
		return 0
	}

	if (*memoria)[c][j] == -1 {
		(*memoria)[c][j] = max(MGN(P, memoria, c-1, j-1)-(*P)[j-1], MGN(P, memoria, c+1, j-1)+(*P)[j-1], MGN(P, memoria, c, j-1))
	}

	return (*memoria)[c][j]
}
