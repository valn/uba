package ej9

import "math"

// Complejidad temporal: O(n*m)
// Complejidad espacial: O(n*m)
func TravesíaVital_TopDown(A *[][]int, memoria *[][]int, i int, j int) int {
	n := len(*A)
	m := len((*A)[0])

	if i == n-1 && j == m-1 {
		return int(math.Abs(float64((*A)[i][j]))) + 1
	}

	if i < 0 || j < 0 || i >= n || j >= m {
		return math.MaxInt32
	}

	if (*memoria)[i][j] == 0 {
		(*memoria)[i][j] = max(min(TravesíaVital_TopDown(A, memoria, i+1, j), TravesíaVital_TopDown(A, memoria, i, j+1))-(*A)[i][j], 1)
	}

	return (*memoria)[i][j]
}

// Complejidad temporal: O(n*m)
// Complejidad espacial: O(mín(n, m))
func TravesíaVital_BottomUp(A *[][]int, i int, j int) int {
	n := len(*A)
	m := len((*A)[0])

	longitud := min(n, m)

	solución := make([]int, longitud)

	solución[longitud-1] = int(math.Abs(float64((*A)[n-1][m-1])) + 1)
	for i := 0; i < longitud-1; i++ {
		solución[i] = math.MaxInt32
	}

	for fila := n - 1; fila >= 0; fila-- {
		for columna := m - 1; columna >= 0; columna-- {
			if fila == n-1 && columna == m-1 {
				continue
			}

			/*
				To be done.

				Posiblemente salga aprovechando la misma idea de max(min(TravesíaVital_TopDown(A, memoria, i+1, j), TravesíaVital_TopDown(A, memoria, i, j+1))-(*A)[i][j], 1)
				Para construir la solución de manera bottom-up. En el vector/slice solución te guardás algo referente a las solcuiones parciales.
			*/

		}
	}

	return solución[0]
}
