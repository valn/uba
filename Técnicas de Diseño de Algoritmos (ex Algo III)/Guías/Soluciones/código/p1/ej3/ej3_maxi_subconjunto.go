package ej3

type Resultado struct {
	Valor   int
	Indices []int
}

/*
	Complejidad sobre el espacio de memoria: O(n^2 + k)
	Pues se almacena la matriz de n x n, los candidatos de tamaño n + 1, y dos arreglos de tamaño k. El resto de cosas que se almacenan son O(1).
	Luego, O(n^2 + n+1 + k + k) = O(n^2 + k)
	Y todo se pasa por referencia

	Complejidad temporal: O(n! * k^2)
	El algoritmo es una variante de backtracking, donde se generan todas las posibles combinaciones de tamaño k de los elementos de la matriz.
	Se puede visualizar haciendo un árbol de llamadas recursivas
		- En el nivel cero, se generan n posibles combinaciones (n)
		- En el nivel uno, se generan n-1 posibles combinaciones para cada una de las n combinaciones del primer nivel (n * (n-1))
		- En el nivel dos, se generan n-2 posibles combinaciones para cada una de las n-1 combinaciones del segundo nivel (n * (n-1) * (n-2))
		- ...
		- En el nivel k (donde se encuentran las hojas) el costo de decidir si esa hoja superpone a la mejor solución hasta el momento es O(k^2)
*/

func MaxiSubconjunto(matriz *[][]int, k *int, candidatos *[]bool, dedito *int, parcial *Resultado, res *Resultado) {
	// Caso base
	if *dedito == *k { // O(1)
		// Calculamos la suma de los elementos de la matriz que están en los índices del subconjunto
		parcial.Valor = SumaPorIndices(matriz, &parcial.Indices) // O(k^2)

		// Si la suma es mayor que la mejor solución hasta el momento, actualizamos la mejor solución
		if parcial.Valor > res.Valor { // O(1)
			res.Valor = parcial.Valor        // O(1)
			for i := range parcial.Indices { // O(k), se puede usar copy(res.Indices, parcial.Indices) para evitar el for
				res.Indices[i] = parcial.Indices[i] // O(1)
			}
		}

		return
	}

	for n := 1; n < len(*candidatos); n++ { // O(n)
		// Estamos en un conjunto, por lo que no hay repetidos, si ya está en el conjunto, no lo volvemos a meter
		if (*candidatos)[n] { // O(1)
			continue
		}

		// Si el número n es menor que el último número del subconjunto, cortamos la rama (optimización por ¿optimalidad?)
		/*
			Si lees esto y te quedás con cara de ???, fijate de hacer el árbol de backtracking
			y te vas a dar cuenta que los casos del estilo: {1,3,2}
			(i.e. el número n = 2 es menor que el último número del subconjunto = 3) repiten casos como el {1,2,3}
		*/
		if *dedito > 0 && n < parcial.Indices[*dedito-1] { // O(1)
			return
		}

		// Si no está en el conjunto, lo metemos
		(*candidatos)[n] = true      // O(1)
		parcial.Indices[*dedito] = n // O(1)
		*dedito++                    // O(1)

		// Llamada recursiva
		MaxiSubconjunto(matriz, k, candidatos, dedito, parcial, res) // O(coste de la llamada recursiva)

		// Backtracking
		(*candidatos)[n] = false // O(1)
		*dedito--                // O(1)
	}
}

func SumaPorIndices(matriz *[][]int, indices *[]int) int { // O(k^2)
	var res int = 0           // O(1)
	for i := range *indices { // O(k)
		for j := range *indices { // O(k)
			res += (*matriz)[(*indices)[i]-1][(*indices)[j]-1] // O(1)
		}
	}
	return res // O(1)
}
