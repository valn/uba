package ej10

import "math"

// Función recursiva: https://i.ibb.co/Vm7JwTC/image.png
// i es la caja que estoy mirando
// p es el peso acumulado
func PilaCauta(w *[]int, s *[]int, i int, p int) int {
	n := len(*w)

	/*
		Variante para los casos base:

		if i < 0 {
			return 0
		}

		if i > n || p > (*s)[i] {
			return -math.MaxInt32
		}
	*/

	if i == 0 && (*s)[i] >= p {
		return 1
	}

	if i == 0 || i > n || p > (*s)[i] {
		return -math.MaxInt32
	}

	return max(PilaCauta(w, s, i-1, p), PilaCauta(w, s, i-1, p+(*w)[i])+1)
}

// Complejidad temporal: O(n * p)
// Complejidad espacial: O(n * p)
// Seguramente se pueda mejorar la complejidad espacial
func PilaCauta_TopDown(w *[]int, s *[]int, i int, p int, memoria *[][]int) int {
	n := len(*w)

	/*
		Variante para los casos base:

		if i < 0 {
			return 0
		}

		if i > n || p > (*s)[i] {
			return -math.MaxInt32
		}
	*/

	if i == 0 && (*s)[i] >= p {
		return 1
	}

	if i == 0 || i > n || p > (*s)[i] {
		return -math.MaxInt32
	}

	if (*memoria)[i][p] == 0 {
		(*memoria)[i][p] = max(PilaCauta(w, s, i-1, p), PilaCauta(w, s, i-1, p+(*w)[i])+1)
	}

	return (*memoria)[i][p]
}
