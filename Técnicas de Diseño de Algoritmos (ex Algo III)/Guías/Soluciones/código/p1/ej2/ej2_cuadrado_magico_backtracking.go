package ej2

// Créditos a honi (https://github.com/honi/uba-aed3/blob/main/Pr%C3%A1cticas/Pr%C3%A1ctica1/ej02/main_optimized.cpp) por la implementación

type ResultadoParcial struct {
	SumaFila     [3]int
	SumaColumna  [3]int
	SumaDiagonal [2]int
}

// Candidatos es un vector de booleanos que indica si un número ya fue utilizado
func CuadradoMagico_BT(matriz *[][]int, candidatos *[]bool, fila int, columna int, parcial *ResultadoParcial) int {
	n := len(*matriz)
	M := NumeroMagico(n)

	// Si llegamos a la última columna, pasamos a la siguiente fila
	if columna == n {
		return CuadradoMagico_BT(matriz, candidatos, fila+1, 0, parcial)
	}

	// Llegamos a la última fila. Por hipótesis, la matriz es un cuadrado mágico válido; devolvemos 1
	if fila == n {
		return 1
	}

	// Definimos res := 0 y buscamos un número candidato
	res := 0
	for i := 1; i <= n*n; i++ {

		// Si el número ya fue utilizado, lo salteamos
		if (*candidatos)[i-1] {
			continue
		}

		// Si la suma de la fila actual + el valor a ingresar supera el número mágico, salteamos
		if parcial.SumaFila[fila]+i > M {
			continue
		}

		// Si estamos en la última columna, verificamos si la suma de la fila actual + el valor a ingresar es igual al número mágico
		if columna == n-1 && parcial.SumaFila[fila]+i != M {
			continue
		}

		// Si la suma de la columna actual + el valor a ingresar supera el número mágico, salteamos
		if parcial.SumaColumna[columna]+i > M {
			continue
		}

		// Si estamos en la última fila, verificamos si la suma de la columna actual + el valor a ingresar es igual al número mágico
		if fila == n-1 && parcial.SumaColumna[columna]+i != M {
			continue
		}

		esDiagonalPrincipal := fila == columna
		esDiagonalSecundaria := fila+columna == n-1

		// Si estamos en la diagonal principal, verificamos si la suma de la diagonal principal + el valor a ingresar supera el número mágico
		if esDiagonalPrincipal && parcial.SumaDiagonal[0]+i > M {
			continue
		}

		// Vemos si la suma de la diagonal principal + el valor a ingresar es igual al número mágico
		if esDiagonalPrincipal && fila == n-1 && parcial.SumaDiagonal[0]+i != M {
			continue
		}

		// Si estamos en la diagonal secundaria, verificamos si la suma de la diagonal secundaria + el valor a ingresar supera el número mágico
		if esDiagonalSecundaria && parcial.SumaDiagonal[1]+i > M {
			continue
		}

		// Vemos si la suma de la diagonal secundaria + el valor a ingresar es igual al número mágico
		if esDiagonalSecundaria && fila == n-1 && parcial.SumaDiagonal[1]+i != M {
			continue
		}

		// Si llegamos hasta acá, el número i es candidato
		(*matriz)[fila][columna] = i
		(*candidatos)[i-1] = true
		parcial.SumaFila[fila] += i
		parcial.SumaColumna[columna] += i
		if esDiagonalPrincipal {
			parcial.SumaDiagonal[0] += i
		}
		if esDiagonalSecundaria {
			parcial.SumaDiagonal[1] += i
		}

		// Llamamos recursivamente a la función para la siguiente columna
		res += CuadradoMagico_BT(matriz, candidatos, fila, columna+1, parcial)

		// Deshacemos los cambios (backtracking)
		(*candidatos)[i-1] = false
		parcial.SumaFila[fila] -= i
		parcial.SumaColumna[columna] -= i
		if esDiagonalPrincipal {
			parcial.SumaDiagonal[0] -= i
		}
		if esDiagonalSecundaria {
			parcial.SumaDiagonal[1] -= i
		}
	}

	return res
}
