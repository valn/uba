package ej2

// Créditos a honi (https://github.com/honi/uba-aed3/blob/main/Pr%C3%A1cticas/Pr%C3%A1ctica1/ej02/main_naive.cpp) por la implementación

// Candidatos es un vector de booleanos que indica si un número ya fue utilizado
func CuadradoMagico(matriz *[][]int, candidatos *[]bool, fila int, columna int) int {
	n := len(*matriz)

	// Si llegamos a la última columna, pasamos a la siguiente fila
	if columna == n {
		return CuadradoMagico(matriz, candidatos, fila+1, 0)
	}

	// Llegamos a la última fila. Por hipótesis, la matriz es un cuadrado mágico válido; devolvemos 1
	if fila == n {
		if EsCuadradoMagicoValido(*matriz) {
			return 1
		}
		return 0
	}

	// Definimos res := 0 y buscamos un número candidato
	res := 0
	for i := 0; i < n*n; i++ {

		// Si el número ya fue utilizado, lo salteamos
		if (*candidatos)[i] {
			continue
		}

		// Si llegamos hasta acá, el número i es candidato
		(*matriz)[fila][columna] = i + 1
		(*candidatos)[i] = true

		// Llamamos recursivamente a la función para la siguiente columna
		res += CuadradoMagico(matriz, candidatos, fila, columna+1)

		// Deshacemos los cambios (backtracking)
		(*candidatos)[i] = false
	}

	return res
}
