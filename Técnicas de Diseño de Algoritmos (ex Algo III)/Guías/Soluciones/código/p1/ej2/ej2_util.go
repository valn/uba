package ej2

// Crea un slice de tamaño n*n con los valores del 1 al n*n
func CrearValores(n int) []int {
	valores := make([]int, n*n) // O(n)
	for i := 0; i < n*n; i++ {  // O(n)
		valores[i] = i + 1 // O(1)
	}
	return valores
}

// Crea una matriz cuadrada de tamaño n*n con todos los valores en 0
func CrearMatriz(n int) [][]int {
	matriz := make([][]int, n) // O(n)
	for i := 0; i < n; i++ {   // O(n)
		matriz[i] = make([]int, n) // O(n)
	}
	return matriz
}

func NumeroMagico(n int) int {
	return n * (n*n + 1) / 2
}

// Verifica si una matriz es un cuadrado mágico
func EsCuadradoMagicoValido(matriz [][]int) bool {
	n := len(matriz)
	numeroMagico := NumeroMagico(n)
	if sumaDiagonalPrincipal(&matriz) != numeroMagico || sumaDiagonalSecundaria(&matriz) != numeroMagico {
		return false
	}

	for i := 0; i < n; i++ {
		if sumaColumna(&matriz, i) != numeroMagico {
			return false
		}
	}

	for i := 0; i < n; i++ {
		if sumaFila(&matriz, i) != numeroMagico {
			return false
		}
	}

	return true
}

// Calcula la suma de una fila de una matriz
func sumaFila(matriz *[][]int, fila_n int) int {
	suma := 0
	for i := 0; i < len((*matriz)[fila_n]); i++ {
		suma += (*matriz)[fila_n][i]
	}
	return suma
}

// Calcula la suma de una columna de una matriz
func sumaColumna(matriz *[][]int, columna_n int) int {
	suma := 0
	for i := 0; i < len(*matriz); i++ {
		suma += (*matriz)[i][columna_n]
	}
	return suma
}

// Calcula la suma de la diagonal principal (de izquierda a derecha) de una matriz cuadrada
func sumaDiagonalPrincipal(matriz *[][]int) int {
	suma := 0
	for i := 0; i < len(*matriz); i++ {
		suma += (*matriz)[i][i]
	}
	return suma
}

// Calcula la suma de la diagonal secundaria (de derecha a izquierda) de una matriz cuadrada
func sumaDiagonalSecundaria(matriz *[][]int) int {
	suma := 0
	for i := 0; i < len(*matriz); i++ {
		suma += (*matriz)[i][len(*matriz)-1-i]
	}
	return suma
}
