package ej5

// LLamo a la función con ss(C, len(C)-1, j)
func ss(C *[]int, i int, j int) bool {
	if i == 0 { // Caso base
		return j == 0
	}

	if (*C)[i] > j { // Corto camino, sé que al hacer j-C[i] voy a tener j negativo, ergo esa rama no tiene solución
		return ss(C, i-1, j)
	}

	// No tomo el último elemento o tomo el último elemento
	return ss(C, i-1, j) || ss(C, i, j-(*C)[i])
}

/*
	a) que se convensa el lector
	b) C = {6,12,6,0,3}, k = 12
		 en la implementación: C = {6,12,6,0,3}, i = 4, j = 12

		 en algún momento del árbol de llamadas recursivas vas a hacer:
		 C = {6,12,6,0,3}, i = 2, j = 9, pues el cero no te cambia nada, aún así hay más ejemplos donde repetís estados
	c) uso una matriz de n x k, tq' n = len(C), donde un indefinido es -1 pues
		 "Dado un multiconjunto C = {c1, . . . , cn} de números naturales y un natural k..."
		 dice el enunciado.
	d) si k << 2^n, gana por goleada el d del ejercicio 5
		 si k >> 2^n, en tiempo sigue ganando el del ej 5, pero recordemos que era pseudopolinomial
		 por lo que la diferencia de tiempo sería despreciable
		 luego, importante, la matriz nos quedaría gigante por lo que en términos de memoria perdería
		 ergo, yo digo que gana el c del ejercicio 1 (aunque por tiempo sigue ganando el d del ej 5)
	f) asumo q' la idea sería usar un array de longitud k y podemos ir pisando resultados
	g) jajajajaja, to be done
*/
