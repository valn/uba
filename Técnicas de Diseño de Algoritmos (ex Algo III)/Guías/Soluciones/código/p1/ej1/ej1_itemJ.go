package ej1

import "fmt"

func ej1() {
	C := []int{6, 12, 6}
	k := 12
	p := []int{}     // Donde voy a ir armando las soluciones
	res := [][]int{} // Donde voy a guardar las soluciones
	backtracking(C, k, &p, &res)
	fmt.Println(res) // Muestra [[12] [6 6]] en la terminal
}

func backtracking(C []int, k int, p *[]int, res *[][]int) {
	if k < 0 { // Factibilidad (detengo la exploración en toda la rama si no es factible)
		return
	}

	if k == 0 { // Optimalidad (reviso en cada paso de la recursión si encontré una solución óptima)
		// Es necesario hacer una copia de p para meterlo en res, ya que p es un slice que se va modificando.
		copyP := make([]int, len(*p))
		copy(copyP, *p)

		*res = append(*res, copyP) // Meto p en res
		return
	}

	if len(C) == 0 { // Caso base, no hay más elementos para explorar
		return
	}

	// No tomo el primer elemento
	backtracking(C[1:], k, p, res)

	// Tomo el primer elemento
	*p = append(*p, C[0]) // Agrego el elemento a la solución parcial
	backtracking(C[1:], k-C[0], p, res)

	// Deshago la toma del primer elemento (backtracking)
	*p = (*p)[:len(*p)-1]
}
