package ej1

func ss(C []int, k int) bool {
	n := len(C) // n >= 0 (restricción)

	if k < 0 { // Corto camino, no hay solución por esta rama
		return false
	}

	if n == 0 { // Caso base
		return k == 0
	}

	// No tomo el último elemento o tomo el último elemento
	return ss(C[:n-1], k) || ss(C[:n-1], k-C[n-1])
}

/*
	Golang "hack": los slices son referencias a un nodo que contiene:
	la posición de memoria del primer elemento de un array, un int
	longitud (cuánto espacio está representando) y un int capacidad
	(cuánto espacio tiene el array). Al hacer C[:n-1] se está creando un
	nuevo slice que apunta al mismo array que C, pero con una longitud menor.
	Es decir, es de costo constante.
	Por eso me ahorro el "dedo" i que se mueve por el array en esta implementación.
	El "dedo" i está "internamente representado en el slice".
*/
