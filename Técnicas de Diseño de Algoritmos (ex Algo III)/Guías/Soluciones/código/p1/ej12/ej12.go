package ej12

func Distinguibles(n int, s int, k int) int {
	if n < 0 {
		return 0
	}

	if n == 0 && s == 0 {
		return 1
	}

	list := make([]int, 0, n)
	for i := 1; i <= k; i++ {
		list = append(list, Distinguibles(n-1, s-i, k))
	}

	sum := 0
	for _, v := range list {
		sum += v
	}

	return sum
}
