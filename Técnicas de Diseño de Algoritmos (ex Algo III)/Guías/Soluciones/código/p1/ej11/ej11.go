package ej11

import (
	"math"
)

func OperacionesSeq(v *[]int, w int, acc int, i int, C *[]string) bool {
	n := len(*v)

	if acc == w && i == n {
		return true
	}

	if w < 0 || i >= n {
		return false
	}

	// Suma
	*C = append(*C, "+")
	if ok := OperacionesSeq(v, w, acc+(*v)[i], i+1, C); ok {
		return true
	}
	*C = (*C)[:len(*C)-1]

	// Multiplicación
	*C = append(*C, "x")
	if ok := OperacionesSeq(v, w, acc*(*v)[i], i+1, C); ok {
		return true
	}
	*C = (*C)[:len(*C)-1]

	// Potencia
	*C = append(*C, "^")
	if ok := OperacionesSeq(v, w, int(math.Pow(float64(acc), float64((*v)[i]))), i+1, C); ok {
		return true
	}
	*C = (*C)[:len(*C)-1]

	return false
}
