package ej6

var B = []int{2, 3, 5, 10, 20, 20}

type Tuple struct{ Fst, Snd int }

func min(a, b *Tuple) *Tuple {
	if a.Fst == b.Fst {
		if a.Snd < b.Snd {
			return a
		}
		return b
	}

	if a.Fst < b.Fst {
		return a
	}

	return b
}
