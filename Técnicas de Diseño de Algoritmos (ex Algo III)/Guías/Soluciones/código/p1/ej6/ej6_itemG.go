package ej6

import (
	"fmt"
	"math"
)

// Definimos B como variable global
func ej6g() {
	c := 14
	n := len(B)

	result := CC_g(n, c)

	// Styling de la salida
	if result.Fst == math.MaxInt32 {
		fmt.Printf("(+inf, +inf)\n")
	} else {
		fmt.Printf("(%d, %d)\n", result.Fst, result.Snd)
	}
}

// Complejidad temporal: O(n*c)
// Approach bottom-up
func CC_g(n int, c int) Tuple {
	// Creo una matriz de n+1 x c+1 inicializada con tuplas vacías
	memoria := make([][]Tuple, n+1)
	for i := range memoria {
		memoria[i] = make([]Tuple, c+1)
	}

	for i := 0; i <= c; i++ { // Casos feos
		memoria[0][i] = Tuple{math.MaxInt32, math.MaxInt32}
	}

	for i := 0; i <= n; i++ { // Casos base
		memoria[i][0] = Tuple{0, 0}
	}

	for i := 1; i <= n; i++ {
		for j := 1; j <= c; j++ {
			withoutBill := memoria[i-1][j] // Caso de no agregar el billete

			/*
				Ya tengo guardada la mejor forma de sumar j-B[i-1] con i-1 billetes
				Me fijo cuál es ese monto y le agrego el billete B[i-1]

				Notar que B[i-1] es el billete actual. Los índices de los billetes van desde 0 hasta n-1 inclusive
				La memoria es de tamaño n+1 x c+1.
			*/
			onlyOneBill := Tuple{math.MaxInt32, math.MaxInt32}
			if B[i-1] >= j { // Si el billete cubre el monto, me puede interesar usar únicamente este billete
				onlyOneBill = Tuple{B[i-1], 1} // Caso utilizar únicamente este billete
			}

			bestBillCombo := Tuple{math.MaxInt32, math.MaxInt32}
			if j-B[i-1] >= 0 { // Evitamos index out of range
				// Buscamos la mejor combinación de billetes posible para utilizar B[i-1]
				bestWayToSum := memoria[i-1][j-B[i-1]]
				bestBillCombo = Tuple{bestWayToSum.Fst + B[i-1], bestWayToSum.Snd + 1} // Caso de agregar el billete (mejor combo)
			}

			withBill := *min(&onlyOneBill, &bestBillCombo) // Nos quedamos con la mejor forma de usar el billete B[i-1]
			memoria[i][j] = *min(&withBill, &withoutBill)  // Vemos si combiene usar el billete o no
		}
	}

	return memoria[n][c]
}
