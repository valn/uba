package ej6

import (
	"fmt"
	"math"
)

// B se encuentra definida como variable global en ej6_util.go
// var B = []int{2, 3, 5, 10, 20, 20}
func ej6c() {
	c := 14
	n := len(B)

	result := CC_c(n, c)

	// Styling de la salida
	if result.Fst == math.MaxInt32 {
		fmt.Printf("(+inf, +inf)\n")
	} else {
		fmt.Printf("(%d, %d)\n", result.Fst, result.Snd)
	}
}

/*
Complejidad temporal: O(2^n), donde n es la cantidad de billetes (i.e. |B|)
Veamos si hay superposición de problemas. Sabemos que hay O(n*c) estados posibles para nuestro algoritmo
Busquemos una cota inferior para las llamadas recursivas a CC(n, c). No es muy complicado darse cuenta que, la cota superior dada antes O(2^n)
Es también una cota inferior.
Por lo que nos queda preguntarnos, es cierto que 2^n >> n*c
Uno está tentado a decir que sí re de una mal, pero acá hay engaña pichanga
Porque c en realidad es un valor de entrada, y en cálculo de complejidad nosotros hablamos del tamaño de la entrada
Por lo que es pseudo-polinomial.
Sabemos que el tamaño de la entrada de c es log(c) (logaritmo en base 2 de c, pero me ahorro la base), luego t = log(c) (es decir, c = 2^t)
Entonces me queda O(2^t * n), y acá se puede ver que es pseudo-polinomial.
Todo esto para decir que hay superposición de problemas sii (2^n)/n >> c.
Y generalmente, esto va a ser cierto. Por lo que podemos aplicar memoización para mejorar la complejidad temporal.
*/
func CC_c(n int, c int) Tuple {
	if c <= 0 {
		return Tuple{0, 0}
	}

	if n <= 0 {
		return Tuple{math.MaxInt32, math.MaxInt32} // (+infinito, +infinito)
	}

	withBill := CC_c(n-1, c-B[n-1])
	withBill.Fst += B[n-1]
	withBill.Snd++

	withoutBill := CC_c(n-1, c)

	return *min(&withBill, &withoutBill)
}
