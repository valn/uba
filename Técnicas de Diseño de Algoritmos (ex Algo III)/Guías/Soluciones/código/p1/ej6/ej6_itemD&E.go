package ej6

import (
	"fmt"
	"math"
)

// Definimos B como variable global
func ej6de() {
	c := 14
	n := len(B)

	// Creo una matriz de n+1 x c+1 inicializada con tuplas vacías
	memoria := make([][]Tuple, n+1)
	for i := range memoria {
		memoria[i] = make([]Tuple, c+1)

		for j := range memoria[i] {
			memoria[i][j] = Tuple{-1, -1}
		}
	}

	result := CC_de(n, c, &memoria)

	// Styling de la salida
	if result.Fst == math.MaxInt32 {
		fmt.Printf("(+inf, +inf)\n")
	} else {
		fmt.Printf("(%d, %d)\n", result.Fst, result.Snd)
	}
}

// Complejidad temporal: O(n*c)
// Approach top-down
func CC_de(n int, c int, memoria *[][]Tuple) Tuple {
	if c <= 0 {
		return Tuple{0, 0}
	}

	if n <= 0 {
		return Tuple{math.MaxInt32, math.MaxInt32} // (+infinito, +infinito)
	}

	if ((*memoria)[n][c] == Tuple{-1, -1}) { // Si no está inicializado
		withBill := CC_de(n-1, c-B[n-1], memoria)
		withBill.Fst += B[n-1]
		withBill.Snd++

		withoutBill := CC_de(n-1, c, memoria)

		(*memoria)[n][c] = *min(&withBill, &withoutBill)
	}

	return (*memoria)[n][c]
}
