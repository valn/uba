package ej6

import (
	"fmt"
	"math"
)

func ej6b() {
	B := []int{2, 3, 5, 10, 20, 20} // Se asume que B es un multiconjunto de enteros positivos, es decir, si tenés dos billetes de 20, entonces B = [2, 3, 5, 10, 20, 20]
	c := 14

	result := CC_b(B, c)

	// Styling de la salida
	if result.Fst == math.MaxInt32 {
		fmt.Printf("(+inf, +inf)\n")
	} else {
		fmt.Printf("(%d, %d)\n", result.Fst, result.Snd)
	}
}

/*
	Función recursiva: https://i.ibb.co/wJWHNsB/Captura-de-pantalla-2024-07-09-033305.png
	Notar que "min" utiliza el orden lexicográfico, es decir, minimiza primero la primera componente y luego la segunda componente en caso de empate.
	A su vez, la operación "+" en tuplas es la suma de enteros componente a componente.

	El orden de los casos de la función recurisva también responde a un orden lexicográfico. Para el caso |B| <= 0 se asume que c > 0, capaz aclararlo a la hora de escribir la función recursiva pueda reducir ambigüedades.
*/
// Complejidad temporal: O(2^n), donde n es la cantidad de billetes (i.e. |B|)
func CC_b(B []int, c int) Tuple {
	n := len(B)

	if c <= 0 {
		return Tuple{0, 0}
	}

	if n <= 0 {
		return Tuple{math.MaxInt32, math.MaxInt32} // (+infinito, +infinito)
	}

	withBill := CC_b(B[:n-1], c-B[n-1])
	withBill.Fst += B[n-1]
	withBill.Snd++

	withoutBill := CC_b(B[:n-1], c)

	return *min(&withBill, &withoutBill)
}
