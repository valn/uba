package ej8

import "math"

// Complejidad espacial y temporal: O(j^2)

func CostoMínTD(C *[]int, memoria *[][]int, i int, j int) int {
	if i >= j {
		return math.MaxInt32
	}

	if count(C, i, j) == 0 {
		return 0
	}

	if (*memoria)[i][j] == -1 {
		minCost := math.MaxInt32
		for _, c := range *C {
			if c > i && c < j {
				costoCorteIzq := CostoMínTD(C, memoria, i, c)
				costoCorteDer := CostoMínTD(C, memoria, c, j)
				minCost = min(minCost, costoCorteIzq+costoCorteDer)
			}
		}
		(*memoria)[i][j] = (j - i) + minCost
	}

	return (*memoria)[i][j]
}
