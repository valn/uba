package ej8

func count(nums *[]int, i, j int) int {
	count := 0
	for _, num := range *nums {
		if num > i && num < j {
			count++
		}
	}
	return count
}
