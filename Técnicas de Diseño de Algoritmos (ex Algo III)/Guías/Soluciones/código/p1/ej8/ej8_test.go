package ej8

import "testing"

func TestCostoMínTopDown(t *testing.T) {
	testCases := []struct {
		name     string
		C        *[]int
		j        int
		expected int
	}{
		{
			name:     "Test Case 1",
			C:        &[]int{2, 4, 7},
			j:        10,
			expected: 20,
		},
		{
			name:     "Test Case 2",
			C:        &[]int{1, 5, 8},
			j:        9,
			expected: 18,
		},
		{
			name:     "Test Case 3",
			C:        &[]int{3, 7, 14},
			j:        15,
			expected: 30,
		},
		{
			name:     "Test Case 4",
			C:        &[]int{5, 10, 15},
			j:        20,
			expected: 40,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			memory := make([][]int, tc.j+1)
			for i := range memory {
				memory[i] = make([]int, tc.j+1)
				for j := range memory[i] {
					memory[i][j] = -1
				}
			}

			result := CostoMínTD(tc.C, &memory, 0, tc.j)
			if result != tc.expected {
				t.Errorf("se obtuvo %d, cuando se esperaba %d", result, tc.expected)
			}
		})
	}
}
