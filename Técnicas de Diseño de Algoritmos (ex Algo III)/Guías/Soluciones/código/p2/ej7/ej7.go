package ej7

func Parejas(A *[]int) int {
	_, parejas := MergeSortCustom(*A)
	return parejas
}

// a = c = 2
// complejidad: n * log n
// pues f(n) = theta(n^log en base c de a) = theta(n^1) = theta(n)
func MergeSortCustom(A []int) ([]int, int) {
	// n = len(A)
	if len(A) <= 1 {
		return A, 0
	}

	m := len(A) / 2

	left, countLeft := MergeSortCustom(A[:m])   // T(n/2)
	right, countRight := MergeSortCustom(A[m:]) // T(n/2)

	slice, count := MergeCustom(left, countLeft, right, countRight) // theta(n)
	// Recorro dos arreglos de longitud n/2, una vez c/u.

	return slice, count
}

func MergeCustom(A []int, countA int, B []int, countB int) ([]int, int) {
	slice := make([]int, len(A)+len(B))
	count := countA + countB

	i := 0
	j := 0
	for i != len(A) && j != len(B) {
		if A[i] <= B[j] {
			slice[i+j] = A[i]
			i++
		} else {
			slice[i+j] = B[j]
			j++
			count++
		}
	}

	for i != len(A) {
		slice[i+j] = A[i]
		i++
	}

	for j != len(B) {
		slice[i+j] = B[j]
		j++
	}

	return slice, count
}
