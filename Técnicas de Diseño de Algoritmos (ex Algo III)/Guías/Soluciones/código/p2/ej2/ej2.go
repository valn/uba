package ej2

func ÍndiceEspejo(A *[]int, low int, high int) bool {
	if low > high {
		return false // Nos pasamos y no encontramos nada
	}

	mid := (low + high) / 2
	pivote := (*A)[mid]

	if mid-pivote > 0 { // Buscamos a la derecha
		return ÍndiceEspejo(A, mid+1, high)
	}

	if mid-pivote < 0 { // Buscamos a la izquierda
		return ÍndiceEspejo(A, low, mid-1)
	}

	return true // Caso mid-pivote == 0, encontramos una solución
}
