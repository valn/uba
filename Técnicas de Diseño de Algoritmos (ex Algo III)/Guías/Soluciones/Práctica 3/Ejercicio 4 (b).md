## Enunciado:
*Demostrar por medio de una reducción al absurdo que todo grafo de n vértices que tenga al menos $\frac{(n-1) \cdot (n-2)}{2} + 2$ aristas es biconexo.*

## Resolución:
Sea $G = (V,E)$, donde $|E| = m_{G}$ y $|V| = n$.
Asumiendo que $m_{G} \geq \frac{(n − 1) \cdot (n − 2)}{2} + 2$, supongamos que $G$ no es biconexo; esto implica que existe $v \in V$ tal que $v$ es un punto de articulación.
De inmediato se deduce que $2 \leq d(v) \leq n-1$, pues $v$ une *al menos* dos componentes conexas.

Sea $G_{-v} = (V_{-v}, E_{_{-v}})$ donde $V_{_{-v}} = V \setminus \{v\}$ y $E_{_{-v}} = E \setminus \{e \in E$ | $v \in e\}$ el grafo sin el vértice distinguido $v \in V$. Por notación llamemos $|E_{_{-v}}| = m_{G - v}$.

Mi idea va a ser operar con $m_{G} \geq \frac{(n − 1) \cdot (n − 2)}{2} + 2$ para llegar a que $m_{G - v} \geq \frac{((n-1) − 1) \cdot ((n-1) − 2)}{2} + 1$, lo que implicaría que $G_{-v}$ es conexo por el item (a).

Desarrollando un poco el polinomio, llegamos a la siguiente igualdad $\frac{((n-1) − 1) \cdot ((n-1) − 2)}{2} + 1 = \frac{n^{2} - 5 \cdot n + 8}{2}$.

Parto de que sé que: $m_{G} \geq \frac{(n − 1) \cdot (n − 2)}{2} + 2$ $\Longleftrightarrow$ $m_{G} - d(v) \geq \frac{(n − 1) \cdot (n − 2)}{2} + 2 - d(v)$ $\Longleftrightarrow$ $m_{G-v} \geq \frac{(n − 1) \cdot (n − 2)}{2} + 2 - d(v)$, pues $m_{G-v} = m_{G} - d(v)$.

Luego, $m_{G-v} \geq \frac{(n − 1) \cdot (n − 2)}{2} + 2 - d(v) \geq \frac{(n − 1) \cdot (n − 2)}{2} + 2 - (n-1)$.

Desarrollando el polinomio, llegamos a la siguiente igualdad
$\frac{(n − 1) \cdot (n − 2)}{2} + 2 - (n-1) = \frac{n^{2} - 5 \cdot n + 8}{2}$.

Luego, $m_{G-v} \geq \frac{(n − 1) \cdot (n − 2)}{2} + 2 - (n-1) = \frac{n^{2} - 5 \cdot n + 8}{2} = \frac{((n-1) − 1) \cdot ((n-1) − 2)}{2} + 1$, lo que implica que $G_{-v}$ es conexo (por item (a)), **¡absurdo!** *dijimos que al sacar $v$ aumentaban las componentes conexas, por lo que no debería ser conexo (debería tener dos componentes o más)*, ¿de dónde vino el absurdo?, de suponer que existía un $v \in V$ que era un punto de articulación.

$\square$
