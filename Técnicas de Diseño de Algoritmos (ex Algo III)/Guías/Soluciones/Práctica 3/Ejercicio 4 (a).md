## Enunciado:
*Demostrar, usando inducción en la cantidad de vértices, que todo grafo de n vértices que tiene más de $\frac{(n-1) \cdot (n-2)}{2}$ aristas es conexo.*

## Resolución:
Sea $G = (V,E)$, donde $|E| = m$ y $|V| = n$.
Defino $P(n)$: $m > \frac{(n-1) \cdot (n-2)}{2} \implies G$ conexo.

Vamos a probarlo por inducción sobre la cantidad de vértices de $G$.

**Caso(s) base (y ejemplitos):**
* $n = 0$: No vale el antecedente, falso implica verdadero, vale.
* $n = 1$: No vale el antecedente, falso implica verdadero, vale.
* $n = 2$: 
  * Si no tiene ninguna arista, no vale el antecedente.
  * Si tiene la única arista que puede tener, vale la propiedad.
De paso sirven como ejemplos para convencer al lector, que vale lo que queremos demostrar.

Tomo de caso base $n = 1$.

**Paso inductivo:**
  * Suponemos la hipótesis inductiva $P(k)$, $\forall 1 \leq k < n$. Es decir, que vale la proposición para todo grafo $G'$ que tenga $k$ vértices.
  * Quiero probar que vale $P(k+1)$.

Sé que si $G$ no cumple el antecedente, entonces mi proposición es trivialmente verdadera.
Asumo que $G$ cumple que: $m > \frac{k \cdot (k-1)}{2}$, y quiero probar que $G$ es conexo.

Si existiese un vértice $v \in V$ tal que $d(v) = 0$, entonces de base el grafo $G$ no es conexo.
Veamos que no existe ningún vértice $v \in V$ tal que $d(v) = 0$.
Sabemos que $m > \frac{k \cdot (k-1)}{2}$, y $|V| = k + 1$. No es casualidad que $m$ es estrictamente mayor a la cantidad de aristas de un grafo $K_{k}$. Es decir, tenemos $k$ vértices, todos conectados entre todos con exactamente $\frac{k \cdot (k-1)}{2}$ aristas, más un vértice distinguido $v$ con $d(v) \geq 1$ pues $m \geq \frac{k \cdot (k-1)}{2} + 1$.

Luego, vale que $1 \leq d(v) \leq k$, $\forall v \in V$.

Sea $G' = (V', E')$ donde $V' = V \setminus \{v\}$ y $E' = E \setminus \{e \in E$ | $v \in e\}$.
Esto implica que $|V'| = k$. Por notación llamemos $m'$ a $|E'|$.

Supongamos que $v \in V$ se conecta a todos los otros vértices de $G$ (es decir, $d(v) = k$). En ese caso $G$ es trivialmente conexo, por lo que vale nuestra proposición sin rechistar.
Supongamos que $v \in V$ no se conecta necesariamente a **todos** los vértices, es decir, $1 \leq d(v) \leq k - 1$.

Vale que $m' = m - d(v) \geq m - (k-1)$ pues en el peor caso $v \in V$ es un vértice que se conecta a casi todos *(por lo que dijimos antes, todos los vértices menos uno (al menos))*.

$m' \geq m - (k-1) > \frac{k \cdot (k-1)}{2} - (k-1)$ pues asumo que vale el antecedente para $G$.

$m' > \frac{k \cdot (k-1)}{2} - (k - 1) = \frac{(k-1) \cdot (k-2)}{2} \implies m' > \frac{(k-1) \cdot (k-2)}{2}$. Por lo que vale el antecedente para $G'$.

Luego, por **HI**, $G'$ es conexo.

Perfecto, ya sabemos que tanto $G$ (por suposición) como $G'$ (por demostración) cumplen el antecedente de nuestra proposición. A su vez por **HI** pudimos concluir que $G'$ es conexo.

Veamos entonces que $G$ es conexo.
Supongamos que tenemos dos vértices cualesquiera $u,w \in V$ tq' $u \neq w$. Separemos en casos.
* Caso $u \neq v$ $\land$ $w \neq v$:
	* Queremos ver que existe camino entre estos dos vértices. Bueno, sabemos existe camino entre $u,w$, ¿por qué?, porque sabemos que esos dos vértices no son $v$; y $G'$ es conexo, por lo que en $G$ hay camino, el mismo que en $G'$.
* Caso $u = v$ $\underline{\lor}$ $w = v$:
  * Sin pérdida de generalidad podemos suponer que $w = v$, pues el caso $u = v$ es análogo.
  * Queremos ver que existe camino entre $v$ y $u$. Sabemos que $1 \leq d(v) \leq k-1$.
    * Caso $\exists (u,v) \in E$: Hay camino trivialmente, la arista $(u,v)$.
    * Caso $\not\exists (u,v) \in E$:
      * Sabemos que no hay arista que te lleve directamente desde $u$ hasta $v$ y viceversa (es grafo, no digrafo). Sin embargo, sabemos que $d(v) \geq 1$, por lo que podemos afirmar que existe una arista $(z,v)$ tal que $z \in V' \subsetneq V$.
      * Sabemos que $z$ es un vértice en $G'$, donde $G'$ es un grafo conexo, por lo que basta ver que existe un camino desde $u$ hasta $z$. Bueno, pero tanto $u$, como $z$ están en $G'$, por lo que caemos en el primer caso, por lo que hay camino: $\{u, \dots, z, v\}$.

$\square$
