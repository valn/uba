## Enunciado:
*¿Se pueden dar cotas mejores que funcionen a partir de algún $n_{0}$? Es decir, ¿existe $c(n) < 1 + \frac{(n−1) \cdot (n−2)}{2}$ (resp. $c(n) < 2 + \frac{(n−1) \cdot (n−2)}{2}$) tal que todo grafo de $n \geq n_{0}$ vértices que tenga al menos $c(n)$ aristas sea conexo (resp. biconexo)?*

## Resolución:
Creo que hay unos teoremitas que dicen que sí.
