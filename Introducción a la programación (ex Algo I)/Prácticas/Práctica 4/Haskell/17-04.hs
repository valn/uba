--1
fibonacci :: Integer -> Integer
fibonacci 0 = 0
fibonacci 1 = 1
fibonacci n = fibonacci (n-1) + fibonacci (n-2)

--2
parteEntera :: Float -> Integer
parteEntera n | (n >= 0) && (n < 1) = 0
              | n >= 1 = parteEntera (n-1) + 1
              | n < 0 = parteEntera (n+1) - 1

--3
esDivisible :: Integer -> Integer -> Bool
esDivisible a b | a == 0 = True
                | a < 0 = False
                | otherwise = esDivisible (a-b) (b)

--4
sumaImpares :: Integer -> Integer
sumaImpares 1 = 1
sumaImpares n = n_esimoImpar + sumaImpares (n-1)
                where n_esimoImpar = 2*n-1

--5
medioFact :: Integer -> Integer
medioFact n | n < 3 = n
            | otherwise = medioFact (n-2) * n

--6
sumaDigitos :: Integer -> Integer
sumaDigitos n | n < 10 = n
              | otherwise = sumaDigitos (div n 10) + mod n 10

--7
todosDigitosIguales :: Integer -> Bool
todosDigitosIguales n | n < 10 = True
                      | posDelDecimo == posDeLaUnidad = todosDigitosIguales (div n 10)
                      | otherwise = False
                      where posDelDecimo = mod (div n 10) 10
                            posDeLaUnidad = mod n 10

--8
iesimoDigito :: Integer -> Integer -> Integer
iesimoDigito n i = mod (div n (10^((cantidadDeDigitos n) - i))) 10

cantidadDeDigitos :: Integer -> Integer
cantidadDeDigitos n | n < 10 = 1
                    | otherwise = cantidadDeDigitos (div n 10) + 1

--9
esCapicua :: Integer -> Bool
esCapicua n | n < 10 = True
            | primerDigito == ultimoDigito = esCapicua (loDeDentro)
            | otherwise = False
            where primerDigito = iesimoDigito n 1
                  ultimoDigito = iesimoDigito n (cantidadDeDigitos n)
                  loDeDentro = div (n - primerDigito*(10^((cantidadDeDigitos n) - 1))) 10

--10
f1 :: Integer -> Integer
f1 0 = 1
f1 n = 2^n + f1 (n-1)

f2 :: Integer -> Float -> Float
f2 1 q = q
f2 n q = q^n + f2 (n-1) q

f3 :: Integer -> Float -> Float
f3 n q = f2 (2*n) q

f4 :: Integer -> Float -> Float
f4 n q = (f3 n q) - (f2 n q) + q^n

--11
eAprox :: Integer -> Float
eAprox 0 = 1
eAprox n = eAprox (n-1) + 1/fromIntegral((factorial n))

factorial :: Integer -> Integer
factorial 0 = 1
factorial n = factorial (n-1) * n

--12
raizDe2Aprox :: Integer -> Float
raizDe2Aprox 1 = 1
raizDe2Aprox n = defRecursiva n - 1

defRecursiva :: Integer -> Float
defRecursiva 1 = 2
defRecursiva n = 2 + (1/(defRecursiva (n-1)))

--13
sumatoriaInterna :: Integer -> Integer -> Integer
sumatoriaInterna i 1 = i
sumatoriaInterna i m = i^m + (sumatoriaInterna i (m-1))

sumatoriaDoble :: Integer -> Integer -> Integer
sumatoriaDoble 1 m = sumatoriaInterna 1 m
sumatoriaDoble n m = sumatoriaInterna n m + sumatoriaDoble (n-1) m

--14
{-
sumaPotenciasOld :: Integer -> Integer -> Integer -> Integer
sumaPotenciasOld q 1 1 = q^2
sumaPotenciasOld q n 1 = q^(n+1) + sumaPotenciasOld q (n-1) 1
sumaPotenciasOld q n m = q^(n+m) + sumaPotenciasOld q n (m-1)
-}

sumaPotenciasInterna :: Integer -> Integer -> Integer -> Integer
sumaPotenciasInterna q i 1 = q^(i+1)
sumaPotenciasInterna q i m = q^(i+m) + (sumaPotenciasInterna q i (m-1))

sumaPotencias :: Integer -> Integer -> Integer -> Integer
sumaPotencias q 1 m = sumaPotenciasInterna q 1 m
sumaPotencias q n m = sumaPotenciasInterna q n m + sumaPotencias q (n-1) m

--15
{-
sumaRacionales2 :: Integer -> Integer -> Float
sumaRacionales2 1 1 = 1
sumaRacionales2 n 1 = fromIntegral(n) + sumaRacionales (n-1) 1
sumaRacionales2 n m = fromIntegral(n)/fromIntegral(m) + sumaRacionales n (m-1)
-}

sumaRacionalesInterna :: Integer -> Integer -> Float
sumaRacionalesInterna p 1 = fromIntegral(p)
sumaRacionalesInterna p m = fromIntegral(p)/fromIntegral(m) + (sumaRacionalesInterna p (m-1))

sumaRacionales :: Integer -> Integer -> Float
sumaRacionales 1 m = sumaRacionalesInterna 1 m
sumaRacionales n m = sumaRacionalesInterna n m + sumaRacionales (n-1) m

--16
-- Obtiene el primer divisor distinto de uno
menorDivisor :: Integer -> Integer
menorDivisor n = menorDivisorHasta n 2

-- Ayuda a la función anterior verificando desde m hasta n (pues n siempre va a tener un divisor, n)
menorDivisorHasta :: Integer -> Integer -> Integer
menorDivisorHasta n m
                     | mod n m == 0 = m
                     | otherwise = menorDivisorHasta n (m+1)

esPrimo :: Integer -> Bool
esPrimo n = n == menorDivisor n

-- Sabemos que (a:b) = (b:a), inclusive en el algoritmo de Euclides pues (a:b) simétrico (visto en la teórica).
-- Si pensamos en (a:b), es indistinto para el algoritmo si a = b, a < b ó a > b
-- Pues si a = b ó a > b el algoritmo funciona de raíz como vimos en la teórica
-- Si a < b (e.g.: (81:90)) volverá a ejecutar el algo. de Euclides con (90: mod 81 90)
-- Y, mod 81 90 = 81 (el resto de dividir a 81 por 90) ent. (90:81) y pasamos al caso a > b
-- Luego, devolvemos siempre a positivo, pues si b es negativo obtenemos el MCD pero en negativo y no es la idea.
-- Otra opción sería "corregir" a y b (i.e. si a y/o b es negativo, pasarlos a positivos y después ejecutar el algoritmo)
algoritmoDeEuclides ::  Integer -> Integer -> Integer
algoritmoDeEuclides a b
                       | b == 0 = abs(a)
                       | otherwise = algoritmoDeEuclides (b) (mod a b)

-- Dos números son coprimos sí y solo sí su MCD es igual a 1.
-- Por el algoritmo de Euclides (visto en la teórica) sabemos que aplicando divisiones sucesivas, se tiene que (a:b) es el último resto no nulo.
sonCoprimos :: Integer -> Integer -> Bool
sonCoprimos n m = algoritmoDeEuclides n m == 1

nEsimoPrimo :: Integer -> Integer
nEsimoPrimo n = nEsimoPrimoAux n 2

nEsimoPrimoAux :: Integer -> Integer -> Integer
nEsimoPrimoAux n p
                  | n == 0 = p-1
                  | esPrimo p = nEsimoPrimoAux (n-1) (p+1)
                  | otherwise = nEsimoPrimoAux n (p+1)

--17
esFibonacci :: Integer -> Bool
esFibonacci n = esFibonacciAux n 0

esFibonacciAux :: Integer ->  Integer -> Bool
esFibonacciAux n m
                  | fibonacci m > n = False
                  | fibonacci m == n = True
                  | otherwise = esFibonacciAux n (m+1)

--18
{-
mayorDigitoPar :: Integer -> Integer
mayorDigitoPar n | 
                  where digitos = cantidadDeDigitos n
                        ultimoDigito = iesimoDigito n digitos
                        esPar = mod n 2 == 0
-}
{-
mayorDigitoIter :: Integer -> Integer -> Integer
mayorDigitoIter n 0 = -1
mayorDigitoIter n i | esElMayorDigito n i = i
                    | otherwise = mayorDigitoIter n (i-1)

esElMayorDigito :: Integer -> Integer -> Bool
esElMayorDigito n 0 = False
esElMayorDigito n d | (esElMayorDigitoIter n d (cantidadDeDigitos n)) = True
                    | otherwise = esElMayorDigito n (d-1)
-}
{-
mayorDigitoPar :: Integer -> Integer
mayorDigitoPar n | mod mayorDig 2 == 0 = mayorDig
                 | otherwise = -1
                 where mayorDig = mayorDigito n

mayorDigito :: Integer -> Integer
mayorDigito n = mayorDigitoIter n 0

mayorDigitoIter :: Integer -> Integer -> Integer
mayorDigitoIter n i | (esElMayorDigito n i (cantidadDeDigitos n)) = i
                    | otherwise = mayorDigitoIter n (i+1)

esElMayorDigito :: Integer -> Integer -> Integer -> Bool
esElMayorDigito n d 0 = True
esElMayorDigito n d c | (d >= iesimoDigito n c) = esElMayorDigito n d (c-1)
                      | otherwise = False
-}

-- n: number; d: digit; q: quantity (of digits)
-- Uso lógica: quiero que si (mod d 2 == 0) => d >= qesimoDigito.
-- Lo que es equivalente a pedir que: (mod d 2 /= 0) || d >= qesimoDigito
esElMayorDigitoPar :: Integer -> Integer -> Integer -> Bool
esElMayorDigitoPar _ _ 0 = True
esElMayorDigitoPar n d q | ((d >= qesimoDigito) && (mod d 2 == 0)) = esElMayorDigitoPar n d (q-1)
                         | otherwise = False
                         where qesimoDigito = iesimoDigito n q