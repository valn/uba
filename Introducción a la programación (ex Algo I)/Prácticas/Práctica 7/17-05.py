import math

# Ej 1

def raizDe2() -> float:
    return round(math.sqrt(2), 4)

def imprimir_hola() -> None:
    print("hola")

def imprimir_un_verso() -> None:
    print("ぜんぶ天気のせいでいいよ\nこの気まずさも倦怠感も\n太陽は隠れながら知らんぷり")
    print()
    print("ガタゴト揺れる満員電車")
    print("すれ違うのは準急列車")
    print("輪郭のない雲の　表情を探してみる")

def factorial(n: int) -> int:
    if n == 0:
        return 1

    return factorial(n-1) * n

# Ej 2

def imprimir_saludo(nombre: str) -> None:
    print("Hola", nombre)

def raiz_cuadrada_de(numbero: float) -> float:
    return math.sqrt(numero)

def imprimir_dos_veces(texto: str) -> None:
    print(2*(texto + " "))

def es_multiplo_de(n: int, m: int) -> bool:
    return n % m == 0

def es_par(numero: int) -> bool:
    return es_multiplo_de(numero, 2)

def cantidad_de_pizzas(comensales: int, min_cant_de_porciones: int) -> int:
    porciones = comensales * min_cant_de_porciones
    return math.ceil(porciones)

# Ej 3

def alguno_es_0(numero1: float, numero2: float) -> bool:
    return (numero1 == 0) or (numero2 == 0)

def ambos_son_0(numero1: float, numero2: float) -> bool:
    return (numero1 == 0) and (numero2 == 0)

def es_nombre_largo(nombre: str) -> bool:
    return (len(nombre) >= 3) or (len(nombre) <= 8)

def es_bisiesto(año: int) -> bool:
    esBisiestoPre2000: bool = es_multiplo_de(año, 400)
    esBisiestoPost2000: bool = (es_multiplo_de(año, 4) and not es_multiplo_de(año, 100))
    return (esBisiestoPre2000 or esBisiestoPost2000)

# Ej 4

# Altura en metros
def peso_pino(altura: int) -> int:
    if altura <= 3:
        return 300*altura
    
    return 900 + 200*(altura - 3)

# Peso en kg
def es_peso_util(peso: int) -> bool:
    return (peso >= 400) or (peso <= 1000)

# Altura en metros
def sirve_pino(altura: int) -> bool:
    return es_peso_util(peso_pino(altura))

# Ej 5

# Me aprovecho de que Python toma los booleanos como 0 y 1
def devolver_el_doble_si_es_par(num: int) -> int:
    res: int = num * (1 + es_par(num))
    return res

def devolver_valor_si_es_par_sino_el_que_sigue(num: int) -> int:
    res: int
    if es_par(num):
        res = num
    else:
        res = num+1
    return res

def devolver_valor_si_es_par_sino_el_que_sigue_2if(num: int) -> int:
    res: int
    if es_par(num):
        res = num
    if not es_par(num):
        res = num+1
    return res

def devolver_el_doble_si_es_multiplo3_el_triple_si_es_multiplo9(num: int) -> int:
    if es_multiplo_de(num, 9):
        return 3*num
    
    if es_multiplo_de(num, 3):
        return 2*num
    
    return num

def longitudDeNombre(nombre: str) -> str:
    if len(nombre) >= 5:
        return "Tu nombre tiene muchas letras!"
    
    return "Tu nombre tiene menos de 5 caracteres"

def agarraLaPala(sexo: str, edad: int) -> str:
    jubilado: bool = (sexo == "F" and edad >= 60) or (sexo == "M" and edad >= 65)
    trabajador: bool = edad >= 18 and not jubilado
    
    if trabajador:
        return "Te toca trabajar"
    
    return "Andá de vacaciones"

# Ej 6

def print_1_10() -> None:
    i: int = 1
    while i <= 10:
        print(i)
        i += 1

def print_10_40_pares() -> None:
    i: int = 10
    while i <= 40:
        if es_par(i):
            print(i)
        i += 1

def print_eco_10() -> None:
    i: int = 0
    while i < 10:
        print("eco")
        i += 1

def cohete(n: int) -> None:
    i: int = n
    while i > 0:
        print(i)
        i -= 1
    print("Despegue")

def viaje(partida: int, llegada: int) -> None:
    i: int = partida-1
    while i >= llegada:
        print("“Viajó un año al pasado, estamos en el año: " + str(i))
        i -= 1

def viaje2(partida: int) -> None:
    llegada: int = -384
    i: int = partida-20
    while i >= llegada:
        if i >= 0:
            print("“Viajó un año al pasado, estamos en el año: " + str(i) + " d.C.")
        else:
            print("“Viajó un año al pasado, estamos en el año: " + str(-i) + " a.C.")
        i -= 20

#Ej 7

def print_1_10_for() -> None:
    for i in range(11):
        print(i)

def print_10_40_pares_for() -> None:
    for i in range(10, 41, 1):
        if es_par(i):
            print(i)

def print_eco_10_for() -> None:
    for i in range(10):
        print("eco")

def cohete_for(n: int) -> None:
    for i in range(n, 0, -1):
        print(i)
    print("Despegue")

def viaje_for(partida: int, llegada: int) -> None:
    for i in range(partida-1, llegada-1, -1):
        print("“Viajó un año al pasado, estamos en el año: " + str(i))

def viaje2_for(partida: int) -> None:
    llegada: int = -384
    
    for i in range(partida-20, llegada-1, -20):
        if i >= 0:
            print("“Viajó un año al pasado, estamos en el año: " + str(i))
        else:
            print("“Viajó un año al pasado, estamos en el año: " + str(-i) + " a.C.")

# Ej 8
# ???

# Ej 9
def rt(x: int, g:int) -> int:
    g = g + 1
    return x + g

g: int = 0
def ro(x: int) -> int:
    global g
    g = g + 1
    return x + g

# Respuestas:
# 1) por cada evaluación de ro(1) se va a usar 1 a la variable global g. Cambia lo que devuelve la función
# 2) al ser una variable local, evaluar una vez rt(1,0) o 3 veces no modifica nada globalmente
# el resultado es el mismo.
# 3) (ponele que hecho)
# 4) (preguntar, hay cosas que no vimos)
