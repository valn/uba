-- Ejercicio 1

--1
longitud :: [t] -> Integer
longitud [] = 0
longitud (x:xs) = longitud xs + 1

--2
ultimo :: [t] -> t
ultimo [x] = x
ultimo (x:xs) = ultimo xs

--3
principio :: [t] -> [t]
principio [x] = []
principio (x:xs) = x:(principio xs)

--4
reverso :: [t] -> [t]
reverso [] = []
reverso (x:xs) = (reverso xs) ++ [x]

-- Ejercicio 2

--1
pertenece :: (Eq t) => t -> [t] -> Bool
pertenece _ [] = False
pertenece n (x:xs) = ((n == x) || pertenece n xs)

--2
todosIguales :: (Eq t) => [t] -> Bool
todosIguales [] = True
todosIguales [x] = True
todosIguales (x:xs) = ((pertenece x xs) && (todosIguales xs))

--3
todosDistintos :: (Eq t) => [t] -> Bool
todosDistintos [] = True
todosDistintos (x:xs) = (not (pertenece x xs) && (todosDistintos xs))
--todosDistintos (x:xs) | pertenece x xs = False
--                      | otherwise = todosDistintos xs

--4
hayRepetidos :: (Eq t) => [t] -> Bool
hayRepetidos xs = not (todosDistintos xs)

--5
quitar :: (Eq t) => t -> [t] -> [t]
quitar _ [] = []
quitar n (x:xs) | n == x = xs
                | otherwise = x:(quitar n xs)

--6
quitarTodos :: (Eq t) => t -> [t] -> [t]
quitarTodos _ [] = []
quitarTodos n (x:xs) | n == x = quitarTodos n xs
                     | otherwise = x:(quitarTodos n xs)

--7
eliminarRepetidos :: (Eq t) => [t] -> [t]
eliminarRepetidos [] = []
eliminarRepetidos (x:xs) = x:quitarTodos x (eliminarRepetidos xs)

cantidadDeApariciones :: (Eq t) => t -> [t] -> Integer
cantidadDeApariciones _ [] = 0
cantidadDeApariciones n (x:xs) | n == x = 1 + cantidadDeApariciones n xs
                               | otherwise = cantidadDeApariciones n xs

--8
mismosElementos :: (Eq t) => [t] -> [t] -> Bool
mismosElementos l u = ((listaContenida l u) && (listaContenida u l))

listaContenida :: (Eq t) => [t] -> [t] -> Bool
listaContenida [] _ = True
listaContenida (x:xs) l = ((pertenece x l) && (listaContenida xs l))

--9
capicua :: (Eq t) => [t] -> Bool
capicua [] = True
capicua [x] = True
capicua (x:xs) = ((x == ultimo xs) && (capicua (principio xs)))

-- Ejercicio 3

--1
sumatoria :: (Num t) => [t] -> t
sumatoria [] = 0
sumatoria (x:xs) = x + sumatoria xs

--2
productoria :: [Integer] -> Integer
productoria [] = 1
productoria (x:xs) = x * productoria xs

--3
maximo :: [Integer] -> Integer
maximo [x] = x
maximo (x:xs) | x > maximo xs = x
              | otherwise = maximo xs

--4
sumarN :: Integer -> [Integer] -> [Integer]
sumarN _ [] = []
sumarN n (x:xs) = (x+n):(sumarN n xs)

--5
sumarElPrimero :: [Integer] -> [Integer]
sumarElPrimero (x:xs) = sumarN x (x:xs)

--6
sumarElUltimo :: [Integer] -> [Integer]
sumarElUltimo l = sumarN (ultimo l) l

--7
pares :: [Integer] -> [Integer]
pares [] = []
pares (x:xs) | mod x 2 == 0 =  x:(pares xs)
             | otherwise = pares xs

--8
multiplosDeN :: Integer -> [Integer] -> [Integer]
multiplosDeN _ [] = []
multiplosDeN n (x:xs) | mod x n == 0 =  x:(multiplosDeN n xs)
                      | otherwise = multiplosDeN n xs

--9
ordenar :: [Integer] -> [Integer]
ordenar [] = []
ordenar l = min:(ordenar (quitar min l))
            where min = minimo l

minimo :: [Integer] -> Integer
minimo [x] = x
minimo (x:xs) | x < minimo xs = x
              | otherwise = minimo xs

-- Ejercicio 4

--1
sacarBlancosRepetidos :: [Char] -> [Char]
sacarBlancosRepetidos [] = []
sacarBlancosRepetidos [x] = [x]
sacarBlancosRepetidos (x:xs) | x == ' ' && head xs == ' ' = sacarBlancosRepetidos (' ' : tail xs)
                             | otherwise = x:sacarBlancosRepetidos xs

sacarPrimerBlanco :: [Char] -> [Char]
sacarPrimerBlanco [] = []
sacarPrimerBlanco (x:xs) | x == ' ' = xs
                         | otherwise = x:xs

sacarUltimoBlanco :: [Char] -> [Char]
sacarUltimoBlanco [] = []
sacarUltimoBlanco (x:xs) | longitud (x:xs) == 1 && x == ' ' = []
                         | otherwise = x:sacarUltimoBlanco xs

sacarBlancosFeos :: [Char] -> [Char]
sacarBlancosFeos palabras = sacarPrimerBlanco (sacarUltimoBlanco palabras)

--2
contarPalabras :: [Char] -> Integer
contarPalabras [] = 0
contarPalabras [x] | x == ' ' = 0
                   | otherwise = 1
contarPalabras palabras = contarEspacios (sacarBlancosFeos (sacarBlancosRepetidos palabras)) + 1

contarEspacios :: [Char] -> Integer
contarEspacios [] = 0
contarEspacios (x:xs) | x == ' ' = 1 + contarEspacios xs
                      | otherwise = contarEspacios xs

--3
palabraMasLarga :: [Char] -> [Char]
palabraMasLarga [] = []
palabraMasLarga p = palabraMasLargaDeListaDePalabras (palabras p)

palabraMasLargaDeListaDePalabras :: [[Char]] -> [Char]
palabraMasLargaDeListaDePalabras [] = []
palabraMasLargaDeListaDePalabras (frase:frases) | longitud frase >= longitud palabraMasLargaDelRestoDePalabras = frase
                                                | otherwise = palabraMasLargaDeListaDePalabras frases
                                                  where palabraMasLargaDelRestoDePalabras = palabraMasLargaDeListaDePalabras frases
--4
palabras :: [Char] -> [[Char]]
palabras p = separarPalabras p [] 

separarPalabras :: [Char] -> [Char] -> [[Char]]
separarPalabras [] palabraActual = [palabraActual]
separarPalabras (x:xs) palabraActual | x /= ' ' = separarPalabras xs (palabraActual ++ [x])
                                     | otherwise = palabraActual:separarPalabras xs []

--5
{-
aplanar :: [[Char]] -> [Char]
aplanar [] = []
aplanar (x:xs) = x ++ aplanar xs
-}
aplanar :: [[Char]] -> [Char]
aplanar palabras = aplanarConNBlancos palabras 0

--6
{-
aplanarConBlancos :: [[Char]] -> [Char]
aplanarConBlancos [] = []
aplanarConBlancos [x] = x
aplanarConBlancos (x:xs) = x ++ [' '] ++ aplanar xs
-}
aplanarConBlancos :: [[Char]] -> [Char]
aplanarConBlancos palabras = aplanarConNBlancos palabras 1

--7
aplanarConNBlancos :: [[Char]] -> Integer -> [Char]
aplanarConNBlancos [] _ = []
aplanarConNBlancos [x] _ = x
aplanarConNBlancos (x:xs) n = x ++ blancos n ++ aplanar xs

blancos :: Integer -> [Char]
blancos 0 = []
blancos n = ' ':blancos (n-1)

-- Ejercicio 5

--1 (Tuve que buscar un algoritmo (genérico) en internet para poder implementarlo en Haskell)
-- Igual me di cuenta que tiene sentido por el algoritmo que vimos en Álgebra
nat2bin :: Integer -> [Integer]
nat2bin x | x < 2 = [x]
          | otherwise = nat2bin (div x 2) ++ [(mod x 2)]

--2 (Este sale fácil tras haber cursado Álgebra)
bin2nat :: [Integer] -> Integer
bin2nat [] = 0
bin2nat (x:xs) = (2^exp * x) + bin2nat xs
            where exp = longitud (x:xs) - 1

--3
nat2hex :: Integer -> [Char]
nat2hex x = intToCharList (base16 x)

intToCharHex :: Integer -> [Char]
intToCharHex x | x == 0 = ['0']
               | x == 1 = ['1']
               | x == 2 = ['2']
               | x == 3 = ['3']
               | x == 4 = ['4']
               | x == 5 = ['5']
               | x == 6 = ['6']
               | x == 7 = ['7']
               | x == 8 = ['8']
               | x == 9 = ['9']
               | x == 10 = ['A']
               | x == 11 = ['B']
               | x == 12 = ['C']
               | x == 13 = ['D']
               | x == 14 = ['E']
               | x == 15 = ['F']
               | x > 16 = (intToCharHex (div x 16) ++ intToCharHex (mod x 16))

intToCharList :: [Integer] -> [Char]
intToCharList [] = []
intToCharList (x:xs) = (intToCharHex x) ++ (intToCharList (xs))

base16 :: Integer -> [Integer]
base16 x | x < 16 = [x]
         | otherwise = base16 (div x 16) ++ [(mod x 16)]

--4
sumaAcumulada :: (Num t) => [t] -> [t]
sumaAcumulada [x] = [x]
sumaAcumulada l = sumaAcumulada (principio l) ++ [sumatoria l]

--5
descomponerEnPrimos :: [Integer] -> [[Integer]]
descomponerEnPrimos [] = []
descomponerEnPrimos (x:xs) = (descompEnPrimosNum x):(descomponerEnPrimos xs)

descompEnPrimosNum :: Integer -> [Integer]
descompEnPrimosNum x = descompEnPrimosNumIter x 1

descompEnPrimosNumIter :: Integer -> Integer -> [Integer]
descompEnPrimosNumIter 1 _ = []
descompEnPrimosNumIter x n | mod x p == 0 = p:(descompEnPrimosNumIter (div x p) n)
                           | otherwise = descompEnPrimosNumIter x (n+1)
                            where p = nEsimoPrimo n

nEsimoPrimo :: Integer -> Integer
nEsimoPrimo n = nEsimoPrimoAux n 2

nEsimoPrimoAux :: Integer -> Integer -> Integer
nEsimoPrimoAux n p
                  | n == 0 = p-1
                  | esPrimo p = nEsimoPrimoAux (n-1) (p+1)
                  | otherwise = nEsimoPrimoAux n (p+1)

menorDivisor :: Integer -> Integer
menorDivisor n = menorDivisorHasta n 2

menorDivisorHasta :: Integer -> Integer -> Integer
menorDivisorHasta n m
                     | mod n m == 0 = m
                     | otherwise = menorDivisorHasta n (m+1)

esPrimo :: Integer -> Bool
esPrimo n = n == menorDivisor n

-- Ejercicio 6

type Set a = [a]

--1
agregarATodos :: Integer -> Set (Set Integer) -> Set (Set Integer)
agregarATodos _ [] = []
agregarATodos n (cs:css) = (agregar n cs):(agregarATodos n css)

--2
partes :: Integer -> Set (Set Integer)
partes 0 = [[]]
partes n = partesHasta n (partes (n-1)) ++ (partes (n-1))

partesHasta :: Integer -> Set (Set Integer) -> Set (Set Integer)
partesHasta n [] = []
partesHasta n (cs:css) = (agregar n cs):(partesHasta n css)

agregar :: Integer -> Set Integer -> Set Integer
agregar x a
           | elem x a = a
           | otherwise = x:a

--3
productoCartesiano :: Set Integer -> Set Integer -> Set (Integer, Integer)
productoCartesiano as [] = []
productoCartesiano [] bs = []
productoCartesiano (a:as) bs = (productoCartesiano as bs) ++ (parOrdenado a bs)

parOrdenado :: Integer -> Set Integer -> Set (Integer, Integer)
parOrdenado a [] = []
parOrdenado a (b:bs) = [(a,b)] ++ parOrdenado a bs
