-- Ejercicio 1

-- a
f :: Integer -> Integer
f 1 = 8
f 4 = 131
f 16 = 16

-- b
g :: Integer -> Integer
g 8 = 16
g 16 = 4
g 131 = 1

-- c
h :: Integer -> Integer
h x = f(g(x))

k :: Integer -> Integer
k x = g(f(x))

-- Ejercicio 2

--a
{- Especificación:
problema absoluto (x : Z) : Z {
    asegura: {res = |x|}
}
-}

-- Aplicamos módulo definición de la función matemática
absoluto :: Integer -> Integer
absoluto x | x >= 0 = x
           | otherwise = -x

--b
{- Especificación:
problema maximoabsoluto (x : Z, y : Z) : Z {
    asegura: {if |x| >= |y| then res = |x| else res = |y| fi}
}
-}

maximoabsoluto :: Integer -> Integer -> Integer
maximoabsoluto x y | absX >= absY = absX
                   | otherwise = absY
                    where absX = absoluto x
                          absY = absoluto y

--c
{- Especificación:
problema maximo3(x : Z, y : Z, z : Z) : Z {
    asegura: { res = maximoabsoluto(maximoabsoluto(x,y),z) }
}
-}

maximo3 :: Integer -> Integer -> Integer -> Integer
maximo3 x y z = maximoabsoluto (maximoabsoluto x y) z

--d
{- Especificación:
problema algunoEs0(a : Q, b : Q) : Bool {
    asegura: { res = True <-> a = 0 v b = 0 }
}
-}

-- pattern matching
algunoEs0PM :: Float -> Float -> Bool
algunoEs0PM 0 _ = True
algunoEs0PM _ 0 = True
algunoEs0PM _ _ = False

-- otra / guardas
algunoEs0 :: Float -> Float -> Bool
algunoEs0 a b = (a == 0) || (b == 0)

--e
{- Especificación:
problema ambosSon0(a : Q, b : Q) : Bool {
    asegura: { res = True <-> a = 0 ^ b = 0 }
}
-}

-- pattern matching
ambosSon0PM :: Float -> Float -> Bool
ambosSon0PM 0 0 = True
ambosSon0PM _ _ = False

-- otra / guardas
ambosSon0 :: Float -> Float -> Bool
ambosSon0 a b = (a == 0) && (b == 0)

--f
{- Especificación:
problema mismoIntervalo(a : R, b : R) : Bool {
    asegura: { res = True <-> ((a,b ∈ (-inf, 3]) v (a,b ∈ (3, 7]) v (a,b ∈ (7, inf))) }
}
-}

mismoIntervalo :: Integer -> Integer -> Bool
mismoIntervalo a b | (a <= 3) && (b <= 3) = True
                   | ((a > 3) && (a <= 7)) && (b > 3) && (b <= 7) = True
                   | (7 < a) && (7 < b) = True
                   | otherwise = False

--g
{- Especificación:

Me morí escribiendo en LaTeX
(\exists l : seq<\mathds{Z}>)(l = <a,b,c> ∧ (res = \sum_{j=0}^{2} l[j] - (#apariciones(l[j],l) - 1) * l[j]))
https://www.unicodeit.net/?(%5Cexists%20l%20%3A%20seq%3C%5Cmathds%7BZ%7D%3E)(l%20%3D%20%3Ca%2Cb%2Cc%3E%20%E2%88%A7%20(res%20%3D%20%5Csum_%7Bj%3D0%7D%5E%7B2%7D%20l%5Bj%5D%20-%20(%23apariciones(l%5Bj%5D%2Cl)%20-%201)%20*%20l%5Bj%5D))

problema sumaDistintos(a : Z, b : Z, c : Z) : Z {
    asegura: {(∃ l : seq<ℤ>)(l = <a,b,c> ∧ (res = ∑ⱼ₌₀² l[j] − (#apariciones(l[j],l) − 1) * l[j]))}
}

#apariciones fue especificada en una práctica anteriormente.

(!!!) Mi especificación no es completamente coherente con mi algoritmo. Creo que el enunciado es confuso, hay que revisar.
Ej, <1,2,2>:
Especificación: 1
Algoritmo: 3
-}

cantidadDeApariciones :: Integer -> [Integer] -> Integer
cantidadDeApariciones _ [] = 0
cantidadDeApariciones n ( l : ls ) | n == l = cantidadDeApariciones n ls + 1
                                   | otherwise = cantidadDeApariciones n ls + 0

sumaRepetidos :: [Integer] -> Integer
sumaRepetidos [] = 0
sumaRepetidos l | (cantidadDeApariciones (head l) l) > 1 = (head l) + sumaRepetidos (tail l)
                | otherwise = sumaRepetidos (tail l)

sumaDistintos ::  Integer -> Integer -> Integer -> Integer
sumaDistintos a b c = a+b+c - sumaRepetidos([a,b,c])

--h
{- Especificación:
problema esMultiploDe(a : N, b : N) : Bool {
    asegura: { res = True <-> mod a b = 0 }
}
-}

esMultiploDe :: Integer -> Integer -> Bool
esMultiploDe a b = mod a b == 0

--i
{- Especificación:
problema digitoUnidades(n : N) : N {
    asegura: { res = mod (div n 10) 10 }
}
-}

digitoUnidades :: Integer -> Integer
digitoUnidades n = mod n 10

digitoDecenas :: Integer -> Integer
digitoDecenas n = digitoUnidades (div n 10)

-- Ejercicio 3

{-
Puedo arrancar reescribiendo mi expresión. En lugar de usar a^2 + a*b*k = 0 puedo decir a^2 = (-1)*a*b*k
O lo que es lo mismo, ya que a != 0, a = (-1)*b*k, y ya que k pertenece a Z, puedo decir: a = b*k' (k' = -k)
Y esta es la expresión para decir que a es un múltiplo de b.

Luego,
-}

estanRelacionados :: Integer -> Integer -> Bool
estanRelacionados a b = esMultiploDe a b

-- Ejercicio 4

--a
{-
Especificación:
problema prodInt(v : (RxR), u : (RxR)) : R {
    asegura: { res = v_0 * u_0 + v_1 * u_1 }
}
-}

prodInt :: (Float,Float) -> (Float,Float) -> Float
prodInt (vx, vy) (ux, uy) = vx*ux + vy*uy

--b
{-
Especificación:
problema todoMenor(v : (RxR), u : (RxR)) : Bool {
    asegura: { res = True <-> v_0 < u_0 ^ v_1 < u_1 }
}
-}

todoMenor :: (Float,Float) -> (Float,Float) -> Bool
todoMenor (vx, vy) (ux, uy) = vx < ux && vy < uy

--c
{-
Especificación:
problema distanciaPuntos(v : (RxR), u : (RxR)) : R {
    asegura: { res = sqrt((u_0 - v_0)^2 + (u_1 - v_1)^2) }
}
-}

distanciaPuntos :: (Float,Float) -> (Float,Float) -> Float
distanciaPuntos (vx, vy) (ux, uy) = sqrt( (ux - vx)^2 + (uy - vy)^2 )

--d
{-
Especificación:
problema sumaTerna(v : (ZxZxZ)) : Z {
    asegura: { res = v_0 + v_1 + v_2 }
}
-}

sumaTerna :: (Integer,Integer,Integer) -> Integer
sumaTerna (x,y,z) = x+y+z

--e
{-
res = \sum_{i=0}^{2} if mod v_i n = 0 then n else 0 fi
https://www.unicodeit.net/?res%20%3D%20%5Csum_%7Bi%3D0%7D%5E%7B2%7D%20if%20mod%20v_i%20n%20%3D%200%20then%20n%20else%200%20fi

Especificación:
problema sumarSoloMultiplos(v : (ZxZxZ), n : N) : Z {
    asegura: { res = ∑ᵢ₌₀² if mod vᵢ n = 0 then n else 0 fi }
}
-}

-- Me aprovecho de que el único múltiplo del 1 es el mismo para el caso base
-- Luego, lo voy poniendo en los lugares que ya recorrí para no repetir
sumarSoloMultiplos :: (Integer,Integer,Integer) -> Integer -> Integer
sumarSoloMultiplos (x,y,z) 1 = x+y+z
sumarSoloMultiplos (x,y,z) n | esMultiploDe x n = x + sumarSoloMultiplos (1,y,z) n
                             | esMultiploDe y n = y + sumarSoloMultiplos (x,1,z) n
                             | esMultiploDe z n = z + sumarSoloMultiplos (x,y,1) n
                             | otherwise = 0

--d
{-
Especificación (me aprovecho de la lógica de cortocircuito):
problema posPrimerPar(v : (ZxZxZ)) : Z {
    asegura: { ((v_0 mod 2 = 0) <=> res = 1) v (v_1 mod 2 = 0) <=> res = 2) v (v_2 mod 2 = 0) <=> res = 3) v (res = 4)) }
}
-}

posPrimerPar :: (Integer,Integer,Integer) -> Integer
posPrimerPar (x,y,z) | mod x 2 == 0 = 1
                     | mod y 2 == 0 = 2
                     | mod z 2 == 0 = 3
                     | otherwise = 4

--g
{-
Especificación:
problema crearPar(x : A, y : B)) : (AxB) {
    asegura: { res = (x,y) }
}
-}

crearPar :: tx -> ty -> (tx,ty)
crearPar x y = (x,y)

--h
{-
Especificación:
problema invertir(v : AxB) : (BxA) {
    asegura: { res = (v_1,v_0) }
}
-}

invertir :: (tx,ty) -> (ty,tx)
invertir (x,y) = (y,x)

-- Ejercicio 5
f5 :: Integer -> Integer
f5 n | n <= 7 = n^2
     | otherwise = 2*n-1

g5 :: Integer -> Integer
g5 n | mod n 2 == 0 = div n 2
     | otherwise = 3*n+1

todosMenores :: (Integer, Integer, Integer) -> Bool
todosMenores (n1,n2,n3) | (f5(n1) > g5(n1)) && (f5(n2) > g5(n2)) && (f5(n3) > g5(n3)) = True
                        | otherwise = False

-- Ejercicio 6
bisiesto :: Integer -> Bool
bisiesto year = not ((not (esMultiploDe year 4)) || ((esMultiploDe year 100) && (not (esMultiploDe year 400))))

-- Ejercicio 7
distanciaManhattan :: (Float, Float, Float) -> (Float, Float, Float) -> Float
distanciaManhattan (p1,p2,p3) (q1,q2,q3) = distanciaPuntos (p1,0) (q1,0) + distanciaPuntos (p2,0) (q2,0) + distanciaPuntos (p3,0) (q3,0)

-- Ejercicio 8
sumaUltimosDosDigitos :: Integer -> Integer
sumaUltimosDosDigitos x = (mod x 10) + mod (div x 10) 10

comparar :: Integer -> Integer -> Integer
comparar a b | (sumaUltimosDosDigitos a) < (sumaUltimosDosDigitos b) = 1
             | (sumaUltimosDosDigitos a) > (sumaUltimosDosDigitos b) = -1
             | (sumaUltimosDosDigitos a) == (sumaUltimosDosDigitos b) = 0

-- Ejercicio 9
-- TODO