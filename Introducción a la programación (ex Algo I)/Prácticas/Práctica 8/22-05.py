import random
import numpy as np

# Ejercicio 1:

#1
def pertenece(s: list, e) -> bool:
    res: bool = False
    for elem in s:
        res = res or (elem == e)
    return res

#2
def divideATodos(s: list, e: int) -> bool:
    res: bool  = True
    for elem in s:
        res = res and (elem % e == 0)
    return res

#3
def sumaTotal(s: list) -> int:
    res: int = 0
    for e in s:
        res += e
    return res

#4
def ordenados(s: list) -> bool:
    res: bool = True
    for i in range(len(s)-1):
        res = res and (s[i] < s[i+1])
    return res

#5
def algunaLongitudSiete(s: list) -> bool:
    res: bool = False
    for palabra in s:
        res = res or (len(palabra) > 7)
    return res

#6
def invertir(str: str) -> str:
    invertida: str = ""
    for i in range(len(str)-1, -1, -1):
        invertida = invertida + str[i]
    return invertida

def palindroma(palabra: str) -> bool:
    return palabra == invertir(palabra)

#7
digitos: list = ["0","1","2","3","4","5","6","7","8","9"]

def esMinuscula(letra: str) -> bool:
    return "a"<=letra<="z"

def esMayuscula(letra: str) -> bool:
    return not esMinuscula(letra)

def tieneAlgunaMinuscula(palabra: str) -> bool:
    res: bool = False
    for letra in palabra:
        res = res or esMinuscula(letra)
    return res

def tieneAlgunaMayuscula(palabra: str) -> bool:
    res: bool = False
    for letra in palabra:
        res = res or esMayuscula(letra)
    return res

def tieneAlgunDigito(str: str) -> bool:
    res: bool = False
    for letra in str:
        res = res or pertenece(digitos, letra)
    return res

def fortaleza(contraseña: str) -> str:
    colores: list = ["ROJA", "AMARILLA", "VERDE"]
    res: str
    
    if len(contraseña) < 5:
        res = colores[0]
    elif len(contraseña) > 8 and tieneAlgunaMayuscula(contraseña) and tieneAlgunaMinuscula(contraseña) and tieneAlgunDigito(contraseña):
        res = colores[2]
    else:
        res = colores[1]
     
    return res

#8
def historialBancario(movimientos: list) -> int:
    res: int = 0
    for transaccion in movimientos:
        if transaccion[0] == "I":
            res += transaccion[1]
        else:
            res -= transaccion[1]
    return res

#9
vocales: list = ["a","e","i","o","u"]

def esVocal(letra: str) -> bool:
    return pertenece(vocales, letra)

def tresVocalesDistintas(palabra: str) -> bool:
    vocalesGuardadas: list = []
    for letra in palabra:
        if esVocal(letra) and not pertenece(vocalesGuardadas, letra):
            vocalesGuardadas.append(letra)
    return len(vocalesGuardadas) > 3

# Ejercicio 2:
def ceroEnPares(lista: list([float])) -> list:
    for i in range(len(lista)):
        if (i % 2 != 0):
            lista[i] = 0
    return lista

def ceroEnPares2(lista: list([float])) -> list:
    res: list([float]) = lista
    for i in range(len(lista)):
        if (i % 2 != 0):
            res[i] = 0
    return res

def textoSinVocales(palabra: str) -> str:
    res: str = ""
    for letra in palabra:
        if not esVocal(letra):
            res = res + letra
    return res

def reemplazaVocales(palabra: str) -> str:
    res: str = ""
    for letra in palabra:
        if not esVocal(letra):
            res = res + letra
        else:
            res = res + "_"
    return res

def daVueltaStr(string: str) -> str:
    return invertir(string)

# Ejercicio 3

def listaDeEstudiantes() -> list([str]):
    salida: str = "listo"
    res: list([str]) = []
    while not pertenece(res, salida):
        estudiante: str = input("Nombre del estudiante (\"" + salida + "\" para salir): ")
        res.append(estudiante)
    res.remove(salida)
    return res

def simulacion() -> list:
    historial: list([(str, float)]) = []
    paso: str
    monto: float
    while True:
            paso = input(("C/D/X: ")).upper()
            if paso != "X":
                monto = input(("Monto: "))
                historial.append((paso, monto))
            else:
                return historial

def numAleatorioSin8y9() -> int:
    numero: int = random.randint(1,12)
    while pertenece([8,9], numero):
        numero: int = random.randint(1,12)
    return numero

def valorDeCarta(carta: int) -> float:
    if pertenece([10,11,12], carta):
        return 0.5
    return carta

def sieteYMedio() -> list([int]):
    historial: list[(int)] = []
    carta: int = numAleatorioSin8y9()
    valor: int = valorDeCarta(carta)
    historial.append(carta)
    while True:
        if valor > 7.5:
            print("Perdiste ;(. Valor obtenido: " + str(valor))
            return historial
        
        decision = input("C/P: ").upper() #C: Sacar carta, P: Plantarse
        
        if decision == "P":
            print("Ganaste :D. Valor obtenido: " + str(valor))
            return historial
        
        carta = numAleatorioSin8y9()
        valor += valorDeCarta(carta)
        historial.append(carta)

# Ejercicio 4

def perteneceACadaUno(s: list([list([int])]), e: int) -> list([bool]):
    res: list([bool]) = []
    for i in range(len(s)):
        if pertenece(s[i], e):
            res.append(True)
        else:
            res.append(False)
    return res

def esMatriz(matriz: list([list([int])])) -> bool:
    res: bool = len(matriz) > 0
    for fila in matriz:
        columnas: int = len(matriz[0])
        filas: int = len(fila)
        res = res and (filas == columnas)
    return res

def filasOrdenadas(matriz: list([list([int])])) -> list([bool]):
    res: list([bool]) = []
    for fila in matriz:
        if ordenados(fila):
            res.append(True)
        else:
            res.append(False)
    return res

# requiere: v y u misma dimensión...
def productoPunto(v: list([float]), u: list([float])) -> float:
    res: float = 0
    for i in range(len(v)):
        res += v[i] * u[i]
    return res

# requiere: matriz cuadrada...
#def transponerMatriz(matriz: list([list([float])])) -> list([list([float])]):
#    res: list([list([float])]) = []
#    for i in range(len(matriz)):
#        matriz[0][i]
        

#def elevarMatiz(d: int, p: float) -> list([list([float])]):
#    m: numpy.ndarray = np.random.randint(0,21, (d, d))
#    columnasRecorridas: int = 0
#    
#    for i in range(len(m)): # índice de la fila (en qué fila estoy?)
#        for j in range(len(m[i])): # índice del valor de la fila (en qué valor estoy?)
#            valorFila: int = m[i][j] # valor de la fila (el valor en sí)
#            m[i][j] = 0
#            valor = valorFila * m[j][columnasRecorridas]
#            m[i][j] += valor

# def elevarMatiz(d: int, p: float) -> list([list([float])]):    
#     matriz: np.ndarray = np.random.randint(0,21, (d, d))
#     matrizTranspuesta: np.ndarray = np.transpose(matriz)
#     res: np.ndarray = np.random.random((d, d))
    
#     while p >= 1:
#         for i in range(len(matriz)):
#             for j in range(len(matrizTranspuesta)):
#                 res[i][j] = productoPunto(matriz[i], matrizTranspuesta[j])
#         p -= 1
#     return res

# requiere: A y B ambas matrices de dimensión R^dxd
def productoDeMatrices(A: list([list([float])]), B: list([list([float])])) -> list([list([float])]):
    d: int = len(A)
    res = np.random.randint(0,1, (d, d))
    
    for i in range(d):
        for j in range(d):
            for n in range(d):
                res[i][j] += A[i][n] * B[n][j]
                
    return res

# Voy a asumir que el enunciado está mal y en realidad p es un int pues:
# Para elevar una matriz a un exponente fraccionario en Python sin utilizar librerías
# Puedo usar estrategia basada en la diagonalización de la matriz
# Sea A la matriz. Quiero encontrar P y D tal que: A = P * D * P^(-1)
# donde D es una matriz diagonal con los autovalores de A en la diagonal,
# y P es la matriz cuyas columnas son los autovectores correspondientes.
# Luego, elevo cada elemento de la diagonal de D al exponente p.
# Y luego obtengo A^p.
# Ahora, esto es un curso de algoritmos y estructuras de datos, no una clase de álgebra lineal.
# Por lo que sospecho que tuvieron un error al escribir el enunciado.
# A su vez, sospecho que pensaron únicamente en p positivo
# Pues si p es negativo entramos en la inversa, y no todas las matrices tienen inversa...
def elevarMatiz(d: int, p: int) -> list([list([float])]):
    matriz = np.random.randint(0,21, (d, d))
    res = matriz
    
    if p > 1:
        for _ in range(p-1):
            res = productoDeMatrices(res, matriz)
        
    return res

print(elevarMatiz(2,1))