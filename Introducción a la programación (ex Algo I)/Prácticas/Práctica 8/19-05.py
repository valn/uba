#1
def pertenece(s: list, e) -> bool:
    res: bool = False
    for elem in s:
        res = res or (elem == e)
    return res

#2
def divideATodos(s: list, e: int) -> bool:
    res: bool  = True
    for elem in s:
        res = res and (elem % e == 0)
    return res

#3
def sumaTotal(s: list) -> int:
    res: int = 0
    for e in s:
        res += e
    return res

#4
def ordenados(s: list) -> bool:
    res: bool = True
    for i in range(len(s)-1):
        res = res and (s[i] < s[i+1])
    return res

#5
def algunaLongitudSiete(s: list) -> bool:
    res: bool = False
    for palabra in s:
        res = res or (len(palabra) > 7)
    return res

#6
def invertir(str: str) -> str:
    invertida: str = ""
    for i in range(len(str)-1, -1, -1):
        invertida = invertida + str[i]
    return invertida

def palindroma(palabra: str) -> bool:
    return palabra == invertir(palabra)

#7
digitos: list = ["0","1","2","3","4","5","6","7","8","9"]

def esMinuscula(letra: str) -> bool:
    return "a"<=letra<="z"

def esMayuscula(letra: str) -> bool:
    return not esMinuscula(letra)

def tieneAlgunaMinuscula(palabra: str) -> bool:
    res: bool = False
    for letra in palabra:
        res = res or esMinuscula(letra)
    return res

def tieneAlgunaMayuscula(palabra: str) -> bool:
    res: bool = False
    for letra in palabra:
        res = res or esMayuscula(letra)
    return res

def tieneAlgunDigito(str: str) -> bool:
    res: bool = False
    for letra in str:
        res = res or pertenece(digitos, letra)
    return res

def fortaleza(contraseña: str) -> str:
    colores: list = ["ROJA", "AMARILLA", "VERDE"]
    res: str
    
    if len(contraseña) < 5:
        res = colores[0]
    elif len(contraseña) > 8 and tieneAlgunaMayuscula(contraseña) and tieneAlgunaMinuscula(contraseña) and tieneAlgunDigito(contraseña):
        res = colores[2]
    else:
    	res = colores[1]
     
    return res

#8
def historialBancario(movimientos: list) -> int:
    res: int = 0
    for transaccion in movimientos:
        if transaccion[0] == "I":
            res += transaccion[1]
        else:
            res -= transaccion[1]
    return res

#9
vocales: list = ["a","e","i","o","u"]

def esVocal(letra: str) -> bool:
    return pertenece(vocales, letra)

def tresVocalesDistintas(palabra: str) -> bool:
    vocalesGuardadas: list = []
    for letra in palabra:
        if esVocal(letra) and not pertenece(vocalesGuardadas, letra):
            vocalesGuardadas.append(letra)
    return len(vocalesGuardadas) > 3
