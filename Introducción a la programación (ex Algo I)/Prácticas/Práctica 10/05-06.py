from typing import List
from random import sample
from queue import LifoQueue as Pila
from queue import Queue as Cola
#import copy
#pilaNueva = copy.deepcopy(pila)

#1
def contarlineas(nombre_del_archivo: str) -> int:
    file = open(nombre_del_archivo, 'r')
    lines: List[str] = file.readlines()
    file.close()
    return len(lines)

def existePalabra(palabra: str, nombre_del_archivo: str) -> bool:
    res: bool = False
    file = open(nombre_del_archivo, 'r')
    lines: List[str] = file.readlines()
    file.close()
    if (palabra in lines or (palabra + '\n') in lines):
        res = True
    return res

def cantidadApariciones(palabra: str, nombre_del_archivo: str) -> int:
    res: int = 0
    file = open(nombre_del_archivo, 'r')
    lines: List[str] = file.readlines()
    file.close()
    for line in lines:
        if (palabra == line or (palabra + '\n') == line):
            res += 1
    return res

#2
def quitarPrimerosEspacios(texto: str) -> str:
    res: str = ""
    encontreNoEspacio: bool = False
    for caracter in texto:
        if caracter != ' ' or encontreNoEspacio:
            res += caracter
            encontreNoEspacio = True
    return res

def quitarPrimerosEspaciosALista(lista: List[str]) -> str:
    res: List[str] = []
    for elemento in lista:
        res.append(quitarPrimerosEspacios(elemento))
    return res

def clonarSinComentarios(nombre_del_archivo: str):
    sinComentarios: List[str] = []
    file = open(nombre_del_archivo, 'r')
    lines: List[str] = file.readlines()
    file.close()
    lines = quitarPrimerosEspaciosALista(lines)
    for line in lines:
        if line[0] != "#":
            sinComentarios.append(line)
    fileSinComentarios = open(nombre_del_archivo + "-copy.txt", 'w')
    fileSinComentarios.writelines(sinComentarios)
    return

#3
def invertirLista(lista: List[str]) -> str:
    res: List[str] = []
    for elemento in lista:
        res.insert(0, elemento)
    return res

def reverso(nombre_del_archivo: str):
    file = open(nombre_del_archivo, 'r')
    lines: List[str] = file.readlines()
    file.close()
    lines = invertirLista(lines)
    reverso = open('reverso.txt', 'w')
    reverso.writelines(lines)
    reverso.close()
    return

#4
def agregarAlFinal(nombre_del_archivo: str, frase: str):
    file = open(nombre_del_archivo, 'a')
    file.write(frase)
    file.close()
    return

#5
def agregarAlPrincipio(nombre_del_archivo: str, frase: str):
    fileReadable = open(nombre_del_archivo, 'r')
    lines: List[str] = fileReadable.readlines()
    fileReadable.close()
    lines.insert(0, frase + "\n")
    fileWriteable = open(nombre_del_archivo, 'w')
    fileWriteable.writelines(lines)
    fileWriteable.close()
    return

#6
def caracterValido(caracter: str) -> bool:
    return 'a'<=caracter<='z' or 'A'<=caracter<='Z' or '0'<=caracter<='9' or caracter == ' ' or caracter == '_'

def palabrasLegiblesDeStr(texto: str) -> List[str]:
    res: List[str] = []
    palabra: str = ""
    for caracter in texto:
        caracter = str(caracter)
        if not caracterValido(caracter):
            if len(palabra) >= 5:
                res.append(palabra)
            palabra = ""
        else:
            palabra += caracter
    
    # Por si no agregué la palabra final; me da fiaca hacer otro fix
    if len(palabra) >= 5:
            res.append(palabra)
    
    return res

def palabrasLegiblesBinario(nombre_del_archivo: str) -> List[str]:
    res: List[str] = []
    file = open(nombre_del_archivo, 'rb') # no lo vimos en la práctica ni la teórica
    lines: List[str] = file.readlines()
    for line in lines:
        res += palabrasLegiblesDeStr(str(line)) # le pongo el str(line) para que me tire la metadata, sino re feo
    return res

#7
def csv(str: str) -> List[str]:
    return str.split(',')
"""
problema promedioEstudiante (in lu: String) : R {
	requiere: {alumnoValido(lu)}
	asegura: {res = sumatoriaDeNotas(lu)/materiasCursadas(lu)}
}

problema sumatoriaDeNotas (in lu: String) : R {
    requiere: {alumnoValido(lu)}
	asegura: {(res = \sum(Sumatoria con i desde 0 hasta |abrirArchivo("notas.csv")|-1 de) if lu = abrirArchivo("notas.csv")[i][0] then abrirArchivo("notas.csv")[i][3] else 0)}
}

problema materiasCursadas (in lu: String) : R {
    requiere: {alumnoValido(lu)}
	asegura: {(res = \sum(Sumatoria con i desde 0 hasta |abrirArchivo("notas.csv")|-1 de) if lu = abrirArchivo("notas.csv")[i][0] then 1 else 0)}
}

pred alumnoValido (lu: String) {
	(E i : Z)(lu = conjuntoDeLUs(abrirArchivo("notas.csv"))[i])
}

problema abrirArchivo (in archivo: String) {
    requiere: {existeElArchivo(archivo)} # Este falta hacerlo, lo mismo, ni idea
	// La idea sería que dado el nombre de un archivo me devuelva una lista de listas spliteando "," del csv, donde cada renglón es una lista, y cada split un elemento de esa lista.
	Ej: [["111/11", "Algo I", "99/99/9999", "7"], ["111/11", "Algo II", "99/99/9999", "8"], ...]
}

# no me molesta que tenga o no repetidos
problema conjuntoDeLUs (in rows: seq<String>) : seq<String> {
    requiere: {esMatriz(rows)}
    asegura: {|res| <= |rows|}
    asegura: {(V i : Z)(0 <= i < |rows| -> pertenece(rows[i][0], res))}
}

# el de esMatriz me lo puedo chorear del ej 4 del cms2.

pred pertenece (e: T, l: seq<T>) {
	(E i : Z)(0 <= i < |l| ^ l[i] = e)
}
"""
def promedioEstudiante(lu: str) -> float:
    sumatoriaDeNotas: float = 0
    materiasCursadas: int = 0
    file = open("notas.csv", 'r', encoding='UTF-8')
    lines: List[str] = file.readlines()
    file.close()
    for line in lines:
        row = csv(line)
        if row[0] == lu:
            sumatoriaDeNotas += int(row[3])
            materiasCursadas += 1
    return sumatoriaDeNotas/materiasCursadas

#print(palabrasLegiblesBinario("feliz_jueves.mp3")) # FELIZ JUEVES!

#8
def generarNrosAlAzar(n: int, desde: int, hasta: int) -> List[int]:
    return sample(lista(desde,hasta), n)

def lista(desde: int, hasta: int) -> List[int]:
    res: List[int] = []
    for n in range(desde, hasta+1, 1):
        res.append(n)
    return res

#print(generarNrosAlAzar(4, 2, 10))

#9
def armarPila(n: int, desde: int, hasta: int) -> Pila:
    listaDeNumeros: List[int] = generarNrosAlAzar(n,desde,hasta)
    p = Pila()
    for numero in listaDeNumeros:
        p.put(numero)
    return p

pila = armarPila(5,1,10)

#10
def cantidadElementos(p: Pila) -> int:
    return p.qsize()

#11
#def buscarElMaximo(p: Pila) -> int: Pareciera que está mal planteado el enunciado. Preguntar

#12
#def estaBienBalanceada(s: str) -> bool:

#13
def armarCola(n: int, desde: int, hasta: int) -> Cola:
    listaDeNumeros: List[int] = generarNrosAlAzar(n,desde,hasta)
    c = Cola()
    for numero in listaDeNumeros:
        c.put(numero)
    return c

cola = armarCola(5,1,10)

#14
def cantidadElementos(c: Cola) -> int:
    return c.qsize()

#15
# Idem arriba

#16 (1)
def armarSecuenciaDeBingo() -> Cola:
    return armarCola(12, 0, 99)

#16 (2)
