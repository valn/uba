import sys

def fibonacciNoRecursivo(n: int) -> int:
    fibonacci: List[int] = []
    
    for i in range(n+1):
        if i == 0:
            fibonacci.append(0)
        elif i == 1:
            fibonacci.append(1)
        else:
            fibonacci.append(fibonacci[i-1] + fibonacci[i-2])
            
    return fibonacci[len(fibonacci)-1]

if __name__ == '__main__':
  x = int(input())
  print(fibonacciNoRecursivo(x))