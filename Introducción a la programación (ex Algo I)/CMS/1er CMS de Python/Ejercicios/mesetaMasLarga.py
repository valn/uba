from typing import List

# Aclaración: Debido a la versión de Python del CMS, para el tipo Lista, la sintaxis de la definición de tipos que deben usar es la siguiente:
# l: List[int]  <--Este es un ejemplo para una lista de enteros.
# Respetar esta sintaxis, ya que el CMS dirá que no pasó ningún test si usan otra notación.
def mesetaMasLarga(l: List[int]) -> int :
    if len(l) == 0:
        return 0
    
    ultimoValor: int = l[0]
    mesetaMasLargaHallada: List[int] = []
    mesetaActual: List[int] = []
  
    for valor in l:
        if valor == ultimoValor:
            mesetaActual.append(valor)
            if len(mesetaActual) > len(mesetaMasLargaHallada):
                mesetaMasLargaHallada = mesetaActual
        else:
            ultimoValor = valor
            mesetaActual = [valor]

    return len(mesetaMasLargaHallada)

if __name__ == '__main__':
  x = input()
  print(mesetaMasLarga([int(j) for j in x.split()]))