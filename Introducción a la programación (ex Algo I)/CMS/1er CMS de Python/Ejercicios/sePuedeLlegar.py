from typing import List
from typing import Tuple

# Aclaración: Debido a la versión de Python del CMS, para el tipo Lista y Tupla, la sintaxis de la definición de tipos que deben usar es la siguiente:
# l: List[int]  <--Este es un ejemplo para una lista de enteros.
# t: Tuple[str,str]  <--Este es un ejemplo para una tupla de strings.
# Respetar esta sintaxis, ya que el CMS dirá que no pasó ningún test si usan otra notación.
def sePuedeLlegar(origen: str, destino: str, vuelos: List[Tuple[str, str]]) -> int :    
    if vuelos == []: # Tuvimos un problema con Aerolineas Argentinas y nos quedamos sin vuelos ;(
        return -1
    
    vuelosRestantes: List[Tuple[str, str]] = vuelos # Me armo la lista de vuelos restantes, pues en la especifiación vuelos es de tipo in
    
    ciudades: List[str] = listaDeCiudades(origen, destino, vuelos) # Me armo una lista con todas las ciudades de los vuelos (sin repetidos)
        
    if not pertenece(ciudades, origen): # Te quedaste atrapado en el origen, pues ningún vuelo sale de ahí
        return -1
        
    vueloActual: List[Tuple[int, int]] = [(ciudades.index(origen), 0)] # Tengo una lista con una tupla, la tupla consiste en el índice (de la lista de ciudades) del lugar donde estoy y cuantas escalas me tomó llegar allí
    while vueloActual: # Mientras siga volando
        indiceCiudad, escalas = vueloActual.pop(0) # "Llegué a destino", separo y popeo el vuelo
        ciudad: str = ciudades[indiceCiudad]
            
        if ciudad == destino: # Si llegué a destino, devuelvo la cantidad de escalas que hice
            return escalas
            
        for vuelo in vuelosRestantes: # Si no llegué a destino reviso mis pasajes y veo si tengo otro vuelo desde esta ciudad
            if vuelo[0] == ciudad:
                vueloActual.append((ciudades.index(vuelo[1]), escalas + 1)) # Tengo un vuelo?, saco el pasaje y me voy corriendo a embarcar
                vuelosRestantes.remove((vuelo[0],vuelo[1])) # Gasté el pasaje, por lo que elimino el vuelo que ya tomé
        
    return -1 # No tenía ningún vuelo? / Me quedé sin vuelos antes de llegar al destino? salí del while entonces devuelvo -1

def listaDeCiudades(origen: str, destino: str, vuelos: List[Tuple[str, str]]) -> List[str]:
    ciudades: List[str] = []
    for vuelo in vuelos:
        ciudadOrigen, ciudadDestino = vuelo
        
        if not pertenece(ciudades, ciudadOrigen):
            ciudades.append(ciudadOrigen)
            
        if not pertenece(ciudades, ciudadDestino):
            ciudades.append(ciudadDestino)
                
    return ciudades

def pertenece(s: List[str], e: str) -> bool:
    res: bool = False
    for elem in s:
        res = res or (elem == e)
    return res

if __name__ == '__main__':
  origen = input()
  destino = input()
  vuelos = input()
  
  print(sePuedeLlegar(origen, destino, [tuple(vuelo.split(',')) for vuelo in vuelos.split()]))