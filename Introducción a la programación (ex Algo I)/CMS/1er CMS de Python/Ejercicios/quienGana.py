import sys

# j1 le gana a j2, ó (excl) j2 le gana a j1, ó (excl) sacaron lo mismo, ergo empate
def quienGana(j1: str, j2: str) -> str : 
    res: str = ""
    if gana(j1,j2):
        res = "Jugador1"
    elif gana(j2,j1):
        res = "Jugador2"
    else:
        res = "Empate"
    return res

# Si gana j1 sobre j2 es porque caigo en alguno de esos casos
def gana (j1: str, j2: str) -> bool:
    return piedraGanaATijera(j1,j2) or tijeraGanaAPapel(j1,j2) or papelGanaAPiedra(j1,j2)

# Casos en cuestión
def piedraGanaATijera (j1: str, j2: str) -> bool:
    return j1 == "Piedra" and j2 == "Tijera"

def tijeraGanaAPapel (j1: str, j2: str) -> bool:
    return j1 == "Tijera" and j2 == "Papel"

def papelGanaAPiedra (j1: str, j2: str) -> bool:
    return j1 == "Papel" and j2 == "Piedra"

if __name__ == '__main__':
  x = input()
  jug = str.split(x)
  print(quienGana(jug[0], jug[1]))