from queue import LifoQueue

# para el tipo pila de enteros, usar: "pila: LifoQueue". La notación "pila: LifoQueue[int]" no funciona.
def calcular_expresion(expr: str) -> float:
  # implementar función con el TAD Pila
  resultado: float
  operandos: LifoQueue = LifoQueue()
  for elemento in expr.split():
    if elemento not in ["*","+","/","-"]:
      operandos.put(float(elemento))
    else:
      operandoDerecha = operandos.get() # el operando a la izquierda del operador es el que va a la derecha de la operación (el último que agregué a la pila)
      operandoIzquierda = operandos.get()
      if elemento == '+':
        resultado = operandoIzquierda + operandoDerecha
      elif elemento == '-':
        resultado = operandoIzquierda - operandoDerecha
      elif elemento == '*':
        resultado = operandoIzquierda * operandoDerecha
      else:
        resultado = operandoIzquierda / operandoDerecha
      operandos.put(resultado)
  return operandos.get()

'''
Ejecución simbólica de "2 5 * 7 +"
operandos@a = [] #la pongo como lista para que se pueda ver
(Agarro el 2 y 5 y los meto en mi pila (operandos))
operandos@b = [2,5]
(Llego a al operador *)
(Hago 2 * 5)
operandos@c = []
(Agrego 10 a operandos)
operandos@d = [10]
(Me queda "7 +" y tengo la operandos en el estado d)
(Agarro el 7 y lo pongo en operandos)
operandos@e = [10, 7]
(Llego al operador +)
(Hago 10 + 7)
operandos@f = []
(Me da 17, lo agrego a operandos)
operandos@g = [17]
(Terminé de recorrer mi for, returneo operandos.get(), que es el único elemento que me queda en mi pila (operandos), la respuesta)
return 17
'''

if __name__ == '__main__':
  x = input() # Por ejemplo: 2 5 * 7 +
  print(round(calcular_expresion(x), 5))