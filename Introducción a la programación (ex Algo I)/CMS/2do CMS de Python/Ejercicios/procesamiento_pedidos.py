from queue import Queue
from typing import List
from typing import Dict
from typing import Union
import json

# ACLARACIÓN: El tipo de "pedidos" debería ser: pedidos: Queue[Dict[str, Union[int, str, Dict[str, int]]]]
# Por no ser soportado por la versión de CMS, usamos simplemente "pedidos: Queue"
def procesamiento_pedidos(pedidos: Queue,
                          stock_productos: Dict[str, int],
                          precios_productos: Dict[str, float]) -> List[Dict[str, Union[int, str, float, Dict[str, int]]]]:
  
  # La idea de esto es intentar respetar el "in" de la especificación
  pedidos_copia: Queue = Queue()
  temp: List[Dict[str, Union[int, str, Dict[str, int]]]] = []
  while not pedidos.empty():
    temp.append(pedidos.get())
  
  for diccionario in temp:
    pedidos.put(diccionario)
    pedidos_copia.put(diccionario)
  # Fin del cachivache este para respetar el "in"
  
  res: List[Dict[str, Union[int, str, float, Dict[str, int]]]] = []
  while not pedidos_copia.empty():
    pedido: Dict[str, Union[int, str, Dict[str, int]]] = pedidos_copia.get()
    productos: Dict[str, float] = pedido["productos"]
    precio_total: float = 0
    productos_a_llevar: Dict[str, float] = {}
    cantidad_a_llevar: int
    estado: str = 'completo'
    # Supongo que el estado es completo, si en algún caso falla, va a cambiar a incompleto
    
    for producto, cantidad in productos.items():
      # Caso: hay stock del producto
      if stock_productos[producto] >= cantidad:
        cantidad_a_llevar = cantidad
        stock_productos[producto] -= cantidad # Reduzco el stock (me compraron)
        
      # Caso: no hay stock de algún producto
      else:
        estado = 'incompleto' # No pudimos concretar su orden ;(
        cantidad_a_llevar = stock_productos[producto]
        stock_productos[producto] = 0 # Reduzco el stock (se llevó todo lo que teníamos)
      
      # Le empiezo a hacer la factura porque acá laburamos en blanco :D
      precio_total += precios_productos[producto] * cantidad_a_llevar # Paso el producto por la caja (le estoy haciendo el ticket)
      productos_a_llevar[producto] = cantidad_a_llevar # Meto el producto en la bolsa (la lista de los productos que se va a llevar)
    
    # Terminé la factura
    res.append({
      'id': pedido['id'],
      'cliente': pedido['cliente'],
      'productos': productos_a_llevar,
      'precio_total': precio_total,
      'estado': estado
      })
  return res
    


if __name__ == '__main__':
  pedidos: Queue = Queue()
  list_pedidos = json.loads(input())
  [pedidos.put(p) for p in list_pedidos]
  stock_productos = json.loads(input())
  precios_productos = json.loads(input())
  print("{} {}".format(procesamiento_pedidos(pedidos, stock_productos, precios_productos), stock_productos))

# Ejemplo input  
# pedidos: [{"id":21,"cliente":"Gabriela", "productos":{"Manzana":2}}, {"id":1,"cliente":"Juan","productos":{"Manzana":2,"Pan":4,"Factura":6}}]
# stock_productos: {"Manzana":10, "Leche":5, "Pan":3, "Factura":0}
# precios_productos: {"Manzana":3.5, "Leche":5.5, "Pan":3.5, "Factura":5}