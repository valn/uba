## Estructura

- **Clases**: Contiene las clases teóricas impartidas durante el curso.
- **Guías**:
  - **Enunciados**: Enunciados de las guías prácticas y ejercicios propuestos.
  - **Soluciones**:
    - **p3**: Soluciones específicas para la práctica 3.
- **Material**: Bibliografía y recursos adicionales para el estudio y profundización de los temas vistos en clase.
- **Repaso pre-parcial**: Ejercicios y apuntes destinados a la preparación para el examen parcial.

## Recursos Externos
Mención especial a Santiago Roussineau por su resumen de la teoría de la materia. El resumen se encuentra disponible en el siguiente enlace: [Resumen de Santiago Roussineau](https://roussineau.notion.site/3d422711ca8e4814a56e119d9ee8a5cd?v=9567c0ed497c4181a1b3c828e8f47157).
