# Sean dos vectores s y q tal que las direcciones de inicio vienen en a0 y a1 respectivamente, a su vez a2 contiene el tama�o de s y q. Se pide copiar la informacion de q a s.

.data:
    s: .word 0x8 0x2 0x3
    q: .word 0x5 0x7 0x1
    longitud: .word 0x3

.text:
    # cargo a registros
    addi a0 zero s
    addi a1 zero q
    lw a2 longitud
    jal ra ciclo
    
    # fin del programa
    li a7, 93
    ecall
    
    
# hacemos la copia
ciclo:
    beqz a2 fin
    lw a3 0(a1)
    sw a3 0(a0)
    addi a1 a1 4
    addi a0 a0 4
    addi a2 a2  -1
    j ciclo
fin:
    ret