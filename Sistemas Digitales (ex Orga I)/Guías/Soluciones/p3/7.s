# Ejercicio 7
# Sumar los n primeros números naturales

# Nota: Se puede hacer recursivo o iterativo, ambas son válidas.

.text
main:
li a0 5
li t0 0
jal ra sum_iterativa

# Ultimas dos reglas para terminar el programa (opcionales)
li a7, 93
ecall

sum_recursiva:
beqz a0 sum_recursiva_caso_base
addi sp sp -16
sw a0 0(sp)
sw ra 4(sp)
addi a0 a0 -1
jal ra sum_iterativa
mv t0 a0
lw a0 0(sp)
lw ra 4(sp)
addi sp sp 16
add a0 a0 t0
ret

sum_recursiva_caso_base:
li a0 0
ret

sum_iterativa:
beqz a0 sum_iterativa_fin
add t0 t0 a0
addi a0 a0 -1
j sum_iterativa

sum_iterativa_fin:
mv a0 t0
ret