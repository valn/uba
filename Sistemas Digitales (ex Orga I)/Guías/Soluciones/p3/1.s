# Dado vector de enteros arreglo y su Longitud, escribir un programa que encuentre el valor maximo en arreglo.

.data:
    arreglo: .word 0x3 0x1 0x4 0x1 0x5 0x9 0x2 0x6
    longitud: .word 0x8

.text:
    addi a1, zero, arreglo # cargo en a1 la posici�n de memoria donde arranca el arreglo
    lw a2 longitud # cargo la longitud del arreglo a a2
    jal ra max_recursivo
    
    # terminamos el programa
    li a7, 93
    ecall

max_recursivo:
    beq a2 x0 fin
    lw a0 0(a1) # cargo en el registro a0 el primer elemento del arreglo
    addi sp sp -16 # hago lugar en el stack
    sw a0 0(sp) # guardo el valor en memoria
    sw ra 4(sp) # guardo el ra
    addi a2 a2 -1 # resto 1 a la longitud
    addi a1, a1 4 # muevo al siguiente elemento
    jal ra max_recursivo # llamado recursivo
                         # puedo asumir q tengo el m�x del subarreglo [1..] calculado en a0
    lw a3 0(sp)
    lw ra 4(sp)
    addi sp sp 16
    bge a3, a0, actualizar_a0_rec
    ret

actualizar_a0_rec:
    mv a0, a3
    ret

max_iterativo:
    lw a0 0(a1) # cargo en el registro a0 el primer elemento del arreglo
    addi a2 a2 -1 # resto 1 a la longitud
    addi a1, a1 4 # muevo al siguiente elemento
    j ciclo

ciclo:
    beq a2 x0 fin # si la longitud es 0, termin�
    lw a3 0(a1) # cargo el valor actual en a3
    addi a2 a2 -1 # resto 1 a la longitud
    addi a1, a1 4 # muevo al siguiente elemento
    bge a3, a0, actualizar_a0 # si el valor que acabo de sacar es mejor que el que ten�a, lo actualizo
    j ciclo
    
actualizar_a0:
    mv a0, a3
    j ciclo
    
fin:
    ret