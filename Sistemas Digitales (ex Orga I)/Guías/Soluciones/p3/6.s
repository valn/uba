#  Ejercicio 6
# a. Escribir la función multiplicacion, que dados dos números a0 y a1 y devuelva el resultado de a0 x a1 en a0.

.text
main:
li a0 1
li a1 5
jal ra multiplicar

# Ultimas dos reglas para terminar el programa (opcionales)
li a7, 93
ecall

# ITEM A

multiplicar: # Me di cuenta después que era más sencillo (y rápido a nivel de cómputo) implementarlo con un ciclo. Lo dejé así, de paso practico recursión
# Caso base
beqz a0 fin # a0 == 0
# Caso recursivo
addi sp sp -16 # Hago lugar en el sp
sw ra 0(sp) # Guardo el ra en el sp
addi a0, a0, -1 # Voy a hacer recursión, por lo que hago a0-1 para calcular (a0-1) x a1
jal ra multiplicar # Me da (a0-1) x a1 en a0
lw ra 0(sp) # Recupero ra
add a0 a1 a0 # Hago la suma de (a0-1) x a1 con a0 = a0 x a1
addi sp sp 16 # Vuelvo a subir el sp
ret # Return

fin:
li a0 0 # Caso 0 x a1
ret