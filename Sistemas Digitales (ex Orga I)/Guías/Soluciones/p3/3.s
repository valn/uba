# Sean dos vectores s y q tal que las direcciones de inicio vienen en a0 y a1 respectivamente, ademas se cuenta con a2 que contiene el tamano de s y q. Se pide copiar los elementos pares de q a s y si no 0.

.data:
    s: .word 0x5 0x7 0x1
    q: .word 0x8 0x2 0x3
    longitud: .word 0x3

.text:
    # cargo a registros
    addi a0 zero s
    addi a1 zero q
    lw a2 longitud
    jal ra ciclo
    
    # fin del programa
    li a7, 93
    ecall
    
    
# hacemos la copia
ciclo:
    beqz a2 fin
    lw a3 0(a1)
    
    # si a3 es par, entonces sw a3 0(a0), de lo contrario skipeo ese paso
    # puedo ver el bit menos significativo, si es 0 es par
    # es decir, quiero ver si cumple la propiedad "0" que es ser impar, puedo usar m�scaras
    # la idea es agarrar a3 y hacer un and bit a bit con el 0x00000001
    # puedo usar 0b01 (as� me aseguro que no me extienda todo con 1 al pasar de 12 bits del inmediato a 32 bits)
    andi a4 a3 0b01 # si a4 es 0, entonces a3 era par (igual si pon�a 1 en lugar de 0b01 funciona �por qu�?)
    beqz a4 copia
    j continue

copia:
    sw a3 0(a0)
    j continue    

continue:
    addi a1 a1 4
    addi a0 a0 4
    addi a2 a2  -1
    j ciclo

fin:
    ret