#  Ejercicio 5
# Hacer un programa que calcule Finoacci(5).

.data
n: .byte 5

.text
main:
lw a0 n # Cargo el n en a0
jal ra fibonacci # Voy a calcular el fibonacci de n

# Ultimas dos reglas para terminar el programa (opcionales)
li a7, 93
ecall

fibonacci:
# Casos base
li t0 1 # Para tener con qué comparar (necesito hacer beq entre registros)
beq a0 t0 fibonacci_1 # n == 1
beqz a0 fibonacci_0 # n == 0
# Caso recursivo
addi sp sp -16 # Hago lugar en el sp
sw a0 0(sp) # Guardo a0 (n) en el sp
sw ra 4(sp) # Guardo la posicion de retorno en el sp
addi a0, a0, -1 # Resto a a0 (n) 1
jal ra fibonacci # Hago Fibonacci de n-1
sw a0 8(sp) # Me guardo el valor de Fibonacci(n-1) en el sp

# Tengo calculado Fibonacci(n-1) en a0, pero necesito también Fibonacci(n-2)
lw a0 0(sp) # Recupero el n
addi a0, a0, -2 # a0 := n - 2 para cualcular Fibonacci(n-2)
jal ra fibonacci # Voy a calcular Fibonacci(n-2)
lw ra 4(sp) # Recupero el ra
lw a1 8(sp) # Recupero el valor de Fibonacci(n-1)
add a0 a1 a0 # Sumo ambos fibonaccis y los guardo en a0
addi sp sp 16 # Vuelvo a subir el sp
ret # Return

fibonacci_0:
li a0 0 # Caso base Fibonacci(0)
ret

fibonacci_1:
li a0 1 # Caso base Fibonacci(1)
ret