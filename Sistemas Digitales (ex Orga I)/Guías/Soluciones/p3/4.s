#  Ejercicio 4
# Dado un vector array que contiene n números enteros ordenados de menor a mayor y un target que es un valor entero, se pide devolver el ´ındice de array adonde se encuentra target, usando búsqueda binaria.

.data
array: .word 1 3 5 7 9 11 13 15 17 19
target: .word 1
n: .word 10

.text
main:
la a1 array # Cargo el a1 el arreglo
lw a2 target # Cargo el número target en a2
lw a3 n # Cárgo el número n en a3
li t0 0 # var: low
add t1 zero a3 # var: high
jal ra binary_search # Busco el índice

# Ultimas dos reglas para terminar el programa (opcionales)
li a7, 93
ecall

binary_search:
bge t0 t1 fin # Salgo de guarda sii t0 (high) >= t1 (low) 
# Voy a usar t2 para hacer operaciones internas
sub t2 t1 t0 # t2 := high - low
srai t2 t2 1 # t2 := (high - low)/2
add a0 t0 t2 # Ahora a0 tiene el índice del elemento del medio (el que voy a revisar)
# Quiero acceder a array[a0], me paro en la posición de memoria de array y le sumo 4 * a0 pues es un arreglo de palabras
slli t2 a0 2 # No necesito mantener el valor en t2, lo piso para guardar 4 * a0
add t3 a1 t2 # t3 := array + 4 * a0 = a1 + t2 = a1 + 4 * a0
lw t4 0(t3) # t4 := array[a0]
beq t4 a2 encontrado
# Si no lo encontré, sigo buscando. Tengo que ver para qué lado
blt t4 a2 true
addi t1 a0 0
j binary_search # Siguiente iteración del ciclo

fin:
li a0 -1 # Quiero devolver en a0 el índice del arreglo que corresponde al elemento, -1 si no está.
ret # Return

encontrado:
ret # Return

true:
addi t0 a0 1
j binary_search
