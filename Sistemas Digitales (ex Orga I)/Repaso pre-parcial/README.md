# Sistemas Digitales (repaso pre-parcial)
### Preguntas teóricas (1C 2024)

1. Observando las instrucciones que realizan operaciones aritméticas, describir a que componentes conectaría la ALU en el datapath. Explicar detalladamente el funcionamiento del siguiente programa en assembler:

   ```assembly
   main:
   	addi a1,x0,1
   	addi a2,x0,2
   	bltu a2,a1,test
   	sub a3,a1,a2
   	jal end
   test:
   	sub a3,a2,a1
   end:
	 	nop
   ```
	 Pensemos en un esquema para el datapath. Propongo:
	 ![Esquema](https://i.ibb.co/gvHD158/image.png)  
	 Vayamos resolviendo las instrucciones para ver cómo haríamos las conexiones con la ALU...  
	 ![Esquema completo](https://i.ibb.co/7Jt1fY1/image.png)  
	 Una vez realizadas nos queda algo como esto, con este datapath (estoy ignorando la unidad de control) podríamos realizar ese conjunto de operaciones aritméticas y saltos.  
	 Ahora, explicando el funcionamiento del programa.  
	 1. Guarda en el registro a1 el número 1.
	 2. Guarda en el registro a2 el número 2.
	 3. Agarra los registros a2 y a1. Agarra sus dos valores y los trata de manera unsigned, realiza una comparación para ver si a2 < a1 (cómo hace la comparación depende de la microarquitectura), en este caso la comparación va a ser falsa pues no es cierto que 2 < 1, por lo que no de va a test, sino que aumenta en 4 el PC.
	 4. Guarda en el registro a3 el resultado de hacer a2 - a1.
	 5. jal end es equivalente a jal x1 end pues no se está especificando el registro destino. Guarda el valor del PC actual + 4 en x1, y actualiza el PCNext a el PC actual + el offset necesario para ir a end.
	 6. Una vez en end se ejecuta nop, que es equivalente a addi x0 x0 0. Por lo que no hace nada. Luego termina el programa.

2. ¿Qué significa que la arquitectura de RISCV sea modular? ¿Qué ventajas puede tener esto?  
   Mientras que el enfoque convencional de las arquitecturas es desarrollar ISAs incrementales, RISC-V opta por un esquema modular.  
	 ¿Qué quiere decir?, RISC-V cuenta con un set de instrucciones "core", es minimalista y completo (RV32I).  
	 Luego, se encuentran módulos/extensiones del mismo, por ejemplo RV32M agrega la multiplicación.  
	 La idea/ventaja de este sistema, es que a la hora de implementar se sabe con qué instrucciones se va a contar (las del núcreo), esto es últil para desarrolladores de compiladores, por ejemplo, pues saben con qué instrucciones van a estar presentes con seguridad.  
	 De manera modular, cada arquitecto de computadoras puede decidir implementar (o no) los distintos módulos de RISC-V.

3. ¿Cuáles son los objetivos no funcionales (o métricas de diseño) qué guían el diseño del set de instrucciones de RISCV? ¿Cómo impactan en su diseño?  
   Las métricas de diseño que dictan el rumbo de RISC-V son:  
   1. Costo (su diseño sencillo redeuce el tamaño del chip, por lo que se pueden minimizar el costo monetario por errores de fabricación)  
   2. Simplicidad (facilita su implementación, ayuda a los programadores a entender mejor el ISA)  
   3. Rendimiento (las operaciones simples tienen un mayor rendimiento pues la electrónica se exparse más rápido)  
   4. Aislamiento de arquitectura e implementación (separa el cómo del qué, las mejoras de rendimiento no dependen del arquitecto del ISA, sino de la implementación)  
   5. Espacio para crecer (al no estar sobrecargado de instrucciones, se reservan opcodes (código de operación) para crear instrucciones (modulares) específicas para necesidades particulares, así se mejora el rendimiento de los chips)
   6. Tamaño del programa (para sistemas embebidos (por ej, una heladera intelgiente) es importante que los programas ocupen lo menos posible, para poder reducir los costos en la memoria del computador, también el acceso a memoria externa (DRAM, Dynamic Random Access Memory) produce bastante más calor, lo que provoca consumir más energía; las instrucciones en RISC-V son de 4 bytes, mientras que en otras arquitecturas van de 1 a 15 bytes)  
   7. Facilidad de programar, compilar, y linkear (RISC-V cuenta con la generosa cantidad de 32 registros, por lo que facilita la tarea de programar y compilar. También el ISA (RISC-V) soporta PIC (Position Independent Code), lo que permite hacer saltos del PC (Program Counter) cosa de tener cargado un programa en memoria e ir moviéndose en instrucciones que se encuentran en partes distintas de la memoria)  

4. ¿Cuáles son las reglas de nomenclatura de las instrucciones del set RISC-V? ¿Qué tipo de instrucciones tiene?  
   Podría decirse que la nomenclatura de las instrucciones del RISC-V es modular. Voy a tomar en cuenta la de RV32I pues es el núcleo de RISC-V. Utiliza palabras del inglés, las cuales acorta para las instrucciones, aunque no sólo es eso, sino que realiza combinaciones 2 a 2 con términos como immediate, unsigned, byte, word, etc... para la composición de instrucciones, de esta forma se obtiene un set de instrucciones fácil de leer, deducir, y recordar.  
   Referente al tipo de instrucciones, son 6. La clasificación de instrucciones de RISC-V son las siguientes:  
   * Tipo-R: Operaciones entre registros.
   * Tipo-I: Inmediatos cortos y cargas (loads).
   * Tipo-S: Almacenamiento (stores).
   * Tipo-B: Condiciones de bifurcación (branches).
   * Tipo-U: Inmediatos largos.
   * Tipo-J: Saltos incondicionales.

5. ¿Cuál es la ventaja de tener un registro de valor constante 0? ¿Cómo maneja las escrituras a este registro?  
   Permite tener instrucciones declarativas sin agregar complejidad al ISA. Por ejemplo, `li {registro} {inmediato}` es lo mismo que `addi {registro} x0 {inmediato}`. También existe `nop` que es `addi x0 x0 0`. Nos permite tener (pseudo)instrucciones que no hacen nada, instrucciones más declarativas, entre otras cosas. También tiene la particularidad que es un registro el cual no se puede escribir, es decir, `addi x0 x0 0x10` no modifica el valor de `x0`.

6. ¿Cómo resuelve la lógica de control (branching)?  
   Esta pregunta tiene varias interpretaciones, voy a responder las que considero coherentes.  
   * La forma de resolver el branching es realizando la operación que determina el camino a seguir. Cómo se realiza la operación que determina el camino a seguir depende de la implementación y no de la ISA en sí misma. Una forma podría ser restar registros, y ver la flag del ALU que diga Zero, Negative, etc..., y de ahí actualizar el PC de la forma correspondiente.  
   * La forma de decodificar la lógica de control (branching) es de la siguiente manera: ![Tipo B](https://i.ibb.co/DQ2w924/image.png)  

7. ¿Cómo resuelve el overflow?  
   Al igual que antes, esta pregunta cuenta con varias interpretaciones.  
   Por lo general decimos que el hay overflow en un half-adder o full-adder <=> ((a_{n-1} == b_{n-1}) && (a_{n-1} != c_{n-1}))  
   Dicho de otro modo, si los bits más significativos son iguales, entonces el resultado deberá tener el mismo signo (caso contrario, overflow).  
   La formula dada dice que hay overflow si ambos bits más significativos son iguales, y el carry que les llega es != a cualquiera de ellos. Si eso llega a pasar se puede observar que no cumple la propiedad de que suma de positivos de positivo, o suma de negativos de negativo.  
   Aún así esto no depende del ISA, sino que depende de quien implemente la ALU para realizar las operaciones aritméticas.
   Según el manual de RISC-V, lo debe implementar el programador.  

8. ¿Cómo resuelve los saltos incondicionales? ¿Por qué lo hace de este modo?  
   Ídem, varias respuestas.  
   Opción A: Depende de la microarquitectura.  
   Opción B (manual): Es bastante similar al tipo U. El inmediato es leido de otra manera, esto es así para faciliar el cableado (fierros), es decir, simplificar el datapath en términos de costos, mientras más instruccione similares tengan el mismo o similar datapath, mejor.  

9. ¿Cómo resuelve la multiplicación de números enteros?  
   En el set de instrucciones base RV32I no exsite la multiplicación de números enteros. Debería el programador realizar dicha función con las instrucciones del set RV32I (por ejemplo, la suma). En caso de estar trabajando con RV32M, sí se cuenta con la instrucción para la multiplicación, cómo se implementa depende de la microarquitectura.

10. ¿En qué consiste la convención de llamadas? ¿Qué registros se preservan? ¿Qué debe hacer la persona que escribe código que sigue esta convención con los registros que no se preservan?  
    Consiste en un contrato entre programadores sobre qué registros sí y qué registros no debemos preservar al terminar nuestra función. Se preservan los "saved registers". Uno debe operar con los registros que no se preservan, o, en caso de utilizar registros que se preservan, garantizar que vuelvan al estado de antes de ser llamados al terminar la llamada a la función. 

11. ¿Qué sucede si no hay suficientes registros como para pasar los parámetros de una función?  
    Se utiliza el stack. Se mueve en 16 (convención) el sp (stack pointer), y se guarda en memoria lo necesario.

12. ¿Qué son las pseudoinstrucciones? ¿Esto es microprogramación?  
    Una pseudoinstrucción es algo útil para programar en assembly. No son instrucciones de la ISA en sí, por lo que no aumenta su complejidad. Son (pseudo)instrucciones que se traducen a otras instrucciones (posta) del ISA. Así uno puede ser más declarativo al escribir. Por ejemplo hacer `la registro símbolo` permite cargar en un _registro_ el valor de un _símbolo_ de 32 bits!, pues carga primero el upper immediate con `lui` y luego hace un `addi`. También está `ret` que es `jalr x0 ra`.

13. ¿Qué son las directivas de ensamblador?  
    Son comandos que empieza con un punto (`.`). Son comandos para el ensamblador y no código traducido, le indican al ensamblador en qué parte de la memoria poner el código y datos en el mapa de la memoria.  
    ![Mapa de la memoria](https://i.ibb.co/LJCgB1j/image.png)

14. ¿Qué significa position independent code? ¿Qué ventaja tiene sobre el código dependiente de posición?  
    Significa código independiente de su posición. Nos permite realizar saltos relativos al PC, es decir, permite modificar el PC a diversas direcciones para ejecutar instrucciones que no son necesariamente contiguas en la memoria donde están cargadas. Una ventaja visible es para realizar ciclos (iteraciones), funciones recursivas, y más cosas que se escapan del scope de la materia.  

15. ¿A qué se llama el heap? ¿A qué se conoce como un heap overflow?  
    (No entra)

16. Describa las similitudes y diferencias entre las instrucciones de formatos B y S. Idem entre las instrucciones J y U.  
    * B y S: Son similares, pero difieren en como leen el inmediato, esto es así para poder realizar optimizaciones de costo (y posiblemente tiempo también) a la hora de implementar la arquitectura. Difieren únicamente en el bit 0, 11, y 12 del inmediato.  
    * J y U: Idem idea que arriba. Pero con más diferencias en los bits del inmediato.  

17. Dados dos registros mostrar como intercambiarlos sin intervención de un tercero.  
    ![Respuesta](https://i.ibb.co/8rxFB1m/image.png)  
    ```assembly
    xor x1 x1 x2
    xor x2 x1 x2
    xor x1 x1 x2
    ```
    Y listo. Seguro se puede hacer similar al famoso problema de leetcode con add y sub, pero al trabajar con bits esto es más interesante.

18. Sabiendo que a1 = 0xffffffff ¿Cuánto queda almacenado en a2 luego de realizar: andi a2, a1, 0xf00?  
    Pensemos primero como se va a extender el inmediato. 0xF00 es equivalente a 1111 0000 0000 es binario (separé en hexas con espacios)  
    Por lo que se extenderá con 1's a la izquierda, por lo que es lo mismo a hacer algo del estilo 0xFFFFFFFF AND 0xFFFFFF00.  
    Y como el 0xFFFFFFFF es el neutro en el AND nos queda que a2 := 0xFFFFFF00.

19. ¿En qué posición dentro de la instrucción se encuentran los bits de los registros destino y origen? ¿Depende del tipo de instrucción o de la instrucción en sí? ¿Por qué fue diseñado así el formato de instrucción?  
    El registro destino se encuentra siempre en las posiciones 11 a 7 de la tira de bits. El registro origen de 19 a 15, y en caso de ser necesario otro registro origen, va ser 24 a 19 (también constante). Esto es siempre así, no depende del tipo de instrucción (más allá que hay instrucciones que no toman registro destino, ejemplo las de tipo B). Fue pensado así para ahorrar tiempo en la decodificación, uno puede optimizar a la hora de realizar la microarquitectura pues si ya sabe donde están los registros, puede ir a buscarlos mientras decodifica la instrucción (pero eso se va del scope de la materia, creo).

20. ¿Qué problemas puede ocasionar utilizar un registro de propósito general para el PC?  
    Podría ocacionar efectos secundarios, alguien podría romper el contrato (convención) lo que levantaría algún exception y rompería nuestro programa.

21. ¿Cómo se hace una lectura del PC?  
    Se va a la instrucción, se decodifica, y se va realizando, en simultaneo estamos también viendo cuál será nuestro PCNext, para poder repetir la operación en la siguiete iteración del clock.

22. RISC-V lee los datos little-endian, ¿qué significa? Da un ejemplo. ¿Cuándo es importante?  
    Significa que el byte de menor peso se almacena en la dirección más baja de memoria y el byte de mayor peso en la más alta.  
    Por ejemplo, definir `ejemplo: .byte 0x00 0x01 0x02 0x03` es equivalente a definir `ejemplo: .word 0x03020100`.  
    Es importante únicamente cuando se accede al mismo dato en modo word y byte.

23. RISC-V maneja el principio de "simplicidad", en relación a esto responda:  
    a) ¿Acceder a un operando en registro es más rápido que buscar el operando en memoria?  
        * Sí.
    b) A partir del inciso anterior, ¿cómo cree que impacta al rendimiento del programa y a la arquitectura la cantidad de registros disponibles?  
        * Mientras más registros, se debe acceder menos a la memoria para guardar/leer información, ergo, es más rápido.

24. ¿Cómo resuelve BGT (branch great than) con RISC-V32?  
    En RISC-V `bgt` es una pseudoinstrucción. `bgt x1 x2 label` (sería x1 > x2) se traduce a `blt x2 x1 label` (sería x2 < x1).  

25. Ensamblar el siguiente código:
    ```assembly
    add a0, a1, a6
    bltz x1, 0x0ABC
    ```
    Ejercicio al lector (agarrá el manual):
    1. Resolvé las pseudoinstrucciones (si las hubiese)
    2. Resolvé las etiquetas (si las hubiese).
    3. Agarrá y decodificá las instrucciones de el lenguaje ensamblador a binario.
