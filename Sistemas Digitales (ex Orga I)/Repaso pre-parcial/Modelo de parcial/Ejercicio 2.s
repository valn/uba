#  Ejercicio 2
# Implemente la funcion factorial en el lenguaje ensamblador RISC V de forma recursi va, respete la convencion de llamada presentada en la materia, explique el uso que le dara a cada registro y como se asegura que sus valores se preservan antes y despues de cada llamada a funcion.

# Nota: Ripes no me deja guardar con tildes, asi que tomenlas como imaginarias
# El registro a0 tomara el lugar del n, es el parametro de la funcion
# Tambien por la convencion, a0 sera el registro al cual se va a guardar el valor del resultado que genere mi funcion
# Utilizo t0 como un registro temporal para recuperar el n en los distintos llamados recursivos, una vez hecha la operacion de multiplicacion me da igual lo que pase con ese registro
# Aseguro que mis valores se preservan pues, modifico unicamente los registros a0 (parametro de mi funcion), t0 (registro temporal), y es cierto que modifico el sp en el prologo pero me aseguro siempre reestablecerlo en el epilogo

.data
n: .byte 0x3

.text
main:
lw a0 n # Cargo el n en a0
jal ra fact # Voy a calcular el factorial de n

# Ultimas dos reglas para terminar el programa (opcionales)
li a7, 93
ecall

fact:
beq a0 x0 fact_base # Si a0 (n) == 0, caso base
addi sp sp -16 # Dejo lugar en el sp para almacenar variables
sw a0 0(sp) # Guardo a0 (n) en el sp
sw ra 4(sp) # Guardo la posicion de retorno en el sp
addi a0, a0, -1 # Resto a a0 (n) 1
jal ra fact # Caso recursivo, calculo el factorial para n-1
            # Lo interesante de este paso es que como estoy asumiendo que devuelvo el valor del factorial de n en a0, puedo asumir que el caso recursivo me calculo (n-1)!
            # Es la misma idea de recursion que en programacion funcional (recursion explicita)
lw t0 0(sp) # Recupero mi a0 (n) y lo pongo en t0
lw ra 4(sp) # Recupero mi posicion de retorno
addi sp sp 16 # Subo nuevamente mi sp
mul a0 t0 a0 # Hago (n-1)! * n
ret # Return

fact_base:
li a0 1 # Devuelvo 1
ret # Return
