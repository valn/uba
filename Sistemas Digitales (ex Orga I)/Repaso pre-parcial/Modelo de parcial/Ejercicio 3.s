# Ejercicio 3
# (Enunciado completo en el PDF)

# Nota: Ripes no me deja guardar con tildes, asi que tomenlas como imaginarias

# Escriba un programa que cuente la cantidad de mediciones que se encuentran por sobre el valor
# 0x0F00. Si la cantidad de valores que superan este límite es mayor a la mitad del largo debemos
# poner un 1 en el registro a0, en caso contrario debemos poner un 0.

.data
# mediciones = [4352, 240, 41472, 4096]
# largo = 4
# limite = 3840
mediciones: .half 0x1100 0x00F0 0xA200 0x1000
largo: .byte 4

.text
main:
li a0 0 # Defino a0 como 0 pues es donde voy ir acumulando los valores que superan 0x0F00 (luego lo voy a pisar para la respuesta)
la a1 mediciones # Cargo la direccion de mediciones en a1
lw a2 largo # Cargo el largo en a2
li a3 0x0F00 # Constante, la quiero en un registro para hacer el sltu
srli t0 a2 1 # t0 := largo / 2
jal ra ciclo # Voy al ciclo
sltu a0 t0 a0 # a0 := if t0 < a0 then 1 else 0

# Ultimas dos reglas para terminar el programa (opcionales)
li a7, 93
ecall

ciclo:
beqz a2 fin # Si terminé de revisar todos los valores, return
# Si estoy acá, sigo en el ciclo
lhu t1 0(a1) # Cargo en t1 la half-word correspondiente
sltu t2 a3 t1 # t2 := if 0x0F00 < t1 then 1 else 0 | (pues a3 := 0x0F00)
add a0 a0 t2 # Agrego t2 a lo que tenía acumulado en a0
addi a1 a1 2 # Subo 2 indices (porque estoy trabajando con half-words) a mi indice de mediciones
addi a2 a2 -1 # Resto 1 al largo
j ciclo # Itero el ciclo

fin:
ret # Return