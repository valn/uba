# Ejercicio 1
# Se cuenta con cuatro datos de un byte cada uno almacenados en el registro s0 y queremos saber cuantos de esos datos son impares. Escriba un programa de ensamblador RISC V que realice esta operacion y almacene el resultado en el registro a0.

.data
dato: .byte 0x11 0xF0 0xA2 0x37

.text
main:
li a0 0 # Seteo a0 en 0 pues es donde voy a guardar los resultados
lw s0 dato # Carga el dato en s0
# Enmascaro los bytes 1x1, vamos del menos significativo al m�s significativo

# Dato 0
slli t0 s0 24 # t0 := 0x11000000
srli t0 t0 24 # t0 := 0x00000011
# Ahora veo el bit menos significativo, si es 1, es impar, si es 0 es par
andi t0 t0 0x1 # t0 := if esPar(0x00000011) then 0x1 else 0x0
add a0 a0 t0 # Sumo a a0 si es par

# Dato 1
slli t0 s0 16 # t0 := 0xF0000000
srli t0 t0 24 # t0 := 0x000000F0
# Ahora veo el bit menos significativo, si es 1, es impar, si es 0 es par
andi t0 t0 0x1 # t0 := if esPar(0x000000F0) then 0x1 else 0x0
add a0 a0 t0 # Sumo a a0 si es par

# Dato 2
slli t0 s0 8 # t0 := 0xA2000000
srli t0 t0 24 # t0 := 0x000000A2
# Ahora veo el bit menos significativo, si es 1, es impar, si es 0 es par
andi t0 t0 0x1 # t0 := if esPar(0x000000A2) then 0x1 else 0x0
add a0 a0 t0 # Sumo a a0 si es par

# Dato 3
add t0 x0 s0 # t0 := 0x37F0A211
srli t0 t0 24 # t0 := 0x00000037
# Ahora veo el bit menos significativo, si es 1, es impar, si es 0 es par
andi t0 t0 0x1 # t0 := if esPar(0x00000037) then 0x1 else 0x0
add a0 a0 t0 # Sumo a a0 si es par