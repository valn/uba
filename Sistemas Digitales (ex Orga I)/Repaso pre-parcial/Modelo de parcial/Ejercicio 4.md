# Ejercicio 4
## ¿De qué tamaño son las direcciones de memoria de la arquitectura RISC-V que vimos en la materia? ¿En cuánto debemos incrementar una dirección si queremos acceder a la próxima palabra? ¿Y al próximo byte?  

La arquitectura RISC-V que vimos en la materia es de 32 bits.  
Es por ello que contamos con 2^32 direcciones de memoria, cada una de ellas equivale a un byte. Generalmente trabajamos con palabras que equivalen a 4 bytes contiguos, es decir 4 índices de memoria contiguos (por ejemplo: 0x00000000 a 0x00000003 (inclusive)), es por ello que debemos incrementar en 4 una dirección de memoria si queremos acceder a la próxima palabra. En cambio si queremos acceder al próximo byte, debemos hacerlo incrementando en 1 el índice correspondiente a la memoria.  
