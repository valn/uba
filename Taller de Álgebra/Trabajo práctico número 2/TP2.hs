-- [REDACTED] Salvador [REDACTED] | [REDACTED]
-- [REDACTED] Benjamín [REDACTED] | [REDACTED]
-- [REDACTED] Valentín | [REDACTED]


type Complejo = (Float,Float)


-- 1.1
re :: Complejo -> Float
re (a, b) = a

-- 1.2
im :: Complejo -> Float
im (a, b) = b

-- 1.3
suma :: Complejo -> Complejo -> Complejo
suma (a,b) (c,d) = (a+c, b+d)

-- 1.4
producto :: Complejo -> Complejo -> Complejo
producto (a,b) (c, d)= (a*c-b*d, a*d+b*c)

-- 1.5
conjugado :: Complejo -> Complejo
conjugado (a, b) = (a, -b)

-- 1.6
inverso :: Complejo -> Complejo
inverso (a, b) = ((re conjZ)/modZ2, (im conjZ)/modZ2)
    where
        z = (a, b)
        conjZ = conjugado z
        modZ2 = a**2 + b**2

-- 1.7
cociente :: Complejo -> Complejo -> Complejo
cociente z w = producto z (inverso w)

-- 1.8
potencia :: Complejo -> Integer -> Complejo
potencia z 0 = (1, 0)
potencia z n = producto z (potencia z (n - 1))

-- 1.9
raicesCuadratica :: Float -> Float -> Float -> (Complejo,Complejo)
raicesCuadratica a b c
                      | discriminante >= 0 = (((-b + sqrt(discriminante)) / (2 * a), 0), ((-b - sqrt(discriminante)) / (2 * a), 0))
                      | discriminante < 0 = ((-b  / (2 * a), raizCompleja), (-b  / (2 * a), -raizCompleja))
                       where 
                            discriminante = b ** 2 - 4 * a * c 
                            raizCompleja = sqrt (-discriminante) / (2 * a)

-- 2.1
modulo :: Complejo -> Float
modulo (a, b) = sqrt(a**2 + b**2)

-- 2.2
-- Distancia entre dos puntos AB es igual a |B-A| (o |A-B| como pide el enunciado, es indistinto).
distancia :: Complejo -> Complejo -> Float
distancia (a, b) (c, d) = modulo((a-c, b-d))

-- 2.3
-- Separamos en cuadrantes
argumento :: Complejo -> Float
argumento (0, 0) = undefined
argumento (a, b)
                | b > 0 = acos (a/modulo z) -- 1er cuadrante y 2do cuadrante
--                | a > 0 && b > 0 = acos (a/modulo z) -- 1er cuadrante
--                | a < 0 && b > 0 = acos (a/modulo z) -- 2do cuadrante
                | a < 0 && b < 0 = pi - asin (b/modulo z) -- 3er cuadrante
                | a > 0 && b < 0 = 2*pi + asin (b/modulo z) -- 4to cuadrante
                 where
                    z = (a, b)

-- 2.4
pasarACartesianas :: Float -> Float -> Complejo
pasarACartesianas r theta = (a, b)
    where
        a = r*cos(theta)
        b = r*sin(theta)

-- 2.5
raizCuadrada :: Complejo -> (Complejo,Complejo)
raizCuadrada (0, 0) = ((0,0), (0,0)) -- Este caso no tiene sentido, pero en la consigna dice z perteneciente a C
raizCuadrada (a, b)
                   | a > 0 && b == 0 = ((sqrt(a), 0), (-sqrt(a), 0)) -- Las raices son dos reales
                   | a < 0 && b == 0 = ((0, sqrt(-a)), (0, -sqrt(-a))) -- Las raices son dos imaginarios puros
                   | b > 0 = ((c,d), (-c,-d)) -- Caso signos iguales
                   | b < 0 = ((c,-d), (-c,d)) -- Caso signos opuestos
                    where
                        c = sqrt(((modulo z) + a)/2)
                        d = sqrt(((modulo z) - a)/2)
                        z = (a, b)

-- 2.6
raicesCuadraticaCompleja :: Complejo -> Complejo -> Complejo -> (Complejo,Complejo)
raicesCuadraticaCompleja a b c = (r1, r2)
    where
        r1 = cociente (suma (producto (-1, 0) b) w1) (producto (2,0) a) -- 2da raíz obtenida
        r2 = cociente (suma (producto (-1, 0) b) w2) (producto (2,0) a) -- 1er raíz obtenida
        primerParteDelDiscriminante = potencia b 2 -- Separamos un poco el discriminante para que no quede tan feo de leer
        segundaParteDelDiscriminante = producto (-1, 0) (producto (producto (4, 0) a) c) -- Idem arriba
        raicesDelDiscriminante = raizCuadrada (suma primerParteDelDiscriminante segundaParteDelDiscriminante) -- Calculamos las raices del discriminante
        w1 = fst raicesDelDiscriminante -- 1er raíz del discriminante obtenida
        w2 = snd raicesDelDiscriminante -- 2da raíz del discriminante obtenida

-- 3.1
raicesNEsimas :: Integer -> [Complejo]
raicesNEsimas 0 = [(1,0)]
raicesNEsimas n = raicesNEsimasConW1 (pasarACartesianas 1 (2*pi/(fromIntegral n))) (n-1)

raicesNEsimasConW1 :: Complejo -> Integer -> [Complejo]
raicesNEsimasConW1 w1 0 = [(1,0)]
raicesNEsimasConW1 w1 k = [potencia w1 k] ++ raicesNEsimasConW1 w1 (k-1)

-- 3.2
-- Tenemos nuestro n, la lista, e
-- Vemos viendo cada Z_k de la lista para ver si cumple la condición que pide el enunciado
-- Si alguno no lo cumple, False
-- Si Z_k la cumple, vamos con Z_k+1, así hasta completar toda la lista
-- Si terminamos la lista sin llegar a ningún caso falso, entonces se cumple el enunciado
sonRaicesNEsimas :: Integer -> [Complejo] -> Float -> Bool
sonRaicesNEsimas n [] e = True
sonRaicesNEsimas n (x:xs) e
                           | modulo (suma (potencia x n) (-1,0)) >= e = False
                           | otherwise = sonRaicesNEsimas n xs e
