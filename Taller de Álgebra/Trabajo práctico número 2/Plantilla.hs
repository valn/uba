-- Apellido Nombre email #1
-- Apellido Nombre email #2
-- Apellido Nombre email #3


type Complejo = (Float,Float)


-- 1.1
re :: Complejo -> Float

-- 1.2
im :: Complejo -> Float

-- 1.3
suma :: Complejo -> Complejo -> Complejo

-- 1.4
producto :: Complejo -> Complejo -> Complejo

-- 1.5
conjugado :: Complejo -> Complejo

-- 1.6
inverso :: Complejo -> Complejo

-- 1.7
cociente :: Complejo -> Complejo -> Complejo

-- 1.8
potencia :: Complejo -> Integer -> Complejo

-- 1.9
raicesCuadratica :: Float -> Float -> Float -> (Complejo,Complejo)

-- 2.1
modulo :: Complejo -> Float

-- 2.2
distancia :: Complejo -> Complejo -> Float

-- 2.3
argumento :: Complejo -> Float 

-- 2.4
pasarACartesianas :: Float -> Float -> Complejo

-- 2.5
raizCuadrada :: Complejo -> (Complejo,Complejo)

-- 2.6
raicesCuadraticaCompleja :: Complejo -> Complejo -> Complejo -> (Complejo,Complejo)

-- 3.1
raicesNEsimas :: Integer -> [Complejo]

-- 3.2
sonRaicesNEsimas :: Integer -> [Complejo] -> Float -> Bool
