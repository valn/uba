identidad :: t -> t
identidad x = x

triple x = 3*x

maximo x y | x >= y = x
           | otherwise = y

distintos x y = x /= y

f :: (Num t, Ord t) => t -> t -> t
f x y | x > y = x

f1 :: (Floating t, Ord t) => t -> t -> t -> Bool
f1 x y z = x**y + z <= x + y**z

-- Nunca va a funcionar pues f3 :: (Floating t, Integral t) => t -> t -> t
f3 x y = div (sqrt x) (sqrt y)

f3_2 :: (Floating t, RealFrac t) => t -> t -> Int
f3_2 x y = div (round (sqrt x)) (round (sqrt y))

-- round 4.3 da 4
-- fromIntegral 4 int a 4 num

-- Se puede hacer sin guardas, i.e. sin |; en su lugar usar || (or)
-- El enunciado decía "dados dos números reales", requiero Num pues números y Ord pues preciso que sean ordenados
estanRelacionados :: (Ord t, Num t) => t -> t -> Bool
estanRelacionados x y | x <= 3 && y <= 3 = True
                      | (x > 3 && x <= 7) && (y > 3 && y <= 7) = True
                      | x > 7 && y > 7 = True
                      | otherwise = False

-- Son vectores de R2 y operaciones símples, por ello me basta con Num
prodInt :: (Num t) => (t , t) -> (t , t) -> t
prodInt (xa , ya) (xb , yb) = xa*yb - ya*yb

-- Requiero números y órden pues comparo elemento de R (son vectores de R2)
todoMenor :: (Ord t, Num t) => (t , t) -> (t , t) -> Bool
todoMenor (xa , ya) (xb , yb) = xa < xb && ya < yb

-- Requiero que sea un Floating pues estoy operando con raices cuadradas
distanciaPuntos :: (Floating t) => (t , t) -> (t , t) -> t
distanciaPuntos (xa , ya) (xb , yb) = sqrt( (xb - xa)^2 + (yb - ya)^2 )

-- Requiero Int pues me piden suma de una terna que pertenece a Z3 (i.e. cada coordenada es un entero)
sumaTerna :: (Int , Int, Int) -> Int
sumaTerna (x , y , z) = x + y + z

-- Me piden una terna par y que devuelva la pos. del núm. o 4. No preciso más que Int
posicPrimerPar :: (Int , Int, Int) -> Int
posicPrimerPar (x , y , z) | mod x 2 == 0 = 1
                           | mod y 2 == 0 = 2
                           | mod z 2 == 0 = 3
                           | otherwise = 4

-- x no tiene motivos para ser = y, i.e x (puede) =/ y.
-- Es por ello que utilizo dos variable de tipo distintas, las cuales no contienen restricción de ningún tipo pues "(debe funcionar para elementos de cualquier tipo"
crearPar :: a -> b -> (a , b)
crearPar x y = (x , y)

-- Misma justificación que antes, x (puede) =/ y
invertir :: (a, b) -> (b, a)
invertir (x , y) = (y , x)
