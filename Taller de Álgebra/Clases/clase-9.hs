division :: Int -> Int -> (Int, Int)
division a 0 = (0, 0)
division a d
            | a < d = (0, a)
            | otherwise = (fst qr' + 1, snd qr')
            where
                qr' = division (a-d) d

division2 :: Int -> Int -> (Int, Int)
division2 0 d = (0, 0)
division2 a d
            | esNegativo a = (fst qrP - 1, snd qrP) 
            | a >= d = (fst qrN + 1, snd qrN)
            | otherwise = (0, a)
            where
                qrN = division (a-d) d
                qrP = division (a+d) d

divisionZ :: Int -> Int -> (Int, Int)
divisionZ 0 d = (0, 0)
divisionZ a d
            | esNegativo a = (-(fst divPositiva) - 1, d - (snd divPositiva)) 
            | a >= d = (fst qr' + 1, snd qr')
            | otherwise = (0, a)
            where
                qr' = division (a-d) d
                divPositiva = division (-a) d

esNegativo :: Int -> Bool
esNegativo a = a < 0

absoluto :: Int -> Int
absoluto n
          | n >= 0 = n
          | otherwise = -n

-- Extraido del TP
mcd ::  Int -> Int -> Int
mcd a b
       | b == 0 = absoluto a
       | otherwise = mcd (b) (mod a b)

menorDivisor :: Int -> Int
menorDivisor 1 = 1
menorDivisor n = menorDivisorDesde n 2

menorDivisorDesde :: Int -> Int -> Int
menorDivisorDesde n m
                   | mod n m == 0 = m
                   | otherwise = menorDivisorDesde n (m+1)

mcd2 :: Int -> Int -> Int
mcd2 0 0 = 0
mcd2 a 0 = absoluto a
mcd2 0 b = absoluto b
mcd2 a b
        | a == 1 || b == 1 = 1
        | mDa == mDb = (mcd2 (div a mDa) (div b mDb)) * mDa
        | mDa > mDb = mcd2 a (div b mDb)
        | mDa < mDb = mcd2 (div a mDa) b
        where
            mDa = menorDivisor a
            mDb = menorDivisor b

{-
mcd2 :: Int -> Int -> Int
mcd2 a 0 = absoluto a
mcd2 a b = (fst qr')*a + snd qr'
    where qr' = division (mcd2 a b) a
-}

fst3 (x , _ , _) = x
snd3 (_ , y , _) = y
trd3 (_ , _ , z) = z

{-
emcd :: Int -> Int -> (Int, Int, Int)
emcd a b
        | 
        where
            a' = div a (mcd (a b))
            b' = div b (mcd (a b))
-}

emcd :: Int -> Int -> (Int, Int, Int)
emcd a 0 = (a, 1, 0)
emcd a b = (g, s, t)
        where
            (g, s', t') = emcd b (mod a b)
            s = t'
            t = s' - (t'*q)
            q = div a b
