factorial :: Int -> Int
factorial n
    | n == 0 = 1
    | otherwise = n * factorial (n - 1)

-- Ejercicios
fib :: Int -> Int
fib n
    | n == 0 = 0
    | n == 1 = 1
    | otherwise = fib(n - 1) + fib(n - 2)

parteEntera :: Float -> Integer
parteEntera x
    | x < 1 = 0
    | otherwise = 1 + parteEntera (x - 1)

multTres :: Int -> Bool
multTres n
    | n == 3 = True
    | n < 3 = False
    | otherwise = multTres (n - 3)

-- sumaImpares 5 --->> 1+3+5+7+9 = 25 | sumaImpares 9 --->> 1+3+5+7+9+11+13+15+17 = 81 = 9+9+9+9+9+9+9+9 = 9^2
-- SUMA ARITMÉTICA DE GAUSS
sumaImpares :: Int -> Int
sumaImpares n
    | n < 1 || mod n 2 == 0 = 0
    | mod n 2 /= 0 = n ^ 2

nesimoImpar :: Int -> Int
nesimoImpar n = 2 * n - 1

sumaImparesRecursivo :: Int -> Int
sumaImparesRecursivo n
    | n == 0 = 0
    | otherwise = (nesimoImpar n) + (sumaImparesRecursivo (n - 1))

medioFact :: Int -> Int
medioFact n
    | n == 1 = 1
    | n == 2 = 2
    | otherwise = n * medioFact (n - 2)

sumaDigitos :: Int -> Int
sumaDigitos n
    | n < 10 = mod n 10
    | otherwise = 1 + sumaDigitos (n - 10)

digitosIguales :: Int -> Bool
digitosIguales n
    | div n 10 == 0 = True
    | mod n 10 == mod (div n 10) 10 = digitosIguales (div n 10)
    | mod n 10 /= mod (div n 10) 10 = False
