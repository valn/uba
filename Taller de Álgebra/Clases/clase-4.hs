
f1 :: Int -> Int
f1 n
    | n == 0 = 1
    | otherwise = f1(n-1) + 2^n

f2 :: Int -> Float -> Float
f2 n q
      | n == 1 = q
      | otherwise = (f2 (n-1) q) + q^n

f3 :: Int -> Float -> Float
f3 n q
      | n == 0 = 0
      | n == 1 = q+q^2
      | otherwise = (f3 (n-1) q) + q^(2*n-1) + q^(2*n)

f4 :: Int -> Float -> Float
f4 n q
      | n == 1 = q+q^2
      | otherwise = (f3 (n-1) q) + q^(2*n-1) + q^(2*n) - (f2 (n-1) q)

factorial :: Int -> Int
factorial n
           | n == 0 = 1
           | otherwise = n * factorial (n - 1)

eAprox :: Int -> Float
eAprox n
        | n == 0 = 1
        | otherwise = eAprox(n-1) + (1/(fromIntegral(factorial n)))

f5 :: Int -> Int -> Float
f5 n m
      | n == 1 = fromIntegral(m)
      | otherwise = (f5 (n-1) m) + (f2 m (fromIntegral n))

-- "q y n dados", hasta m
sumaPotenciasAux :: Int -> Int -> Int -> Int
sumaPotenciasAux q n m
                      | m == 1 = q^(n+1)
                      | otherwise = q^(n+m) + (sumaPotenciasAux q n (m-1))

sumaPotencias :: Int -> Int -> Int -> Int
sumaPotencias q n m
                   | n == 1 = sumaPotenciasAux q 1 m
                   | otherwise = (sumaPotenciasAux q n m) + (sumaPotencias q (n-1) m)

sumaRacionalesAux :: Float -> Float -> Float
sumaRacionalesAux n m
                      | m == 1 = n
                      | otherwise = (sumaRacionalesAux n (m-1)) + (n/m)

sumaRacionales :: Float -> Float -> Float
sumaRacionales n m
                   | n == 1 = sumaRacionalesAux 1 m
                   | otherwise = (sumaRacionalesAux n m) + (sumaRacionales (n-1) m)

g1 :: Int -> Int -> Int
g1 i n
      | n == i = i^n
      | otherwise = i^n + (g1 i (n-1))

-- el g2 fue puro dolor, agonía y sufrimiento. Me encantó
-- desarrollo un poco la serie https://i.imgur.com/kBMnt5M.png
-- luego hago g2Aux que responde a un iter (m), en la cual paso el mismo n https://i.imgur.com/LVgdZVl.png
-- después planteo g2Aux2 para iterar todos los términos desde n hasta 1 (quiero hacer g2Aux n n-1 + g2Aux n n-2 + g2Aux n n-3 ...)
-- si no uso g2Aux2 me queda (g2Aux n n-1 + g2Aux n-1 n-2 + ...)
-- finalmente hago g2, utilizando las dos funciones anteriores pensando en https://i.imgur.com/roVvDzt.png
-- y listo
g2Aux :: Int -> Int -> Int
g2Aux n m
      | n == m = m^n
      | otherwise = (g2Aux (n-1) m) + m^n

g2Aux2 :: Int -> Int -> Int
g2Aux2 n m
      | m == 1 = n
      | otherwise = (g2Aux n m) + (g2Aux2 n (m-1))

g2 :: Int -> Int
g2 n
      | n == 1 = g2Aux n 1
      | otherwise = (g2Aux2 n n)

g3 :: Int -> Int
g3 n
    | n == 2 = 4
    | mod n 2 == 0 = (g3 (n-1)) + (2^n)
    | otherwise = g3 (n-1)

digitosIguales :: Int -> Bool
digitosIguales n
    | div n 10 == 0 = True
    | mod n 10 == mod (div n 10) 10 = digitosIguales (div n 10)
    | mod n 10 /= mod (div n 10) 10 = False

sumaNatMenores :: Int -> Int
sumaNatMenores n
                | n == 1 = 1
                | digitosIguales n = n + (sumaNatMenores (n-1))
                | otherwise = sumaNatMenores (n-1)
