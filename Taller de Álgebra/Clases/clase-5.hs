sumaDivisores :: Int -> Int
sumaDivisores n = sumaDivisoresHasta n n

sumaDivisoresHasta :: Int -> Int -> Int
sumaDivisoresHasta n m
                | m == 1 = m
                | mod n m == 0 = sumaDivisoresHasta n (m-1) + m
                | otherwise = sumaDivisoresHasta n (m-1)

menorDivisor :: Int -> Int
menorDivisor n = menorDivisorAux n 2

menorDivisorAux :: Int -> Int -> Int
menorDivisorAux n m
                   | mod n m == 0 = m
                   | otherwise = menorDivisorAux n (m+1)

esPrimo :: Int -> Bool
esPrimo 0 = False
esPrimo 1 = False
esPrimo n = menorDivisor n == n

nEsimoPrimo :: Int -> Int
nEsimoPrimo n = nEsimoPrimoAux n 2

nEsimoPrimoAux :: Int -> Int -> Int
nEsimoPrimoAux n p
                  | n == 0 = p-1
                  | esPrimo p = nEsimoPrimoAux (n-1) (p+1)
                  | otherwise = nEsimoPrimoAux n (p+1)

factorial :: Int -> Int
factorial n
           | n == 0 = 1
           | otherwise = n * factorial (n - 1)

-- Implementar menorFactDesde :: Int -> Int que dado m ≥ 1 encuentra el mínimo n ≥ m tal que n = k! para algun k.
menorFactDesde :: Int -> Int
menorFactDesde m = menorFactDesdeAux m 1

menorFactDesdeAux :: Int -> Int -> Int
menorFactDesdeAux n k
                     | factorial k >= n = factorial k
                     | otherwise = menorFactDesdeAux n (k+1)

mayorFactHasta :: Int -> Int
mayorFactHasta m = mayorFactHastaAux m 1

mayorFactHastaAux :: Int -> Int -> Int
mayorFactHastaAux n k
                     | factorial k > n = factorial (k-1)
                     | otherwise = mayorFactHastaAux n (k+1)

esFact :: Int -> Bool
esFact n = esFactAux n 1

esFactAux :: Int -> Int -> Bool
esFactAux n k
             | factorial k > n = False
             | factorial k == n = True
             | otherwise = esFactAux n (k+1)

fibonacci :: Int -> Int
fibonacci 0 = 0
fibonacci 1 = 1
fibonacci n = fibonacci (n-1) + fibonacci (n-2)

esFibonacci :: Int -> Bool
esFibonacci n = esFibonacciAux n 0

esFibonacciAux :: Int ->  Int -> Bool
esFibonacciAux n m
                  | fibonacci m > n = False
                  | fibonacci m == n = True
                  | otherwise = esFibonacciAux n (m+1)

esSumaInicialDePrimos :: Int -> Bool
esSumaInicialDePrimos n = esSumaInicialDePrimosDesde n 0

esSumaInicialDePrimosDesde  :: Int -> Int -> Bool
esSumaInicialDePrimosDesde n m
                              | sumaDePrimerosnPrimos m == n = True
                              | sumaDePrimerosnPrimos m > n = False
                              | otherwise = esSumaInicialDePrimosDesde n (m+1)

sumaDePrimerosnPrimos :: Int -> Int
sumaDePrimerosnPrimos n
                       | n == 0 = 0
                       | otherwise = nEsimoPrimo n + sumaDePrimerosnPrimos (n-1)
