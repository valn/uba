absoluto :: Int -> Int
absoluto n
          | n >= 0 = n
          | otherwise = -n

mcd ::  Int -> Int -> Int
mcd a b
       | b == 0 = absoluto a
       | otherwise = mcd (b) (mod a b)

emcd :: Int -> Int -> (Int, Int, Int)
emcd a 0 = (a, 1, 0)
emcd a b = (g, s, t)
        where
            (g, s', t') = emcd b (mod a b)
            s = t'
            t = s' - (t'*q)
            q = div a b

tieneSolucion :: Int -> Int -> Int -> Bool
tieneSolucion a b m = mod b (mcd a m) == 0

solucionParticular :: Int -> Int -> Int -> Int
solucionParticular a b m | tieneSolucion a b m = c*x
                        where
                            (mcd, x, _) = emcd a m
                            c = div b mcd

solucionGeneral :: Int -> Int -> Int -> (Int, Int)
solucionGeneral a b m
                     | not (tieneSolucion a b m) = undefined
                     | mcd /= 1 = solucionGeneral a' b' m'
                     | otherwise = (c*x, m)
                    where
                        (a', b', m') = (div a mcd, div b mcd, div m mcd)
                        (mcd, x, _) = emcd a m
                        c = div b mcd
