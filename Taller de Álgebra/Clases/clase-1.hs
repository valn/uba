-- Algunos ejemplos:

-- Propios
subConj n = 2 ^ n
cuadrado x = x ^ 2

-- Inicio de la clase
double x = 2 * x

f x y = x * x + y * y
g x y z = x + y + z * z

suma x y = x + y
normaVectorial x1 x2 = sqrt(cuadrado x1 + cuadrado x2)
funcionConstante8 x = 8

-- F. por partes
fPart n | n == 0 = 1
        | otherwise = 0

gPart n | n == 0 = 1
        | n /= 0 = 0

hPart 0 = 1
hPart n = 0

signo n | n > 1 = 1
        | n == 0 = 0
        | n < -1 = -1

-- Forzando erroes
f1 n | n >= 3 = 5

f2 n | n < 3 = 5
     | n > 1 = 6

-- Cantidad de soluciones de una función cuadrática mónica (a = 1)
cantidadDeSolCuadMon b c | b^2 - 4*c > 0 = 2
                         | b^2 - 4*c == 0 = 1
                         | otherwise = 0

cantidadDeSolCuadMon2 b c | d > 0 = 2
                          | d == 0 = 1
                          | otherwise = 0
                          where d = b^2 - 4*c

-- Signatura
double2 :: Int -> Int
double2 x = 2 * x

-- Comienzo de la práctica:

-- Implementación de funciones especificando signatura
-- Funciones "de soporte"
maximo :: Int -> Int -> Int
maximo x y | x >= y = x
           | otherwise = y

esIgualACero :: Float -> Bool
esIgualACero n | n == 0 = True
               | otherwise = False

-- Polémico. Reafirmo, el cero no es natural.
esNatural :: Int -> Bool
esNatural n | n > 0 = True
            | otherwise = False
-- Fin de las funciones "de soporte"

absoluto :: Int -> Int
absoluto n | n >= 0 = n
           | otherwise = -n

maximoabsoluto :: Int -> Int -> Int
maximoabsoluto x y = maximo (absoluto x) (absoluto y)

maximo3 :: Int -> Int -> Int -> Int
maximo3 x y z = maximo (maximo x y) z

algunoEs0 :: Float -> Float -> Bool
algunoEs0 x y | (esIgualACero x)|| (esIgualACero y) = True
              | otherwise = False

algunoEs0PM :: Float -> Float -> Bool
algunoEs0PM 0 _ = True
algunoEs0PM _ 0 = True
algunoEs0PM x y = False

ambosSon0 :: Float -> Float -> Bool
ambosSon0 x y | (esIgualACero x) && (esIgualACero y) = True
              | otherwise = False

ambosSon0PM :: Float -> Float -> Bool
ambosSon0PM 0 0 = True
ambosSon0PM x y = False

-- Si X es múltiplo de Y entoncen True, otherwise = False
-- Utilizando "mod (dividendo) (divisor)" me devuelve el resto. Si el resto es 0 entonces el dividendo (x) es múltiplo del divisor (y)
-- "False" si no es natural (o no es múltiplo)
esMultiploDe :: Int -> Int -> Bool
esMultiploDe x y | esNatural x && esNatural y && mod x y == 0 = True
                 | otherwise = False

-- Sup. 197, quiero el 7; 97, 7; 7, 7; 0, 0
-- -1 si no es natural
digitoUnidades :: Int -> Int
digitoUnidades n | esNatural n = mod n 10
                 | otherwise = -1

-- Sup. 197, quiero el 9; 97, 9; 7, 0; 0, 0
-- -1 si no es natural
digitoDecenas :: Int -> Int
digitoDecenas n | esNatural n = digitoUnidades(div n 10)
                | otherwise = -1
