factorial :: Integer -> Integer
factorial n
           | n == 0 = 1
           | otherwise = n * factorial (n - 1)

combinatorio :: Integer -> Integer -> Integer
combinatorio n k
                | k == 0 = 1
                | k == n = 1
                | otherwise = combinatorio (n-1) k + combinatorio (n-1) (k-1)

combinatorio2 :: Integer -> Integer -> Integer
combinatorio2 n k = div (factorial n) ((factorial k)* factorial (n-k))

type Set a = [a]

agregar :: [Integer] -> Set [Integer] -> [Set Integer]
agregar x a
           | elem x a = a
           | otherwise = x:a

variaciones :: Set Integer -> Integer -> Set [Integer]
variaciones xs 0 = [[]]
variaciones xs n = agregarTodosEnCadaLista xs (variaciones xs (n-1))

agregarElemEnCadaLista :: Integer -> Set [Integer] -> Set [Integer]
agregarElemEnCadaLista n [] = []
agregarElemEnCadaLista n (xs : xss) = agregar (n:xs) (agregarElemEnCadaLista n xss)

{-
agregarElemEnCadaLista 1 [[2]] --- [[1,2]]
agregarElemEnCadaLista 1 [[2,3]] --- [[1,2,3]]
agregarElemEnCadaLista 1 [[2,3], [4]] --- [[1,2,3], [1,4]]

agregarElemEnCadaLista 1 [[3,4]] --- [[1,3,4]]
agregarElemEnCadaLista 2 [[3,4]] --- [[2,3,4]]
-}

agregarTodosEnCadaLista :: Set Integer -> Set [Integer] -> Set [Integer]
agregarTodosEnCadaLista [] xss = []
agregarTodosEnCadaLista (y : ys) xss = (agregarElemEnCadaLista y xss) ++ (agregarTodosEnCadaLista ys xss)

{-
agregarTodosEnCadaLista [1,2] [[3,4]] --- [[1,3,4], [2,3,4]]
agregarTodosEnCadaLista [1,2] [[3,4], [5,6]] --- [[1,3,4], [2,3,4], [1,5,6], [2,5,6]]
-}

insertarEn :: [Integer] -> Integer -> Integer -> [Integer]
insertarEn xs n 1 = n:xs
insertarEn (x : xs) n i = x:(insertarEn xs n (i-1))

permutaciones :: Integer -> Set [Integer]
permutaciones 1 = [[1]]
permutaciones n = (insertarNumEnTodosLasListas n (permutaciones (n-1)))

insertarNumEnTodosLosIndices :: Integer -> [Integer] -> Integer -> Set [Integer]
insertarNumEnTodosLosIndices n xs 1 = (insertarEn xs n 1):[]
insertarNumEnTodosLosIndices n xs i = (insertarEn xs n i):(insertarNumEnTodosLosIndices n xs (i-1))

insertarNumEnTodosLasListas :: Integer -> Set [Integer] -> Set [Integer]
insertarNumEnTodosLasListas n [] = []
insertarNumEnTodosLasListas n (xs : xss) = (insertarNumEnTodosLosIndices n xs n) ++ (insertarNumEnTodosLasListas n xss)

{-
permutaciones 1 ... [[1]]
permutaciones 2 ... [[1, 2], [2, 1]]
permutaciones 3 ... [[1, 2, 3], [1, 3, 2], [3, 1, 2], [2, 1, 3], [2, 3, 1], [3, 2, 1]]
-}

{-
insertarNumEnTodosLosIndices 3 [1, 2] 3 ... [[1,2,3],[1,3,2],[3,1,2]]
insertarNumEnTodosLasListas 3 [[1, 2], [2, 1]] ... [[1,2,3],[1,3,2],[3,1,2],[2,1,3],[2,3,1],[3,2,1]]
-}

-- desarrollo https://cms.dm.uba.ar/academico/materias/2docuat2015/algebra_I/Combinatoria-Puddu.pdf página 9
-- cantidad: combinatorio2 (b+c-1) (b)
bolitasEnCajas :: Integer -> Integer -> Set [Integer]
bolitasEnCajas b c = variaciones [1..b] c
