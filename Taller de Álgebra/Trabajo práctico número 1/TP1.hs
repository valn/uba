-- [REDACTED] Salvador [REDACTED]
-- [REDACTED] Benjamín [REDACTED]
-- [REDACTED] Valentín
 
-- EJERCICIO 1. sonCoprimos
-- Dos números son coprimos sí y solo sí su MCD es igual a 1.
-- Por el algoritmo de Euclides (visto en la teórica) sabemos que aplicando divisiones sucesivas, se tiene que (a:b) es el último resto no nulo.
sonCoprimos :: Integer -> Integer -> Bool
sonCoprimos n m = algoritmoDeEuclides n m == 1

-- Función utilizada para conseguir el resto
valorAbsoluto :: Integer -> Integer
valorAbsoluto n
               | n < 0 = -n
               | otherwise = n

-- Sabemos que (a:b) = (b:a), inclusive en el algoritmo de Euclides pues (a:b) simétrico (visto en la teórica).
-- Si pensamos en (a:b), es indistinto para el algoritmo si a = b, a < b ó a > b
-- Pues si a = b ó a > b el algoritmo funciona de raíz como vimos en la teórica
-- Si a < b (e.g.: (81:90)) volverá a ejecutar el algo. de Euclides con (90: mod 81 90)
-- Y, mod 81 90 = 81 (el resto de dividir a 81 por 90) ent. (90:81) y pasamos al caso a > b
-- Luego, devolvemos siempre a positivo, pues si b es negativo obtenemos el MCD pero en negativo y no es la idea.
-- Otra opción sería "corregir" a y b (i.e. si a y/o b es negativo, pasarlos a positivos y después ejecutar el algoritmo)
algoritmoDeEuclides ::  Integer -> Integer -> Integer
algoritmoDeEuclides a b
                       | b == 0 = valorAbsoluto a
                       | otherwise = algoritmoDeEuclides (b) (mod a b)

-- EJERCICIO 2: es2Pseudoprimo
-- Los 2-pseudoprimos son los números naturales compuestos (no pueden ser ni primos, ni 0 [trivial pues no se puede dividir por 0], ni 1 o -1) (llamémoslos n), que dividen 2^(n−1) − 1.
-- Podemos reutilizar funciones vistas en clases anteriores para crear noEsPrimo.
-- Excluimos al uno manualmente, pues no ni compuesto ni primo y cumple. (Véase la exclusión manual para no dejar mal definida "noEsPrimo")
es2Pseudoprimo :: Integer -> Bool
es2Pseudoprimo n
                | (not (n == 1)) && (noEsPrimo n) && (mod (2^(n-1) - 1) n == 0) = True
                | otherwise = False

-- Obtiene el primer divisor distinto de uno
menorDivisor :: Integer -> Integer
menorDivisor n = menorDivisorHasta n 2

-- Ayuda a la función anterior verificando desde m hasta n (pues n siempre va a tener un divisor, n)
menorDivisorHasta :: Integer -> Integer -> Integer
menorDivisorHasta n m
                     | mod n m == 0 = m
                     | otherwise = menorDivisorHasta n (m+1)

-- Función auxiliar para es2Pseudoprimo
noEsPrimo :: Integer -> Bool
noEsPrimo 0 = True
noEsPrimo 1 = True
noEsPrimo n = not (menorDivisor n == n)

-- EJERCICIO 3: cantidad3Pseudoprimos
-- Primero necesitamos la función es3Pseudoprimo, luego
-- usaremos la función auxiliar cantidad3PseudoprimosHasta (utilizando así dos parámetros) para contar la cantidad de 3-pseudoprimos que existen entre 1 y m (para la función original).
cantidad3Pseudoprimos :: Integer -> Integer
cantidad3Pseudoprimos m = cantidad3PseudoprimosHasta m 1

-- Revisa todos los k naturales buscando 3Pseudoprimos, una vez enontrado uno, ejecuta +1 y suma uno a k (caso de que no, recursión). Luego, una vez que k llega a m+1 sé que conté todos los 3Pseudoprimos (estoy incluyendo contar m).
cantidad3PseudoprimosHasta :: Integer -> Integer -> Integer
cantidad3PseudoprimosHasta m k
                              | k == m+1 = 0
                              | es3Pseudoprimo k = cantidad3PseudoprimosHasta m (k+1) + 1
                              | otherwise = cantidad3PseudoprimosHasta m (k+1)

-- Idem es2Pseudoprimo
es3Pseudoprimo :: Integer -> Bool
es3Pseudoprimo n
                | (not (n == 1)) && (noEsPrimo n) && (mod (3^(n-1) - 1) n == 0) = True
                | otherwise = False

-- EJERCICIO 4: kesimo2y3Pseudoprimo
-- Primero definimos la función es2y3Pseudoprimo para poder saber así si un número n es 2-pseudoprimo y 3-pseudoprimo simultaneamente.
-- Después, definimos una función auxiliar kesimo2y3PseudoprimoHasta que nos permite iterar hasta poder posicionarnos sobre el 2y3Pseudoprimo buscado.
-- (Idem a lo visto en la clase 5 en nEsimoPrimo)
kesimo2y3Pseudoprimo :: Integer -> Integer
kesimo2y3Pseudoprimo n = kesimo2y3PseudoprimoHasta n 2

-- Función corta auxiliar para verificar la simultaneidad de 2Pseudoprimos y 3Pseudoprimos (escribir código más limpio)
es2y3Pseudoprimo :: Integer -> Bool
es2y3Pseudoprimo n = es3Pseudoprimo n && es2Pseudoprimo n

-- Similar a cantidad3PseudoprimosHasta,
-- Si encuentra un p 2-Pseudoprimo y 3-Pseudoprimo resta un valor a n
-- Siempre termina en recursión hasta que n es 0 (es decir, llegó al n-ésimo pseudoprimo)
-- Luego resta uno a p pues por la última recursión al n-ésimo pseudoprimo le sumamos 1
kesimo2y3PseudoprimoHasta :: Integer -> Integer -> Integer
kesimo2y3PseudoprimoHasta n p
                             | n == 0 = p-1
                             | es2y3Pseudoprimo p = kesimo2y3PseudoprimoHasta (n-1) (p+1)
                             | otherwise = kesimo2y3PseudoprimoHasta n (p+1)

-- EJERCICIO 5: esCarmichael
esCarmichael :: Integer -> Bool
esCarmichael n = esCarmichaelDesde n (n-1)


-- La definición de los números de Carmichael dice que n es un número de Carmichael si n es un natural compuesto que cumple lo siguente:
-- si a es coprimo con n (a entre 1 y n-1), n también debe ser a-pseudoprimo
-- Esta función está pensada para que le pasen n y n-1
-- Nuestro caso base es a = 1
-- Si n y a son coprimos pero n NO es a-pseudoprimo, entonces podemos devolver False
-- Caso contrario (n y a no son coprimos) vamos con el término anterior a n-1 (n-1-1 = n-2)
-- Vamos yendo así hasta llegar a 1 (en el lugar de n-1)
-- Luego, sabemos que con 1 siempre se va a cumplir pues todo número n es coprimo con 1
-- y todos los números son "1-pseudoprimos".
-- Si llegamos al caso a = 1 es porque todos los números estrictamente anteriores a n cumplen la cond.
-- impuesta por Carmichael, ent. n es un número de Carmichael.
esCarmichaelDesde :: Integer -> Integer -> Bool
esCarmichaelDesde n a
                     | a == 1 = True
                     | sonCoprimos n a && not (esaPseudoprimo n a) = False
                     | otherwise = esCarmichaelDesde n (a-1)

esaPseudoprimo :: Integer -> Integer -> Bool
esaPseudoprimo n a
                  | (not (n == 1)) && (noEsPrimo n) && (mod (a^(n-1) - 1) n == 0) = True
                  | otherwise = False
