#import "@preview/ilm:1.2.1": *
#import "@preview/codly:1.0.0": *
#show: codly-init.with()

#set text(lang: "es")

#show: ilm.with(
  title: [Arquitectura y Organización de Computadores],
  author: "@valnrms",
  date: datetime.today(),
  date-format: "Fecha de compilación: [day padding:zero] / [month padding:zero] / [year repr:full].",
  abstract: [
    Resumen (teórico-práctico) de la materia Arquitectura y Organización de Computadores de la Universidad Nacional de Buenos Aires. \

    Profesor: #link("https://ar.linkedin.com/in/afurfaro")[Alejandro Furfaro]. \
  ],
  preface: [
    #align(center + horizon)[
      Este texto asume que el lector tiene una ligera base del lenguaje de programación C. \
      Está principalmente hecho para mí. De ninguna manera reemplaza cualquier material oficial que pueda tener la cátedra, ni mucho menos las clases teórias y prácticas de la materia. \
      \
      Algo trivial, aunque es importante recalcar, este texto puede contener $#text(font:"Fira Math")[_orrorez_]$.
    ]
  ],
)

= Introducción a C, \~kinda\~
== Source y header files
En C, es común dividir el código en dos tipos de archivos: *source files* y *header files*. \
Los *source files* contienen la implementación de las funciones, mientras que los *header files* contienen las declaraciones de las funciones. Por ejemplo, si tenemos el siguiente código en C:
```C
// hola.c
#include <stdio.h> // Incluye la biblioteca stdio.h.

void hola() { // Implementación y declaración de la función hola.
  printf("Hola!\n"); // Imprime en pantalla.
}
```

Podemos dividirlo en dos archivos, uno de implementación y otro de declaración:

```C
// hola.h
#ifndef HOLA_H
#define HOLA_H

void hola(); // Declaración de la función hola.

#endif
```

```C
// hola.c
#include <stdio.h> // Incluye la biblioteca stdio.h.

void hola() { // Implementación de la función hola. Notar que una implementación es también una declaración.
  printf("Hola!\n"); // Imprime en pantalla.
}
```

`#ifndef` y `#define` son directivas del preprocesador de C. Se denominan *include guards* y sirven para evitar la inclusión múltiple de un archivo header. \
`#ifndef` significa *if not defined* y `#define` significa *define*. \

`#endif` es el cierre de la directiva `#ifndef`. \

Si el símbolo `HOLA_H` no está definido, se define y se incluye el contenido del archivo. Si ya está definido, no se incluye el contenido del archivo. \
Esto evita errores de compilación por definiciones múltiples _(ejemplo: `main.c` incluye a `a.h`, que incluye a `b.h`, que incluye a `a.h`)_. \

Luego si queremos usar la función `hola` en otro archivo, simplemente incluimos el archivo header:

```C
// main.c
#include <hola.h> // Incluye el archivo header hola.h.

int main() { // main es el punto de entrada del programa.
  hola(); // Llama a la función hola.
  return 0; // Devuelve un 0 al SO.
}
```

Luego el *linker* se encargará de unir ambos archivos en un solo archivo ejecutable. \
¿Qué es el linker?, ahora lo veremos...

== Compilador, linker y demás
Tenemos el siguiente código en C:
```C
#include <stdio.h> // Incluye la biblioteca stdio.h.

int main() { // main es el punto de entrada del programa.
  printf("Hola!\n"); // Imprime en pantalla.
  return 0; // Devuelve un 0 al SO.
}
```
Y ahora queremos compilarlo, es por ello que ejecutamos:
```bash
$ gcc hola.c -o hola
```
Esto nos devuelve un archivo ejecutable llamado `hola`:
```bash
$ ./hola
Hola!
```
Sin embargo, es interesante preguntarse, ¿qué sucede cuando ejecutamos el comando `gcc`? \

Al compilar un código en lenguaje C, suceden varias cosas. \
Primero, el compilador traduce el código fuente a *lenguaje objeto* (archivos `.o`). \
Este tipo de archivos se encuentra ya en lenguaje máquina, sin embargo, falta el proceso de *enlazamiento* _(linkering)_. \

El linker tiene más sentido cuando se trabaja con más de un archivo fuente. Por lo que vamos a suponer que: \

Tenemos más de un archivo fuente, por ejemplo, `hola.c` y `chau.c`. Donde `hola.c` contiene la función `main`, que en algún momento llama a una función `chau` que se encuentra en `chau.c`. \

```C
// chau.h
#ifndef CHAU_H
#define CHAU_H

void chau(); // Declaración de la función chau.

#endif
```

```C
// chau.c
#include <stdio.h> // Incluye la biblioteca stdio.h.

void chau() { // Implementación de la función chau.
  printf("Chau!\n"); // Imprime en pantalla.
}
```

```C
// main.c
#include <chau.h> // Incluye el archivo header chau.h.

int main() { // main es el punto de entrada del programa.
  chau(); // Llama a la función chau.
  return 0; // Devuelve un 0 al SO.
}
```

El compilador traducirá ambos archivos a lenguaje objeto, generando `hola.o` y `chau.o`. \
Luego, el *linker* se encargará de unir ambos archivos objeto en un solo archivo ejecutable. \

¿De qué manera?, buscará, por ejemplo `chau.h`, tanto en los archivos objeto que le pasamos, como en algunos directorios predefinidos del sistema. En caso de no encontrarlo, levantará un error. \

Podemos forzar ese paso intermedio en `gcc` con el siguiente commando:
```bash
$ gcc -c main.c -o main.o
$ gcc -c chau.c -o chau.o
$ gcc main.o chau.o -o binario
$ ./binario
Chau!
```

#align(center)[
  #image("images/compiler.png", width: 72%)
]

== Extras
`sizeof()` devuelve el tamaño en bytes de un tipo de dato. Se puede tanto pasarle un tipo de dato como una variable. \
```C
sizeof(char); // 1
sizeof(short); // 2
sizeof(int); // 4
sizeof(long); // 8
```
```C
int variable = 777;
sizeof(variable); // 4
```

- $++i$ incrementa $i$ y luego devuelve el valor de $i$.
- $i++$ devuelve el valor de $i$ y luego incrementa $i$.
- $alpha = beta = gamma = 0$ asigna 0 a $alpha$, $beta$ y $gamma$.
- $alpha = beta = gamma$ asigna el valor de $gamma$ a $alpha$ y $beta$.

```C
#include <stdio.h>

int main() {
  int a = 0;

  a = 1;
  printf("a = %d\n",a); // a = 1
  printf("a++ = %d\n",a++); // a++ = 1
  printf("++a = %d\n", ++a); // ++a = 3
  printf("a = %d\n",a); // a = 3

  return 0;
}
```

El shifting a derecha depende del tipo de dato (lógico vs aritmético). \
En general _(depende de la implementación)_: si es unsigned, agrega 0s a la izquierda; y si es signed, agrega 1s a la izquierda. \
Como depende de la implemetanción, la \# de shifteo no puede ser mayor que el tamaño del tipo de dato, se lo denomina *undefined behavior*.

- La forma de saber que termina una cadena de caracteres es con el caracter nulo `\0`.

- No se puede devolver un array en C, pero sí se puede devolver un puntero _(dirección de memoria)_ a un array.

- En C los parámetros de una función son pasados por valor, es decir, se crea una copia local de la variable. \

- Scope:
  - Block scope: variables declaradas dentro de un bloque de código.
  - File scope: variables declaradas fuera de cualquier función, son globales.

- Duración:
  - Static duration: variables que duran durante toda la ejecución del programa.
  - Dynamic duration: variables que duran mientras el bloque de código está en ejecución.
  - Automatic duration: variables que duran mientras la función está en ejecución.

Se puede forzar que una variable sea estática con la palabra clave `static`, ejemplo: `static int variable = 0;`.

- Se puede usar `#define NOMBRE VALOR` para definir constantes, se reemplaza en tiempo de preprocesamiento. Son como macros. \

- Algo de *Strings*:
```C
int main() {
  char s[] = "Hola!"; // s es el nombre del array
  char *u = "string"; // u es un puntero a un char

  printf("s = %s\n", s);
  printf("u = %s\n", u);
  s[0] = 'h'; // s = "hola!"
  u[0] = 'S'; // ERROR
}
```

```bash
$ gcc -Wall -Wextra -pedantic strings.c -o strings
$ ./strings
s = Hola!
u = string
[1] 65377 segmentation fault (core dumped) ./strings
```

- Puede haber muchas declaraciones de una función, pero solo una definición.

Para declarar una variable global, hay que tener cuidado. \
Hacer `int variable;` en un archivo header, no solo declara la variable, sino que también la define. \
Para evitar esto, se puede usar `extern int variable;` en un archivo header y `int variable;` en un *único* archivo fuente.

== Makefile
Un *Makefile* es un archivo que contiene un conjunto de reglas que le dicen a `make` cómo compilar y enlazar un programa. \
No es exclusivo de C, también se puede usar en otros lenguajes como C++, Python, Go, etc. \

Un buen uso de *Makefile* es para compilar programas grandes, donde hay muchos archivos fuente y se necesita compilarlos de manera eficiente. \
Nos permite compilar solo los archivos que cambiaron, en lugar de compilar todo el programa. \

Un ejemplo de *Makefile*:
```Makefile
CC:=gcc
CFLAGS:= -Wall -Wextra -pedantic -std=c11 -O0 -ggdb

main: main.o
$(CC) $(CFLAGS) $^ -o $@

main.o: main.c
$(CC) $(CFLAGS) -c $< -o $@
```

- `CC` es el compilador que vamos a usar.
- `CFLAGS` son las flags que vamos a pasar al compilador.
- `main` es el target que queremos compilar.
- `main.o` es el archivo objeto que queremos generar.
- `$^` es una variable mágica que contiene todas las dependencias.
- `$@` es una variable mágica que contiene el target.
- `$<` es una variable mágica que contiene la primera dependencia.

```bash
$ make
gcc -Wall -Wextra -pedantic -std=c11 -O0 -ggdb -c main.c -o main.o
gcc -Wall -Wextra -pedantic -std=c11 -O0 -ggdb main.o -o main
$ make
make: 'main' is up to date.
```

Ejemplo más groso de Makefile:
```Makefile
CC:=gcc
CFLAGS:= -Wall -Wextra -pedantic -std=c11 -O0 -ggdb

TARGET := binario
.PHONY: all clean rebuild

all: $(TARGET)

$(TARGET): main.o funca.o funcb.o
$(CC) $(CFLAGS) $^ -o $@

funca.o: funca.c funca.h
$(CC) $(CFLAGS) -c $< -o $@

funcb.o: funcb.c funcb.h
$(CC) $(CFLAGS) -c $< -o $@

main.o: main.c funca.h funcb.h
$(CC) $(CFLAGS) -c $< -o $@

clean:
rm -rf *.o
rm -rf $(TARGET)

rebuild: clean all
```

#pagebreak()
= Introducción a arquitectura y organización de computadores
== Arquitectura vs microarquitectura
- *Arquitectura:* Es con lo que el programador cuenta para realizar aplicaciones/sistemas operativos. Por lo general se mantiene constante a lo largo de varias generaciones de microprocesadores para mantener la compatibilidad con el software existente. \
  - Registros (nombres, tamaño).
  - Set de instrucciones (qué podemos hacer con ese procesador).
  - Estrucutras de memoria _(por ejemplo, descriptores de segmento y de página)_.

Más cercano al mundo de la *especificación*.

- *Microarquitectura:* Es la implementación en el silicio de la arquitectura. Lo que está detrás del set de registros y del modelo de programación. Puede ser muy simple o sumamente robusta y poderosa. \ Cambia de un modelo a otro dentro de una misma familia.

Más cercano al mundo de la *implementación*. \
Está más relacionado con los fierros. \

== Definición de la arquitectura de un computador
¿Cómo decidimos/definimos una arquitectura de un computador? Es la tarea más ardua de un disenador.

- Se trata de definir los aspectos más relevantes en la arquitectura de un computador que maximicen su rendimiento, sin dejar de satisfacer otras limitaciones impuestas por los usuarios, como un costo económico que lo haga accesible, o un consumo de energía moderado.
  - ¿Cuánto va a costar?, ¿cuánto va a consumir, priorizamos rendimiento o eficiencia?, ¿cuántos registros vamos a tener?, etc.

- Comprende el diseño del set de instrucciones, el manejo de la memoria y sus modos de direccionamiento, los restantes bloques funcionales que componen el CPU, el diseño lógico, y el proceso de implementación.
  - ¿Se pueden realizar operaciones entre registros y memoria?, ¿cómo se va a manejar la memoria?, etc.

- La implementación comprende el diseño del circuito integrado, su encapsulado, montaje, alimentación y refrigeración.
  - ¿Va a ser necesario un cooler?, ¿va a correr con una batería?, etc.

Para definir la arquitectura de un procesador es importante tener un entendimiento en las siguientes áreas:
- Diseño lógico _(System Verilog, VHDL, etc.)_.
- Tecnología de encapsulado.
- Funcionamiento y diseño de compiladores y de sistemas operativos _(¿qué instrucciones son las más típicas en un compilador?, ¿y un S.O.?, quiero que esos algoritmos sean eficientes)_.

== Instruction Set Architecture (ISA)
Nos referimos a *Instruction Set Architecture*, como el set de instrucciones visibles por el programador. Es además el límite entre el software y el hardware. \
Es una definición, es parte de la *especificación*, es el ¿qué?.

- *Clases de ISA:*
  - ISAs con registros de propósito general vs registros dedicados.
  - ISAs registro-memoria vs ISAs load-store.

- *Direccionamiento de memoria:*
  - Alineación obligatoria de datos vs administración de a bytes.
    - Cómo trato la memoria, ¿la accedo de a bytes?, ¿requiero que los datos estén alineados (está en una posición de memoria que es múltiplo de su tamaño)?,

- *Modos de direccionamiento:*
  - Como se especifican los operandos: ¿puedo tener un operando en memoria, o laburo sólo con los registros?

- *Tipos y tamaños de operandos:*
  - Enteros, punto flotante, punto fijo.
  - Diferentes tamaños y precisiones.
    - ¿Qué tamaño tienen los tipos de datos?
    - Precisión de los números en punto flotante, etc.

- *Operaciones:*
  - Pocas y simples (RISC) o muchas y complejas (CISC).

- *Instrucciones de control de flujo:*
  - Saltos condicionales, calls.

- *Longitud del código:*
  - Instrucciones de tamaño fijo (RISC-V) vs variable (Intel).

== Microarquitectura $=$ organización $+$ hardware
La organización es el ¿cómo?, es parte de la *implementación*.
- Organización e interconexión de memoria.
- Diseño de los diferentes bloques de la CPU y para implementar el set de instrucciones.
- Implementación de paralelismo a nivel de instrucciones, y/o de datos.
- Podemos así encontrar CPUs con diferentes organizaciones, pero con la misma ISA.

Por ejemplo, los procesadores Ryzen de AMD y los procesadores Core de Intel, ambos implementan la ISA x86-64, pero tienen diferentes organizaciones. \

- Hardware
  - Se refiere a los detalles de diseno lógico y tecnología de fabricación.
  - Existiran por lo tanto, procesadores con la misma ISA y la misma organización, pero absolutamente distintos a nivel de hardware y diseño lógico detallado.

Por ejemplo, el Pentium 4, diseñado para desktop, y el Pentium 4-M para notebooks. \
Este último tiene una cantidad de lógica para control del consumo de energía, siendo su hardware muy diferente del Pentium 4 para desktop. \

== Modelo de Von Neumann
Antes las computadoras eran programadas con cables y switches, y tenían una cinta para la entrada y salida de datos. \
Von Neumann implementó un modelo de computadora (créditos a Turing) que se basa en la idea de que los datos y las instrucciones se almacenan en la misma memoria y se pueden manipular de la misma manera. \
El modelo de Von Neumann consiste en:
- Modelo de programa almacenado (un lugar donde se almacenan las instrucciones).
- Datos y programa en el mismo (único) banco de memoria.
- Maquina de estados:
#align(center)[
  #image("images/von_neumann.png", width: 55%)
]

Suponiendo que cada estado _(Búsqueda de la instrucción, Decodificación de la instrucción, Búsqueda de operando(s), Ejecución de la instrucción y Escritura del resultado)_ toma un ciclo de reloj, el tiempo total para ejecutar una instrucción es de 5 ciclos de reloj. \

Si tenías que ejecutar 100 instrucciones, el tiempo total era de 500 ciclos de reloj. \
El CPU buscaba la instrucción, mientras el resto de las unidades no hacían nada. \
Luego la decodificaba, mientras el resto de las unidades no hacían nada. \
Y así..., hasta que se ejecutaba la instrucción. \

Por ello se implementó el concepto de *pipeline*, introducido (creo) por Intel en la organización del 8086. \

#align(center)[
  #image("images/8086-8088.png", width: 53%)
  #text(size: 8pt)[*A la izquierda, la unidad de ejecución. A la derecha la unidad que fetchea instrucciones; agregándolas a la cola de instrucciones.*]
]

== Pipeline

Se basa en dividir el proceso de ejecución de una instrucción en varias etapas, de manera que cada etapa se ejecuta en un ciclo de reloj distinto. \

Supongamos que tenemos una serie de instrucciones a ejecutar. \

Lo primero que hacemos es buscar la instrucción $(1)$. Luego, pasamos a decodificar la instrucción $(1)$. \
Mientras $(1)$ es decodificada, la componente encargada de buscar instrucciones, va buscando la instrucción $(2)$. \
Y así sucesivamente... \

#align(center)[
  #image("images/pipeline.png", width: 70%)
  #text(size: 8pt)[*Este modelo es teorico y representa la situación ideal.*]
]

=== Múltiples accesos a memoria

Bueno pero acá surje un problema. \
Tenemos un procesador que solo tiene una etapa para acceder a memoria, y la comparte para acceso a datos e instrucciones. \
O sea, guardamos todo en el mismo lugar. \

Si la instrucción $(1)$ requiere que vayamos a buscar un dato en memoria (tenemos un operando en una posición de memoria), y la instrucción $(2)$ está cargada en memoria (y queremos buscarla), no podemos realizar realizar ambas operaciones en simultáneo; puesto a que tenemos un único bus para acceder a memoria. \
Tenemos un único banco de memoria, y un único bus para acceder a esa memoria. Si quisieramos acceder a la memoria en simultáneo, explotaría todo #emoji.bomb. \

El primero approach que se nos puede ocurrir para solucionar este problema con el pipeline es posponer operaciones. Hacé una primero, y luego la otra. \
No deja al pipeline completamente inservible, pero nos alejamos bastante del modelo teórico ideal que planteamos arriba. \

#align(center)[
  #image("images/pipeline_mutiple_memory_access.png", width: 85%)
  #text(size: 8pt)[*
  _Se muestran en rojo las operaciones en conflicto basándonos en el ejemplo anterior._ \
  Se toma un cierto orden de precedencia, posponiendo las operaciones \
  más próximas y priorizando las operaciones más antiguas. \
  *]
]

=== Branches
Un branch es una instrucción que altera el flujo de ejecución del programa. \
Por ejemplo, `call`, `jmp`, `ret`, `je`, `jne`, etc. \
Es decir, subrutinas, saltos condicionales, saltos incondicionales, etc. \

El branch hace que todo lo que estaba preprocesado deba descartarse. Y el pipeline se vacía debiendo transcurrir $n - 1$ ciclos de reloj hasta el próximo resultado. Siendo $n$ la cantidad de etapas del pipeline. Esto se conoce como *branch penalty*. \

Nos vemos obligados a vaciar todo el pipeline, y empezar a cargar instrucciones desde la nueva dirección de memoria. \

#align(center)[
  #image("images/pipeline_branch_penalty.png", width: 100%)
  #text(size: 8pt)[*
  Al ejecutarse una instrucción de tipo branch, se descartan todas las instrucciones que estaban preprocesadas. \
  *]
]

== Entorno del programador de aplicaciones
=== Arquitectura básica de 16 bits

#align(center)[
  #image("images/entornos/8086.png", width: 75%)
  #text(size: 8pt)[*
  Entorno básico de ejecución de 16 bits.
  *]
]

Intel llama a los registros desde el *AX* hasta el *SP*, como registros de propósito general. Sin embargo, esto tiene un poquito de mentira. \
Algunos de esos registros tienen características que son propias. Por ejemplo, el SP es un registro que se utiliza para apuntar a la cima de la pila. \

El registro *AX* (o el *AL*) se lo denomina registro acumulador. \
El nombre viene por la historia. El registro acumulador era un registro privilegiado que soportaba todas las operaciones posibles. \

El registro *BX* (o el *BL*) se lo denomina registro base (o puntero base). \

El registro *CX* (o el *CL*) se lo denomina registro contador. \
Por ejemplo, cuando *CX* llega a $0$, el procesador toma alguna decisión. \

El registro *DX* (o el *DL*) se lo denomina registro de datos. Su propósito es guardar datos. \

Los registros *SI* y *DI* son registros punteros. Se utilizan fundamentalmente como punteros a memoria. También se pueden usar para guardar algún dato de forma temporaria para hacer alguna operación, y no los uso como puntero. \
Pero si los quiero usar como puntero a memoria trabajan como registros índices, no como registros base. \
Es decir, en lugar de apuntar a la base de un segmento (por ejemplo, el comienzo de un vector), apuntan a un lugar específico de la memoria (por ejemplo, al valor en cuestión que estoy iterando en un vector). \
Se pueden autoincrementar (para recorrer un vector, por ejemplo). \
*S* de *source*, y *D* de *destination*. \

El registro *BP* es el registro para trabajar con la pila. \

El registro *SP* es el puntero de la pila. Esto lo vimos en *Sistemas Digitales*. \
Este registro no se usa para otra cosa. \

Ahora veremos los registros de segmento (*CS*, *SS* *DS*, *ES*). \

Cada segmento define un trozo de memoria. Profundización de esto más adelante. \
- *CS*: code segment, donde está el código.
- *SS*: stack segment, donde está la pila.
- *DS*: data segment, donde están los datos.
- *ES*: extra segment, donde están datos extra. \

*IP*: instruction pointer, apunta a la próxima instrucción a ejecutar. \
*Flags*: contiene información sobre el resultado de la última operación. \

=== Arquitectura de 32 bits compatible

#align(center)[
  #image("images/entornos/80386.png", width: 85%)
  #text(size: 8pt)[*
  Entorno básico de ejecución del 80386. \
  Agrega dos segmentos adicionales para datos, FS y GS. \
  Tambien, extienden los registros de propósito general a 32 bits (notar que empeizan con E, E de extended). \
  Idem para el IP y Flags. \
  *]
]

Algo interesante es cómo se manejan los dispositivos I/O. \
Los procesadores de Intel manejan los dispositivos I/O en un espacio de direccionamiento separado. Contrario a ARM y el resto de los procesadores, que manejan los dispositivos I/O en el mismo espacio de direccionamiento que la memoria. \
Entonces al leer o escribir en una dirección de memoria, se debe marcar con un bit si se está accediendo a un dispositivo I/O o a memoria. \
El espacio de direccionamiento de I/O es de 64KB. Sí, también en la actualidad. \

==== Floating Point Unit (FPU)

#align(center)[
  #image("images/fpu.png", width: 80%)
  #text(size: 8pt)[*
  Entorno básico de ejecución de la FPU. \
  *]
]

Existe desde el 8086, pero como no entraba, se integra en un chip separado. \
Se lo llamaba *copro* de *coprocessor*, era el 8087. \

No fue hasta el 80486 que se integró en el mismo chip. Salvo alguna cosa rara de después, pero en general digamos que a partir del 80486 se integró la FPU. \

La FPU es una unidad de punto flotante. Nos permite hacer operaciones con números en punto flotante. \
Multiplicación, división, suma, resta, etc., nos permite representar números reales (con las limitaciones de la computación, obvio). \

Contamos con 8 registros, *R0* a *R7*. Y funcionan como una pila. Es decir, en *R0* se guarda el último valor que se calculó, y en *R1* el penúltimo, y así sucesivamente. \
Miden 80 bits de ancho. \

#align(center)[
  #image("images/datos_de_la_fpu.png", width: 90%)
  #text(size: 8pt)[*
  Formatos de los datos de punto flotante de la FPU. \
  *]
]

Se expresa en notación científica. \
Desde abajo para arriba, en C se llaman: float, double, extended double. \

=== Extensiones MMX

Con el tiempo, los procesadores de Intel, empezaron a incorporar arquitectura interna y recursos, para procesar algoritmos de video y de audio. Para hacer lo que se conoce como Digital Signal Processing (DSP). \

#align(center)[
  #image("images/mmx.png", width: 70%)
  #text(size: 8pt)[*
  Entorno básico de ejecución de las extensiones MMX. \
  *]
]

*MMX*: MultiMedia eXtensions. \

Y estos registros tienen trampita. Los registros MMX son, en realidad, la parte baja de los registros de la FPU. Son sus 64 bits menos significativos. \

Esto genera que no puedas mezclar operaciones de la FPU con operaciones de MMX. \

=== Registros XMM
Hasta que llega en Pentium 3. Y se incorporan los registros XMM. \

#align(center)[
  #image("images/xmm.png", width: 80%)
  #text(size: 8pt)[*
  Entorno básico de ejecución de los registros XMM. \
  *]
]

Son registros de 128 bits. Estos sí son independientes, no como los registros MMX. \
Ahora para hacer DSP, podés operar paquetes de 16 bytes. \
También te permite trabajar con números de punto flotante. \

=== IA-32 actual

Pasando a un vistazo general de la arquitectura IA-32 actual:

#align(center)[
  #image("images/entornos/ia-32.png", width: 100%)
  #text(size: 8pt)[*
  Entorno básico de ejecución IA-32 actual. \
  *]
]

=== Arquitectura Intel® 64 (o amd64)

#align(center)[
  #image("images/entornos/amd64.png", width: 100%)
  #text(size: 8pt)[*
  Entorno básico de ejecución de la arquitectura Intel® 64. \
  *]
]

== Modos de direccionamiento
=== Modo implícito
Los operandos están implícitos en la instrucción. \
Ejemplos de instrucciones con direccionamiento implícito:
- `CLC`: clear carry flag.
- `STC`: set carry flag.
- `CMC`: complement carry flag.
- `CLD`: clear direction flag.
- `STD`: set direction flag.
- `CLI`: clear interrupt flag.
- `STI`: set interrupt flag. \
  - Setea el flag de interrupciones (IF). \
- `HLT`: halt. \
  - Detiene la ejecución del procesador hasta que se reciba una interrupción. \
- `NOP`: no operation. \
  - No hace nada. \
- ...

=== Modo inmediato
El operando fuente viene dentro del código de la instrucción. No es necesario acceder a memoria para obtener el operando tras decodificar la instrucción. \

Aún no vimos assembly, aún así, ejemplo de instrucción con direccionamiento inmediato:
```asm
; ////////////////////////////////////////////////////////////////////
; ascii recibe en al un byte decimal no empaquetado
; y retorna su ascii en el mismo registro
; ////////////////////////////////////////////////////////////////////
ascii:
    add al, '0'
    cmp al, '9'
    jle listo
    add al, 'A' - '9' - 1
listo:
    ret
```

Veamos la primer instrucción. \
Sumá al registro *AL* es ASCII de $0$ (el número 48). El ASCII de $0$ está en el código de la instrucción, es un claro ejemplo de direccionamiento inmediato. \

=== Modo registro
Todos los operandos involucrados en la instrucción son registros del procesador. No importa si hay uno, dos, o más operandos. Son todos registros. \


=== Modos de direccionamiento a memoria
Uno de los operandos de la instrucción es una dirección de memoria. Y el resultado de la instrucción se almacena en memoria. \

Para Intel, la memoria se organiza como una secuencia de bytes. \