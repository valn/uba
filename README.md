# Universidad de Buenos Aires
## Ciencias de la Computación 🖥️
![Logo del Departamento de Computación](https://www.dc.uba.ar/wp-content/uploads/2018/12/logo-dc.svg)

Este repositorio contiene _(algunos)_ de mis apuntes, guías resueltas, trabajos prácticos, y demás recursos relacionados con las materias que cursé (y sigo cursando) en la carrera de **Ciencias de la Computación** en la Universidad de Buenos Aires.

Posiblemente haya más de un error en este repositorio, se recomienda discresión.

## Consultas ❓
Para consultas recomiendo mandarme un mensaje por Telegram, estoy en los grupos de la facultad. Mi username está en los PDFs resueltos en LaTeX.

## Contribuciones 📎
Si tenés alguna sugerencia o mejora que aportar, no dudes en abrir un issue o enviar un merge request.

### Falta X materia 😢
Posiblemente no la cursé, o tengo mayoría del materia (o todo) en papel.  
Recomiendo visitar los resueltos de [Joni](https://github.com/honi) & [Yago](https://github.com/yagopajarino).
