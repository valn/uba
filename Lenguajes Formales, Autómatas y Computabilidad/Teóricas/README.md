## Primer parcial

- [Teórica 1 - Introducción a los lenguajes formales](Teórica%201%20-%20Introducción%20a%20los%20lenguajes%20formales.pdf)
- [Teórica 2 - Autómatas finitos y gramáticas regulares](Teórica%202%20-%20Autómatas%20finitos%20y%20gramáticas%20regulares.pdf)
- [Teórica 3 - Autómatas de pila y gramáticas libres de contexto](Teórica%203%20-%20Autómatas%20de%20pila%20y%20gramáticas%20libres%20de%20contexto.pdf)
- [Teórica 4 - Autómatas finitos no determinísticos con transiciones instantáneas](Teórica%204%20-%20Autómatas%20finitos%20no%20determinísticos%20con%20transiciones%20instantáneas.pdf)
- [Teórica 5 - Expresiones regulares (regex)](Teórica%205%20-%20Expresiones%20regulares%20(regex).pdf)
- [Teórica 6 - Autómatas de pila y gramáticas libres de contexto](Teórica%206%20-%20Autómatas%20de%20pila%20y%20gramáticas%20libres%20de%20contexto.pdf)

## Segundo parcial
> La parte de gramáticas libres de contexto de la teórica 6 se incluye en el segundo parcial.
- [Teórica 7 - Lenguajes libres de contexto, lema de pumping, propiedades de clausura, algoritmos de decisión](Teórica%207%20-%20Lenguajes%20libres%20de%20contexto,%20lema%20de%20pumping,%20propiedades%20de%20clausura,%20algoritmos%20de%20decisión.pdf)
- [Teórica 8 - Autómatas de pila determinísticos y gramáticas LR](Teórica%208%20-%20Autómatas%20de%20pila%20determinísticos%20y%20gramáticas%20LR.pdf)
- [Teórica 9 - Máquinas de Turing, funciones parcialmente computables, conjuntos](Teórica%209%20-%20Máquinas%20de%20Turing,%20funciones%20parcialmente%20computables,%20conjuntos.pdf)
- [Teórica 10 - Funciones parcialemnte computables y función halt](Teórica%2010%20-%20Funciones%20parcialemnte%20computables%20y%20función%20halt.pdf)
- [Teórica 11 - Indecibilitad](Teórica%2011%20-%20Indecibilitad.pdf)
