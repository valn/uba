## Primer parcial

- [Práctica 1 - Lenguajes formales](Práctica%201%20-%20Lenguajes%20formales.pdf)
- [Práctica 2 - Automatas finitos](Práctica%202%20-%20Automatas%20finitos.pdf)
- [Práctica 3 - Lema de Pumping y propiedades de lenguajes regulares](Práctica%203%20-%20Lema%20de%20Pumping%20y%20propiedades%20de%20lenguajes%20regulares.pdf)
- [Práctica 4 - Expresiones regulares y conversiones](Práctica%204%20-%20Expresiones%20regulares%20y%20conversiones.pdf)
- [Práctica 5 - Autómatas de pila](Práctica%205%20-%20Autómatas%20de%20pila.pdf)

## Segundo parcial
- [Práctica 6 - Gramáticas libres de contexto](Práctica%206%20-%20Gramáticas%20libres%20de%20contexto.pdf)
- [Práctica 7 - Funciones primitivas recursivas](Práctica%207%20-%20Funciones%20primitivas%20recursivas.pdf)
- [Práctica 8 - Funciones computables](Práctica%208%20-%20Funciones%20computables.pdf)
- [Práctica 9 - Límites de la computabilidad (la no computabilidad y cómo demostrarla)](Práctica%209%20-%20Límites%20de%20la%20computabilidad%20(la%20no%20computabilidad%20y%20cómo%20demostrarla).pdf)
