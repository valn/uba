## Primer parcial

- [Guía 1 - Lenguajes](Guía%201%20-%20Lenguajes.pdf)
- [Guía 2 - Autómatas finitos](Guía%202%20-%20Autómatas%20finitos.pdf)
- [Guía 3 - Lema de pumping para lenguajes regulares](Guía%203%20-%20Lema%20de%20pumping%20para%20lenguajes%20regulares.pdf)
- [Guía 4 - Expresiones regulares](Guía%204%20-%20Expresiones%20regulares.pdf)
- [Guía 5 - Autómatas de pila](Guía%205%20-%20Autómatas%20de%20pila.pdf)

## Segundo parcial
- [Guía 6 - Gramáticas](Guía%206%20-%20Gramáticas.pdf)
- [Guía 7 - Funciones primitivas recursivas](Guía%207%20-%20Funciones%20primitivas%20recursivas.pdf)
- [Guía 8 - Funciones computables](Guía%208%20-%20Funciones%20computables.pdf)
- [Guía 9 - Indecidibilidad](Guía%209%20-%20Indecidibilidad.pdf)
