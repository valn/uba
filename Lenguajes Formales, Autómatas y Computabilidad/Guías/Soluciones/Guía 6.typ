#import "./template/main.typ" as template

#show: template.conf.with(
  título: "Lenguajes Formales, Autómatas y Computabilidad",
  subtítulo: "Guía 6: resolución",
  extra: (
    [_\@valnrms_],
  )
)

= Ejercicio 1
Para cada una de las siguientes gramáticas, determinar su tipo según la jerarquía de Chomsky, describir el lenguaje generado y dar una derivación para una cadena perteneciente al mismo.

Recordemos la jerarquía de Chomsky:
#align(center)[
  #image("./assets/chomsky.svg", width: 70%)
]
y los tipos de gramáticas:
#align(center)[
  #image("./assets/gramáticas.svg", width: 85%)
]

*a.*
Viendo las producciones, nos damos cuenta que a la izquierda de $->$ siempre hay un único símbolo no terminal, por lo que estamos ante una grámatica de tipo 2 (libre de contexto) o de tipo 3 (regular).

Para determinar cuál es, veamos las producciones. A la derecha de $->$ hay siempre, o bien un único no terminal, o bien un terminal solamente, o bien un terminal seguido de un no terminal, o bien un no terminal solamente. Por lo tanto, estamos ante una gramática de tipo 3 (regular).

Para determinar el lenguaje generado, pensemos cadenas que pertenezcan al lenguaje generado por esta gramática.
- a
- aa
- aaa
- b
- ba
- aaaab
- aaaaba

Pareciera ser el lenguaje definido por la expresión regular $a^* . (a | b) . (a | lambda)$. \
Demos una derivación para "a": $S -> T -> a U -> a$.

\

*b.*
Igual que antes, puede ser de tipo 2 (libre de contexto) o de tipo 3 (regular). Veamos las producciones.

Chequeate que de tipo 3 (regular) no puede ser. Recordemos que las gramáticas regulares son o bien a izquierda, o bien a derecha. \
En este caso, hay producciones de ambos lados, por lo que no puede ser de tipo 3 (regular). Ergo, es de tipo 2 (libre de contexto). \

Describe el lenguaje denotado por la siguiente expresión regular: $b^* . (a a)^* . b$.
Una derivación para "aab": $S -> T b -> a a T b -> a a b$. \

\

*c.*
Idem arriba, es de tipo 2 (libre de contexto). \
De hecho, tiene el mismo poder expresivo que una gramática de tipo 0 (recurisvamente enumerable). \

Describe: $a^n . b . a^n | n >= 0$. \
Derivación para "aba": $S -> a S a -> a b a$. \

\

*d.*

Acá vemos las producciones ($->$) y notamos que a la izquierda hay no terminales acompañado de terminales. Más red flag que tu ex, es de tipo 0 (recursivamente enumerable) o de tipo 1 (sensible al contexto).

Vemos que vale la condición de que $|alpha| <= |beta|$ para cada producción $alpha -> beta$. Por lo tanto, es de tipo 1 (sensible al contexto). \
Capaz a alguien le hace ruido la producción $S -> a T S$, porque en la tabla de tipos de gramáticas dice que S no aparece en el lado derecho. A mí también me hace ruido, pero en clase dieron un ejemplo donde estaba $S$ en el lado derecho y dijeron que era de tipo 1, así que me aferro a eso. \
Asumo que se quiere referir que ni debe estar la producción $S -> S$, es decir, quedarte boludeando con el mismo no terminal. \

Ejemplitos:
- ab
- aabb
- aaabbb

Lenguaje: $a^n . b^n | n >= 1$. \
Derivación para "aabb": $S -> a T S -> a T a b -> a a T b -> a a b b$.

\

*e.*

Es de tipo 1 (sensible al contexto), mismo caso que antes. \
 
Lenguaje: $(a | b)^*$. \
Derivación para "ab": $S -> S A B -> A B -> a B -> a b$.

\

*f.*

Este es de tipo 0 (recursivamente enumerable). Fijate que repetimos lo de los no terminales a izquierda (o sea, hay más de un no terminal (este caso), o hay un no terminal acompañado de un terminal (caso del item $d.$)). Pero acá tenemos la producción $a a T -> T c$, y no es cierto que $|alpha| <= |beta|$ (i.e. $|a a T| = 3 <= 2 = |T c|$). \

Ejemplitos:
- c
- bc
- bbc
- abc

El lenguaje genera algo del estilo $T^+ . c$. Hay que ver qué es $T$. Sacando la primer producción $S -> T S | c$, vemos que es algo tipo: \
"poné todas las a que quieras, y después poné la misma cantidad de b". O sea, $a^n . b^n | n >= 1$.
(falta terminar)

\

= Ejercicio 2

*a.*

$G_(2a) = <{S, M, F}, {a, b}, P_(2a), S>$ con $P_(2a)$:

A izquierda: \
$S -> a S | b S | M$ \
$M -> a a F$ \
$F -> a F | b F | lambda$ \

A derecha: \
$S -> S a | S b | M$ \
$M -> F a a$ \
$F -> F a | F b | lambda$ \

*b.*

$G_(2b) = <{S, T, U}, {a, b}, P_(2b), S>$ con $P_(2b)$:

A izquierda: \
$S -> a S | T$ \
$T -> b . U | lambda$ \
$U -> a T | b T$ \

A derecha: \
$S -> T a | T b | U$ \
$T -> S b $ \
$U -> U a | lambda$ \

*c.*

$G_(2c) = <{S, T, U}, {a, b}, P_(2c), S>$ con $P_(2c)$:

A izquierda: \
$S -> a b S | b a S | T$ \
$T -> b b S | U$ \
$U -> a a S | lambda$ \

A derecha: \
$S -> S a a | T$ \
$T -> T b b | U$ \
$U -> U a b | U b a | S | lambda$ \

= Ejercicio 3

Dar una gramática independiente del contexto que lo genere.
Elegir una cadena del lenguaje de longitud mayor o igual a 4, y exhibir una derivación más a la izquierda, una derivación más a la derecha y un árbol de derivación según la gramática dada.

*a.*

$G = <{S}, {a, b}, P, S>$ con $P:$ \
$S -> a S b | lambda$ \

*b.*

$G = <{S_1, T}, {a, b}, P_1, S_1>$ con $P_1:$ \
$S_1 -> a S_1 b | T b$ \
$T -> T b | lambda$ \

*c.*
Tengo ${a^n b^m | m > n}$ hecho en el item b. \
Consigo ${a^n b^m | m < n}$, hago la unión y listo. \

$G = <{S_2, U}, {a, b}, P_2, S_2>$ con $P_2:$ \
$S_2 -> a S_2 b | a U$ \
$U -> a U | lambda$ \

Luego,

$G = {S, S_1, S_2, T, U}, {a, b}, P, S$ con $P:$ \
$S -> S_1 | S_2$ \
$S_1 -> a S_1 b | T b$ \
$S_2 -> a S_2 b | a U$ \
$T -> T b | lambda$ \
$U -> a U | lambda$ \

*d.*

$G = {S}, {a, b, \#}, P, S$ con $P:$ \
$S -> a S a | b S b | \#$ \

*e.*

$G = {S}, {a, b}, P, S$ con $P:$ \
$S -> a S a | b S b | lambda$ \

*f.*

$G = {S}, {a, b}, P, S$ con $P:$ \
$S -> a S a | b S b | a | b | lambda$ \

*g.*

$G = {S, T}, {a, b}, P, S$ con $P:$ \
$S -> a S a | b S b | a T b | b T a$ \
$T -> a T | b T | lambda$

*h.*
$G = {S, A, B}, {a, b}, P, S$ con $P:$ \
$S -> a B S | b A S | lambda $ \
$A -> b A A | a$ \
$B -> a B B | b$ \

*i.*
$G = {S, A}, {a, b}, P, S$ con $P:$ \
_To be done._
 
*k.*

Idem antes, quiero que sea $!=$, hice $>$ arriba, me falta hacer $<$ y unirlos. \
Lo dejo al lector. Tengo sueño. \

*l.*

$G = {S, T, U, V, F}, {a, b}, P, S$ con $P:$ \
$S -> T | U | V | lambda$ \
$T -> S a b b | a S b b | a b S b | a b b S$ \
$U -> S b a b | b S a b | b a S b | b a b S$ \
$V -> S b b a | b S b a | b b S a | b b a S$ \

*m.*

Parece dificil pero no lo es. Hacete ${a^n b^m | m = 2n}$ y ${a^n c^m | n = 2m}$ por separado (con todo lo de arriba debería salir), y después unilos. \

*n.*

$G = {S}, {(,)}, P, S$ con $P:$ \
$S -> (S) | SS | lambda$ \

O mejor aún, si queremos que no sea ambiguo: \
$S -> (S)S | lambda$ \
Definimos la asociatividad. \

= Ejercicio 4

*a.*
Me piden demostrar que: \
$cal(L) "es independiente del contexto" arrow.r.double cal(L)^2 = cal(L) . cal(L) "es independiente del contexto"$.

Sea $G =$ $<V_N, V_T, P, S>$ una gramática independiente del contexto que genera $cal(L)$, es decir $cal(L) = cal(L)(G)$. \
Sí, es la $cal(L)$ caligráfica para la _función_ que toma un atómata, una gramática y para el definir el lenguaje, es un re abuso de notación. #text(rgb("#fd0000"))[Deal with it.] \
Luego, por el teorema visto en la teórica 6 _(Chomsky 1962, Evey 1963)_, sé que para cada gramática $G$ libre de contexto existe un autómata de pila $M$ tal que $cal(L)(G) = cal(L)_lambda (M)$. \
Y a su vez sé que para cada autómata de pila por pila vacía $M$ existe un autómata de pila por estado final $M'$ tal que $cal(L)_lambda (M) = cal(L)(M')$. \
Entonces, sea $M'$ el autómata de pila por estado final que reconoce $cal(L)$. \
Me copio el autómata (imaginate un autómata, el que más te guste, ahora le hacés CTRL+C y CTRL+V, y lo ponés al lado del original). \
Ahora, agarro todos los estados finales del autómata original y les enchufo una transición lambda al estado inicial del autómata copiado. \
#text(gray)[(acá estaría bueno un dibujito jeje, pero no hay #strike[presupuesto] tiempo)] \
Por lo que vimos en la teórica, ahora tenemos un autómata $M^2'$ que reconoce $cal(L)^2$. \
Ahora nuevamente, por el teorema de antes, existe un autómata de pila por pila vacía $M^2$ tal que $cal(L)(M^2') = cal(L)_lambda (M^2) = cal(L)^2$. \
Luego, por el teorema visto en la teórica 6 _(Chomsky 1962, Evey 1963)_ (ahora usamos la vuelta), sé que para cada autómata de pila por pila vacía $M^2$ existe una gramática libre de contexto $G^2$ tal que $cal(L)_lambda (M^2) = cal(L)(G^2) = cal(L)^2$. \
Por lo tanto, entongo una gramática independiente del contexto $G^2$ que genera $cal(L)^2$, ergo $cal(L)^2$ es independiente del contexto. \
#align(right)[$square$]

*b.*

Lo mismo que el anterior pero con $n$ copias en lugar de dos, y con concatenaciones de las copias (estados finales a estado inicial). \

*c.*

También, muy similar al primero. En lugar de hacer una copia, metés una $lambda$-transición de los estados finales al estado inicial. \

*d.*

Idem que el a., pero cuando tenés que armarte el autómata, en lugar de hacer la copia hacés lo siguiente: \
- Agregás un nuevo estado $R$ que va a pasar a ser el nuevo estado inicial. \
- Ponés transiciones lambda desde $R$ a todos los estados finales del autómata original. \
- Sacás los estados finales del autómata original. \
- Ponés como estado final el estado incial del autómata original. \
Listo.

*e.*
Este por autómatas capaz es medio... fiaca. \
Si tenemos dos gramáticas independientes del contexto $G_1$ y $G_2$ que generan $cal(L_1)$ y $cal(L_2)$ respectivamente, podemos hacer lo siguiente: \
Sea $G_1 = <V_(N^1), V_(T^1), P^1, S^1>$ y $G_2 = <V_(N^2), V_(T^2), P^2, S^2>$. \
Definimos una nueva gramática \
$G = <V_N, V_T, P, S>$ con
$V_N = V_(N^1) union V_(N^2) union \{S\}$, \
$V_T = V_(T^1) union V_(T^2)$, \
$P = P^1 union P^2 union \{S -> S^1 | S^2\}$. \
Y nada, fijate que la producción que agregamos no rompe que sea independiente del contexto, y sabemos por hipótesis que todas las demás producciones son independientes del contexto. \
Entonces, $cal(L)(G) = cal(L)(G_1) union cal(L)(G_2) = cal(L_1) union cal(L_2)$ es independiente del contexto. \

= Ejercicio 5

*a.*
$G = <{S}, {a, b}, P, S>$ con $P:$ \
$S -> a S b | b S a | a S a | b S b | a$

*b.*

Sea $alpha$ la cadena que genera la gramática: o bien $alpha$ es impar, o bien $alpha = w w$ es par para algún $w$. \
Si $alpha$ es impar, problema resuelto pues no existe $w$ tal que $alpha = w w$. \
Si $alpha$ es par, quiero que $exists i, 0 <= i < |w| "tq'" alpha[i] != alpha[|w| + i]$ \

$G = <{S}, {a, b}, P, S>$ con $P:$ \
$S -> I | P$ "Va a ser impar | va a ser par" \ 
$I -> I a a | a I a | a a I | I b b | b I b | b b I | I a b | a I b | a b I | I b a | b I a | b a I | a | b$ "Hago lo que quiero manteniendo paridad, luego la rompo con: a | b" \
$P -> a P a | b P b | Q$ "Hago lo que quiero manteniendo $alpha = w w$, cuando tomo $Q$ sé que rompí $alpha = w w$" \
$Q -> a Q b | b Q a | lambda$ "Mantengo que esté roto" \

*c.*

Hell nah 💀💀, basta de $!=$, la paso mal. \
Mejor hacer el > y el <, y después hacer la unión. \
Hacete ${a^n b^(2m) | m > n}$ y ${a^n b^(2m) | m < n}$ por separado, y después unilos. \

Para lo primero: \
$G = <{S, T}, {a, b}, P, S>$ con $P:$ \
$S -> a S b b | T b b$ \
$T -> T b b | lambda$ \

Para lo segundo: \
$G = <{S, T}, {a, b}, P, S>$ con $P:$ \
$S -> a S b b | a T$ \
$T -> a T | lambda$ \

Y el resto es historia conocida.

*d.*

$G = <{S, T, I}, {a, b, \#, 1}, P, S>$ con $P:$ \
$S -> a T | b S | \#$
$T -> a T | b S I$
$I -> 1$

= Ejercicio 6

"[abba,[ab,baba:11],ba,[]:1111]"
"[a,b:11]"
"[[],b:111]"
"[a,[],b:111]"
"[a,[]"

$G = <{S, E}, {a, b, 1, [, ], #text(size: 16pt)[*,*], :}, P, S>$ con $P:$ \
$S -> [] | [E 1]$ \
$L -> []#text(size: 16pt)[*,*] E | $
$E -> L | P | : $ \
$P -> a P | b P | #text(size: 16pt)[*,*]E$ \

= Ejercicio 7

$G = <{S, T, P, Q, Q_x, Q_y, Q_(x|y), A, A_x, A_y, A_(x|y)}, {x,y,c,d,p,q,r,s,not,and,or,arrow.double,forall,exists,(,)}, P, S>$ con $P:$ \

$S -> S or S | S and S | S arrow.double S | T$ \
$T -> not Q | Q | not P | P$ \
$P -> p(A) | q(A) | r(A) | s(A)$ \
$Q -> forall x (Q_x) | forall y (Q_y) | exists x (Q_x) | exists y (Q_y)$ \
$Q_x -> forall x (Q_x) | forall y (Q_(x|y)) | exists x (Q_x) | exists y (Q_(x|y)) | p(A_x) | q(A_x) | r(A_x) | s(A_x)$ \
$Q_y -> forall x (Q_(x|y)) | forall y (Q_y) | exists x (Q_(x|y)) | exists y (Q_y) | p(A_y) | q(A_y) | r(A_y) | s(A_y)$ \
$Q_(x|y) -> forall x (Q_(x|y)) | forall y (Q_(x|y)) | exists x (Q_(x|y)) | exists y (Q_(x|y)) | p(A_(x|y)) | q(A_(x|y)) | r(A_(x|y)) | s(A_(x|y))$ \
$A -> c,A | d,A | c | d$ \
$A_x -> x,A_x | c,A_x | d,A_x | x | c | d$ \
$A_y -> y,A_y | c,A_y | d,A_y | y | c | d$ \
$A_(x|y) -> x,A_(x|y) | y,A_(x|y) | c,A_(x|y) | d,A_(x|y) | x | y | c | d$ \

= Ejercicio 8

*a.* y *b.* \

$G = <{E,T,F}, {+,-,*,\/,(,)}, P, E>$ con $P:$ \

$E -> E + T | E - T | T$ \
$T -> T * F | T \/ F | F$ \
$F -> (E) | id$ \

*c.* Te lo dejo a vos jeje. Lo hice en papel. \

= Ejercicio 9

TBD.
