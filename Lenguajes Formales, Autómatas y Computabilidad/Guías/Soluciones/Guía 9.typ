#import "./template/main.typ" as template
#import "@preview/codelst:2.0.2": sourcecode
#import "./template/spp.typ" as algorithmic
#import algorithmic : algorithm
#let rainbow = gradient.linear(..color.map.rainbow)
#let Programa(Programa, cantArgumentos) = $Psi_Programa^((cantArgumentos))$
#let Universal(nroPrograma, cantArgumentos) = $Phi_nroPrograma^((cantArgumentos))$
#let UniversalEz(nroPrograma, Argumento) = $Phi_nroPrograma^((1)) (Argumento)$
#let y = $" " and " "$
#let comp = $" " circle.tiny " "$
#let Termina = $arrow.b$
#let Cuelga = $arrow.t$
#let Casos(Condición, True, False) = $cases(True "si" Condición, False "si no")$
#let sii = $<==>$

#show: template.conf.with(
  título: "Lenguajes Formales, Autómatas y Computabilidad",
  subtítulo: "Guía 9: resolución",
  extra: (
    [_\@valnrms_],
  )
)

= Ejercicio 1

*a.*

Me voy a tomar el laburo de hacer este item detalladamente para sentar un poco los conceptos de diagonalización.

Me pide, usando la ténica de diagonalización, demostrar que

$f_1 (x,y) = cases(1 "si" Phi_x^((1)) (y) arrow.b, 0 "si no") = cases(1 "si" Phi_x^((1)) (y) arrow.b, 0 "si" Phi_x^((1)) (y) arrow.t)$

no es computable. _Cuando hablamos de que algo es o no computable, lo usamos como sinónimo de que es totalmente computable o que no es totalmente computable._

#text(gray,"No es mandatory darse cuenta de esto para el ejercicio, pero es la función HALT.")

Arranco. Quiero ver que $f_1$ no es computable. Supongamos que sí, que es computable, esto implica que existe un programa $P$ que la computa, es decir $Psi_P^((2)) = f_1$.

Sea $D$ un programa _(que me defino yo, a gusto y piacere)_ tal que,

#algorithm({
  import algorithmic: *
  Program($D$+":")("",{
    Asignation($Z_1$, $f_1 (X_1,X_1)$, comment: [Asigno a $Z_1$ el valor de $f_1 (X_1,X_1)$, esto lo puedo hacer porque asumí que $f_1$ es computable (lo que implica que existe un programa $P$ que computa $f_1$).])
    Newline
    Comment([Si $Phi_(X_1)^((1)) (X_1) arrow.b$ _(es decir, no se colgó)_, entonces $Z_1 = f_1 (X_1,X_1) = 1$, por lo que en $D$ se entra al while, y por lo tanto, se cuelga.])
    Comment([Si $Phi_(X_1)^((1)) (X_1) arrow.t$ _(es decir, se colgó)_, entonces $Z_1 = f_1 (X_1,X_1) = 0$, por lo que en $D$ no se entra al while, y por lo tanto, no se cuelga y devuelve 0.])
    While(cond: $Z_1 != 0$, {
      Pass
    })
  })
})

En términos de función, $Psi_D^((1)) (x) = cases(arrow.t "si" f_1 (x,x) = 1, 0 "si no") = cases(arrow.t "si" f_1 (x,x) = 1, 0 "si" f_1 (x,x) = 0)$

Sea $\#(D) = e in NN$, es decir, la $e$ es la codificación del programa $D$. \
¿Qué pasa con $Psi_D^((1)) (e)$?, veamos. \

De base $Psi_D^((1)) (e)$ tiene dos chances. O bien se cuelga, o bien me devuelve $0$. \

#text(gray, [Recordar que, si $\#(D) = e$, entonces $Psi_D^((1)) (e) = Phi_e^((1)) (e)$.])

Supongamos que se cuelga, es decir, \
$Psi_D^((1)) (e) arrow.t$ $<==>$ $f_1(e,e) = 1$ $<==>$ $Phi_e^((1)) (e) arrow.b$ $<==>$ $Psi_D^((1)) (e) arrow.b$ #h(5cm) #text(rainbow, weight: "bold")[¡ABSURDO!]

Bueno, eso no funcionó. Supongamos que $Psi_D^((1)) (e) = 0$, es decir, que no se cuelga. \
$Psi_D^((1)) (e) = 0$ $<==>$ $f_1(e,e) = 0$ $<==>$ $Phi_e^((1)) (e) arrow.t$ $<==>$ $Psi_D^((1)) (e) arrow.t$ #h(4.6cm) #text(rainbow, weight: "bold")[¡ABSURDO!]

¿De dónde vinieron los absurdos?, de suponer que $f_1$ era computable. Por lo tanto, $f_1$ no es computable. \ 
#align(right)[$square$]

*b.*

Ahora le meto x2.
Idem arriba pero para $f_2 (x,y) = Casos(Universal(x,1) (y) = 0, 1, 0)$

#algorithm({
  import algorithmic: *
  Program($D$+":")("",{
    Asignation($Y$, $f_2 (X_1,X_1)$)
  })
})

Expresado como función, $Psi_D^((1)) (x) = cases(1 "si" Universal(x,1) (x) = 0, 0 "si no")$

El programa $D$ del item anterior también servía _(reemplazando $f_1$ por $f_2$)_. Para el lector, ¿por qué? \
Algo que no vi que se charle mucho es cómoa armar estos programas porque son un medio galerazo. A mí lo que me sirve es arrancar por el final de los $<==>$. Es decir, \

Sea $\#(D) = e$, \

$Programa(D, 1) (e) = 0$ #sii $Universal(e,1) (e) = e$ #sii $f_2(e,e) = 1$ #sii $???_1$ \
$Programa(D, 1) (e) != 0$ #sii $Universal(e,1) (e) != e$ #sii $f_2(e,e) = 0$ #sii $???_2$ \

Ahora me falta buscar cómo completar $???_1$ y $???_2$, de manera tal de obtener absurdos. \
De ahí los pienso y obtengo mi programa $D$. \

Quiero que $???_1$ sea algo del tipo $Programa(D, 1) (e) != 0$, puede ser $= 1$, colgarse $Cuelga$, o cualquier cosa que haga verdadero $Programa(D, 1) (e) != 0$. \

Para $???_2$ quiero específicamente que sea $Programa(D, 1) (e) = 0$. \

Ahora, convencerse por qué el programa $D$ del item anterior también servía _(reemplazando $f_1$ por $f_2$)_. \

Bueno, retomo la demo. Tengo asumido que $f_2$ es computable, es por ello que puedo usarla en $D$. \
Veamos que pasa con $Programa(D,1) (e)$. Como $f_2$ es computable, sé que sus posibles valores son $0$ o $1$. \

Usando el programa
#algorithm({
  import algorithmic: *
  Program($D$+":")("",{
    Asignation($Y$, $f_2 (X_1,X_1)$)
  })
})

Si $Programa(D,1) (e) = 0$, \
$Programa(D,1) (e) = 0$ #sii $f_2(e,e) = 0$ #sii $Universal(e,1) (e) != 0$ #sii $Programa(D, 1) (e) != 0$ #h(3.5cm) #text(rainbow, weight: "bold")[¡ABSURDO!]

Si $Programa(D,1) (e) = 1$, \
$Programa(D,1) (e) = 1$ #sii $f_2(e,e) = 1$ #sii $Universal(e,1) (e) = 0$ #sii $Programa(D, 1) (e) = 0$ #h(3.5cm) #text(rainbow, weight: "bold")[¡ABSURDO!]

Por lo tanto, $f_2$ no es computable. \
#align(right)[$square$]

#pagebreak()

*c.*

Me piden demostrar que $f_3(x,y,z) = Casos(Universal(x,1)(y) Termina " " and " " Universal(x,1)(y) > z,1,0)$ no es computable. \

Asumo $f_3$ computable.

#algorithm({
  import algorithmic: *
  Program($D$+":")("",{
    Asignation($Z_1$, $f_3(X_1,X_1,X_1)$)
    Newline
    While(cond: $Z_1 != 0$, {
      Pass
    })
    Newline
    Asignation($Y$, $X_1$, comment: [Macro computable])
    Asignation($Y$, $Y + 1$,comment: [Macro computable])
  })
})

Como función, $Programa(D,1)(x) = Casos(f_3(x,x,x) = 1, Cuelga, x+1)$. \

$Programa(D,1)(e) Cuelga$ #sii $f_3(e,e,e) = 1$ #sii $Universal(e,1)(e) Termina and  Universal(e,1)(e) > e$ $=>$ $Programa(D,1)(e) Termina$ #h(2.5cm) #text(rainbow, weight: "bold")[¡ABSURDO!] \

$Programa(D,1)(e) = e+1$ #sii $f_3(e,e,e) = 0$ #sii $Universal(e,1)(e) Cuelga or Universal(e,1)(e) <= e$ $=>$ $Universal(e,1)(e) <= e$ #h(1.06cm) #text(rainbow, weight: "bold")[¡ABSURDO!] \
Notar que sé que el programa no se cuelga en este caso, por lo que el caso $Universal(e,1)(e) Cuelga$ del $or$ lo medio ignoré. \

Por lo tanto, $f_3$ no es computable. \

*d.*

Me piden demostrar que $f_4(x) = Casos(Universal(x,1) (x) Termina " " and " " Universal(x,1) (x) != x,1,0)$ no es computable. \

Asumo $f_4$ computable.

#algorithm({
  import algorithmic: *
  Program($D$+":")("",{
    Asignation($Z_1$, $f_4(X_1)$)
    Newline
    While(cond: $Z_1 != 0$, {
      Pass
    })
    Newline
    Asignation($Y$, $X_1$)
    Asignation($Y$, $Y + 1$)
  })
})

Como función, $Programa(D,1)(x) = Casos(f_4(x) = 1, Cuelga, x+1)$. \

$Programa(D,1)(e) Cuelga$ #sii $f_4(e) = 1$ #sii $Universal(e,1)(e) Termina and Universal(e,1)(e) != e$ $=>$ $Programa(D,1)(e) Termina$ #h(2.5cm) #text(rainbow, weight: "bold")[¡ABSURDO!] \

$Programa(D,1)(e) = e+1$ #sii $f_4(e) = 0$ #sii $Universal(e,1)(e) Cuelga or Universal(e,1)(e) = e$ $=>$ $Universal(e,1)(e) = e$ #h(1.06cm) #text(rainbow, weight: "bold")[¡ABSURDO!] \

Por lo tanto, $f_4$ no es computable. \

#pagebreak()

= Ejercicio 2

Reduciendo cualquier función del ejercicio 1, probar que las siguientes funciones no son computables:

/*
Recordemos la técnica de reducciones. Usamos la técnica de reducciones para demostrar que un lenguaje no es computable, utilizando otro lenguaje que sepamos que no es computable. \

Sea $A, B subset.eq Sigma^*$.

*Notación:* $A <= B$ #h(1cm) #text(rainbow, weight: "bold")["$A$ se reduce a $B$"] \
*Reducción:* $A <= B$ #sii $exists f : Sigma^* -> Sigma^* "total computable, tal que" forall w in Sigma^*, w in A sii f(w) in B$.

A su vez, recordar que:
- $cal(L)$ es computable #sii $cal(L) <= {lambda}$ #h(7.5cm) #text(rainbow, weight: "bold")[$cal(L)$ es un lenguaje.]
- Sea $cal(L_2)$ computable no trivial $(cal(L_2 != emptyset), cal(L_2) != Sigma^*)$, entonces $forall cal(L_1)$, $cal(L_1)$ es computable #sii $cal(L_1) <= cal(L_2)$
- $cal(L)$ es computable #sii $cal(L)^c$ es computable. #h(6cm) #text(rainbow, weight: "bold")[$cal(L)$ es un lenguaje.]
- $cal(L)$ es computable #sii $cal(L)$ es c.e. $and$ $cal(L)^c$ es c.e. #h(5.65cm) #text(rainbow, weight: "bold")[$cal(L)$ es un lenguaje.]
*/

*a.*

$g_1 (x,y) = Casos(Universal(x,1)(y) Cuelga, 1,0) = cases(1 "si" Universal(x,1)(y) Cuelga, 0 "si" Universal(x,1)(y) Termina)$ \

Voy a usar que sé que $f_1$ del _item a_ del _ejercicio 1_ anterior no es computable. \

Sé que $f_1 (x,y) = Casos(Universal(x,1) (y) Termina,1,0) = cases(1 "si" Universal(x,1) (y) Termina, 0 "si" Universal(x,1) (y) Cuelga)$. Luego, \

$f_1(x,y) = alpha(g_1(x,y)) = Casos(Universal(x,1)(y) Cuelga, alpha(1), alpha(0)) = cases(0 "si" Universal(x,1)(y) Cuelga, 1 "si" Universal(x,1)(y) Termina) = cases(1 "si" Universal(x,1)(y) Termina, 0 "si" Universal(x,1)(y) Cuelga) = Casos(Universal(x,1)(y) Termina,1,0)$ \

Recuerdo, $alpha(x) = Casos(x = 0, 1, 0)$, o dicho de otro modo, el predicado computable $not$. \

Pero pará, nosotros sabemos por el _ejercicio 1, item a_ que $f_1$ no es computable, por lo que $g_1$ no puede ser computable, ya que si lo fuese, tendríamos que $f_1$ también lo es. \

*b.*

$g_2(x,y,z,w) = Casos(UniversalEz(x,z) Termina #y UniversalEz(y,w) Termina #y UniversalEz(x,z) " " > " " UniversalEz(y,w),1,0)$

/*

Entonces, puedo definir $g_1$ como, \
$g_1 (x,y) = alpha(f_1(x,y)) = Casos(Universal(x,1) (y) Termina,alpha(1),alpha(0)) = Casos(Universal(x,1) (y) Termina,0,1) = cases(0 "si" Universal(x,1) (y) Termina, 1 "si" Universal(x,1) (y) Cuelga) = cases(1 "si" Universal(x,1) (y) Cuelga, 0 "si" Universal(x,1) (y) Termina) = Casos(Universal(x,1) (y) Cuelga, 1, 0)$ \

Recuerdo, $alpha(x) = Casos(x = 0, 1, 0)$, o dicho de otro modo, el predicado computable $not$.

Luego, $g_1$ es computable #sii $f_1$ es computable. Pero $f_1$ no es computable, por lo que $g_1$ tampoco es computable. \
*/

$f_3(x,y,z) = g_2(x,\#(u_1^1),y,z) = Casos(UniversalEz(x,y) Termina #y UniversalEz(\#(u_1^1),z) Termina #y UniversalEz(x,y) " " > " " UniversalEz(\#(u_1^1),z),1,0) = Casos(UniversalEz(x,y) Termina #y UniversalEz(x,y) " " > " " UniversalEz(\#(u_1^1),z),1,0) = Casos(UniversalEz(x,y) Termina #y UniversalEz(x,y) " " > " " z,1,0)$ \

Como $f_3$ no es computable, $g_2$ no puede serlo. \

*c.*

$g_3 (x,y,z) = Casos(UniversalEz(x,y) Termina #y UniversalEz(x,y) != z,z+1,0)$ \

$f_4(x) = g_3(x,x,x) minus.dot x = Casos(UniversalEz(x,x) Termina #y UniversalEz(x,x) != x,x+1 minus.dot x,0 minus.dot x) = Casos(UniversalEz(x,x) Termina #y UniversalEz(x,x) != x,1,0)$ \

*d.*

$g_4 (x,y,z) = Casos(UniversalEz(y,z) Termina #y (Universal(x,1) comp Universal(y,1)) Termina,(Universal(x,1) comp Universal(y,1))(z),0)$ \

Sea $"uno"(x) = s(n(x)) = 1$. Es absolutamente computable, es más, es p.r. \

#text(gray,[Recordar, \ $n(x) = 0$. \ $s(x) = x + 1$. \ $g circle.tiny f = g(f(\_)).$])

$f_1(x,y) = g_4 (\#("uno"), x, y) = Casos(UniversalEz(x,y) Termina,1,0)$ \

Luego, $f_1$ no es computable, por lo que $g_4$ tampoco lo es. \

#pagebreak()

= Ejercicio 3

$g^'_3 (x,y,z) = Casos(UniversalEz(x,y) Termina #y UniversalEz(x,y) != z ,z,0)$ \

Veamos primero que pasa con $f_4 (0)$.

$f_4 (0) = Casos(UniversalEz(0,0) Termina " " and " " UniversalEz(0,0) != 0,1,0)$ \
Recordemos qué programa se codifica con el número $0$. _Flashback al apunte de Franco..._
Exáctamente, el programa que tiene como única instrucción *pass*. Sea $emptyset$ el programa que tiene como única instrucción *pass*. \

#algorithm({
  import algorithmic: *
  Program($emptyset$+":")("",{
    Pass
  })
})

Entonces, sabemos que el programa termina, por lo que $UniversalEz(0,0) Termina$ es verdadero. Sin embargo, $UniversalEz(0,0) != 0$ es falso, pues el programa arranca con $Y = 0$, no hace nada, y termina. Por lo que $f_4(0) = 0$. \

Sea $h(x) = Casos(x != 0 ,1 , 0)$. Sé que es computable pues $!=$ es un predicado computable, y $s(x)$ también lo es. En particular, $h$ es p.r. \

$f_4(x) = h(g^'_3(x,x,x)) = Casos(UniversalEz(x,x) Termina #y UniversalEz(x,x) != x ,h(x), h(0)) = Casos(UniversalEz(x,x) Termina #y UniversalEz(x,x) != x ,h(x), 0)$. \

Luego, si $x = 0$:

$f_4(0) = h(g^'_3(0,0,0)) = Casos(UniversalEz(0,0) Termina #y UniversalEz(0,0) != 0 ,h(0), h(0)) = Casos(UniversalEz(0,0) Termina #y UniversalEz(0,0) != 0 ,0, 0) = 0$ pues $UniversalEz(0,0) = 0$. \

Si $x != 0$:

$f_4(x) = h(g^'_3(x,x,x)) = Casos(UniversalEz(x,x) Termina #y UniversalEz(x,x) != x ,h(x), h(0)) = Casos(UniversalEz(x,x) Termina #y UniversalEz(x,x) != x ,1, 0)$, que es la def. de $f_4$. \

= Ejercicio 4 (TODO)

Bueno, probar que existen funciones parcialmente computables que no son extensibles se resume en: "dame una que no sea extensible". \

= Ejercicio 5

Lo es. Doy mi vida, mi fé, mi dignidad, mi credibilidad y mi orgullo por ello. \
#align(right)[$square$]

Ahora un poco más en serio. La función en sí es computable, totalmente computable; nadie dijo que tengamos los valores para calcularla, pero que existe un programa que la computa, existe. \
¿Si supieses la cantidad de estrellas en el universo el 23 de junio de 1912 a las 2:16 (hora de Londres), podrías calcular esta función? La respuestá es sí. \

= Ejercicio 6

*a.* Si $S$ es computable, entonces es c.e. \
Verdadero. Pues, \
$S$ *totalmente* computable $=>$ su predicado característico es *totalmente* computable _(saber si una palabra pertenece al lenguaje $S$)_ $=>$ $S$ es un lenguaje computablemente enumerable (c.e.), pues su predicado característico es _parcialmente_ computable \
Recordar que: computable $equiv$ *totalmente* computable $=>$ _parcialmente_ computable). \

*b.* Si $S$ es c.e., entonces $S$ es computable o su complemento lo es.  \
Falso. No confundir con, \
$S$ es computable $<=>$ $S$ es c.e. $and$ $S^c$ es c.e. \

Para probar que lo que dice el enunciado es falso: \
Sabemos que los lenguajes computables son cerrados respecto al complemento. Es decir \
$S$ es computable #sii $S^c$ es computable. \

Luego, basta con dar un lenguaje que sea c.e., pero que no sea computable. \
Ejemplo, $cal("HALT") = {<\#(P), x> | P(x) Termina}$ es c.e. (pues su predicado característico es parcialmente computable), pero no es computable. \

*c.* Si $S$ es c.e., entonces su complemento es c.e. \
Falso. Los lenguajes c.e. no son cerrados respecto al complemento. Ejemplo, el lenguaje $cal("HALT")$ es c.e., pero su complemento $cal("HALT")^c = {<\#(P), x> | P(x) Cuelga}$ no lo es (pues su predicado característico no es parcialmente computable). \

= Ejercicio 7 (TODO)
