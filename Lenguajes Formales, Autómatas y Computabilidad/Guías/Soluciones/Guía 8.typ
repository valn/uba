#import "./template/main.typ" as template
#import "@preview/codelst:2.0.2": sourcecode
#import "./template/spp.typ" as algorithmic
#import algorithmic : algorithm

#show: template.conf.with(
  título: "Lenguajes Formales, Autómatas y Computabilidad",
  subtítulo: "Guía 8: resolución",
  extra: (
    [_\@valnrms_],
  )
)

= Ejercicio 1
#algorithm({
  import algorithmic: *
  Macro($V = V + k$ + ":",{
    Comment([Incrementamos la variable $V$, $k$ veces.])
    Increment($V$, comment: [$V$ vale $V + 1$])
    Increment($V$, comment: [$V$ vale $V + 2$])
    Comment([...], colour: black, prefix: "")
    Increment($V$, comment: [$V$ vale $V + k$])
  })
})

#algorithm({
  import algorithmic: *
  Macro($V = k$ + ":",{
    Comment([Seteamos la variable $V$ en $0$, luego le sumamos $k$.])
    Asignation($V$, $0$, comment: [Seteo $V$ en $0$])
    Asignation($V$, $V + k$, comment: [Uso la macro anterior para sumar $k$ a $V$])
  })
})

#algorithm({
  import algorithmic: *
  Macro($V_1 = V_1 + V_2$ + ":",{
    Asignation($Z$, $V_2$, comment: [Macro definida en el apunte de Franco])
    While(cond: $Z != 0$, {
      Increment($V_1$)
      Decrement($Z$)
    })
  })
})

#sourcecode[```C
/*
if V != 0 then {
  P
}
*/

// Se define con la sig. macro:

Z = V
while Z != 0 do {
  P
  Z = 0
}
```]

#sourcecode[```C
/*
if V != 0 then {
  P_1
} else {
  P_2
}
*/

// Se define con la sig. macro:

Z_2 = 1

if V != 0 then {
  P_1
  Z_2 = 0
}

if Z_2 != 0 then {
  P_2
}
```]

#sourcecode[```C
// loop

Z = 1
while Z != 0 do {
  pass
}
```]

#sourcecode[```C
// V = Ψ_P^(n)(V_1,...,V_n)

P
V = Y
```]

= Ejercicio 2

*a.* \
#sourcecode[```C
Y = X_1
Y = Y + X_2 
```]

*b.*
*pass*.

*c.* \
El primero $Psi_P^((1)) (x_1)$, indica el valor computado por $P$ cuando la variable de entrada $X_1$ toma el valor $x_1$, y el resto de las variables de entrada el valor 0. Siguiendo el código, es sencillo ver que para todo $x_1$ vale que $Psi_P^((1)) (x_1) = x_1$. \

El segundo $Psi_P^((2)) (x_1, x_2)$, indica el valor computado por $P$ cuando las variables de entrada $X_1$ y $X_2$ toman los valores $x_1$ y $x_2$ (respectivamente), y el resto de las variables de entrada el valor 0. Siguiendo el código, es sencillo ver que para todo $x_1, x_2$ vale que $Psi_P^((2)) (x_1, x_2) = x_1 + x_2$. \

El tercero $Psi_P^((3)) (x_1, x_2, x_3)$, indica el valor computado por $P$ cuando las variables de entrada $X_1$, $X_2$ y $X_3$ toman los valores $x_1$, $x_2$ y $x_3$ (respectivamente), y el resto de las variables de entrada el valor 0. Siguiendo el código, es sencillo ver que para todo $x_1, x_2, x_3$ vale que $Psi_P^((3)) (x_1, x_2, x_3) = x_1 + x_2$, pues nunca utilizo la variable $X_3$. \

= Ejercicio 3

*a.* Habría que mostrar que, las funciones iniciales:
- $n(x) = 0$
- $s(x) = x + 1$
- $u_i^n (x_1,...,x_n) = x_i$ para $i in {1,...,n}$
Son computables.

#sourcecode[```C
// n(x) = 0

Y = 0
```]

#sourcecode[```C
// s(x) = x + 1

Y = X
Y = Y + 1
```]

#sourcecode[```C
// u_i^n (x_1,...,x_n) = x_i

Y = X_i // i-ésima variable de entrada
```]

Es evidente que, las funciones iniciales son *totalmente computables*. \
Luego, hace mostrar que las técnicas de composición de funciones recursivas primitivas y la recursión primitiva, preservan la computabilidad. \

*b.* \

Como $f(x,y) = x y$ es una función recursiva primitiva, entonces, $f(x,y)$ es *computable*. Sale de inmediato de la demo del ítem anterior. \

= Ejercicio 4

*a.* \
#sourcecode[```C
// while r(x_1,...,x_n) do { P }

Z = r(X_1,...,X_n)

while Z != 0 do {
  P
  Z = r(X_1,...,X_n)
}
```]

*b.* \
#sourcecode[```C
// if r(x_1,...,x_n) then { P_1 } else { P_2 }

Z = r(X_1,...,X_n)

if Z != 0 then {
  P_1
} else {
  P_2
}
```]

= Ejercicio 5

*a.* \

Macro:
#sourcecode[```C
// if r(x_1,...,x_n) then { P }

Z = r(X_1,...,X_n)
if Z != 0 then {
  P
}
```]

#sourcecode[```C
Z_1 = X_1

while Z_1 != rho_Sigma (lambda) do {
  if cab_Sigma (Z_1) == #_Sigma (a) then {
    Z_2++
  } else {
    Z_3++
  }

  Z_1 = cola_Sigma (Z_1)
}

if Z_2 == Z_3 then {
  Y = 1
}
```]

*c.* \

#sourcecode[```C
Z_1 = X_1

while cab_Sigma (Z_1) == rho_Sigma (a) do {
  Z_2++
  Z_1 = cola_Sigma (Z_1)
}

while cab_Sigma (Z_1) == rho_Sigma (b) do {
  Z_3++
  Z_1 = cola_Sigma (Z_1)
}

while cab_Sigma (Z_1) == rho_Sigma (c) do {
  Z_4++
  Z_1 = cola_Sigma (Z_1)
}

if Z_1 == rho_Sigma (lambda) && Z_2 = Z_3 && Z_3 = Z_4 then {
  Y = 1
}
```]

*e.* \

//Este es duro. Yo diría "ya probé que hay una gramática libre de contexto que no es recuriva a izquierda (por lo que es primitiva recursiva) que genera este lenguaje!, por lo que es computable!". \

#sourcecode[```C
Z_1 = X_1

if |Z_1| mod 2 == 0 then {
  Z_2 = |Z_1| div 2
  Z_3 = sub(Z_1, 1, Z_2)
  Z_5 = Z_2 + 1
  Z_6 = 2 * Z_2
  Z_4 = sub(Z_1, Z_5, Z_6)

  // En Z_2 tengo la longitud de Z_1/2
  // En Z_3 tengo la primera mitad de Z_1
  // En Z_4 tengo la segunda mitad de Z_1

  Y = 1
  while Z_2 != 0 do {
    if cab(Z_3) == cab(Z_4) then {
      Z_3 = cola(Z_3)
      Z_4 = cola(Z_4)
      Z_2--
    } else {
      Y = 0
      Z_2 = 0
    }
  }
}
```]

= Ejercicio 6

Me piden probar que $h(x)$ es *parcialmente computable*. Basta con dar un programa $P$ en el lenguaje $S++$ que compute (parcialmente) a $h(x)$. \
Puedo asumir que $f(x)$ y $g(x)$ son *parcialmente computables*. En particular $g(x)$ es totalmente computable. \

#sourcecode[```C

if x >= 5 or x = 3 then {
  Y = f(x)
} else {
  Y = g(x)
}
```]

= Ejercicio 7

*a.* \

#sourcecode[```C
Z = X_(n+1) // y = X_(n+1)

while p(X_1,...,X_n, Z) == 0 do {
  Z++
}

Y = Z
```]

*b.* \

Sea $rho_f: NN^2 -> {0, 1}$ el predicado característico de $f : NN -> NN$ tal que, $rho_f(x,y) = cases(1 "si" f(x) = y, 0 "si no")$. Interpreto a $f$ como un mapeo de $NN$ a $NN$, pues es biyectiva, y además, sé que $rho_f(x,y)$ es un predicado computable (pues $f$ es computable). \

#sourcecode[```C
// rho_f (x,y)

Z_1 = X_1
Z_2 = X_2

Z_3 = f(Z_1) // No sé si el programa que computa la función f me modifica el valor que le paso, es por eso que le paso Z_1 en lugar de X_1

if Z_3 == Z_2 then {
  Y = 1
} else {
  Y = 0
}
```]

Y,

#sourcecode[```C
// rho_(f^-1) (x,y)

Y = rho_f (X_2,X_1)
```]

Sé que el mapeo de $f$ es único pues es biyectiva, luego,

#sourcecode[```C
// Programa que computa f^(-1)

Y = mu_(rho_(f^-1)) (X_1, 0)
```]

= Ejercicio 8

Idea: destripamos el programa (decodificarlo) y nos fijamos si tiene alguna instrucción while en la cual no utilice alguna variable de tipo $Z_i$. Posiblemente esté copado pensar el caso (el enunciado es medio ambiguo en esto, sobre si hay que tenerlo en cuenta o no), que capaz tengamos alguna varible que no es de tipo $Z_i$ en la condición de un while, pero dentro de otro while. \
Para ese caso nos va a interesar meternos adentro del programa del while y ver si es aburrido o no. \

Sé que $x = [\#(I_1), \#(I_2), ..., \#(I_k)]$ \

Hagamos top-down. \

$r(x) = not(exists(1 <= i <= |x|) ("esWhileDivertido"(x[i])))$ \
Es decir, $r(x) = 1$ si y solo si todas sus instrucciones while son aburridas. \

Ahora tenemos otro problema, armar el predicado $"esWhileDivertido"$. \

Recordar que x es una instrucción de un programa en $S++$, que se codifican con tuplas. \

$"var"(x) = l(x - 1)$ \
$"tipo"(x) = r(x - 1)$ \
$"esWhile"(x) = "tipo"(x) >= 2$ \
$"cuerpoDeWhile"(x) = "tipo"(x) - 2$ \
$"esTipoZ"(x) = (x != 0) and (x mod 2 == 0)$ \

$"esWhileDivertido"(x) = "esWhile"(x) and (not"esTipoZ"("var"(x)) or exists_(1<=i<=|"cuerpo"(x)|) ("esWhileDivertido"("cuerpo"(x)[i])))$ \

Luego, como todo lo que usé es p.r. entonces $r(x)$ es p.r. \

= Ejercicio 9

*a.* \

#sourcecode[```C
// f_1(x,y) = {1 si y in Dom Phi_x^(1), "se cuelga" si no}
// En criollo, f_1(x,y) = 1 si Phi_x^(1) está definida en y (o sea, si le paso al programa x el valor y como parámetro, no se cuelga), en caso contrario, se cuelga.

Z = 0
while Y != 1 do { // Asumiendo que Y arranca en 0, claro.
  Y = Step(Z, X_1, X_2)
}
```]

*b.* \

Hay que usar generación infinita para hacer esto (empieza a sonar Everlong). Larga vida a PLP. \

#sourcecode[```C
while Y != 1 do {
  Z_n = Z_1 // Z_n va a representar el t en los steps
  Z_(n-1) = Z_2 // Z_(n-1) va a representar la variable que se le pasa a Phi_x^(1)

  Z_1++

  if (Z_1 > Z_2) then {
    Z_2++
    Z_1 = 0
  }
}
```]

*c.* \

$f_3 (x, y) = exists_t ( Phi_x^(1) (t) = y )$ \

*d.* \

$f_4 (x, y) = exists_t ( f_1 (x,t) and f_3 (x,t) )$ \
