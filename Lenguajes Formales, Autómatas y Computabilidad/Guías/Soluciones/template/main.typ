#let conf(
  título: none,
  subtítulo: none,
  extra: none,
  page-conf: (:),
  doc,
) = {
  set text(lang: "es")
  set heading(numbering: "1.")
  set enum(numbering: "ai)")

  if extra != none {
    if type(extra) != array {
      panic("parámetro extra debe ser una lista")
    }
    for elem in extra {
      if type(elem) != content {
        panic("extra debe ser una lista con elementos de tipo content")
      }
    }
  }

  let header = [
    #stack(
      dir: ltr,
      align(
        bottom + left,
        stack(
          dir: ttb,
          spacing: 3pt,
          text(12pt, "Universidad de Buenos Aires"),
          text(10pt, "Facultad de Ciencias Exactas y Naturales"),
          text(""),
        ),
      ),
      align(bottom + right)[#image("./assets/dc.svg", width: 25%)],
    )
    #v(-15pt)
    #line(length: 100%, stroke: 0.4pt)
  ]


  let title = (text(20pt, título),)
  if subtítulo != none {
    title.push(text(12pt, subtítulo))
  }
  title = stack(spacing: 6pt, ..title)

  if extra != none {
    title = stack(
      spacing: 8pt,
      title,
      stack(spacing: 5pt, text(10pt, ..extra)),
    )
  }
  title = align(center, title)

  let header-sep = 15pt
  set page(
    paper: "a4",
    margin: (
      left: 1in,
      right: 1in,
      top: 1in + header-sep,
      bottom: 1in,
    ),
    header: header,
    header-ascent: header-sep,
    ..page-conf,
  )

  title
  doc
}
