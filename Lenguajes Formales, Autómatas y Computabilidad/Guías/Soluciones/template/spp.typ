// SPDX-FileCopyrightText: 2023 Jade Lovelace
//
// SPDX-License-Identifier: MIT

/*
 * Generated AST:
 * (change_indent: int, body: ((ast | content)[] | content | ast)
 */

#let ast_to_content_list(indent, ast) = {
    if type(ast) == "array" {
        ast.map(d => ast_to_content_list(indent, d))
    } else if type(ast) == "content" {
        (pad(left: indent * 0.5em, ast),)
    } else if type(ast) == "dictionary" {
        let new_indent = ast.at("change_indent", default: 0) + indent
        ast_to_content_list(new_indent, ast.body)
    }
}

#let algorithm(..bits) = {
    let content = bits.pos().map(b => ast_to_content_list(0, b)).flatten()
    let table_bits = ()
    let lineno = 1

    while lineno <= content.len() {
        table_bits.push([#lineno:])
        table_bits.push(content.at(lineno - 1))
        lineno = lineno + 1
    }
    table(
        columns: (18pt, 100%),
        // line spacing
        inset: 0.3em,
        stroke: none,
        ..table_bits
    )
}

#let while_block(kw1: "", kw2: "", cond: "", ..body) = (
    (strong(kw1) + " " + cond + " " + strong(kw2) + " " + ${$),
    (change_indent: 4, body: body.pos()),
    $}$
)

#let codeblock(kw1: "", kw2: "", cond: "", ..body) = (
    (strong(kw1) + " " + cond + " " + strong(kw2)),
    (change_indent: 4, body: body.pos())
)

#let pseudo(name, kw: "", args: (), ..body) = (
    codeblock(kw1: kw, cond: (smallcaps(name) + "" + args.join(", ") + ""), ..body)
)

#let Macro = pseudo
#let Program(ProgramName) = pseudo.with(kw: ProgramName)

// S++
#let Newline = ((body: ""),)
#let InlineComment(comment, colour, prefix) = if comment != [] {
    text(colour, prefix + " " + comment)
}
#let Pass = (InlineComment(text(weight: "bold", "pass"), black, ""),)
#let Comment(comment) = (InlineComment(comment),)
#let Comment(comment, colour: gray, prefix: "//") = (InlineComment(comment, colour, prefix),)
#let Increment(var, comment: [], ) = (var + $++$ + " " + InlineComment(comment, gray, "//"),)
#let Decrement(var, comment: []) = (var + $--$ + " " + InlineComment(comment, gray, "//"),)
#let While = while_block.with(kw1: "while", kw2: "do")
#let Asignation(var, value, comment: []) = (var + " " + $=$ + " " + value + " " + InlineComment(comment, gray, "//"),)
#let If = while_block.with(kw1: "if", kw2: "then")