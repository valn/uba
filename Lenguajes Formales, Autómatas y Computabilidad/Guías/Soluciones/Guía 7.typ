#import "./template/main.typ" as template

#show: template.conf.with(
  título: "Lenguajes Formales, Autómatas y Computabilidad",
  subtítulo: "Guía 7: resolución",
  extra: (
    [_\@valnrms_],
  )
)

= Ejercicio 1

$f(x) = k = s(...(s(n(x))))$ donde hay $k$ composiciones. Ejemplo: \
$f(x) = 3 = s(s(s(n(x))))$. $n(x)$ tiene cero composiciones, $s(n(x))$ tiene una composición, $s(s(n(x)))$ tiene dos composiciones y $s(s(s(n(x))))$ tiene tres composiciones. \

= Ejercicio 2

*a.* $f(x,y) = x + y$. \

- $n = 1$
- $f(x) = mu_1^1(x)$
- $g(\_) = s(mu_1^3(\_))$

$h : NN^2 -> NN "queda dada por:"$ \

$h(x,0) = f(x) = mu_1^1(x)$ \
$h(x,y + 1) = g(h(x,y),x,y) = s(mu_1^3(h(x,y),x,y)) = s(h(x,y))$ \

Pasando en limpio: \

$h(x,0) = mu_1^1(x)$ \
$h(x,y + 1) = s(h(x,y))$ \

*b.* $f(x,y) = x dot y$. \

Notar: puedo asumir que tengo la función $"suma"(x,y) = x + y$ pues la hice en el item anterior. \

- $n = 1$
- $f(x) = n(x) = 0$
- $g(x,y,z) = "suma"(x,y)$

$h : NN^2 -> NN "queda dada por:"$ \

$h(x,0) = f(x) = n(x) = 0$ \
$h(x,y + 1) = g(h(x,y),x,y) = "suma"(h(x,y),x) = h(x,y) + x$ \

*c.* $f(x,y) = x^y$. \

Notar: puedo asumir que tengo la función $"prod"(x,y) = x dot y$ pues la hice en el item anterior. \

- $n = 1$
- $f(x) = s(n(x)) = 1$
- $g(x,y,z) = "prod"(x,y)$

$h : NN^2 -> NN "queda dada por:"$ \

$h(x,0) = f(x) = s(n(x)) = 1$ \
$h(x,y + 1) = g(h(x,y),x,y) = "prod"(h(x,y),x) = h(x,y) dot x$ \

*d.*

Notar: puedo asumir que tengo la función $"exp"(x,y) = x^y$ pues la hice en el item anterior. \

- $n = 1$
- $f(x) = s(n(x)) = 1$
- $g(x,y,z) = cases(
  mu_2^3(x,y,z) "si" z = 0,
  "exp"(x,y) "si no"
)$

$h : NN^2 -> NN "queda dada por:"$ \

$h(x,0) = f(x) = s(n(x)) = 1$ \
$h(x,y + 1) = g(h(x,y),x,y) =
cases(
  mu_2^3(h(x,y),x,y) "si" y = 0,
  "exp"(h(x,y),x) "si no"
) =
cases(
  x "si" y = 0,
  h(x,y)^x "si no"
)$ \

*e.* $f(x) = x minus.dot 1$. \

- $f = n = 0$
- $g = mu_2^2$

$h : NN -> NN "queda dada por:"$ \

$h(0) = f = 0$ \
$h(x + 1) = g(h(x), x) = mu_2^2(h(x), x) = x$ \

*f.* $f(x,y) = x minus.dot y$. \

Notar: puedo asumir que tengo la función $"anterior"(x) = x minus.dot 1$ pues la hice en el item anterior. \

- $n = 1$
- $f = mu_1^1$
- $g(x,y,z) = "anterior"(x)$

$h(x,0) = f(x) = mu_1^1(x) = x$ \
$h(x, y + 1) = g(h(x,y), x , y) = "anterior"(x) = h(x,y) minus.dot 1$ \

*g.* $f(x,y) = "máx"(x,y)$ \

Sea $<=$ el siguiente predicado primitivo recursivo: \

$<=(x) = cases(
  1 "si" x = 0,
  0 "si no"
)$

Y se usa de la siguiente manera: si quiero preguntar por $x <= y$, hago $<=(x minus.dot y)$. \

Luego,

$"máx"(x,y) = cases(
  mu_2^2(x,y) = y "si" <=(x minus.dot y),
  mu_1^2(x,y) = x "si no"
)$

*h.* $f(x,y) = "mín"(x,y)$ \

Usando el predicado $<=$ definido en el item anterior: \

$"mín"(x,y) = cases(
  mu_1^2(x,y) = x "si" <=(x minus.dot y),
  mu_2^2(x,y) = y "si no"
)$

= Ejercicio 3

*a.* \

$g(0,x_1,...,x_n) = f(0,x_1,...,x_n)$ \
$g(t+1,x_1,...,x_n) =  "suma"(g(t,x_1,...,x_n),f(t+1,x_1,...,x_n)) = g(t,x_1,...,x_n) + f(t+1,x_1,...,x_n)$ \

*b.* \

$g(0,x_1,...,x_n) = f(0,x_1,...,x_n)$ \
$g(t+1,x_1,...,x_n) = "producto"(g(t,x_1,...,x_n),f(t+1,x_1,...,x_n)) = g(t,x_1,...,x_n) dot f(t+1,x_1,...,x_n)$ \

= Ejericio 4

Sea $alpha$ el siguiente predicado primitivo recursivo: \

$alpha(x) = cases(
  1 "si" x = 0,
  0 "si no"
)$

Y $not$ el siguiente predicado primitivo recursivo: \

$not(p(x_1,...,x_n)) = alpha(p(x_1,...,x_n)) = cases(
  1 "si" p(x_1,...,x_n) = 0,
  0 "si no"
)$

- $<=(x,y)$ visto como $x <= y$:

$<=(x,y) = alpha(x minus.dot y) = cases(
  1 "si" x minus.dot y = 0,
  0 "si no"
)$

- $>=(x,y)$ visto como $x >= y$:

$>=(x,y)$ $=$ $<=(y,x) = alpha(y minus.dot x) = cases(
  1 "si" y minus.dot x = 0,
  0 "si no"
)$

- $=(x,y)$ visto como $x = y$:

$=(x,y)$ $=$ $<=(x,y) dot >=(x,y)$

- $!=(x,y)$ visto como $x != y$:

$!=(x,y)$ $=$ $not(=(x,y))$

- $<(x,y)$ visto como $x < y$:

$<(x,y)$ $=$ $<=(x,y) dot not(=(x,y))$

- $>(x,y)$ visto como $x > y$:

$>(x,y)$ $=$ $>=(x,y) dot not(=(x,y))$

= Ejercicio 5

*a.* \

$not(p(x_1,...,x_n)) = alpha(p(x_1,...,x_n)) = cases(
  1 "si" p(x_1,...,x_n) = 0,
  0 "si no"
)$

*b.* \

$and(p(x_1,...,x_n),q(y_1,...,y_m)) = cases(
  1 "si" p(x_1,...,x_n) dot q(y_1,...,y_m) = 1,
  0 "si no"
)$

*c.* \

$or(p(x_1,...,x_n),q(y_1,...,y_m)) = cases(
  0 "si" p(x_1,...,x_n) + q(y_1,...,y_m) = 0,
  1 "si no"
)$

= Ejercicio 6

$h(x_1,...,x_n) = cases(f_1(x_1,...,x_n) "si" p_1(x_1,...,x_n),.,.,.,f_k(x_1,...,x_n) "si" p_k(x_1,...,x_n),g(x_1,...,x_n) "en otro caso") = f_1(x_1,...,x_n) dot p_1(x_1,...,x_n) + ... + f_k(x_1,...,x_n) dot p_k(x_1,...,x_n) + g(x_1,...,x_n) dot not(p_1(x_1,...,x_n) or ... or p_k(x_1,...,x_n))$

Luego, como $f_1,...,f_k,g$ son funciones primitivas recursivas, $p_1,...,p_k$ son predicados primitivos recursivos y $not,or$ son predicados primitivos recursivos, $h$ es primitiva recursiva.

= Ejercicio 7

$a.*$ \

$"par"(0) = 1$ \
$"par"(x + 1) = not("par"(x))$ \

O, equivalentemente, \

$"par"(x) = cases(1 "si" x = 0, not("par"(x)) "si no")$

*b.*

_Idea:_ \
$f(0) = 0$ \
$f(1) = 0$ \
$f(x) = f(x minus.dot 2) + 1$ \

Luego, \
$f(x) = cases(
  0 "si" x = 0 or x = 1,
  f(x minus.dot 2) + 1 "si no"
)$ \

*c.*

Por el ejercicio 6, basta ver que $t = 0$, $t = 2k+1$ y $t = 2k+2$ son predicados primitivos recursivos disjuntos. \
Asumo $k >= 0$, pues no lo especifica el enunciado. \

El primer predicado es verdadero si y solo si $t = 0$. \
Asumamos $t = 0$, ergo cae en el primer predicado (da 1), luego basta ver que $exists.not k >= 0 "tq'" 0 = 2k + 1 or 0 = 2k + 2$. \

Despejando el segundo predicado, $2k + 1 = 0$ implica $k = -1/2$, pero $k in NN^0$, absurdo. \

Despejando el tercer predicado, $2k + 2 = 0$ implica $k = -1$, pero $k in NN^0$, absurdo. \

Vimos entonces que el primer predicado es disjunto de los otros dos. \

Ahora, asumamos $t = 2k + 1$ para algún $k in NN^0$, ergo cae en el segundo predicado (da 1), o sea, el primer predicado dio 0, el segundo 1 y el tercero aún no sabemos. \

Despejando con el tercer predicado, $2k + 1 = 2k + 2$ implica $1 = 2$, absurdo. \
Por lo tanto, el segundo predicado es disjunto del tercero. \

Luego, el primero es disjunto del segundo y del tercero. \
El segundo es disjunto del primero y del tercero. \
Y el tercero es disjunto del primero y del segundo. \

Por lo tanto, los tres predicados son disjuntos. \

= Ejercicio 8

$"cantidad"_p (x_1,...,x_n,y,z) = sum_(t=0)^z (t >= y) and p(x_1,...,x_n,t)$ \

$"todos"_p (x_1,...,x_n,y,z) = product_(t=0)^z (t < y) or p(x_1,...,x_n,t)$ \

$"alguno"_p (x_1,...,x_n,y,z) = cases(1 "si" "cantidad"_p (x_1,...,x_n,y,z) >= 1, 0 "si no")$ \

$"mínimo"_p (x_1,...,x_n,y,z) = "mín"_(t <= z) (y <= t and p(x_1,...,x_n,t))$ \

Puedo definir el máximo pidiendo el mínimo $t$ que cumpla el predicado $p$, y que no exista un $t'$ tal que $t' > t$ y $p(x_1,...,x_n,t')$. \

$"máximo"_p (x_1,...,x_n,y,z) = "mínimo"_q (x_1,...,x_n,y,z)$ donde $q(x_1,...,x_n,t) = p(x_1,...,x_n,t) and not("alguno"_p(x_1,...,x_n,t+1,z))$ \

Sea $m = "mínimo"_p(x_1,...,x_n,y,z)$ y $M = "máximo"_p(x_1,...,x_n,y,z)$. \

$"único"_p(x_1,...,x_n,y,z) = cases(m "si" m = M, z + 1 "si no")$ \

= Ejercicio 9

$"cociente"(x,y) = "mín"_(t <= x) ((t+1) dot y > x)$ \

$"resto"(x,y) = x minus.dot "cociente"(x,y) dot y$ \

$"divide"(x,y) = cases(1 "si" "resto"(y,x) = 0, 0 "si no")$ \

$"primo"(x) = not("noEsPrimo"(x))$ \

$"noEsPrimo"(x) = cases(1 "si" "alguno"_("divide"(d,x)) (x,2,x-1), 0 "si no")$ \

$"raíz"(x,y) = "único"_p(x,y,0,x)$ con $p = (x,y,t) = exp(y,t) = x$ \

$"nprimo"(0) = 0$ \
$"nprimo"(x+1) = "mín"_(t<=K(n)) ("primo"(t) and t > "nprimo"(x))$ \

Necesitamos una cota $K(n)$ que sea buena, es decir \
- suficientemente grande y
- recursiva primitiva

$K(n) = p_n! + 1$ funciona. \

= Ejercicio 10

$z = <x,y> = 2^x (2y + 1) minus.dot 1$ \

$l,r : NN -> NN$ \

$l(z) = "mín"_(x<=z) (exists_(y<=z) (z = <x,y>))$ \
$r(z) = "mín"_(y<=z) (exists_(x<=z) (z = <x,y>))$ \

= Ejercicio 11

_Idea:_ \
$"fib"(0) = 0$ \
$"fib"(1) = 1$ \
$"fib"(n+2) = "fib"(n+1) + "fib"(n)$ \

Planteo $"fib"'$ con esta idea: \
$"fib"'(n) = <"fib"(n), "fib"(n+1)>$ \

Quiero ver que $"fib"'$ es p.r. \

$"fib"'(0) = <"fib"(0), "fib"(1)> = <0,1>$ \
$"fib"'(n+1) = <"fib"(n+1), "fib"(n+2)> = <"fib"(n+1), "fib"(n+1) + "fib"(n)> = <r("fib"'(n)), r("fib"'(n)) + l("fib"'(n))>$ \

Es todo composiciones, sumas, crear y observar tuplas, y todo sobre $"fib"'(n)$. \

Luego, $"fib"'$ es p.r. \

Entonces, reescribo $"fib"$ con $"fib"'$. \
$"fib"(n) = l("fib"'(n))$ \
Por lo tanto, $"fib"$ es p.r. \

= Ejercicio 12

*a.* \

Esto se resume en el teorema fundamental de la aritmética. El mismo dice que, todo número natural $n > 1$ se puede expresar de forma única como producto de primos. \

$n = p_1^(a_1) dot ... dot p_k^(a_k) = product_(i=1)^k p_i^(a_i)$ \

El TFA nos garantiza que la codificación de una secuencia $[a_1,...,a_n]$ es única. \
Y, a su vez, que para cada número $n = p_1^(a_1) dot ... dot p_k^(a_k)$ hay una única secuencia $[a_1,...,a_n]$ que lo codifica. Esto siempre y cuando no tengamos ceros al final de la secuencia. \

*b.* \

La secuencia vacía se codifica con el número 0. Sería la productoria de ningún número primo. \

$l$ es la codificación de la secuencia $[a_1,...,a_n]$ \

$|l| = "max"_("primo" <= l) "divide"("nprimo"("primo"),l)$ \

$l["índice"] = "max"_("exponente" <= l) "divide"("nprimo"("índice")^"exponente",l)$ \

$[x] = 2^x$

$|l_1| = n and |l_2| = m$

$l_1 circle.tiny l_2 = product_(i=1)^n p_i^(l_1[i]) dot product_(i=1)^m p_(n+i)^(l_2[i])$

$"sub"([a_1,...,a_n],i,j) = product_(k=i)^j p_(k-i+1)^(l_1[k])$ \

*c.* \

$rho : NN^* -> NN$ \

$rho([]) = 0$ \
$rho([a_1,...,a_n]) = 2^(a_1+1) dot 3^(a_2+1) dot ... dot p_n^(a_n+1) = product_(i=1)^n p_i^(a_i+1)$ \

Se me rompe el observador y la creación que me definí arriba. Se pueden fixear volviéndolos a definir para esta codificación $rho$. \

= Ejercicio 13

Sé que $Sigma$ tiene una enumeración de símbolos, tal que $\#_Sigma : Sigma -> {1,2,...,|Sigma|}$. \

Con lo que sé de secuencias, puedo codificar palabras. Puedo definir una palabra, como una secuencia de símbolos de $Sigma^*$. Asumo que las palabras no pueden terminar con $lambda$s (mismo problema que con los $0$s en secuencias). \

$rho_Sigma : Sigma^* -> NN$ \

$rho_Sigma(lambda) = []$ \
$rho_Sigma(s_1.s_2...s_n) = [\#_Sigma(s_1),\#_Sigma(s_2),...,\#_Sigma(s_n)]$ \

Sea $cal(s) = rho_Sigma(x.cal(w))$. \

$"cab"_Sigma (cal(s)) = cal(s)[1] = \#_Sigma(x)$ \

$"cola"_Sigma (cal(s)) = "sub"(cal(s),2,|cal(s)|)$ \

$"longitud"_Sigma (cal(s)) = |cal(s)|$ \

$"concat"_Sigma (cal(s_1),cal(s_2)) = cal(s_1) circle.tiny cal(s_2)$ \

$"rev"_Sigma (lambda) = lambda$ \
$"rev"_Sigma (cal(s)) = "concat"_Sigma("rev"_Sigma("cola"_Sigma (cal(s))), "cab"_Sigma (cal(s)))$ \

