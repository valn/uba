#import "@preview/ilm:1.2.1": *
#import "@preview/codly:1.0.0": *
#import "@preview/syntree:0.2.0": *
#import "@preview/finite:0.4.1": *
#import "@preview/algorithmic:0.1.0"
#import algorithmic: *
#show link: underline
#let Algoritmo = algorithmic.function_like.with(kw: "Algoritmo")
#show: codly-init.with()

#let comment = rgb("777777")
#let important = gradient.linear(
  rgb("#0d0887"),
  rgb("#42039d"),
  rgb("#6a00a8"),
  rgb("#900da3"),
  rgb("#b12a90"),
  rgb("#cb4678"),
  rgb("#e16462"),
  rgb("#cb4678"),
  rgb("#b12a90"),
  rgb("#900da3"),
  rgb("#6a00a8"),
  rgb("#42039d"),
  rgb("#0d0887"),
)

#let sequent = $tack.r$
#let sii = $arrow.l.r.double.long$
#let sii2 = $arrow.l.r.double$
#let implica = $arrow.r.double.long$
#let implica2 = $arrow.r.double$
#let implicaVuelta = $arrow.l.double.long$
#let implicaVuelta2 = $arrow.l.double$
#let QED = align(right)[$square$]
#let SigmaEstrella = $Sigma^*$
#let SigmaMás = $Sigma^+$
#let Gramática(NoTerminales, Terminales, Producciones, Start) = $angle.l NoTerminales, Terminales, Producciones, Start angle.r$
#let AF(Estados, AlfabetoEntrada, FunciónTransición, EstadoInicial, Final) = $angle.l Estados, AlfabetoEntrada, FunciónTransición, EstadoInicial, Final angle.r$
#let AP(Estados, AlfabetoEntrada, AlfabetoPila, FunciónTransición, EstadoInicial, ConfiguraciónInicialPila,  Final) = $angle.l Estados, AlfabetoEntrada, AlfabetoPila, FunciónTransición, EstadoInicial, ConfiguraciónInicialPila, Final angle.r$
#let MT(Estados, AlfabetoEntrada, FunciónTransición, EstadoInicial, EstadoFinal) = $angle.l Estados, AlfabetoEntrada, FunciónTransición, EstadoInicial, EstadoFinal angle.r$
#let deltaSombrero = $accent(delta, hat)$
#let ComplementoConjunto(Conjunto) = $accent(Conjunto, -)$
#let ClausuraLambda = $C l_lambda$
#let PartesDe = $cal(P)$
#let Tree = $cal(T)$
#let LenguajeDe = $cal(L)$
#let GeneradoPor = $cal(G)$
#let to = $->$
#let frown = $⌢$
#let LR = $L R$
#let pre = $p r e$
#let max = "máx"
#let min = "mín"

#set text(lang: "es")

#show: ilm.with(
  title: [Lenguajes Formales, Autómatas y Computabilidad],
  author: "@valnrms",
  date: datetime.today(),
  date-format: "Fecha de compilación: [day padding:zero] / [month padding:zero] / [year repr:full].",
  abstract: [
    Apunte teórico de la materia Lenguajes Formales, Autómatas y Computabilidad de la Universidad Nacional de Buenos Aires,
    dictada en el segundo cuatrimestre de 2024. \

    Profesora: #link("https://staff.dc.uba.ar/becher/")[Verónica Becher]. Gracias por el lindo cuatrimestre y las ganas puestas en dictar la materia.\
  ],
  preface: [
    #align(center + horizon)[
      Agradecimiento especial a Franco Frizzo, Leonardo Cremona, Elisa Orduna, Sabrina Silvero y Simón Lew Deveali por su aguante durante todo el cuatrimestre. \
      También a Pablo Barenbaum por ayudar a esclarecer conceptos que me resultaban confusos. \

      Este texto está principalmente hecho para mí. \
      De ninguna manera reemplaza cualquier material oficial que pueda tener la cátedra, ni mucho menos las clases teórias, ni prácticas, de la materia. \
      Se asume cierto entendimiento de las materias correlativas a esta. \
      \
      Algo trivial, aunque es importante recalcar, este texto puede contener $#text(font:"New Computer Modern", style: "italic")[orrorez]$.
    ]
  ],
)

= Lenguajes formales, gramáticas y la jerarquía de Chomsky
== Alfabetos y palabras

Vamos a comenzar estudiando lenguajes. Pensemos, sin rompernos mucho la cabeza, en un lenguaje como el español. Este lenguaje está compuesto por palabras, que a su vez, las palabras, están compuestas por símbolos. Algunos ejemplos de símbolos son "!", "¿", "a", "A", "7", etc... \

En la materia llamamos *alfabeto* $Sigma$ a un *conjunto finito*, *no vacío*, *de símbolos*. \

Luego, las *palabras* se definen como la *secuencia finita* de *cero o más elementos* de un *alfabeto* $Sigma$. \
Por ejemplo, la palabra "hola" es una palabra sobre el alfabeto $Sigma_"hola" = {h, o, l, a} = {a, h, l, o}$. \

Uno ahora podría preguntarse, ¿y "alho"?, ¿es una palabra sobre el alfabeto $Sigma_"hola"$? La respuesta es que sí. Actualmente estamos analizando la sintaxis, no la semántica de los lenguajes. \
"¿Y es necesario usar todos los elementos del alfabeto para formar una palabra?", no. Por ejemplo, "h" es una palabra sobre el alfabeto $Sigma_"hola"$. Lo mismo ocurre para "hhhlaaaoaaa". \

Si hilamos fino en la definición de palabra, se puede notar que dijimos *cero* o más elementos. Esto implica la existencia de la palabra vacía, que se denota como $lambda$. \

== Clausura de Kleene sobre un alfabeto
Dado un alfabeto $Sigma$, la *clausura de Kleene* del alfabeto $Sigma$ _(denotada como $SigmaEstrella$)_ es el conjunto de todas las palabras que se pueden formar con los elementos de $Sigma$. \
Es decir, donde $Sigma^0 = {lambda}$, $Sigma^1 = Sigma$, $Sigma^2 = Sigma times Sigma$, ... #h(0.3cm) | #h(0.3cm) $SigmaEstrella = Sigma^0 union Sigma^1 union Sigma^2 union ... = union_(i>=0) Sigma^i$. \

Donde el producto de alfabetos, sigue la definición de producto cartesiano de cualquier conjunto. \

=== Clausura positiva
La *clausura positiva* de un alfabeto $Sigma$ _(denotada como $SigmaMás$)_ es el conjunto de todas las palabras que se pueden formar con los elementos de $Sigma$, excluyendo la palabra vacía. \
Es decir, donde $Sigma^1 = Sigma$, $Sigma^2 = Sigma times Sigma$, ... #h(0.5cm) | #h(0.5cm) $SigmaMás = Sigma^1 union Sigma^2 union ... = union_(i>=1) Sigma^i$. \


== Cardinalidad de un alfabeto
"¿Cuál es la cardinalidad del conjunto $Sigma^i$?" \
El conjunto $Sigma$ tiene $|Sigma|$ elementos. Para cada $i >= 0$, $Sigma^i$ tiene $|Sigma|^i$ elementos. \

El conjunto $SigmaEstrella$ tiene una cantidad infinita numerable de palabras. \ Es decir, $|SigmaEstrella|$ es igual a la cardinalidad de $NN$. #h(6cm) *$<-$ Teorema* \

*Demostración:* \
Pensemos en una relación de orden $prec$, para $SigmaEstrella times SigmaEstrella$. \ Asumamos un orden lexicográfico entre los elementos del alfabeto. \
Extendamos $prec$ a un orden lexicográfico entre todas las palabras de la misma longitud (utilizando el orden $prec$ del alfabeto), tal que las palabras de menor longitud son menores que las de mayor longitud. \

Luego, definiamos la función biyectiva $f: NN to SigmaEstrella$ tal que $f(i) = i$-ésima palabra dada por el orden $prec$. Por lo que podemos mapear cada palabra de $SigmaEstrella$ a un número natural. Y sabemos que el conjunto de los números naturales es infinito numerable. \
#QED \

== Lenguaje sobre un alfabeto
Un *lenguaje* $L$ sobre un alfabeto $Sigma$ es un *conjunto* de palabras que se pueden formar con los elementos de $Sigma$. Es decir, $L subset.eq SigmaEstrella$. \
Ejemplos: $emptyset$, ${ lambda }$ _(distinto a $emptyset$)_, ${0, 01, 011, 0111, 01111, ...}$ es un lenguaje sobre $Sigma = {0, 1}$. \

=== ¿Cuántos lenguajes hay?
Para responder esta pregunta, va a ser importante recordar lo visto sobre la cardinalidad de un cojunto de partes. \

==== Cardinalidad de un conjunto de partes
Dado un conjunto $A$, se define $PartesDe(A)$ como *el conjunto de todos los subconjuntos* de $A$. \
Sea $A$ un conjunto finito, entonces $|PartesDe(A)| = 2^(|A|)$. \

Luego, si $Sigma$ es un alfabeto, $|PartesDe(SigmaEstrella)| = 2^(NN)$. \

==== La cantidad de lenguajes es no numerable
$|SigmaEstrella| < |PartesDe(SigmaEstrella)|$ #h(11cm) *$<-$ Teorema* \

*Demostración:* \
Sale usando el argumento de la diagonal de Cantor. \
Supongamos que $|PartesDe(SigmaEstrella)|$ es infinito numerable. Entonces, podemos enumerar los lenguajes como $L_1, L_2, L_3, ...$. \
Y demostramos anteriormente que $|SigmaEstrella|$ es infinito numerable. Por lo que cualquier subconjunto _(lenguaje)_ de $SigmaEstrella$ es numerable. \
Por lo que para cada $L_1, L_2, L_3, ...$ podemos numerar sus palabras. Luego, \

#align(center)[
$L_1 : w_(1,1), w_(1,2), w_(1,3), ...$ \
$L_2 : w_(2,1), w_(2,2), w_(2,3), ...$ \
$L_3 : w_(3,1), w_(3,2), w_(3,3), ...$ \
...
]
Definamos $LenguajeDe = {u_1, u_2, u_3, ...}$ donde $u_1 prec u_2 prec u_3 prec ...$, y $forall i$, $w_(i,i) prec u_i$. \
Notar que $LenguajeDe$ debe estar en nuestra tablita de arriba, pues es un lenguaje y la tablita que creamos suponiendo que $|PartesDe(SigmaEstrella)|$ es numerable, contiene todos los lenguajes. \
Luego, la $i$-ésima palabra de $LenguajeDe$ difiere de la $i$-ésima palabra de $L_i$ en la $i$-ésima posición, para cualquier $i$. \
Por lo que $LenguajeDe$ no es ninguno de los $L_i$ (o sea, no puede estar en nuestra tablita), lo que es una contradicción. #h(11.2cm) #text(important)[*¡ABSURDO!*]
¿De dónde vino el absurdo?, de suponer que $|PartesDe(SigmaEstrella)|$ era infinito numerable. \
#QED \

== Gramáticas

Una gramática es una $4$-upla $G = Gramática(V_N, V_T, P, S)$ donde \
- $V_N$ es un conjunto de *símbolos no terminales*. \
- $V_T$ es un conjunto de *símbolos terminales*. \
- $P$ es un conjunto de *producciones*, que es un conjunto finito de \

#align(center)[$(V_N union V_T)^* V_N (V_N union V_T)^* times (V_N union V_T)^*$,]

estas producciones son entonces pares ordenados $(alpha, beta)$, que
usualmente son notados como $alpha to beta$.
- $S in V_N$ es el *símbolo inicial*. \

Por ejemplo, sea $G = Gramática(V_N, V_T, P, S)$ la gramática _(en particular, libre de contexto; se profundiza después)_ tal que \
- $V_N = {S}$
- $V_T = {+, *, a,(,)}$
- $P$: \
  - $S to S + S$
  - $S to S * S$
  - $S to (S)$
  - $S to a$ \

=== Lenguaje generado por una gramática

Generalmente solemos utilizar la siguiente frase: *"Las gramáticas generan _(lenguajes)_, los autómatas reconocen _(lenguajes)_ y las expresiones regulares describen _(lenguajes)_"*. \

Es por ello que una gramática, como la que definimos anteriormente, genera un lenguaje. Al lenguaje generado por una gramática $G$ lo denotamos como $LenguajeDe(G)$. \

Dada una gramática $G = Gramática(V_N, V_T, P, S)$, se define el lenguaje generado por $G$ como \
#align(center)[
  $LenguajeDe(G) = { w in V^*_T : S =>^+_G w }$
]
donde $=>^+_G$ es derivación en uno o más pasos, que se obtiene de la clausura transitiva de la derivación directa $=>_G$. \

=== Derivación directa

Si $alpha beta gamma in (V_N union V_T)^*$ y $(beta to delta) in P$, entonces $alpha beta gamma$ se deriva directamente en $alpha delta gamma$. \
Esto lo denotamos como:
#align(center)[
  $alpha beta gamma =>_G alpha delta gamma$
]
Entonces, $=>_G$ es una relación sobre $(V_N union V_T)^*$, es decir, $=>_G$ $subset.eq (V_N union V_T)^* times (V_N union V_T)^*$. \
Podemos componer la relación $=>_G$ consigo misma, cero o más veces; a su vez, podemos componerla para obtener la clausura transitiva de la derivación directa, denotada como $=>^+_G$. \

==== Clausura de Kleene de la relación de derivación

La relación $id_((V_N union V_T)^*)$ se define como:
#align(center)[
  $alpha id_((V_N union V_T)^*) beta sii alpha = beta$. \
]
donde $alpha, beta in (V_N union V_T)^*$, y vale para cualquier $alpha, beta$. \

La clausura de Kleene de la relación de derivación $=>_G$ se define como:
#align(center)[
  $(=>_G)^0 = id_((V_N union V_T)^*)$ \
  Luego, \
  $(=>_G)^* = (=>_G)^+ union id_((V_N union V_T)^*) = union_(i>=0) (=>_G)^i$
]

=== Forma sentencial de una gramática

Una forma sentencial de una gramática es *cualquier cadena de símbolos* que se puede generar a partir del *símbolo inicial* $(S)$ de la gramática mediante una serie de derivaciones aplicando las reglas de producción de la gramática.

Es decir, sea $G = Gramática(V_N, V_T, P, S)$ una gramática:
- S es una *forma sentencial* de $G$. \
- Si $alpha beta gamma$ es una forma sentencial de $G$, y $(beta to delta) in P$, entonces $alpha delta gamma$ es también una forma sentencial de $G$. \

Las formas sentenciales están formadas por elementos de $(V_N union V_T)^*$. \

=== Derivaciones a izquierda y a derecha

Al realizar una derivación, podemos elegir entre derivar a izquierda o a derecha. \
- Derivación más a la izquierda $=>_L$.
- Derivación más a la derecha $=>_R$.

Para hacer $i$ pasos de una derivación escribimos, respectivamente, $=>^i_L$ y $=>^i_R$. \
Para la clausura transitiva de la derivación, escribimos $=>^+_L$ y $=>^+_R$. \
Y para la clausura de Kleene de la derivación, escribimos $=>^*_L$ y $=>^*_R$. \

=== Árboles de derivación
Asumamos una gramática $G = Gramática(V_N, V_T, P, S)$.

==== Definición

#text(important)[_*Un árbol de derivación es una representación gráfica de una derivación.*_]

Las etiquetas de las hojas están en $V_T union {lambda}$, y las etiquetas de los nodos internos están en $V_N$. \
Las etiquetas de sus hijos son los símbolos del cuerpo de una producción (lado derecho).

Un nodo tiene etiqueta $A$ y tiene $n$ descendientes etiquetados $X_1, X_2, ..., X_n$, exactamente cuando hay una derivación que usa una producción $A to X_1X_2...X_n$.

#text(comment)[Si no se entiende mucho, no te preocupes. Lo vas a entender mejor con un ejemplo.]

==== Ejemplo

Sea $G = Gramática({S}, {+, *, a,(,)}, P, S)$ la gramática libre de contexto con $P$:
- $S to S + S$
- $S to S * S$
- $S to (S)$
- $S to a$

#grid(
  columns: (1fr, 1fr),
  [
    #align(center)[
      #syntree(
        terminal: (weight: "bold"),
        child-spacing: 4em, // default 1em
        layer-spacing: 2em, // default 2.3em
        "[S ( [S a] )]"
      )
      #text(size: 9.9pt)[$S =>_L (S) =>_L (a)$]
    ]
  ],
  [
    #align(center)[
      #syntree(
        terminal: (weight: "bold"),
        child-spacing: 4em, // default 1em
        layer-spacing: 2em, // default 2.3em
          "[S ( [S [S a] + [S a]] )]"
      )
      #text(size: 9.9pt)[$S =>_L (S) =>_L (S + S) =>_L (a + S) =>_L (a + S) =>_L (a + a)$]
    ]
  ],
)

==== Camino de un árbol de derivación

Dada una gramática $Gramática(V_N, V_T, P, S)$ y dados $X in V_T$ o $X in V_N$. \
Llamamos *camino de $X$ en un árbol $Tree(A)$* con $A in V_N$, a la secuencia $A, X_1, ..., X_k, X$ que corresponde a las etiquetas de los vértices de la rama del árbol. \
Una hoja de un árbol $Tree(A)$, es un símbolo $t in V_T$ para el cual hay un camino que empieza en $A$ y termina en $t$. \

==== Altura de un árbol de derivación

La altura de $Tree(A)$, con $A in V_N$, es \
#align(center)[
  $max{|alpha x| : A alpha x "es un camino de" x and x "es una hoja de" Tree(A)}$.
]

Consideremos este árbol de derivación para $id + id * id$ en la gramática libre de contexto \ $G = Gramática(V_N, V_T, P, E)$ con $V_N = {E, T, F}$ y $V_T = {#text(weight: "bold")[id], #text(weight: "bold")[const], +, *, (, )}$, y las producciones: \
#align(center)[
  $E to E + T | T$ \
  $T to T * F | F$ \
  $F to #text(weight: "bold")[id] | #text(weight: "bold")[const] | (E)$ \
]

Este es un árbol de derivación para $id + id * id$ en la gramática $G$ (y es único): \

#align(center)[
  /*
  #syntree(
    terminal: (weight: "bold"),
    child-spacing: 3em, // default 1em
    layer-spacing: 1.4em, // default 2.3em
    "[$E$ [$E$ [$T$ [$F$ id]] ] $+$ [$T$ [$T$ [$F$ id]] [$*$] [$F$ id]]]"
  )
  */
  #tree(text(important)[$E$],
    tree($E$, 
      tree($T$, 
        tree($F$,
          text(weight: "bold")[id],
          child-spacing: 3em, // default 1em
          layer-spacing: 1.4em, // default 2.3em
        ),
        child-spacing: 3em, // default 1em
        layer-spacing: 1.4em, // default 2.3em
      ),
    child-spacing: 3em, // default 1em
    layer-spacing: 1.4em, // default 2.3em
    ),
    tree($+$,
      child-spacing: 3em, // default 1em
      layer-spacing: 1.4em, // default 2.3em
    ),
    tree($T$, 
      tree($T$, 
        tree($F$,
          text(weight: "bold")[id],
          child-spacing: 3em, // default 1em
          layer-spacing: 1.4em, // default 2.3em
        ),
        child-spacing: 3em, // default 1em
        layer-spacing: 1.4em, // default 2.3em
      ),
      tree($*$,
        child-spacing: 3em, // default 1em
        layer-spacing: 1.4em, // default 2.3em
      ),
      tree($F$,
        text(weight: "bold")[id],
        child-spacing: 3em, // default 1em
        layer-spacing: 1.4em, // default 2.3em
      ),
      child-spacing: 3em, // default 1em
      layer-spacing: 1.4em, // default 2.3em
    ),
    child-spacing: 3em, // default 1em
    layer-spacing: 1.4em, // default 2.3em
  )
]

La altura de $Tree(A) = max{|alpha x| : A alpha x "es un camino de" x and x "es una hoja de" Tree(A)}$. \
Luego, $A = #text(important)[E]$, $alpha = E, T, F$ y $x = #text(weight: "bold")[id]$, por lo tanto, la altura es $4$. \

==== Lema sobre la cota de la longitud de una palabra a partir de la altura de un árbol de derivación <lema_cota_longitud_palabra>

Sea $G = Gramática(V_N, V_T, P, S)$ una gramática libre de contexto con $P != emptyset$, sea $alpha in (V_N union V_T)^*$ y sea $Tree(S)$ un árbol de derivación para $alpha$ en $G$ cuya altura designaremos $h$. \

Si \
#align(center)[
  $a = max{k : (k = |beta|, (A to beta) in P, beta != lambda) or (k = 1, (A to lambda) in P)}$, \
  #text(comment)[en término de árbol, $a$ es la cantidad máxima de hijos que puede tener un nodo]
]

entonces $a^h >= |alpha|$.

==== Demostración del lema
Por inducción en $h$. \

- *Caso base:* $h = 0$. \
  El único árbol de derivación posible es el símbolo distinguido $S$, cuya altura es $0$. \
  Por lo tanto, $a^h = a^0 = 1 >= 1 = |S|$. \

#grid(
  columns: (2fr, 1fr),
  [
- *Paso inductivo:* $h > 0$. \
  Asumimos que vale para $h$, probemos que vale para $h+1$. \
  Sea $gamma$ la base del árbol $Tree(S)$ para la altura $h$. Asumamos la *H.I.*: $a^h >= |gamma|$. \

  Sea $alpha$ la base de $Tree(S)$ para la altura $h+1$. \
  
  Luego, $a|gamma| >= |alpha|$. \
  Pero, por *H.I.*, $a^h >= |gamma|$, entonces $a^(h+1) = a a^h >= a|gamma| >= |alpha|$. \
  ],
  [
    #align(right + bottom)[
      #image("./assets/Lema_Cota_Longitud_Palabra.svg", width: 100%)
    ]
  ]
)

#QED

== Jerarquía de Chomsky

La *jerarquía de Chomsky* es una clasificación de las gramáticas formales en cuatro clases, según su capacidad de generar lenguajes formales. \

#grid(
  columns: (2fr, 1fr),
  [
    === Gramáticas de tipo 0 (gramáticas sin restricciones)
    #align(center)[
      $alpha to beta$, con $alpha, beta in (V_N union V_T)^*$
    ]

    === Gramáticas de tipo 1 (gramáticas sensibles al contexto)
    #align(center)[
      $alpha to beta$, con $alpha, beta in (V_N union V_T)^*$ y $|alpha| <= |beta|$
    ]

    === Gramáticas de tipo 2 (gramáticas libres de contexto)
    #align(center)[
      $A to beta$, con $A in V_N$ y $beta in (V_N union V_T)^*$
    ]

    === Gramáticas de tipo 3 (gramáticas regulares)

    #align(center)[
      $A to a$, $A to a B$, $A to lambda$, con $A, B in V_N$ y $a in V_T$
    ]
  ],
  [
    #align(left + bottom)[
      #image("./assets/Chomsky.svg", width: 135%)
    ]
  ]
)
#text(comment)[La jerarquía de gramáticas da origen a la jerarquía de los lenguajes.]

== Algunos lemas sobre gramáticas

*Definición de una gramática no recursiva a izquierda:* \

Una gramática no es recursiva a izquierda si no tiene derivaciones $A =>^+_L A alpha$, para ningún $alpha in (V_N union V_T)^*$. \

=== Regular
Sea $G = Gramática(V_N, V_T, P, S)$ regular, no recursiva a izquierda. \
Si $A =>^i_L w B$, entonces $i = |w|$.

*Demostración:* \
Consideremos el siguiente árbol de derivación para $w = a_1a_2...a_n$:

#grid(
  columns: (3.2fr, 1fr),
  [
#align(left)[
  Si agarramos el árbol de derivación que vemos a la derecha, y realizamos un corte en la altura $i$, obtenemos un subarbol con $i$ hojas, $a_1...a_i$, donde el único nodo que no es hoja tiene como etiqueta un símbolo $in V_N$. \
  #grid(
    columns: (1fr, 1fr),
    [
      #align(center)[
        #align(center)[
          #syntree(
            terminal: (weight: "bold"),
            child-spacing: 6em, // default 1em
            layer-spacing: 2.2em, // default 2.3em
            "[S a A]"
          )
        ]
      ]
    ],
    [
      #align(center + horizon)[
        *$<-$* Ejemplo con $i = 1$. \
      ]
    ],
  )
  A su vez, debido a la forma de las producciones, cada derivación agrega uno, y solo uno, símbolo terminal. \
  Por lo que en $i$ derivaciones, se agregan los $i$ símbolos terminales y el no terminal de la derecha. \

  #QED
]
  ],
  [
    #align(right)[
      #syntree(
        terminal: (weight: "bold"),
        child-spacing: 1.6em, // default 1em
        layer-spacing: 2em, // default 2.3em
        "[S a [A a [B a [A a]]]]"
      )
      #align(center)[$P$:]
      $S to a A | lambda$ \
      $A to a | a B$ \
      $B to a A$ \
    ]
  ],
)

=== Libre de contexto <lema_libre_de_contexto>

Sea $G = Gramática(V_N, V_T, P, S)$ una gramática libre de contexto, no recursiva a izquierda. \
Existe una constante $c$ tal que si $A =>^i_L w B alpha$ entonces $i <= c^(|w|+2)$, \ donde $A, B in V_N$, $w in V^*_T$ y $alpha in (V_N union V_T)^*$. \

*Demostración:* \

Llamemos $k = |V_N|$ y $n = |w|$. Consideremos el árbol de derivación más a la izquierda para $A =>^i_L w B alpha$. \

#align(center)[
  #image("./assets/Lema_Cota_Libres_de_Contexto.svg", width: 40%)
]

Sea $n_0$ el nodo con etiqueta $B$ en la derivación $A =>^i_L w B alpha$. \
Por ser la derivación más a la izquierda, todos los caminos a la derecha del camino desde la raíz a $n_0$ son más cortos, o del mismo largo. \
Supongamos que hay un camino de longitud mayor o igual que $(n+2)k$ nodos de la raíz a la hoja, es decir: \
Sea $A = X_0 frown X_1 frown ... frown X_((n+2)k) = B$, el camino desde la raíz a $n_0$. \
Separamoslos en $n+2$ segmentos de $k+1$ nodos,
#align(center)[
  $A = X_0 frown ... frown X_k, #h(0.5cm) X_k frown ... frown X_(2k), #h(0.5cm) ..., #h(0.5cm) X_((n+1)k) frown ... frown X_((n+2)k) = B$.
]
#text(comment)[Notar que se repite el final de uno con el principio del siguiente.] \

Consideremos los $n+2$ subárboles de derivación, con $beta in (V_N union V_T)^*$ apropiados. \
#align(center)[
  $A = X_0 =>^k_L beta_1 X_k, #h(0.5cm) X_k =>^k_L beta_2 X_(2k), #h(0.5cm) ..., #h(0.5cm) X_((n+1)k) =>^k_L beta_(n+2) X_((n+2)k)$.
]

Es imposible que cada uno produzca uno o más símbolos de $w B$, porque $|w B| = n + 1$. \
Al menos uno no produce ningún símbolo de $w B$, \

#align(center)[
  $X_(j k) =>_L beta_(j k + 1) X_(j k + 1) =>_L ... =>_L beta_((j+1)k) X_((j+1)k)$
]

y deriva solamente $lambda$. \

Cada uno de $X_(j k), beta_(j k + 1) X_(j k + 1), ..., beta_((j+1)k) X_((j+1)k)$ empiezan con un símbolo de $V_N$, son en total $k + 1$. \
Necesariamente hay dos que empiezan con el mismo símbolo. Pero esto contradice que la gramática no es recursiva a izquierda. \
Entonces, nuestra suposición de que el camino de la raíz a $n_0$ tiene $n+2$ segmentos de $k+1$ nodos es imposible. \

Concluimos que su longitud es menor que $(n+2)k$. \
Sea $cal(l)$ el máximo número de símbolos en la parte derecha de una producción de la gramática, la cantidad de nodos del árbol de derivación es a lo sumo $cal(l)^(k(n+2))$. \

Por lo tanto, $A =>^i_L w B alpha$ con $i <= cal(l)^(k(n+2))$. \

Para finalizar la demostración basta tomar $c = cal(l)^k$. \

#QED

= Autómatas finitos determinísticos, no determinísticos y gramáticas regulares

== Autómata finito determinístico (AFD)

Un *autómata finito determinístico (AFD)* es una máquina abstracta definida formalmente como una $5$-upla $AF(Q, Sigma, delta, q_0, F)$ donde \
- $Q$ es un conjunto finito de estados. \
- $Sigma$ es el alfabeto de entrada. \
- $delta: Q times Sigma to Q$ es la función de transición. \
- $q_0 in Q$ es el estado inicial. \
- $F subset.eq Q$ es el conjunto de estados finales. \

=== Función de transición generalizada $deltaSombrero$

Definimos $deltaSombrero : Q times SigmaEstrella to Q$,
- $deltaSombrero(q, lambda) = q$, para todo $q in Q$.
- $deltaSombrero(q, x a) = delta(deltaSombrero(q,x),a)$, con $q in Q$, $x in SigmaEstrella$ y $a in Sigma$.

Notar que $deltaSombrero(q,a) = delta(deltaSombrero(q, lambda), a) = delta(q,a)$. \
Muchas veces usamos el símbolo $delta$ para ambas funciones. #h(2cm) *$<-$ Abuso de notación*

=== Lenguaje aceptado por un AFD

El lenguaje aceptado por un AFD $M = AF(Q, Sigma, delta, q_0, F)$, al que denotamos como $LenguajeDe(M)$, es el conjunto de cadenas de $SigmaEstrella$ aceptadas por $M$. \

#align(center)[
  $LenguajeDe(M) = { x in SigmaEstrella : deltaSombrero(q_0, x) in F }$.
]

Entendemos a los autómatas finitos como funciones tales que para cada cadena dan un valor booleano: la aceptación o la no aceptación,

#align(center)[
  $M : SigmaEstrella to {0, 1}$.
]

=== Configuración instantánea de un AFD

Sea AFD $M = AF(Q, Sigma, delta, q_0, F)$. Una configuración instantánea de $M$ es un par $(q, x)$ en $Q times SigmaEstrella$, donde $q in Q$ es el estado actual y $x in SigmaEstrella$ es la cadena de entrada restante por procesar. \

=== Transición entre configuraciones instantáneas $sequent$

Llamamos transición a la siguiente relación sobre $Q times SigmaEstrella$: \
#align(center)[
  $(q, x) sequent (p, y)$ si $(delta(q, a) = p and x = a y)$.
]

De lo anterior tenemos que $(q, x y) sequent^* (p, y)$ si y solo si $deltaSombrero(q, x) = p$, es decir, si se puede pasar del estado $q$ al estado $p$ leyendo la cadena $x$. \

== Autómata finito no determinístico (AFND)

Un *autómata finito no determinístico (AFND)* es una máquina abstracta definida formalmente como una $5$-upla $AF(Q, Sigma, delta, q_0, F)$ donde \
- $Q$ es un conjunto finito de estados. \
- $Sigma$ es el alfabeto de entrada. \
- $delta: Q times Sigma to PartesDe(Q)$ es la función de transición. \
- $q_0 in Q$ es el estado inicial. \
- $F subset.eq Q$ es el conjunto de estados finales. \

=== Función de transición generalizada $deltaSombrero$

Definimos $deltaSombrero : Q times SigmaEstrella to PartesDe(Q)$,
- $deltaSombrero(q, lambda) = {q}$, para todo $q in Q$.
- $deltaSombrero(q, x a) = {p in Q : exists r in deltaSombrero(q,x) and p in delta(r,a)}$ con $q in Q$, $x in SigmaEstrella$ y $a in Sigma$.

Notar que
#align(center)[
  $deltaSombrero(q, lambda a) &= {p in Q : exists r in deltaSombrero(q,lambda) and p in delta(r,a)} \
  &= {p in Q : exists r in {q} and p in delta(r,a)} \
  &= {p in Q : p in delta(q,a)} \
  &= delta(q,a).$
]

Nuevamente, *abuso de notación*, muchas veces usamos el símbolo $delta$ para ambas funciones.

=== Lenguaje aceptado por un AFND

El lenguaje aceptado por un AFND $M = AF(Q, Sigma, delta, q_0, F)$, al que denotamos como $LenguajeDe(M)$, es el conjunto de cadenas de $SigmaEstrella$ aceptadas por $M$. \

#align(center)[
  $LenguajeDe(M) = { x in SigmaEstrella : deltaSombrero(q_0, x) sect F != emptyset }$.
]

=== Configuración instantánea de un AFND

Sea AFND $M = AF(Q, Sigma, delta, q_0, F)$. Una configuración instantánea de $M$ es un par $(q, x)$ en $Q times SigmaEstrella$, donde $q in Q$ es el estado actual y $x in SigmaEstrella$ es la cadena de entrada restante por procesar. \

=== Transición entre configuraciones instantáneas $sequent$

Llamamos transición a la siguiente relación sobre $Q times SigmaEstrella$: \
#align(center)[
  $(q, x) sequent (p, y)$ si $(p in delta(q, a) and x = a y)$.
]

=== Función de transición de conjuntos de estados en un AFND

Podemos extender la función de transición aún más, haciendo que mapee conjuntos de estados y cadenas en conjuntos de estados.

- $delta : PartesDe(Q) times Sigma to PartesDe(Q)$ dada por $delta(P, a) = union.big_(q in P) delta(q, a)$.
- $deltaSombrero : PartesDe(Q) times SigmaEstrella to PartesDe(Q)$ dada por $deltaSombrero(P, x) = union.big_(q in P) deltaSombrero(q, x)$.

La primer extensión nos permite escribir $deltaSombrero(q, x a)$ como

#align(center)[
  $deltaSombrero(q, x a) = delta(deltaSombrero(q, x), a)$.
]

Si estás confuso al leer esto no te preocupes. \
Es muy común tratar ambas extensiones como $delta$ indistintamente, diferenciándolas por el contexto. Si es $Q times Sigma$, $PartesDe(Q) times Sigma$, $PartesDe(Q) times SigmaEstrella$ o $Q times SigmaEstrella$. \
Generalmente en las demostraciones vamos a usar $delta$ sin importar si es:
- $delta : Q times Sigma to Q$,
- $delta : Q times SigmaEstrella to Q$,
- $delta : Q times Sigma to PartesDe(Q)$.
- $delta : Q times SigmaEstrella to PartesDe(Q)$.
- $delta : PartesDe(Q) times Sigma to PartesDe(Q)$,
- $delta : PartesDe(Q) times SigmaEstrella to PartesDe(Q)$.

Y lo diferenciamos por el contexto. \

En la siguiente sección (AFND-$lambda$) vamos a ver más extensiones sobre $delta$. De igual forma, vamos a seguir abusando de la notación. \

== Autómata finito no determinístico con transiciones $lambda$ (AFND-$lambda$)

Un *autómata finito no determinístico con transiciones lambda (AFND-$lambda$)* es un AFND que puede hacer transiciones sin consumir ningún símbolo de entrada, es definido formalmente como una $5$-upla $AF(Q, Sigma, delta, q_0, F)$ donde \

- $Q$ es un conjunto finito de estados. \
- $Sigma$ es el alfabeto de entrada, tal que $lambda in.not Sigma$. \
- $delta: Q times (Sigma union {lambda}) to PartesDe(Q)$ es la función de transición. \
- $q_0 in Q$ es el estado inicial. \
- $F subset.eq Q$ es el conjunto de estados finales. \

=== Clausura $lambda$

La clausura $lambda$ de un estado $q$, $ClausuraLambda(q)$, es el conjunto de estados alcanzables desde $q$, siguiendo sólo transiciones $lambda$. \
Usamos la noción de clausura transitivo-reflexiva para definir $ClausuraLambda$. \

=== Clausura $lambda$ de un estado

Dado un AFND-$lambda$ $AF(Q, Sigma, delta, q_0, F)$, y sea $R subset.eq Q times Q$ tal que $(q, p) in R$ si y solo si $p in delta(q, lambda)$. \
Definimos $ClausuraLambda : Q to PartesDe(Q)$ como \

#align(center)[
  $ClausuraLambda(q) = {p in Q : (q, p) in R}$.
]

Notar que $q in ClausuraLambda(q)$.

=== Clausura $lambda$ de un conjunto de estados $P$

#align(center)[
  $ClausuraLambda(P) = union.big_(q in P) ClausuraLambda(q)$.
]

Notar que una es $ClausuraLambda : PartesDe(Q) to PartesDe(Q)$ y la otra es $ClausuraLambda : Q to PartesDe(Q)$.

Extendemos la definición de $delta$ a un conjunto de estados para AFND-$lambda$, \
#align(center)[
  $delta : PartesDe(Q) times (Sigma union {lambda}) to PartesDe(Q)$ dada por $delta(P, a) = union.big_(q in P) delta(q, a)$.
]

=== Función de transición $deltaSombrero$ (sin $lambda$)

Dado un AFND $M = AF(Q, Sigma, delta, q_0, F)$ con $delta : Q times (Sigma union {lambda}) to PartesDe(Q)$, definimos la función de transición sin $lambda$, $deltaSombrero : Q times Sigma to PartesDe(Q)$, como \

#align(center)[
  $deltaSombrero(q, a) = ClausuraLambda(delta(ClausuraLambda(q), a))$. \
]

Notar que $deltaSombrero(q, a)$ puede ser distinto de $delta(q, a)$. \

Extendemos $deltaSombrero$ a conjuntos de estados, \
#align(center)[
  $deltaSombrero : PartesDe(Q) times Sigma to PartesDe(Q)$ dada por $deltaSombrero(P, a) = union.big_(q in P) deltaSombrero(q, a)$.
]

Extendemos $deltaSombrero$ a palabras, $deltaSombrero : Q times SigmaEstrella to PartesDe(Q):$
#align(center)[
  $deltaSombrero(q, lambda) = ClausuraLambda(q)$, \
  $deltaSombrero(q, x a) = deltaSombrero(deltaSombrero(q, x), a)$. \
]

=== Lenguaje aceptado por un AFND-$lambda$

Sea AFND-$lambda$ $M = AF(Q, Sigma, delta, q_0, F)$. El lenguaje aceptado por M, $LenguajeDe(M)$, es el conjunto de cadenas aceptadas por $M$, \

#align(center)[
  $LenguajeDe(M) = { x in SigmaEstrella : deltaSombrero(q_0, x) sect F != emptyset }$.
]

=== Equivalencia entre AFND y AFND-$lambda$

Dado un AFND-$lambda$ $M = AF(Q, Sigma, delta, q_0, F)$, podemos construir un AFND equivalente $M' = AF(Q', Sigma, delta', q_0', F')$ tal que $LenguajeDe(M) = LenguajeDe(M')$. \

*Demostración:* \

Sea AFND-$lambda$ $M = AF(Q, Sigma, delta, q_0, F)$ donde $delta : Q times (Sigma union {delta}) to PartesDe(Q)$. \

Definimos AFND $M' = AF(Q, Sigma, delta', q_0, F')$ donde $delta' : Q times Sigma to PartesDe(Q)$, \

Vamos a usar las versiones extendidas de $delta$ y $deltaSombrero$, la de conjunto de estados y palabras de $SigmaEstrella$. \

#align(center)[
  $delta'(q, a) = deltaSombrero(q, a)$, para cada $a in Sigma$ y $q in Q$, \

  $F' = cases(F &"si " ClausuraLambda(q_0) sect F = emptyset, F union {q_0} &"si no.")$. \
]

Observar que $F' supset.eq F$. \

Debemos ver que $forall x in SigmaEstrella, x in LenguajeDe(M) sii x in LenguajeDe(M')$. \

*Demostración:* \

*Caso $|x| = 0$:* i.e., $x = lambda$. \

$implica2)$ Supongamos que $x = lambda in LenguajeDe(M)$. Eso quiere decir que: o $q_0 in F$, o que existe alguna transición $lambda$ desde $q_0$ a algún estado final. \
Más formalmente, \

#align(center)[
  $lambda in LenguajeDe(M) &implica2 deltaSombrero(q_0, lambda) sect F != emptyset \
  &implica2 ClausuraLambda(q_0) sect F != emptyset \
  &implica2 F' = F union {q_0} \
  &implica2 q_0 in F' \
  &implica2 delta'(q_0, lambda) = {q_0} sect F' != emptyset \
  &implica2 lambda in LenguajeDe(M').$
]

$implicaVuelta2)$ Supongamos que $x = lambda in LenguajeDe(M')$. Eso quiere decir que $q_0 in F'$, por lo que o $ClausuraLambda(q_0) sect F != emptyset$ o $q_0 in F$. \
Más formalmente, \

#align(center)[
  $lambda in LenguajeDe(M') &implica2 delta'(q_0, lambda) = {q_0} sect F' != emptyset \
  &implica2 q_0 in F' \
  &implica2 F' = F union {q_0} or q_0 in F \
  &implica2 ClausuraLambda(q_0) sect F != emptyset or q_0 in F \
  &implica2 deltaSombrero(q_0, lambda) sect F != emptyset or q_0 in F \
  &implica2 lambda in LenguajeDe(M).$
]

*Caso $|x| >= 1$:*

Queremos ver que $x in LenguajeDe(M) sii x in LenguajeDe(M')$. Demostremos antes que $delta'(q, x) = deltaSombrero(q, x)$, $forall q in Q$ y $forall x in SigmaMás$. \

Hagamos inducción en el largo de la cadena $x$. \

*Caso base:* $|x| = 1$, i.e., $x = a$ donde $a in Sigma$. \
Nos queda 
#align(center)[
  $delta'(q, a) &=_#text(weight: "bold")[definición de $delta'$ en $M'$] deltaSombrero(q, a)$.
]

*Caso inductivo:* $|x| = n+1$, i.e., $x = y a$ donde $y in SigmaEstrella$ y $a in Sigma$. \

#align(center)[
  $delta'(q, y a) &= delta'(delta'(q, y), a) \
  &=_#text(weight: "bold")[HI] delta'(deltaSombrero(q, y), a) \
  &= union.big_(p in deltaSombrero(q, y)) delta'(p, a) \
  &= union.big_(p in deltaSombrero(q, y)) deltaSombrero(p, a) \
  &= deltaSombrero(deltaSombrero(q, y), a) \
  &= deltaSombrero(q, y a).$
]

#QED

== Equivalencia entre AFD y AFND

Es trivial ver que, *para todo AFD podemos construir un AFND equivalente*; solamente modificamos la función de transición, de manera tal que para cada transición $delta(q,a) = p$ en el AFD, tenemos $delta(q,a) = {p}$ en el AFND. \

Lo que no es tan obvio es que lo recíproco también es cierto: *para cada AFND existe un AFD equivalente*.

Dado un AFND $M = AF(Q, Sigma, delta, q_0, F)$, podemos construir un AFD $M' = AF(Q', Sigma, delta', q_0', F')$ tal que $LenguajeDe(M) = LenguajeDe(M')$. #h(9.4cm) *$<-$ Teorema*

*Demostración:* \
Sea $M = AF(Q, Sigma, delta, q_0, F)$ un AFND, definamos el AFD $M' = AF(Q', Sigma, delta', q'_0, F')$ veamos cómo se construye.

- Los estados del autómata $M'$, vamos a denotarlos como $[q_1, ..., q_i]$, con $q_1, ..., q_i in Q$. \
  Notar que son subconjuntos de $Q$, es decir $Q' = PartesDe(Q)$.

- El $Sigma$ del autómata $M'$ es el mismo que el de $M$, se ve en la definición de la $5$-upla.

- Para las transiciones de $M'$, vamos a definir la función de transición $delta' : Q times Sigma to Q$ como $delta'([q_1, ..., q_i], a) = [p_1, ..., p_j]$ si y solo si $delta({q_1, ..., q_i}, a) = {p_1, ..., p_j}$. \

- El estado inicial de $M'$ lo vamos a definir como $q'_0 = [q_0]$

- Para los estados finales de $M'$, vamos a tomar aquellos nodos $[q_1, ..., q_i]$ tales que alguno de los elementos en ${q_1, ..., q_i} $, es estado final en $M$. \
  Luego, $F' = {[q_1, ..., q_i] in Q' : {q_1, ..., q_i} sect F != emptyset}$.

Bueno, con esto ya tenemos definido el AFD $M'$. Falta probar que $LenguajeDe(M) = LenguajeDe(M')$. \

Para ello vamos a necesitar *un lema*, probemos que \
#align(center)[
  $delta'(q'_0, x) = delta'([q_0], x) = [r_1, ..., r_k] sii delta({q_0}, x) = delta(q_0, x) = {r_1, ..., r_k}$, \
  simplificando, \
  $delta'(q'_0, x) = [r_1, ..., r_k] sii delta(q_0, x) = {r_1, ..., r_k}$.
]

Asumiendo que $delta'([q_1, ..., q_i], a) = [p_1, ..., p_j] sii_#text(weight: "bold")[definición de $delta'$] delta({q_1, ..., q_j}, a) = {p_1, ..., p_i}$ es verdadero. \

Vamos a hacerlo por inducción en la longitud de la cadena $x$. \

*Caso base:* $|x| = 0$, i.e., $x = lambda$. \

#align(center)[
  $delta'(q'_0, lambda) = delta'([q_0], lambda) = [q_0] sii {q_0} = delta(q_0, lambda) = delta({q_0}, lambda)$. \
  El caso base es verdadero.
]

*Paso inductivo:* asumimos que vale para $x$ tal que $|x| = n$, probemos que vale para $|x| = n+1$. \

Queremos probar que
#align(center)[
  $delta'(q'_0, x a) = [s_1, ..., s_l] sii delta(q_0, x a) = {s_1, ..., s_l}$. \

  Asumiendo que
  $delta'(q'_0, x) = [r_1, ..., r_k] sii_#text(weight: "bold")[HI] deltaSombrero(q_0, x) = {r_1, ..., r_k}$
  es verdadero.
]

Entonces,

#align(center)[
  $delta'(q'_0, x a) =_#text(weight: "bold")[por definición de $delta'$ en $M'$] delta'(delta'(q'_0, x), a) = [s_1, ..., s_l]$ \
  #sii \
  $exists [r_1, ..., r_k] : delta'(q'_0, x) = [r_1, ..., r_k] and delta'([r_1, ..., r_k], a) = [s_1, ..., s_l]$ \
  $sii_#text(weight: "bold")[por HI y def. de $delta'$ en $M'$]$ \
  $exists [r_1, ..., r_k] : delta(q_0, x) = {r_1, ..., r_k} and delta({r_1, ..., r_k}, a) = {s_1, ..., s_l}$ \
  #sii \
  $delta(q_0, x a) = delta(delta(q_0, x), a) = {s_1, ..., s_l}$.
]

Con esto queda demostrado el lema. Falta probar que $LenguajeDe(M) = LenguajeDe(M')$. \

#align(center)[
  $x in LenguajeDe(M)$ \
  #sii \
  $delta(q_0, x) = {r_1, ..., r_k} and {r_1, ..., r_k} sect F != emptyset$ \
  $sii_#text(weight: "bold")[por lema anterior]$ \
  $delta'(q'_0, x) = [r_1, ..., r_k] and [r_1, ..., r_k] sect F' != emptyset$ \
  #sii \
  $x in LenguajeDe(M')$ \
]

Concluimos, $LenguajeDe(M) = LenguajeDe(M')$. #QED \

== Gramáticas regulares

=== Todo lenguaje generado por una gramática reg., tiene un AFND que lo reconoce

Dada una gramática regular $G = Gramática(V_N, V_T, P, S)$, existe un AFND $M = AF(Q, Sigma, delta, q_0, F)$ tal que $LenguajeDe(G) = LenguajeDe(M)$. #h(10.2cm) *$<-$ Teorema*

*Demostración:* \
Definimos $M$ tal que:
- $Q = {q_A : A in V_N} union {q_f}$. #h(0.6cm) #text(comment)[$<-$ Los estados son los no terminales + un estado $q_f$.]
- $Sigma = V_T$. #h(0.6cm) #text(comment)[$<-$ El alfabeto de entrada es el conjunto de símbolos terminales.]
- $delta(q_A, a)$ con $A,B in V_N$ y $a in V_T$.
  - $q_B in delta(q_A, a) sii2 (A to a B) in P$.
  - $q_f in delta(q_A, a) sii2 (A to a) in P$.
- $q_0 = q_S$. #h(0.6cm) #text(comment)[$<-$ El estado inicial es el símbolo inicial.]
- $F = {q_A : A in V_N,  (A to lambda) in P} union {q_f}$. #h(0.6cm) #text(comment)[$<-$ Todo estado con producción que deriva en $lambda$ y nuestro estado $q_f$.]

Para probar que $LenguajeDe(G) = LenguajeDe(M)$, vamos a probar un lema: \
#align(center)[
  $forall w in V^*_T$, si $A =>^* w B$ entonces $q_B in deltaSombrero(q_A, w)$. 
]

*Demostración del lema:* \
Hacemos inducción en la longitud de $w$. \

*Caso base:* $|w| = 0$, i.e., $w = lambda$. \

#align(center)[
  Si $A =>^* lambda B$, entonces $A = B$ y $q_B in delta(q_A, lambda)$, por definición de $delta$. \
  Luego, el caso base es verdadero.
]

*Paso inductivo:* asumimos que vale para $w$ tal que $|w| = n$, probemos que vale para $|w| = n+1$. \

Queremos probar que si
#align(center)[
  $A =>^* w a B$ entonces $q_B in deltaSombrero(q_A, w a).$ \

  Asumiendo que si $A =>^* w B$ entonces $q_B in deltaSombrero(q_A, w)$ es verdadero.
]


Entonces,
#align(center)[
  $A =>^* w a B &sii exists C in V_N : A =>^* w C and (C to a B) in P \
  &implica_#text(weight: "bold")[HI] exists q_C in Q : q_C in deltaSombrero(q_A, w) and q_B in delta(q_C, a) \
  &sii q_B in delta(deltaSombrero(q_C,w),a) =_#text(weight: "bold")[def. de $delta$] deltaSombrero(q_A, w a).$
]

#QED

Falta probar que $LenguajeDe(G) = LenguajeDe(M)$. Sea $w in V^*_T, a in V_T$. \

Caso cadena vacía: \
#align(center)[
  $lambda in LenguajeDe(G) &sii S =>^* lambda \
  &sii (S to lambda) in P \
  &sii q_S in F \
  &sii lambda in LenguajeDe(M).$
]

Caso cadena no vacía: \
#align(center)[
  $w a in LenguajeDe(G) &sii S =>^* w a \
  &sii (exists A in V_N : S =>^* w A and (A to a) in P) or (exists B in V_N : S =>^* w a B and (B to lambda) in P) \
  &sii (exists A in V_N : S =>^* w A and q_f in delta(q_A, a)) or (exists B in V_N : S =>^* w a B and q_B in F) \
  &implica_#text(weight: "bold")[Lem] (exists q_A in Q : q_A in deltaSombrero(q_S, w) and q_f in delta(q_A, a)) or (exists q_B in Q : q_B in deltaSombrero(q_S, w a) and q_B in F) \
  &sii (delta(deltaSombrero(q_S, w), a) =_#text(weight: "bold")[def. de $delta$] deltaSombrero(q_S, w a)  sect F != emptyset) or (deltaSombrero(q_S, w a) sect F != emptyset) \
  &sii deltaSombrero(q_S, w a)  sect F != emptyset \
  &sii w a in LenguajeDe(M).$
]

Con esto queda demostrado que $LenguajeDe(G) = LenguajeDe(M)$. \

#QED

=== Todo AFD, tiene una gramática regular que lo genera

Dado un AFD $M = AF(Q, Sigma, delta, q_0, F)$, existe una gramática regular $G = Gramática(V_N, V_T, P, S)$ tal que $LenguajeDe(M) = LenguajeDe(G)$. #h(11cm) *$<-$ Teorema*

*Demostración:* \
Definimos la gramática $G = Gramática(V_N, V_T, P, S)$, donde:
- $V_N = {A_q : q in Q}$.
- $V_T = Sigma$.
- $P$, con $A_p, A_q in V_N$ y $a in V_T$:
  - $(A_q to a A_p) in P sii delta(q,a) = p /*and p in.not F*/$.
  - $(A_q to a) in P sii delta(q,a) = p and p in F$.
  - $(S to lambda) in P sii q_0 in F$.
- $S = A_(q_0)$.

Para probar que $LenguajeDe(M) = LenguajeDe(G)$, vamos a probar un lema: \
#align(center)[
  $delta(p, w) = q sii A_p =>^* w A_q$.
]

*Demostración del lema:* \
Hacemos inducción en la longitud de $w$. \

*Caso base:* $|w| = 0$, i.e., $w = lambda$. \

#align(center)[
  Para $w = lambda$, es cierto que $delta(p, lambda) =_#text(weight: "bold")[def. de $delta$] p sii A_p =>^*_#text(weight: "bold")[en particular, cero pasos] lambda A_p$. \
]

*Paso inductivo:* asumimos que vale para $w$ tal que $|w| = n$, probemos que vale para $|w| = n+1$. \

Queremos probar que
#align(center)[
  $delta(p, w a) = q sii A_p =>^* w a A_q$.
]

Entonces,

#align(center)[
  $delta(p, w a) = q &sii exists r in Q : delta(p, w) = r and delta(r, a) = q \
  &sii exists r in Q : delta(p, w) = r and (A_r to a A_q) in P \
  &sii_#text(weight: "bold")[HI] exists A_r in V_N : A_p =>^* w A_r and (A_r to a A_q) in P \
  &sii A_p =>^* w a A_q.$
]

#QED

Falta probar que $LenguajeDe(M) = LenguajeDe(G)$. \

Caso cadena vacía: \
#align(center)[
  $lambda in LenguajeDe(M) &sii q_0 in F \
  &sii (S to lambda) in P \
  &sii S =>^* lambda \
  &sii lambda in LenguajeDe(G).$
]

Caso cadena no vacía: \
#align(center)[
  $w a in LenguajeDe(M) &sii delta(q_0, w a) in F \
  &sii exists p in Q : delta(q_0, w) = p and delta(p, a) in F \
  &sii exists p in Q : delta(q_0, w) = p and (A_p to a) in P \
  &sii_#text(weight: "bold")[Lem] exists A_p in V_N : A_(q_0) =>^* w A_p and (A_p to a) in P \
  &sii A_(q_0) =>^* w a \
  &sii w a in LenguajeDe(G).$
]

Con esto queda demostrado que $LenguajeDe(M) = LenguajeDe(G)$. \

#QED

= Lenguajes regulares: lema de pumping y propiedades de clausura

== Lema de pumping para lenguajes regulares
=== Introducción
Si las longitudes de las cadenas de un lenguaje $L$ están acotadas superiormente, por ejemplo: $x in L implica2 |x| <= 20$, entonces $L$ tiene que ser *finito*. \

¿Qué condición tiene que cumplir el grafo de un autómata finito para que el lenguaje que éste acepta sea infinito? \

Debe existir un camino desde el estado inicial hasta algún estado final que pase por algún ciclo. \

#align(center)[
  #cetz.canvas({
    import draw: state, transition

    state((-4, 0), "U", label: "", initial: "")
    state((2, 0), "V", label: "")
    state((8, 0), "W", label: "", final: true)

    transition("U", "V", label: text(size: 14pt)[$u$], curve: -.5, stroke: (dash: "dashed"))
    transition("V", "V", label: text(size: 14pt)[$v$], curve: 1, stroke: (dash: "dashed"))
    transition("V", "W", label: text(size: 14pt)[$w$], curve: .5, stroke: (dash: "dashed"))
  })
]

El *lema de pumping* es una propiedad que cumplen todos los lenguajes regulares. \
Eso quiere decir que si un lenguaje es regular, entonces cumple con el lema de pumping. \
O dicho de otro modo, *si un lenguaje no cumple con el lema de pumping, entonces no es regular*. \

=== Lema

Sea $L$ un lenguaje regular. Existe una longitud $n$ tal que para toda $z in L$ con $|z| >= n$ existe $u, v, w in SigmaEstrella$ tales que \
#align(center)[
  $z = u v w$ \
  $|u v| <= n$ \
  $|v| >= 1$ \
  $forall i >= 0, u v^i w in L$.
]

=== Demostración

Para la demostración del lema de pumping, vamos a demostrar primero un lema sobre las transiciones entre configuraciones instantáneas. \

Sea el AFD $M = AF(Q, Sigma, delta, q_0, F)$. \
Para todo $q in Q$ y $x, y in SigmaEstrella$, si $(q, x y) sequent^* (q, y)$ entonces $forall i >= 0, (q, x^i y) sequent^* (q, y).$

Es decir, si partiendo de un estado $q$, terminamos en el mismo estado $q$ tras leer la cadena $x$, entonces hay un ciclo en el AFD que nos permite leer la cadena $x$ cero o más veces. \

*Demostración del lema sobre transiciones entre configuraciones instantáneas:* \

Hacemos inducción en $i$, fijamos $q in Q$ y $x in SigmaEstrella$; asumimos que $forall y in SigmaEstrella$, $(q, x y) sequent^* (q, y)$. \

*Caso base:* $i = 0$. \

#align(center)[
  Nos queda \
  $(q, x y) sequent^* (q, y)$ entonces $(q, y) sequent^* (q, y)$, \
  lo que al asumir el antecedente es trivialmente cierto. \
]

*Paso inductivo:* asumimos que vale para $i$, probemos que vale para $i+1$. \

#align(center)[
  $(q, x^(i+1) y) =_#text(weight: "bold")[def] (q, x x^i y)$ \

  Luego, por asunción del antecedente, \

  $(q, x x^i y) sequent^* (q, x^i y)$ \

  $implica_#text(weight: "bold")[HI] (q, x^i y) sequent^* (q, y).$ \
]

Concluimos que $(q, x^(i+1) y) sequent^* (q, y)$. \

#QED

Con este lema, podemos demostrar el lema de pumping. \

*Demostración del lema de pumping:* \

Sea AFD $M = AF(Q, Sigma, delta, q_0, F)$ tal que $LenguajeDe(M) = L$. Sea $n$ su cantidad de estados. Sea $z$ una cadena de longitud $m >= n$, $z = a_1 ... a_m$. \

Para aceptar $z$, usamos $m$ transiciones _(aristas del grafo)_, por lo tanto $m + 1$ estados _(nodos del grafo)_. \

Como $m + 1 > n$, para aceptar $z$, *el autómata pasa dos o más veces por un mismo estado al reconocer la cadena*. \

Sea $q_z_0, q_z_1,...,q_z_m in Q$, con $q_z_0 = q_0$ y $q_z_m$ un estado final, la sucesión de estados desde $q_0$ hasta aceptar $z$.

Gráficamente, \
#align(center)[
  #cetz.canvas({
    import draw: state, transition

    state((-4, 0), "q0", label: $q_0$, initial: "")
    state((2, 0), "qzmid", label: $q_z_i = q_z_j$, radius: (1, .8))
    state((8, 0), "qzm", label: $q_z_m$, final: true)

    transition("q0", "qzmid", label: $a_1 ... a_i$, curve: -.5, stroke: (dash: "dashed"))
    transition("qzmid", "qzmid", label: $a_(i+1) ... a_j$, curve: 1, stroke: (dash: "dashed"))
    transition("qzmid", "qzm", label: $a_(j+1) ... a_m$, curve: .5, stroke: (dash: "dashed"))
  })
]

Formalmente, \
Existen $i, j$ tales que $q_z_i = q_z_j$ con $0 <= i < j <= n$. \

Luego, $z = u v w$, con \
#align(center)[
  $u = cases(a_1 ... a_i &"si" i > 0, lambda &"si" i = 0)$ \

  $v = a_(i+1) ... a_j$ \

  $w = cases(a_(j+1) ... a_m &"si" j < m, lambda &"si" j = m)$
]

Es fácil ver que $|u v| <= n$ y $|v| >= 1$. \
Pues, \
#align(center)[
  $|u v| = i + (j - i) = j <= n$ es verdadero por $0 <= i < j <= n$. \

  $|v| = j - i >= 1 sii i + 1 <= j sii i < j$ es verdadero por $0 <= i < j <= n$. \

  A su vez, \
  $(q_0, u v w) sequent^* (q_z_i, v w) sequent^* (q_z_j, w) sequent^* (q_z_m, lambda)$, \
  es verdadero por construcción. \
]

Pero $q_z_i = q_z_j$. Finalmente, \

#align(center)[
  Como $(q_z_i, v w) sequent^* (q_z_j, w)$, \

  por el lema sobre transiciones entre configuraciones instantáneas, \

  podemos decir que $forall i >= 0, (q_z_i, v^i w) sequent^* (q_z_j, w)$, \

  y sabemos que $(q_z_j, w) sequent^* (q_z_m, lambda)$.
]

Por lo que concluimos que $forall i >= 0, u v^i w in L$. \

#QED

=== Algunas demostraciones donde se aplica el lema de pumping

Sea AFD $M = AF(Q, Sigma, delta, q_0, F)$ con $|Q| = n$, tal que $LenguajeDe(M) = L$. \

==== $L != emptyset sii exists x in SigmaEstrella : deltaSombrero(q_0, x) in F and |x| < n$

$implica2)$ Como $L != emptyset$, existe $x in L$. Por el lema de pumping, sé que existe un $m$ tal que para toda cadena $|z| >= m$, se puede descomponer $z = u v w$ con $|u v| <= n$ y $|v| >= 1$, de manera tal que $forall i >= 0, u v^i w in L$. \

Propongo $z = x$. \

- Supongamos que $|x| < n$. \
  Sé que $deltaSombrero(q_0, x) in F$ por hipótesis $(L != emptyset)$. \
  Y $|x| < n$ por el caso. \

- Supongamos que $|x| >= n$. \
  Por el lema de pumping, sé que puedo descomponer $x = u v w$ con $|u v| <= n$ y $|v| >= 1$, de manera tal que $forall i >= 0, u v^i w in L$. \
  Ya que al tener longitud $>= n$, pasa por un ciclo _(ver demostración del lema de pumping)_. \
  Asumiendo dada la descomposición, por el lema de pumping, puedo tomar $i = 0$. \
  Luego, $|u w| <_#text[pues $|v| >= 1$] |u v w| = |x| <= n implica |u w| < n$, y $u w in L$ por el lema de pumping. \

#QED

$implicaVuelta2)$ $exists x in SigmaEstrella : deltaSombrero(q_0, x) in F and |x| < n implica2 exists x in SigmaEstrella : deltaSombrero(q_0, x) in F implica2 x in L implica2 L != emptyset$. \
Dicho de otro modo, es trivial. \

#QED

==== $L "es infinito" sii exists x in SigmaEstrella : deltaSombrero(q_0, x) in F and n <= |x| < 2n$

$implica2)$ Como $L "es infinito"$, sé particularmente que $L != emptyset$, por lo que $exists x in SigmaEstrella : deltaSombrero(q_0, x) in F$. \
Quiero ver que, en particular ese $x$ cumple con $n <= |x| < 2n$. \

Intuitivamente, si $L "es infinito"$, entonces tiene que haber un ciclo en el AFD, pues debe haber algún lugar donde se esten "generando" cadenas infinitas. \
Esto me permite afirmar que hay al menos una cadena que cumple con $|x| >= n$. \
Me falta ver que al menos una cadena cumple con $|x| < 2n$. \

Supongamos que no hay ninguna cadena en $L$ que cumpla con $n <= |x| < 2n$. \
Sin pérdida de generalidad, sea $|x| = 2n$. \
En caso de que $|x| > 2n$, se puede repetir el argumento de la demostración cuantas veces sea necesario. #text(comment)[Si te queda $x$ tal que $|x| < 2n$, llegaste al absurdo que buscábamos, si te queda $x$ tal que $|x| >= 2n$, podés bombear $x$ otra vez pues es $>= n$, luego, repetís el argumento.] \

Luego, por el lema de pumping, como $|x| >= n$, puedo descomponer $x = u v w$ con $|u v| <= n$ y $|v| >= 1$, de manera tal que $forall i >= 0, u v^i w in L$. \
Por lo que $|u w| in L$. \

Como $|u v w| = 2n$ y $1 <= |v| <= n$, tenemos que \
#align(center)[
  $n <=_"como mucho le saqué n" |u w| <_"como mínimo le saqué 1" 2n$.
]

Contradiciendo que no hay ninguna cadena en $L$ que cumpla con $n <= |x| < 2n$. \

#QED

$implicaVuelta2)$ Supongo que $exists x in SigmaEstrella : deltaSombrero(q_0, x) in F and n <= |x| < 2n$. \
Por el lema de pumping, puedo descomponer $x = u v w$ con $|u v| <= n$ y $|v| >= 1$, de manera tal que $forall i >= 0, u v^i w in L$, esto último implica que $L "es infinito"$. \

#QED

=== Propiedades de clausura de los lenguajes regulares

==== Unión
El conjunto de lenguajes regulares incluido en $SigmaEstrella$ es cerrado respecto de la unión. \

===== Demostración

Sean $L_1$ y $L_2$ lenguajes regulares. Quiero probar que $L_1 union L_2$ es un lenguaje regular. \

Sean $M_1 = AF(Q_1, Sigma, delta_1, q_1_0, F_1)$ y $M_2 = AF(Q_2, Sigma, delta_2, q_2_0, F_2)$ dos AFD, donde $Q_1 sect Q_2 = emptyset$ tales que $LenguajeDe(M_1) = L_1$ y $LenguajeDe(M_2) = L_2$. \

Defino AFD $M = AF(Q_1 times Q_2, Sigma, delta, (q_1_0, q_2_0), F)$, donde \

- $forall q_1 in Q_1, forall q_2 in Q_2, forall a in Sigma, delta((q_1, q_2), a) = (delta_1(q_1, a), delta_2(q_2, a))$

- $F = {(f_1, f_2) : f_1 in F_1 or f_2 in F_2}$

tal que $LenguajeDe(M) = L$. \

Para probar que $L = L_1 union L_2$, basta probar que: $x in LenguajeDe(M) sii x in LenguajeDe(M_1) or x in LenguajeDe(M_2)$.

#align(center)[
  $x in LenguajeDe(M) &sii delta((q_1_0, q_2_0), x) in F \
  &sii (delta_1(q_1_0, x), delta_2(q_2_0, x)) in F \
  &sii delta_1(q_1_0, x) in F_1 or delta_2(q_2_0, x) in F_2 \
  &sii x in LenguajeDe(M_1) or x in LenguajeDe(M_2).$
]

#QED

==== Concatenación

El conjunto de lenguajes regulares incluido en $SigmaEstrella$ es cerrado respecto de la concatenación. \
 
===== Demostración

Sean $L_1$ y $L_2$ lenguajes regulares. Quiero probar que $L_1 L_2$ es un lenguaje regular. \

Sean $M_1 = AF(Q_1, Sigma, delta_1, q_1_0, F_1)$ y $M_2 = AF(Q_2, Sigma, delta_2, q_2_0, F_2)$ AFDs, donde $Q_1 sect Q_2 = emptyset$ tales que $LenguajeDe(M_1) = L_1$ y $LenguajeDe(M_2) = L_2$. \

Defino AFND-$lambda$ $M = AF(Q_1 union Q_2, Sigma union {lambda}, delta, q_1_0, F_2)$, donde \

- $forall q in Q_1, forall a in Sigma, delta_1(q,a) in delta(q,a)$

- $forall q in Q_2, forall a in Sigma, delta_2(q,a) in delta(q,a)$

- $forall f in F_1, forall a in Sigma : q_2_0 in delta_1(f, lambda)$

Ahora falta probar que $LenguajeDe(M) = L_1 L_2$.\
Para ello probamos que $x_1 x_2 in LenguajeDe(M) sii x_1 in LenguajeDe(M_1) and x_2 in LenguajeDe(M_2)$. \

Sean $x_1, x_2 in SigmaEstrella$. \
#align(center)[
  $x_1 x_2 in LenguajeDe(M) &sii delta(q_1_0, x_1 x_2) in F_2 \
  &sii delta_1(q_1_0, x_1) in F_1 and q_2_0 in ClausuraLambda(f) and delta_2(q_2_0, x_2) in F_2 \
  &sii delta_1(q_1_0, x_1) in F_1 and delta_2(q_2_0, x_2) in F_2 \
  &sii x_1 in LenguajeDe(M_1) and x_2 in LenguajeDe(M_2).$
]

#QED

==== Complemento

El conjunto de lenguajes regulares incluido en $SigmaEstrella$ es cerrado respecto al complemento. \

===== Demostración

Sea $L subset.eq Delta^*$ regular, con $Delta subset.eq Sigma$. \
Sea $M = AF(Q, Delta, delta, q_0, F)$ tal que $LenguajeDe(M) = L$ y $delta$ está definida para todos los elementos del alfabeto $Delta$. \

El AFD $M' = AF(Q, Delta, delta, q_0, Q - F)$ acepta $Delta^* - LenguajeDe(M)$. \
Es decir, $LenguajeDe(M') = Delta^* - LenguajeDe(M)$. \

En caso de que $Delta = Sigma, ComplementoConjunto(LenguajeDe(M)) = LenguajeDe(M') union SigmaEstrella (Sigma - Delta) SigmaEstrella$, \
que es la unión de dos lenguajes regulares y por lo tanto regular. \

#QED

==== Intersección

El conjunto de lenguajes regulares incluido en $SigmaEstrella$ es cerrado respecto de la intersección. \

===== Demostración

Dados $M_1$ y $M_2$ AFDs tales que $LenguajeDe(M_1) = L_1$ y $LenguajeDe(M_2) = L_2$, el autómata $M$ que aceptará el lenguaje $L_1 sect L_2$, estará dado por $M = AF(Q_1 times Q_2, Sigma, delta, (q_1_0, q_2_0), F)$ con: \

- $delta((q_1, q_2), a) = (delta_1(q_1, a), delta_2(q_2, a))$ para $q_1 in Q_1$ y $q_2 in Q_2$.

- $F = F_1 times F_2 = {(f_1, f_2) : f_1 in F_1 and f_2 in F_2}$.

Luego,

#align(center)[
  $x in LenguajeDe(M) &sii delta((q_1_0, q_2_0), x) in F \
  &sii (delta_1(q_1_0, x), delta_2(q_2_0, x)) in F \
  &sii delta_1(q_1_0, x) in F_1 and delta_2(q_2_0, x) in F_2 \
  &sii x in LenguajeDe(M_1) and x in LenguajeDe(M_2).$
]

#QED

Otra forma de demostrarlo es sabiendo que el conjunto de lenguajes regulares incluido en $SigmaEstrella$ es cerrado respecto de la unión y complementación. \
Para ello, aplicamos las leyes de De Morgan. \

Dados $L_1$ y $L_2$ lenguajes regulares, \

#align(center)[
  $L_1 sect L_2 = ComplementoConjunto(ComplementoConjunto(L_1 sect L_2)) = ComplementoConjunto(ComplementoConjunto(L_1) union ComplementoConjunto(L_2))$.
]

#QED

==== Conclusión de las propiedades de clausura anteriores

De los teoremas anteriores podemos afirmar que el conjunto de los lenguajes regulares incluidos en $SigmaEstrella$ es un álgebra booleana de conjuntos. \

==== Reversa

El conjunto de lenguajes regulares incluido en $SigmaEstrella$ es cerrado respecto de la reversa. \

Falta la demostración. \

==== Union e intersección finita

La unión e intersección finita de lenguajes regulares dan por resultado un lenguaje regular. \

===== Demostración

Debemos ver que,

#align(center)[
  $forall n in NN, union.big_(i=1)^n L_i "es regular"$, \
  y \
  $forall n in NN, sect.big_(i=1)^n L_i "es regular"$. \
]

Por inducción en $n$:

*Caso base:* $n = 0$. \

$union.big_(i=1)^0 L_i = emptyset$ y $sect.big_(i=1)^0 L_i = emptyset$, que son lenguajes regulares. \

*Caso inductivo:* supongamos que vale para $n$, probemos que vale para $n+1$. \

$union.big_(i=1)^(n+1) L_i = union.big_(i=1)^n L_i union L_(n+1)$, que es un lenguaje regular por la propiedad de clausura de la unión. \

$sect.big_(i=1)^(n+1) L_i = sect.big_(i=1)^n L_i sect L_(n+1)$, que es un lenguaje regular por la propiedad de clausura de la intersección. \

#QED

- *Observación:* los lenguajes regulares no están clausurados por unión infinita.

Ejemplo clásico, para cada $i >= 1$, $L_i = {a^i b^i}$ es regular. \
Sin embargo, $union.big_(i=1)^infinity L_i = union.big_(i=1)^infinity {a^i b^i} = {a^n b^n : n in NN}$ no es regular. \

==== Todo lenguaje finito es regular

===== Demostración

Sea $L$ un lenguaje finito, con $n$ cadenas, $L = {alpha_1, alpha_2, ..., alpha_n}$. \

Para cada $i = 1,2,...,n$, sea $L_i = {alpha_i}$. Cada $L_i = {alpha_i}$ es regular.

Entonces, $L = union.big_(i=1)^n L_i = union.big_(i=1)^n {alpha_i}$, que es un lenguaje regular, pues vimos que la unión finita de lenguajes regulares es regular. \ 

= Expresiones regulares

== Definición

Dado un alfabeto $Sigma$, una expresión regular denota un lenguaje sobre $Sigma$:

- $emptyset$ es una expresión regular que denota el conjunto vacío $emptyset$.

- $lambda$ es una expresión regular que denota el conjunto {$lambda$}.

- para cada $a in Sigma$, $a$ es una expresión regular que denota el conjunto {$a$}.

- si $r$ y $s$ denotan los lenguajes $R$ y $S$ entonces
  - $r | s$ denota $R union S$,
  - $r s$ denota $R S$,
  - $r^*$ denota $R^*$, y
  - $r^+$ denota $R^+$.

== Todo lenguaje descrito por una expresión regular, tiene un autómata finito que lo acepta



Dada una expresión regular $r$, existe un AFND-$lambda$ $M$ con un solo estado final y sin transiciones a partir del mismo tal que $LenguajeDe(M) = LenguajeDe(r)$. #h(6.5cm) *$<-$ Teorema*

=== Demostración

*Casos base:* \

$r = emptyset$

#align(center)[
  #cetz.canvas({
    import draw: state, transition

    state((0, 0), "q0", label: $q_0$, initial: "")
  })
]

$r = lambda$

#align(center)[
  #cetz.canvas({
    import draw: state, transition

    state((0, 0), "q0", label: $q_0$, initial: "", final: true)
  })
]

$r = a$

#align(center)[
  #cetz.canvas({
    import draw: state, transition

    state((0, 0), "q0", label: $q_0$, initial: "")
    state((8, 0), "qf", label: $q_f$, final: true)

    transition("q0", "qf", label: $a$)
  })
]

*Paso inductivo:* \

Supongamos que la expresión regular es $r_1 | r_2$, $r_1 r_2$, $r_1^*$, ó $r_2^+$ y asumimos que vale la propiedad para $r_1$ y para $r_2$. \
Es decir, tanto para $r_1$ como para $r_2$ existen AFND-$lambda$ $M_1$ y $M_2$ con un solo estado final y sin transiciones a partir del mismo, tal que $LenguajeDe(M_1) = LenguajeDe(r_1)$ y $LenguajeDe(M_2) = LenguajeDe(r_2)$. \

$r = r_1 | r_2$

Por *H.I.*, existen $M_1 = AF(Q_1, Sigma_1, delta_1, q_1, {f_1})$ y $M_2 = AF(Q_2, Sigma_2, delta_2, q_2, {f_2})$ con un solo estado final y sin transiciones a partir del mismo, tales que $LenguajeDe(M_1) = LenguajeDe(r_1)$ y $LenguajeDe(M_2) = LenguajeDe(r_2)$. \

Sea $M = AF(Q_1 union Q_2 union {q_0, q_f}, Sigma_1 union Sigma_2, delta, q_0, {q_f})$. \

#align(center)[
  #cetz.canvas({
    import cetz.draw: circle, line, content
    import draw: state, transition

    state((0, 0), "q0", label: $q_0$, initial: "")


    circle((6,2), name: "M1", radius: (3.2, 1.6), stroke: black, fill: auto)
    content((rel: (-2.5, 1.5), to: "M1"), text($M_1$))
    state((4, 2), "q1", label: $q_1$)
    state((8, 2), "f1", label: $f_1$)

    circle((6,-2), name: "M2", radius: (3.2, 1.6), stroke: black, fill: auto)
    content((rel: (-2.5, 1.5), to: "M2"), text($M_2$))
    state((4, -2), "q2", label: $q_2$)
    state((8, -2), "f2", label: $f_2$)

    state((12, 0), "qf", label: $q_f$, final:true)

    transition("q0", "q1", label: $lambda$, curve: 1)
    transition("q0", "q2", label: $lambda$, curve: -1)

    transition("f1", "qf", label: $lambda$, curve: 1)
    transition("f2", "qf", label: $lambda$, curve: -1)
  })
]

- $delta(q_0, lambda) = {q_1, q_2}$.
- $delta(q, a) = delta_1(q, a)$ para $q in Q_1 - {f_1}$ y $a in Sigma_1 union {lambda}$.
- $delta(q, a) = delta_2(q, a)$ para $q in Q_2 - {f_2}$ y $a in Sigma_2 union {lambda}$.
- $delta(f_1, lambda) = delta(f_2, lambda) = {q_f}$. \

$r = r_1 r_2$

Por *H.I.* existen $M_1 = AF(Q_1, Sigma_1, delta_1, q_1, {f_1})$ y $M_2 = AF(Q_2, Sigma_2, delta_2, q_2, {f_2})$ con un solo estado final y sin transiciones a partir del mismo, tales que $LenguajeDe(M_1) = LenguajeDe(r_1)$ y $LenguajeDe(M_2) = LenguajeDe(r_2)$. \
Entones, podemos construir el autómata $M = AF(Q_1 union Q_2, Sigma_1 union Sigma_2, delta, q_1, {f_2})$. \

#align(center)[
  #cetz.canvas({
    import cetz.draw: circle, line, content
    import draw: state, transition

    circle((2,0), name: "M1", radius: (3.2, 1.6), stroke: black, fill: auto)
    content((rel: (-2.5, 1.5), to: "M1"), text($M_1$))
    state((0.3, 0), "q1", label: $q_1$, initial: "")
    state((4, 0), "f1", label: $f_1$)

    circle((10,0), name: "M2", radius: (3.2, 1.6), stroke: black, fill: auto)
    content((rel: (-2.5, 1.5), to: "M2"), text($M_2$))
    state((8, 0), "q2", label: $q_2$)
    state((12, 0), "f2", label: $f_2$, final: true)

    transition("f1", "q2", label: $lambda$, curve: 0.5)
  })
]

- $delta(q, a) = delta_1(q, a)$ para $q in Q_1 - {f_1}$ y $a in Sigma_1 union {lambda}$.
- $delta(f_1, lambda) = {q_2}$.
- $delta(q, a) = delta_2(q, a)$ para $q in Q_2 - {f_2}$ y $a in Sigma_2 union {lambda}$. \

$r = r_1^*$

Por *H.I.* existe $M_1 = AF(Q_1, Sigma_1, delta_1, q_1, {f_1})$ con un solo estado final y sin transiciones a partir del mismo, tal que $LenguajeDe(M_1) = LenguajeDe(r_1)$. \
Entonces, podemos construir el autómata $M = AF(Q_1 union {q_0, f_0}, Sigma_1, delta, q_0, {f_0})$. \

#align(center)[
  #cetz.canvas({
    import cetz.draw: circle, line, content
    import draw: state, transition

    circle((6,0), name: "M1", radius: (3.2, 1.6), stroke: black, fill: auto)
    content((rel: (-2.5, 1.5), to: "M1"), text($M_1$))
    state((4, 0), "q1", label: $q_1$)
    state((8, 0), "f1", label: $f_1$)

    state((0, 0), "q0", label: $q_0$, initial: "")
    state((12, 0), "f0", label: $f_0$, final: true)

    transition("q0", "q1", label: $lambda$, curve: 0.5)
    transition("f1", "f0", label: $lambda$, curve: 0.5)
    transition("f1", "q1", label: $lambda$, curve: 0.8)
    transition("q0", "f0", label: $lambda$, curve: 2.6) 
  })
]

- $delta(q, a) = delta_1(q, a)$ para $q in Q_1 - {f_1}$ y $a in Sigma_1 union {lambda}$.
- $delta(q_0, lambda) = delta(f_1, lambda) = {q_1, f_0}$. \

$r = r_1^+$

Dado que $r_1^+ = r_1 r_1^*$, queda demostrado por los casos anteriores.

#QED

== Todo lenguaje aceptado por un autómata finito, tiene una expresión regular que lo describe

Dado un AFD $M = AF(Q, Sigma, delta, q_1, F)$, existe una expresión regular $r$ tal que $LenguajeDe(M) = LenguajeDe(r)$. #h(12.8cm) *$<-$ Teorema*

=== Demostración

Denoetemos con $R^k_(i,j)$ el conjunto de cadenas de $Sigma^*$ que llevan al autómata $M$ desde el estado $q_i$ al estado $q_j$ pasando por estados cuyo índice es, a lo sumo, $k$. \
Definamos $R_(i,j)$ en forma recursiva: \

#align(center)[
  $R^0_(i,j) &= cases(
    {a : delta(q_i, a) = q_j} &"si" i != j,
    {a : delta(q_i, a) = q_j} union {lambda} &"si" i = j
   ) \

   R^k_(i,j) &= R^(k-1)_(i,k) (R^(k-1)_(k,k))^(*) R^(k-1)_(k,j) union R^(k-1)_(i,j) &"para" k in NN >= 1
  $
]

Demostremos que para todo $R^k_(i,j)$, existe una expresión regular $r^k_(i,j)$ tal que $LenguajeDe(r^k_(i,j)) = R^k_(i,j)$. \ 

*Caso base:* $k = 0$. \
Debemos dar $r^0_(i,j)$ tal que $LenguajeDe(r^0_(i,j)) = R^0_(i,j)$. \
$R_(i,j)$ es el conjunto de cadenas de un solo caracter o $lambda$. \

Por lo tanto, $r^0_(i,j)$ es: \
- $a_1 | ... | a_p$
  - con $a_1, ..., a_p$ símbolos de $Sigma$, si $delta(q_i, a_s) = q_j$ para $s = 1, ..., p$ y $q_i != q_j$.
- $a_1 | ... | a_p | lambda$
  - con $a_1, ..., a_p$ símbolos de $Sigma$, si $delta(q_i, a_s) = q_j$ para $s = 1, ..., p$ y además $q_i = q_j$.
- $emptyset$, si no existe ningún $a_i$ que una $q_i$ y $q_j$ y $q_i != q_j$.
- $lambda$, si no existe ningún $a_i$ que una $q_i$ y $q_j$ y además $q_i = q_j$.

*Paso inductivo:* \

Por *H.I.* tenemos: \

#align(center)[
  $LenguajeDe(r^(k-1)_(i,k)) = R^(k-1)_(i,k)$ #h(1cm) $LenguajeDe(r^(k-1)_(k,k)) = R^(k-1)_(k,k)$ #h(1cm) $LenguajeDe(r^(k-1)_(k,j)) = R^(k-1)_(k,j)$ #h(1cm) $LenguajeDe(r^(k-1)_(i,j)) = R^(k-1)_(i,j)$
]

Definimos $r^k_(i,j) = r^(k-1)_(i,k) (r^(k-1)_(k,k))^* r^(k-1)_(k,j) | r^(k-1)_(i,j)$ y verificamos que: \

#align(center)[
  $LenguajeDe(r^k_(i,j)) &= LenguajeDe(r^(k-1)_(i,k) (r^(k-1)_(k,k))^* r^(k-1)_(k,j) | r^(k-1)_(i,j)) \
  &= LenguajeDe(r^(k-1)_(i,k) (r^(k-1)_(k,k))^* r^(k-1)_(k,j)) union LenguajeDe(r^(k-1)_(i,j)) \
  &= LenguajeDe(r^(k-1)_(i,k)) LenguajeDe((r^(k-1)_(k,k))^*) LenguajeDe(r^(k-1)_(k,j)) union LenguajeDe(r^(k-1)_(i,j)) \
  &= LenguajeDe(r^(k-1)_(i,k)) (LenguajeDe(r^(k-1)_(k,k)))^* LenguajeDe(r^(k-1)_(k,j)) union LenguajeDe(r^(k-1)_(i,j)) \
  &= R^(k-1)_(i,k) (R^(k-1)_(k,k))^* R^(k-1)_(k,j) union R^(k-1)_(i,j) \
  &= R^k_(i,j).$
]

Entonces, como $Q = {q_1, ..., q_n}$ y $q_1$ es el estado inicial de $M$, \

#align(center)[
  $LenguajeDe(M) &= R^n_1_j_1 union ... union R^n_1_j_m", con" F = {q_j_1, ..., q_j_m} \
  &= LenguajeDe(r^n_1_j_1) union ... union LenguajeDe(r^n_1_j_m) \
  &= LenguajeDe(r^n_1_j_1 | ... | r^n_1_j_m).$
]

Concluimos que $LenguajeDe(M) = LenguajeDe(r^n_1_j_1 | ... | r^n_1_j_m)$. \

#QED

= Autómatas de pila y gramáticas libres de contexto

== Autómatas de pila

Un *autómata de pila* (AP) $M = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$ es un modelo computacional definido formalmente como un sistema de siete componentes:

- $Q$ es un conjunto finito de estados.
- $Sigma$ es un alfabeto finito de entrada.
- $Gamma$ es un alfabeto finito de la pila.
- $delta : Q times (Sigma union {lambda}) times Gamma -> PartesDe(Q times Gamma^*)$ es la función de transición:
  - $delta(q, a, Z) = {(p_1, gamma_1), ..., (p_m, gamma_m)}$.
- $q_0 in Q$ es el estado inicial.
- $Z_0 in Gamma$ es la configuración inicial de la pila.
- $F subset.eq Q$ es el conjunto de estados finales.

La interpretación de $delta(q, x, Z) = {(p_1, gamma_1), ..., (p_n, gamma_n)}$, con $q, p_1, ..., p_n in Q$, \ $x in (Sigma union {lambda})$, $Z in Gamma$, y $gamma_i in Gamma^*$ es la siguiente. \

Cuando el estado del autómata es $q$, el símbolo que la cabeza lectora está inspeccionando en ese momento es $x$, y en el tope de la pila nos encontramos el símbolo $Z$, se realizan las siguientes acciones:

1. Si $x in Sigma$, es decir no es la cadena vacía, la cabeza lectora avanza una posición para inspeccionar el siguiente símbolo.
2. Se elimina el símbolo $Z$ de la pila del autómata.
3. Se selecciona un par $(p_i, gamma_i)$ entre los existentes en la definición de $delta(q, x, Z)$.
4. Se apila la cadena $gamma_i = c_1 c_2 ... c_k$, con $c_i in Gamma$ en la pila del autómata, quedando el símbolo $c_1$ en el tope de la pila.
5. Se cambia el control del autómata al estado $p_i$.

=== Autómatas de pila determinísticos

Un autómata de pila $M = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$ es determinístico si para todo $q in Q$, $x in Sigma$, $Z in Gamma$ se cumplen las siguientes condiciones:

1. $\#delta(q, x, Z) <= 1$.
2. $\#delta(q, lambda, Z) <= 1$.
3. si $\#delta(q, lambda, Z) = 1$ entonces $\#delta(q, x, Z) = 0$.

=== La no equivalencia entre los APD y los APND

No es cierto que para cada autómata de pila no determinístico existe otro determinístico que reconoce el mismo lenguaje.

Esto se demuestra con un contraejemplo. \
$L = {omega omega^R}$, donde $omega^R$ es la cadena $omega$ invertida, es aceptado por un APND, pero no es aceptado por ningún APD. \

La intuición detrás de esto es que para reconocer $omega omega^R$, el AP debería saber cuando leyó completamente $omega$, y recordarla, para luego compararla con $omega^R$. \
Sin embargo, ¿cuándo sabe el AP que terminó de leer $omega$? \
En un APND, el autómata puede bifurcarse en varios caminos, hasta encontrar el corte correcto en $omega omega^R$. \
En un APD, el autómata debería adivinar cuál es el corte correcto, lo cual no es posible. \

=== Configuración de un autómata de pila

Sea un autómata de pila $M = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$. \
Una *configuración* de $M$ es una terna $(q, omega, gamma)$, donde: \

- $q in Q$ es el estado actual del autómata.
- $omega in Sigma^*$ es la parte de la cadena de entrada que falta procesar.
- $gamma in Gamma^*$ es el contenido de la pila.

La *configuración inicial* del autómata para la cadena $omega_0$ es $(q_0, omega_0, Z_0)$. \

=== Cambio de configuración $sequent$

Sea un autómata de pila $M = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$. \
Para todo $x in Sigma$, $omega in Sigma^*$, $Z in Gamma$, $gamma, pi in Gamma^*$, $q, q' in Q$:

- $(q, x omega, Z pi) sequent (q', omega, gamma pi)$ si $(q', gamma) in delta(q, x, Z)$.
- $(q, omega, Z pi) sequent (q', omega, gamma pi)$ si $(q', gamma) in delta(q, lambda, Z)$.

=== Lenguaje aceptado por un autómata de pila

Sea un autómata de pila $M = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$. \

El lenguaje aceptado por $M$ por *estado final* es: \
#align(center)[
  $LenguajeDe(M) = {omega in Sigma^* : exists f in F, exists gamma in Gamma^* "que cumplen" (q_0, omega, Z_0) sequent^* (f, lambda, gamma)}$.
]

El lenguaje aceptado por $M$ por *pila vacía* es: \
#align(center)[
  $LenguajeDe_(lambda)(M) = {omega in Sigma^* : exists p in Q "que cumple" (q_0, omega, Z_0) sequent^* (p, lambda, lambda)}$.
]

=== Los autómatas de pila *no determinísticos* por pila vacía y por estado final son equivalentes

Para cada *APND* $M$ existe un *APND* $M'$, tal que: \
#align(center)[
  $LenguajeDe(M) = LenguajeDe_(lambda)(M')$
]

*Demostración:* \

Sea $M = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$ un *APND*. \
Definimos el *APND* $M' = AP(Q union {q_lambda, q'_0}, Sigma, Gamma union {X_0}, delta', q'_0, X_0, emptyset)$:

#align(center)[
  #cetz.canvas({
    import cetz.draw: circle, line, content
    import draw: state, transition

    // Estados del autómata M
    circle((0,0), name: "M", radius: (5.5, 2), stroke: black, fill: auto)
    content((rel: (-2.2, 1.5), to: "M"), text($M$))
    state((-4.5, 0), "q0", label: $q_0$)
    circle((2,0), name: "F", radius: (3, 1.5), stroke: black, fill: auto,)
    content((rel: (-2, .75), to: "F"), text($F$))
    state((3, -.5), "f1", label: "",)
    state((1, .5), "f2", label: "",)

    // Estados del autómata M'
    state((8, 0), "q_lambda", label: $q_lambda$)
    state((-7, 0), "q'0", label: $q'_0$, initial: "")

    // Transiciones del autómata M
    transition("q'0", "q0", label: $lambda, X_0 | Z_0 X_0$, curve: 1,)
    transition("f1", "q_lambda", label: $lambda, Z | lambda$, curve: 1.5)
    transition("f2", "q_lambda", curve: 0.8)
    transition("q_lambda", "q_lambda", label: $lambda, Z | lambda$, curve: 0.75, anchor: (bottom))
  })
]

Formalmente, \

1. $delta'(q'_0, lambda, X_0) = {(q_0, Z_0 X_0)}$. #text(comment)[$<- M'$ entra al estado inicial de $M$ con $Z_0 X_0$ en la pila, así evita la pila vacía.]

2. $forall q in Q, forall x in Sigma union {lambda}, forall Z in Gamma, delta'(q,x,Z) = delta(q,x,Z)$ #text(comment)[$<- M'$ mantiene las transiciones de $M$.]

3. $forall f in F, forall Z in Gamma union {X_0}, (q_lambda, lambda) in delta'(f, lambda, Z)$ #text(comment)[$<-$ Si $M$ llega a un estado final, lo mandamos a vaciar la pila.]

4. $forall Z in Gamma union {X_0}, delta'(q_lambda, lambda, Z) = {(q_lambda, lambda)}$ #text(comment)[$<-$ Vamos vaciamos la pila de $M'$.]

Queremos ver que $LenguajeDe(M) = LenguajeDe_(lambda)(M')$. \
Para ello veamos que $LenguajeDe(M) subset.eq LenguajeDe_(lambda)(M')$ y $LenguajeDe(M) supset.eq LenguajeDe_(lambda)(M')$. \

Si $omega in LenguajeDe(M)$, entonces $(q_0, omega, Z_0) sequent^*_M (f, lambda, gamma),$ con $f in F, gamma in Gamma^*$. \

Luego, como $delta'(q'_0, lambda, X_0) =_#text(weight: "bold")[definición de $delta'$] {(q_0, Z_0 X_0)}$ sabemos que \

#align(center)[
  $(q'_0, omega, X_0) sequent_M' (q_0, omega, Z_0 X_0)$
]

se cumple. \

A su vez, sabemos que $forall q in Q, forall x in Sigma union {lambda}, forall Z in Gamma, delta'(q,x,Z) =_#text(weight: "bold")[definición de $delta'$] delta(q,x,Z)$ es cierto, lo que nos permite afirmar que \

#align(center)[
  $(q_0, omega, Z_0 X_0) sequent^*_M' (f, lambda, gamma X_0)$.
]

Por otro lado, como $forall f in F, forall Z in Gamma union {X_0}, (q_lambda, lambda) in_#text(weight: "bold")[definición de $delta'$] delta'(f, lambda, Z)$, y \

$forall Z in Gamma union {X_0}, delta'(q_lambda, lambda, Z) =_#text(weight: "bold")[definición de $delta'$] {(q_lambda, lambda)}$ sabemos que \

#align(center)[
  $(f, lambda, gamma X_0) sequent^*_M' (q_lambda, lambda, lambda)$.
]

Por lo tanto, $(q'_0, omega, X_0) sequent^*_M' (q_lambda, lambda, lambda)$, lo que implica que $omega in LenguajeDe_(lambda)(M')$. \

Veamos que $LenguajeDe(M) supset.eq LenguajeDe_(lambda)(M')$. \

Si $omega in LenguajeDe_(lambda)(M')$, existe la secuencia \

#align(center)[
  $(q'_0, omega, X_0) sequent_M' underbrace((q_0, omega, Z_0 X_0) sequent^*_M' (f, lambda, gamma X_0), A) sequent^*_M' (q_lambda, lambda, lambda)$.
]

Pero la transición $A$ implica

#align(center)[
  $(q_0, omega, Z_0) sequent^*_M (f, lambda, gamma)$.
]

Por lo que concluimos que, si $omega in LenguajeDe_(lambda)(M')$, entonces $omega in LenguajeDe(M)$. \

#QED

Para cada *APND* $M' = AP(Q', Sigma, Gamma', delta', q'_0, X_0, emptyset)$ existe un *APND* $M = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$, tal que: \
#align(center)[
  $LenguajeDe_(lambda)(M') = LenguajeDe(M)$
]

*Demostración:* \

Definimos $M = AP(Q' union {q_0, q_f}, Sigma, Gamma' union {Z_0}, delta, q_0, Z_0, {q_f})$, donde \

#align(center)[
  #cetz.canvas({
    import cetz.draw: circle, line, content
    import draw: state, transition

    // Estados del autómata M
    circle((0,0), name: "M'", radius: (4, 2), stroke: black, fill: auto)
    content((rel: (-2, 1.4), to: "M'"), text($M'$))
    state((-3, 0), "q'0", label: $q'_0$)
    state((3, -.3), "q'1", label: "",)
    state((1, .5), "q'2", label: "",)
    state((-.75, -.75), "q'3", label: "",)

    // Estados del autómata M'
    state((-7, 1), "q0", label: $q_0$, initial: "")
    state((8, 1), "qf", label: $q_f$, final: true)

    // Transiciones del autómata M
    transition("q0", "q'0", label: $lambda, Z_0 | X_0 Z_0$, curve: 0.25)
    transition("q'1", "qf", curve: .2)
    transition("q'2", "qf", curve: 0.8)
    transition("q'3", "qf", curve: -1.9, label: $lambda, Z_0 | lambda$)
    transition("q'0", "qf", curve: 1.5, label: $lambda, Z_0 | lambda$)
  })
]

- $delta(q_0, lambda, Z_0) = {(q'_0, X_0 Z_0)}$ #text(comment)[$<- M$ entra al estado inicial de $M'$ con $X_0 Z_0$ en la pila, así evitamos vaciar la pila.]

- $forall q in Q', forall x in Sigma {lambda}, forall Z in Gamma', delta(q, x, Z) = delta'(q, x, Z)$ #text(comment)[$<- M$ mantiene las transiciones de $M'$.]

- $forall q in Q', (q_f, lambda) in delta(q, lambda, Z)$ #text(comment)[$<-$ Cuando se vacía la pila simulada en $M'$, $M$ salta al estado final $q_f$.]

Nos queda por argumentar que $omega in LenguajeDe_(lambda)(M')$ si y solo si $omega in LenguajeDe(M)$. \

- Si $omega in LenguajeDe_(lambda)(M')$ entonces $(q'_0, omega, X_0) sequent^*_M' (q, lambda, lambda)$. \
Con lo que al tener encuenta la definición de $M$ asegura

#align(center)[
  $(q_0, omega, Z_0) sequent_M (q'_0, omega, X_0 Z_0) sequent^*_M (q, lambda, Z_0) sequent_M (q_f, lambda, lambda)$, \
  y por lo tanto $omega in LenguajeDe(M)$.
]

- Si $omega in LenguajeDe(M)$ entonces \

#align(center)[
  $(q_0, omega, Z_0) sequent_M (q'_0, omega, X_0 Z_0) sequent^*_M (q, lambda, Z_0) sequent_M (q_f, lambda, lambda),$ \
  pero por definición de $M$, \
  $(q_f, lambda, X_0 Z_0) sequent^*_M (q, lambda, Z_0)$ si y solo si $(q'_0, omega, X_0) sequent^*_M' (q, lambda, lambda)$. \

  Concluimos $(q'_0, omega, X_0) sequent^*_M' (q, lambda, lambda)$, y por lo tanto $omega in LenguajeDe_(lambda)(M')$.
]

#QED

=== Los autómatas de pila *determinísticos* por estado final tienen mayor poder expresivo que los autómatas de pila *determinísticos* por pila vacía

Los lenguajes que aceptan los *autómatas de pila determinísticos por pila vacía*, son exclusivamente *libres de prefijos*. \
Es decir, sea $L$ en un lenguaje aceptado por un *APD* por pila vacía, entonces \

#align(center)[
  si $alpha in L implica forall beta != lambda, alpha beta in L$.
]

Por lo que el poder expresivo de los *autómatas de pila determinísticos por estado final* es *mayor* que el de los *autómatas de pila determinísticos por pila vacía*, pues estos últimos no pueden reconocer lenguajes que no sean libres de prefijos. \

=== Preguntas<demo_af_contenido_en_ap>

1. Si $M = AP(Q, Sigma, Gamma, delta, q_0, Z_0, emptyset)$ es un autómata de pila entonces cada palabra $omega in L_(lambda)(M)$ es reconocida por $M$ en a lo sumo $|omega| * \#Q * \#Gamma$ transiciones. \
   Es decir, $exists n <= |omega| * \#Q * \#Gamma, exists p in Q$, tal que $(q_0, omega, Z_0) sequent^n (q_m, lambda, lambda)$. \

Esta afirmación es *verdadera*. \

*Demostración:* \

Sabemos que $|omega| * \#Q * \#Gamma$ es el total de "estados" posibles para hacer una transición que puede tener el autómata $M$ al procesar la cadena $omega$. \

Luego, asumiendo que $omega in L_(lambda)(M)$, entonces existe una secuencia de configuraciones $(q_0, omega, Z_0) sequent^* (q_m, lambda, lambda)$, con $q_m in Q$. \

Supongamos que no existe $n <= |omega| * \#Q * \#Gamma$ tal que $(q_0, omega, Z_0) sequent^n (q_m, lambda, lambda)$. \
Es decir, $(q_0, omega, Z_0) sequent^n (q_m, lambda, lambda)$ con $n > |omega| * \#Q * \#Gamma$. \

Si extendemos la secuencia de configuraciones tenemos algo del estilo:
#align(center)[
  $(q_0, omega_0, Z_0) sequent (q_1, omega_1, gamma_1) sequent ... sequent (q_(m-1), omega_(m-1), gamma_(m-1)) sequent (q_m, lambda, lambda)$.
]

Sin embargo, dijimos que $n > |omega| * \#Q * \#Gamma$, lo que implica que $n$ es mayor al total de configuraciones posibles. \
Es decir, tiene que haber al menos una configuración repetida en la secuencia. \
#align(center)[
  #text(size: 11.5pt)[
      $(q_0, omega_0, Z_0) sequent ... sequent underbrace((q_i, omega_i, Z_i gamma_j) sequent ... sequent (q_i, omega_i, Z_i gamma_k), "ciclo") sequent ... sequent (q_(m-1), omega_(m-1), gamma_(m-1)) sequent (q_m, lambda, lambda)$.
  ]
]

Por lo que podríamos quitar ese ciclo. \
Si al quitarlo nos queda una secuencia de configuraciones $(q_0, omega, Z_0) sequent^n (q_m, lambda, lambda)$, con $n > |omega| * \#Q * \#Gamma$, resta repetir el argumento, hasta que no quede ningún ciclo. \

Luego no va a quedar $(q_0, omega, Z_0) sequent^n (q_m, lambda, lambda)$ con $n <= |omega| * \#Q * \#Gamma$, lo que contradice la suposición de que no existía tal $n$. \

#QED

2. Dado un autómata finito $M = AF(Q, Sigma, delta, q_0, F)$, dar un autómata de pila $M' = AP(Q', Sigma, Gamma', delta', q_0', Z'_0, emptyset)$ tal que $LenguajeDe(M) = LenguajeDe_(lambda)(M')$. \

Este enunciado *se puede hacer*. \

*Justificación:* \

Te hacés el APND por estado final $M'' = AP(Q, Sigma, {Z_0}, delta'', q_0, Z_0, F)$, donde replicás el comportamiento ($delta$) del AF $M$ en $M''$ ignorando la pila. \
Luego, pasamos de $M''$ a $M'$, donde $M'$ es el autómata de pila por pila vacía que acepta el lenguaje de $M''$. \

3. Consideremos la demostración del teorema que afirma que para cada autómata $M = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$ existe un autómata $M'$ tal que $LenguajeDe(M) = LenguajeDe_(lambda)(M')$. \ ¿Si $M$ es determinístico, entonces el autómata $M'$ construido en la demostración también lo es? \

Esta pregunta es *falsa*. \

*Justificación:* \

Los estados finales tienen transiciones $lambda$ que te llevan a vaciar la pila. Capaz no tenías que tomar esa transición, pues ese estado final era en realidad un estado intermedio para la aceptación de la cadena. \
Respondiendo directamente a la pregunta, no pues los estados finales pueden tener transiciones que llevan a otro estado (distinto de las $lambda$), pues pueden ser estados intermedios. \

4. Consideremos la demostración del teorema que afirma que dado $M' = AP(Q', Sigma, Gamma', delta', q_0', X_0, emptyset)$ existe un autómata $M$ tal que $LenguajeDe_(lambda)(M') = LenguajeDe(M)$. \ ¿Si $M'$ es determinístico, entonces el autómata $M$ construido en la demostración también lo es? \

Esta pregunta es *verdadera*. \

*Justificación:* \

Si podés tomar la transición $lambda$ con $Z_0$ en el tope de la pila para ir al estado final, es porque la vaciaste en $M'$. Recordar que no hay transiciones que tomen $Z_0$ como tope de la pila en $M'$.
Asumiendo que $M'$ es determinístico, entonces $M$ también lo es. \

== Gramáticas libres de contexto

Una gramática $G = Gramática(V_N, V_T, P, S)$ es libre de contexto si las producciones en $P$ son de la forma: \
#align(center)[
  $A to alpha$, con $A in V_N$ y $alpha in (V_N union V_T)^*$. \
]

=== Derivación de la relación $=>$

Sea $G = Gramática(V_N, V_T, P, S)$ una gramática. \
Si $alpha, beta, gamma_1, gamma_2 in (V_N union V_T)^*$ y $alpha to beta in P$, entonces \

#align(center)[
  $gamma_1 alpha gamma_2 => gamma_1 beta gamma_2$.
]

La relación $=>$ es un subconjunto de $(V_N union V_T) times (V_N union V_T)$ y significa derivar en un solo paso. \
Las relaciones $=>^*$ y $=>^+$ son la clausura transitiva y reflexo-transitiva respectivamente (uno o más pasos de derivación, y cero o más pasos). \

Si $alpha in (V_N union V_T)^*$ y $S =>^* alpha$, entonces decimos que $alpha$ es una *forma sentencial* de $G$. \

=== Lenguaje generado por una gramática $G$

Dada una gramática $G = Gramática(V_N, V_T, P, S)$, el lenguaje generado por $G$ es: \

#align(center)[
  $LenguajeDe(G) = {omega in V^*_T : S =>^* omega}$.
]

== Relación entre autómatas de pila y gramáticas libres de contexto

=== Todo lenguaje generado por una gramática libre de contexto, tiene un autómata de pila que lo acepta

Para cada gramática $G$ libre de contexto existe un autómata de pila $M$ tal que \

#align(center)[
  $LenguajeDe(G) = LenguajeDe_(lambda)(M)$.
]

==== Demostración

Sea $G = Gramática(V_N, V_T, P, S)$ una gramática libre de contexto, definimos el *APND*

#align(center)[
  $M = AP({q}, V_T, V_N union V_T, delta, q, S, emptyset)$
]

donde $delta : Q times (V_T union {lambda}) times (V_N union V_T) -> PartesDe(Q times (V_N union V_T)^*)$ es tal que: \

- $(A to alpha) in P sii (q, alpha) in delta(q, lambda, A)$.
- $forall x in V_T, delta(q,x,x) = {(q, lambda)}$.

Queremos ver que \
#align(center)[
  $forall omega in V^*_T, S =>^+ omega$ si y solo si $(q, omega, S) sequent^+ (q, lambda, lambda)$.
]

Para ello, vamos primero a demostrar un lema. \

*Lema:* \
#align(center)[
  $forall A in V_N, forall omega in V^*_T, A =>^+ omega$ si y solo si $(q, omega, A) sequent^+ (q, lambda, lambda)$. 
]

*Demostración del lema:* \

Por inducción en $m$, la cantidad de pasos de la derivación. \

- *Caso base:* $m = 1$. \
  Queremos ver que $A =>^1 omega$ si y solo si $(q, omega, A) sequent^1 (q, lambda, lambda)$. \

  Tenemos $A =>^1 omega$ para $omega = x_1 ... x_k$ con $k >= 0$. \
  Esto es lo mismo que decir que $(A to x_1 ... x_k) in P$. \
  Y esto vale sii $(q, x_1 ... x_k) in delta(q, lambda, A)$, por definición del autómata $M$. \
  Por lo tanto, $(q, x_1 ... x_k, A) sequent^1$. Luego, nuevamente por definición del autómata $M$, tenemos que $(q, x_1 ... x_k, x_1 ... x_k) sequent^k (q, lambda, lambda)$. \

- *Paso inductivo:* \
  Asumamos que vale para todo $j < m$. \
  Es decir, $forall A in V_N, forall omega in V^*_T, forall j < m, A =>^j omega$ si y solo si $(q, omega, A) sequent^j (q, lambda, lambda)$.

  Por definición de la derivación, $A =>^m omega$ si y solo si $(A to X_1 ... X_k) in P$, tal que \
  $forall i in NN_(<= k), X_i =>^(m_i) omega_i$, para algún $m_i < m and omega = omega_1 ... omega_k$. \

  Por definición de $M$, $(A to X_1 ... X_k) in P sii (q, X_1 ... X_k) in delta(q, lambda, A)$. \
  Dicho de otra forma, $(A to X_1 ... X_k) in P sii (q, omega, A) sequent (q, omega, X_1 ... X_k)$. \

  - Si $X_i in V_N$, entonces, por *hipótesis inductiva*, $(q, omega_i, X_i) sequent^+ (q, lambda, lambda)$. \
  - Si $X_i in V_T$, entonces $X_i = omega_i$ y por definición de $M$, $(q, omega_i, X_i) sequent^1 (q, lambda, lambda)$. \

  Por lo tanto,
  #align(center)[
    $(q, omega, A) sequent (q, omega_1 ... omega_k, X_1 ... X_k) sequent^+ (q, omega_2 ... omega_k, X_2 ... X_k) sequent^+ (q, omega_k, X_k) sequent^+ (q, lambda, lambda)$.
  ]

  #QED

Volviendo a la demostración del teorema, \
El lema de recién dice que \
#align(center)[
  $forall A in V_N, forall omega in V^*_T, A =>^+ omega$ si y solo si $(q, omega, A) sequent^+ (q, lambda, lambda)$.
]

Tomando $A = S$, tenemos que \
#align(center)[
  $forall omega in V^*_T, S =>^+ omega$ si y solo si $(q, omega, S) sequent^+ (q, lambda, lambda)$.
]
Que es lo que queríamos probar. \

#QED

=== Todo lenguaje aceptado por un autómata de pila, es libre de contexto

Si $M$ es un autómata de pila, entonces $LenguajeDe_(lambda)(M)$ es libre de contexto. \

==== Demostración

Dado el autómata de pila $M = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$, definimos $G = Gramática(V_N, V_T, P, S)$, donde \

- $S$ es un símbolo nuevo.
- $V_N = {[q, Z, p] : q in Q, Z in Gamma, p in Q} union {S}$.
- $V_T = Sigma$.
- Con la siguientes reglas para las producciones:
  - $forall q in Q, (S to [q_0, Z_0, q]) in P$.
  - $([q, Z, q_1] to x) in P sii (q_1, lambda) in delta(q, x, Z)$.
  - $([q, Z, q_1] to lambda) in P sii (q_1, lambda) in delta(q, lambda, Z)$.
  - Para cada $q, q_1, q_2, ..., q_(m+1) in Q, x in Sigma, Z, Y_1, ..., Y_m in Gamma$:
    - $([q, Z, q_(m+1)] to x[q_1, Y_1, q_2] ... [q_m, Y_m, q_(m+1)]) in P sii (q_1, Y_1 ... Y_m) in delta(q, x, Z)$.
    - $([q, Z, q_(m+1)] to [q_1, Y_1, q_2] ... [q_m, Y_m, q_(m+1)]Y) in P sii (q_1, Y_1 ... Y_m) in delta(q, lambda, Z)$.

$G$ es tal que su derivación más a la izquierda es una simulación de $M$.

Queremos ver que $omega in LenguajeDe_(lambda)(M) sii omega in  LenguajeDe(G)$. \

Para ello vamos a demostrar primero un lema. \
*Lema:* \
#align(center)[
  $forall q in Q, forall Z in Gamma, forall p in P, (q, omega, Z) sequent^+_M (p, lambda, lambda) sii [q, Z, p] =>^+_G omega$.
]

*Demostración del lema:* \

1. $implica2$) Veamos por inducción que, $forall i in NN_(>=1)$, se cumple que \
#align(center)[
   $forall q in Q, forall Z in Gamma, forall p in P, (q, omega, Z) sequent^i_M (p, lambda, lambda) implica [q, Z, p] =>^i_G omega$.
]

Sea $x in Sigma union {lambda}.$

- *Caso base:* $i = 1$. \
  Tenemos $(q, omega, Z) sequent^1_M (p, lambda, lambda)$, lo que implica que $(p, lambda) in delta(q, x, Z)$. \
  Luego, por definición de $G$, $([q, Z, p] to x) in P$, o, lo que es lo mismo, $[q, Z, p] =>^1_G x$.

- *Paso inductivo:* $i > 1$. \
  Tenemos $omega = x omega'$ con $omega' in SigmaEstrella$ y $(q, omega, Z) sequent^i_M (p, lambda, lambda)$. \

  Existen $Y_1, ..., Y_n in Gamma$ tales que $(q, x omega', Z) sequent_M (q_1, omega', Y_1, ..., Y_n) sequent^(i-1)_M (p, lambda, lambda)$. \

  Necesariamente $omega'$ se descompone como $omega' = omega'_1 ... omega'_n$, tales que para $1 <= j <= n, omega_1 ... omega_j$ hacen que $Y_j$ quede en el tope de la pila. \

  #align(center)[
    #cetz.canvas({
      import cetz.draw: rect, line, content
      import draw: state, transition

      // Estados del autómata M
      state((-6, 0), "q", label: $q$)
      state((-3.5, 1.5), "q1", label: $q_1$)
      state((0, 3.5), "q2", label: $q_2$)
      state((4, .5), "qn", label: $q_n$)
      state((7, -1), "p", label: $p$)

      // Transiciones del autómata M
      transition("q", "q1", label: $x$, curve: -.5)
      transition("q1", "q2", label: $omega_1$, curve: .5, stroke:(dash: "dashed"))
      transition("q2", "qn", label: $omega_2 ... omega_(n-1)$, curve: .75, stroke:(dash: "loosely-dotted"))
      transition("qn", "p", label: $omega_n$, curve: -.5, stroke:(dash: "dashed"))

      // Pila de q
      rect((-6 - .5, 5), (rel: (.5, 1), to: "q"), name: "Stack-q", radius: 5%)
      content((rel: (0, -1.5), to: "Stack-q"), text($Z$))

      // Pila de q1
      rect((-3.5 - .5, 1.5 + 5), (rel: (.5, 1), to: "q1"), name: "Stack-q1", radius: 5%)
      content((rel: (0, -1.5), to: "Stack-q1"), text($Y_n$))
      content((rel: (.12, -1), to: "Stack-q1"), text(rotate(90deg)[$-$]))
      content((rel: (.12, -.5), to: "Stack-q1"), text(rotate(90deg)[$-$]))
      content((rel: (.12, 0), to: "Stack-q1"), text(rotate(90deg)[$-$]))
      content((rel: (.12, .5), to: "Stack-q1"), text(rotate(90deg)[$-$]))
      content((rel: (.12, 1), to: "Stack-q1"), text(rotate(90deg)[$-$]))
      content((rel: (0, 1.5), to: "Stack-q1"), text($Y_1$))

      // Pila de q2
      rect((-.5, 3.5 + 5), (rel: (.5, 1), to: "q2"), name: "Stack-q2", radius: 5%)
      content((rel: (0, -1.5), to: "Stack-q2"), text($Y_n$))
      content((rel: (.12, -1), to: "Stack-q2"), text(rotate(90deg)[$-$]))
      content((rel: (.12, -.5), to: "Stack-q2"), text(rotate(90deg)[$-$]))
      content((rel: (.12, 0), to: "Stack-q2"), text(rotate(90deg)[$-$]))
      content((rel: (.12, .5), to: "Stack-q2"), text(rotate(90deg)[$-$]))
      content((rel: (.12, 1), to: "Stack-q2"), text(rotate(90deg)[$-$]))
      content((rel: (0, 1.5), to: "Stack-q2"), text($Y_2$))

      // Pila de qn
      rect((4, .5 + 5), (rel: (.5 + .5, 1), to: "qn"), name: "Stack-qn", radius: 5%)
      content((rel: (0, -1.5), to: "Stack-qn"), text($Y_n$))

      // Pila de p
      rect((7 - .5, -1 + 5), (rel: (.5, 1), to: "p"), name: "Stack-p", radius: 5%)
    })
  ]

  Existen $q_2, ..., q_(n+1) in Q$ tales que $1 <= j <= n, k_i < i, (q_j, omega'_j, Y_j) sequent^(k_i)_M (q_(j+1), lambda, lambda)$. \

  Por *hipótesis inductiva*, \
  para cada $1 <= j <= n$, si $(q_j, omega'_j, Y_j) sequent^(k_i)_M (q_(j+1), lambda, lambda)$, entonces $[q_j, Y_j, q_(j+1)] =>^(*)_G omega'_j$. \

  Pero en $G$ tenemos la producción $([q, Z, q_(n+1)] to x[q_1, Y_1, q_2] ... [q_n, Y_n, q_(n+1)]) in P$. \

  Usando que para cada $j$, $[q_j, Y_j, q_(j+1)] =>^(*)_G omega'_j$, tenemos que \
  $[q, Z, q_(n+1)] =>^(*)_G x omega'_1 ... omega'_n = x omega' = omega$. \

2. $implicaVuelta2$) Veamos por inducción que, $forall i in NN_(>=1)$, se cumple que \
#align(center)[
   $forall q in Q, forall Z in Gamma, forall p in P, [q, Z, p] =>^i_G omega implica (q, omega, Z) sequent^i_M (p, lambda, lambda)$.
]

Sea $x in Sigma union {lambda}.$

- *Caso base:* $i = 1$. \
  Tenemos $[q, Z, p] =>^1_G x$, lo que implica que $([q, Z, p] to x) in P$. \
  Luego, por definición de $M$, $(p, lambda) in delta(q, x, Z)$, lo que implica que $(q, x, Z) sequent_M (p, lambda, lambda)$. \

- *Paso inductivo:* $i > 1$. \
  Si $[q, Z, p] =>^i_G omega$, entonces $[q, Z, p] =>_G x [q_1, Y_1, q_2] ... [q_n, Y_n, p] =>^(i-1)_G omega$. \

  Descomponemos $omega$ como $omega = x omega_1 ... omega_n$ tal que para cada $1 <= j <= n$, cada derivación toma menos de $i$ pasos: $[q_j, Y_j, q_(j+1)] =>^(k_i)_G omega_j$, con $k_i < i$. \

  Por *hipótesis inductiva*, para cada $1 <= j <= n, (q_j, omega_j, Y_j) sequent^(*)_M (q_(j+1), lambda, lambda)$. \

  Entonces, $(q_j, omega_j, Y_j, Y_(j+1), ..., Y_n) sequent^(*)_M (q_(j+1), lambda, Y_(j+1), ..., Y_n)$. \
  Partimos de $[q, Z, p] =>_G x [q_1, Y_1, q_2] ... [q_n, Y_n, p]$. \
  Por definición de $M$, $(q, x, Z) sequent_M (q_1, lambda, Y_1 ... Y_n)$. \

  Llamando $p$ al $q_(n+1)$, obtenemos $(q, x y_1 ... y_n, Z) sequent_M (q_1, y_1 ... y_n, Y_1 ... Y_n) sequent^(*)_M (p, lambda, lambda)$. \

#QED

Volviendo a la demostración del teorema, \
El lema de recién dice que \
#align(center)[
  $forall q in Q, forall Z in Gamma, forall p in P, (q, omega, Z) sequent^+_M (p, lambda, lambda) sii [q, Z, p] =>^+_G omega$.
]

Tomando $q = q_0$ y $Z = Z_0$, tenemos que \
#align(center)[
  $forall p in P, (q_0, omega, Z_0) sequent^+_M (p, lambda, lambda) sii [q_0, Z_0, p] =>^+_G omega$.
]

Por la definición de $G$, $(S to [q_0, Z_0, q]) in P$, entonces, \
#align(center)[
  $forall p in P, (q_0, omega, Z_0) sequent^+_M (p, lambda, lambda) sii S =>^+_G omega$.
]

O, lo que es lo mismo,
#align(center)[
  $omega in LenguajeDe_(lambda)(M) sii omega in LenguajeDe(G)$.
]

#QED

= Lenguajes libres de contexto: lema de pumping, algoritmos de decisión, propiedades de clausura, gramáticas ambiguas y lenguajes inherentemente ambiguos

== Lenguajes libres de contexto

Los lenguajes libres de contexto son:
- Exactamente los lenguajes generados por las gramáticas libres de contexto \ $G = Gramática(V_N, V_T, P, S)$. \
- Exactamente los lenguajes aceptados por los autómatas de pila $M = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$. \

Y, como veremos más adelante, se reconocen en tiempo cúbico, tomando como parámetro la longitud de la palabra _(algoritmo CYK)_. \

== Lema de pumping para lenguajes libres de contexto

Para todo lenguaje $L$ libre de contexto, $exists n > 0$ tal que para $forall alpha in L$ con $|alpha| >= n$: \
- Existe una descomposición de $alpha$ en cadenas $r, x, y, z, s$, es decir, $alpha = r x y z s$.
  - Cumpliendo que:
    - $|x y z| <= n$.
    - $|x z| >= 1$.
    - $forall i >= 0$, la cadena $r x^i y z^i s in L$. \

=== Demostración

Sea $G = Gramática(V_N, V_T, P, S)$ una gramática libre de contexto tal que $L = LenguajeDe(G)$. \
Sea $a = max{k : (k = |beta|, (A to beta) in P, beta != lambda) or (k = 1, (A to lambda) in P)}$. \

Tomemos $n = a^(|V_N| + 1)$. \
Sea $alpha in L$ tal que $|alpha| >= n$. \
Sea $Tree(S)$ un árbol de derivación de la cadena $alpha$ en $G$, de altura mínima. \
#text(comment)[Aún no lo vimos, pero una misma cadena $alpha$ en una gramática $G$ puede tener $1, 2,$ o $infinity$ árboles de derivación. De entre todos nos quedamos cualquiera que posea altura mínima.] \

Por el lema visto en la #underline[@lema_cota_longitud_palabra[sección]], $a^h >= |alpha|$, con $h$ la altura de $Tree(S)$. \

Por lo tanto, $a^h >= |alpha| >= n = a^(|V_N| + 1)$. \

Separemos la demostración en dos casos: \

1. $a = 0$. \
Por definición de $a$, $a = 0$ no es posible. \
$to a >= 1$. \

2. $a = 1$. \

Si $a = 1$, implica que el lenguaje $lambda in L$ o que todas las cadenas $!= lambda$ en $L$ tienen longitud $1$. \
Es decir, en $L$ las cadenas no tienen longitud mayor a $1$. \
Entonces, seleccionando $n = 2$, el lema de pumping se hace trivialmente verdadero (pues es cierto que $exists n$ tal que toda la implicación se hace verdadera; en particular el antecedente $|alpha| >= n$ sería falso, y por lo tanto la implicación sería verdadera). \

3. $a >= 2$. \

Luego, $h >= |V_N| + 1$. \
#text(comment)[Esto $arrow.t$ vale *sii* $a >= 2$.] \

Entonces, $h >= |V_N| + 1$ quiere decir que la altura del árbol de derivación de la cadena $alpha$ va a ser mayor o igual a la cantidad de no terminales de la gramática $G$, más uno. \
Es decir, va a existir algún símbolo en $alpha$ tal que su camino de longitud será $h >=|V_N| + 1$. \

Luego, como la cantidad de símbolos no terminales es $|V_N|$, entonces en ese camino de longitud $h$ va a haber al menos un no terminal que se repite. \

Llamemos $A$ a ese no terminal repetido. Recorriendo dicho camino *en forma ascendente*, consideremos la primera y la segunda aparición de dicho no terminal, tal como se ve en la figura. \


#align(center)[
  #image("./assets/Pumping_Libres_de_Contexto.svg", width: 50%)
]

La segunda aparición de $A$ da lugar a la cadena $x y z$. \
Como tenemos la garantía de que esa segunda aparición de $A$ está \ a una distancia $h' <= |V_N| + 1$ respecto de la base (no hay no termnales repetidos en el subárbol $Tree(A)$ de la cadena $y$ de altura $h'$, pues agarramos la primera aparición), entonces es cierto que

#align(center)[
  $|x y z| <=_#text[@lema_cota_longitud_palabra[lema de la sección]] h' <= |V_N| + 1 = n$.
]

Notar que $x$ y $z$ no pueden ser simultáneamente nulas, ya que sino el árbol de derivación para $alpha$ no sería de altura mínima. \

Es decir, como el árbol considerado en esta demostración es de *altura mínima* de entre todos aquellos que sirven para generar la cadena $alpha$, siempre se puede encontrar un camino de longitud máxima entre la raíz ($S$) y alguna hoja, en el que $x$ y $z$ no sean simultáneamente nulas. \

Esto ocurre porque, de no ser así, todos los caminos que van desde la raíz $S$ hasta una hoja, que además sean de longitud máxima (o sea, igual a la altura $h$ del árbol) podrían ser #text(important)["colapsados"] y entonces, de este modo, podría obtenerse un árbol de derivación para la cadena $alpha$ que tendría *menor altura*, lo que es una #text(important)[*contradicción*]. \

Luego, existen cadenas $r, x, y, z, s in V^*_T$ tales que $alpha = r x y z s$, $|x y z| <= n$, $|x z| >= 1$ y
#align(center)[
  $S =>^* r A s =>^* r x A z s =>^* r x y z s$.
]

Por lo tanto tenemos $A =>^* y$ y también $A =>^* x A z$. \

Demostremos ahora que $forall i >= 0$, $r x^i y z^i s in L$, por inducción en $i$. \

- *Caso base:* $i = 0$. \
  Tenemos que $S =>^* r A s =>^* r y s$. \
  Por lo tanto, $r x^0 y z^0 s = r y s in L$.

- *Paso inductivo:* $i > 0$. \
  Asumimos que $r x^i y z^i s in L$. \
  Veamos que se cumple para $i + 1$.  Sabemos que $S =>^* r x^i A z^i s =>^* r x^i y z^i s$. \

  Pero como $A =>^* x A z$ podemos decir que $r x^i A z^i s =>^* r x^(i+1) y z^(i+1) s$. \
  Luego nos queda $S =>^* r x^i A z^i s =>^* r x^(i+1) A z^(i+1) s =>^* r x^(i+1) y z^(i+1) s$. \

  Por lo tanto, $r x^(i+1) y z^(i+1) s in L$. \

#QED

== Algoritmos de decisión para lenguajes libres de contexto

=== Decidir si un lenguaje libre de contexto es vacío

Sea $n$ la constante del lema de pumping para lenguajes libres de contexto. \
$L = emptyset sii$ ninguna palabra de longitud menor que $n$ pertenece a $L$. \

#text(important)[Si hubiera una de longitud $n$ o más, por el lema de pumping también habría una de longitud menor que $n$.]

#algorithm({
  import algorithmic: *
  Function("ObtenerNoTerminalesActivos", args: ($G = Gramática(V_N, V_T, P, S)$,), {
    Cmt[Algoritmo para obtener el conjunto de no terminales *activos* de una gramática.]
    State[]
    Assign[$i$][$1$]
    Assign[$N_0$][$emptyset$]
    Assign[$N_1$][${A : (A to alpha) in P, alpha in V_T^*}$]
    While(cond: $N_i != N_(i-1)$, {
      Assign[$i$][$i + 1$]
      Assign[$N_i$][${A : (A to alpha) in P, alpha in (N_(i-1) union V_T)^*} union N_(i-1)$]
    })
    Return[$N_i$]
  })
})

#algorithm({
  import algorithmic: *
  Function("ElLenguajeGeneradoPorLaGramáticaEsVacío?", args: ($G = Gramática(V_N, V_T, P, S)$,), {
    Cmt[Algoritmo para decidir si el lenguaje generado por una gramática es vacío.]
    State[]
    Assign[$V_N_"activos"$][ObtenerNoTerminalesActivos($G$)]
    Return[$S in.not V_N_"activos"$]
  })
})

=== Decidir si un lenguaje libre de contexto es finito

Sea $n$ la constante del lema de pumping para lenguajes libres de contexto. \
$L$ es finito $sii$ ninguna palabra de longitud entre $n$ y $2n - 1$ pertenece a $L$. \

==== Demostración

  - $implica2)$
  Asumimos que $L$ es finito. \
  Supongamos que tiene una palabra $alpha$ de longitud entre $n$ y $2n - 1$. \
  Por el lema de pumping, esa palabra se puede descomponer como $alpha = r x y z s$ con $|x y z| <= n$ y $|x z| >= 1$, haciendo cierto que $r x^i y z^i s in L$ para todo $i >= 0$. \
  Sin embargo, eso implica que $L$ tiene infinitas palabras de longitud mayor que $n$. Absurdo. \

  - $implicaVuelta2)$ \
  Asumimos que $L$ no tiene ninguna palabra de longitud entre $n$ y $2n - 1$. \
  Supongamos que $L$ es infinito. \
  #text(comment)[Notar que si todas las palabras de $L$ tienen longitud $< n$, entonces $L$ es finito trivialmente.] \
  Sea $alpha$ la palabra en $L$ de longitud más corta, tal que su longitud sea mayor o igual que $2n$. \
  El lema de pumping afirma que existe una palabra más corta que $alpha$ tal que esa palabra también pertenece $L$. Luego, hay una factorización $alpha = r x y z s$ con $|x y z| <= n$, $|x z| >= 1$ y tenemos que $r x^0 y z^0 s = r y s in L$. \

  Dado que $|x y z| <= n$ y $|x z| >= 1$, \
  #align(center)[
    $#text(important)[$|alpha| - n$] <= |alpha| - |x y z| = |r s| #text(important)[$<= |r y s|$] = |alpha| - |x z| #text(important)[$<= |alpha| - 1$]$. \
  ]

  Si $|alpha| = 2n$, nos queda $n <= |r y s| <= 2n - 1$, que contradice nuestro antecedente. \
  Si $|alpha| > 2n$, entonces $|r y s| < |alpha|$. Luego, o bien $|r y s|$ es entre $n$ y $2n - 1$ y llegamos a una contradicción, o bien $|r y s| >= 2n$, contradiciendo que $alpha$ era la palabra de longitud más corta, tal que su longitud era $>= 2n$. \

  #QED

=== Decidir si una palabra pertenece a un lenguaje libre de contexto

Hay un algoritmo para decidir la pertenecia de una palabra a un lenguaje
libre de contexto. Se llama algoritmo de parsing CYK o Earley. \

=== Cota en la cantidad de pasos en una derivación

Por el lema visto en la #underline[@lema_libre_de_contexto[sección]], podemos obtener un *corolario* tomando $w = lambda$. \
Sea $G$ libre de contexto y no recursiva a izquierda. Existe una constante $c$ tal que para todo par de no terminales $A, B$, si $A =>^i_L B alpha$ entonces $i <= c^2$. \

== Propiedades de clausura de los lenguajes libres de contexto

Los lenguajes libres de contexto *están cerrados* por
- unión,
- concatenación,
- reversa,
- y clausura de Kleene.

*No están cerrados* por intersección, diferencia, ni complemento. \

Sin embargo, *la intersección de libre de contexto con regular es libre de contexto*. \

=== Unión $checkmark$

Supongamos $G_1 = Gramática(V_N_1, V_T_1, P_1, S_1)$ y $G_2 = Gramática(V_N_2, V_T_2, P_2, S_2)$ dos gramáticas libres de contexto. \
Definimos $G = Gramática(V_N_1 union V_N_2 union {S}, V_T_1 union V_T_2, P, S)$, donde $P = P_1 union P_2 union {S to S_1|S_2}$. \

=== Concatenación $checkmark$

Supongamos $G_1 = Gramática(V_N_1, V_T_1, P_1, S_1)$ y $G_2 = Gramática(V_N_2, V_T_2, P_2, S_2)$ dos gramáticas libres de contexto. \
Definimos $G = Gramática(V_N_1 union V_N_2 union {S}, V_T_1 union V_T_2, P, S)$, donde $P = P_1 union P_2 union {S to S_1 S_2}$. \

=== Reversa $checkmark$

Supongamos $G = Gramática(V_N, V_T, P, S)$ una gramática libre de contexto donde $L = LenguajeDe(G)$. \
Definimos $G' = Gramática(V_N, V_T, P', S)$ tal que $L^r = LenguajeDe(G')$ así:

- Para cada producción $(X to alpha) in P$, ponemos $(X to alpha^r) in P'$. \

- Supongamos que $(S to u X v) in P$ y $(X to alpha) in P$. \
  Entonces, ponemos $(S to v^r X u^r) in P'$ y $(X to alpha^r) in P'$. \
  Luego, $S =>_G u alpha v$ y $S =>_G' v^r alpha^r u^r$. \

Dado que $v^r alpha^r u^r = (u alpha v)^r$, tenemos que $S =>_G' (u alpha v)^r$. \

=== Clausura de Kleene $checkmark$

Supongamos $G = Gramática(V_N, V_T, P, S)$ una gramática libre de contexto. \
Definimos $G' = Gramática(V_N union {S'}, V_T, P', S')$, donde $P' = P union {S' to S S'|lambda}$. \

=== Intersección $crossmark$

Sean $L_1 = {a^i b^j c^j}$ y $L_2 = {a^i b^i c^j}$ dos lenguajes libres de contexto. \
Notemos que $L_1 sect L_2 = {a^i b^i c^i}$ no es libre de contexto. \

=== Diferencia $crossmark$

Si lo fuese, entonces $SigmaEstrella - L$ debería ser libre de contexto.

=== Complemento $crossmark$

Supongamos que el complemento fuera libre de contexto. \
Entonces, $L_1 sect L_2 = ComplementoConjunto(ComplementoConjunto(L_1) union ComplementoConjunto(L_2))$ sería ser libre de contexto. \

#QED

== Gramáticas ambiguas

Una gramática libre de contexto $G$ es ambigua si existe una palabra $alpha in LenguajeDe(G)$ con más de una derivación más a la izquierda. \

=== Ejemplo

Dada la gramática $G = Gramática({E}, {+, *, #text(weight: "bold")[id], #text(weight: "bold")[const]}, P, E)$ con \
#align(center)[
  $(E to E + E) in P$ \
  $(E to E * E) in P$ \
  $(E to #text(weight: "bold")[id]) in P$ \
  $(E to #text(weight: "bold")[const]) in P$ \
]

Para $G$ podemos dar dos árboles  de derivación distintos para la palabra *$id + id * id$*: \

#grid(
  columns: (1fr, 1fr),
  [
    #align(center)[
      #syntree(
        terminal: (weight: "bold"),
        child-spacing: 3em, // default 1em
        layer-spacing: 2em, // default 2.3em
        "[$E$ [$E$ id] $+$ [$E$ [$E$ id] $*$ [$E$ id]]]"
      )
    ]
  ],
  [
    #align(center)[
      #syntree(
        terminal: (weight: "bold"),
        child-spacing: 3em, // default 1em
        layer-spacing: 2em, // default 2.3em
          "[$E$ [$E$ [$E$ id] $+$ [$E$ id]] $*$ [$E$ id]]"
      )
    ]
  ],
)

== Lenguaje inherentemente ambiguo

Un lenguaje libre de contexto es inherentemente ambiguo si solamente admite gramáticas ambiguas. \

=== Ejemplo

Este lengueje es inherentemente ambiguo:
#align(center)[
  ${a^n b^m c^m d^n | n, m > 0} union {a^n b^n c^m d^m | n, m > 0}$.
]

Es libre de contexto porque es la unión de dos conjuntos que lo son. \
Hopcroft y Ullman (1979) mostraron que no hay forma de derivar de manera no ambigua las cadenas de la intersección de ambos lenguajes, es decir del conjunto
#align(center)[
  ${a^n b^n c^n d^n | n > 0}$,
]
además este conjunto intersección no es libre de contexto. \

*El problema de si una gramática es ambigua es indecidible.* \

Es equivalente al #link("https://es.wikipedia.org/wiki/Problema_de_correspondencia_de_Post")[Problema de Correspondencia de Post]: \
Dadas dos listas de palabras sobre un alfabeto (con al menos dos símbolos) $alpha_1, ..., alpha_N$ y $beta_1, ..., beta_N$, decidir si hay una secuencia de índices $(i_k)_1^k$ con $K >= 1$ y $1 <= i_k <= N$ tal que $alpha_(i_1) ... alpha_(i_K) = beta_(i_1) ... beta_(i_K)$. \

Por ejemplo, \
$alpha_1 = a$, $alpha_2 = a b$, $alpha_3 = b b a$ \
$beta_1 = b a a$, $beta_2 = a a$, $beta_3 = b b$. \

Solución: $(3, 2, 3, 1)$ porque \
#align(center)[
  $alpha_3 alpha_2 alpha_3 alpha_1 = b b a + a b + b b a + a = b b a a b b a a = b b + a a + b b + b a a = beta_3 beta_2 beta_3 beta_1$.
]

== Decisión de lenguajes libres de contexto

Sea $G$ una gramática libre de contexto y $L = LenguajeDe(G)$. \

*Hay algoritmos para:*
- Decidir si $L = emptyset$. \
- Decidir si $L$ es finito. \
- Decidir si $L$ es infinito. \
- Decidir si una palabra $alpha in SigmaEstrella$ pertenece a $L$, #text(important)[algoritmo CYK]. \
  #text(comment)[En tiempo del orden cúbico en la longitud de $w$.] \
  #text(comment)[En casos especiales _(determinismo)_, en tiempo lineal.] \

*No hay algoritmos para:*
- Decidir si $L$ es regular. \
- Decidir si $G$ una gramática libre de contexto es ambigua. \
- Decidir si $L_1 = L_2$, con $L_1, L_2$ lenguajes libres de contexto. \
- Decidir si $L = SigmaEstrella$. \
- Decidir si $L_1 subset.eq L_2$, con $L_1, L_2$ lenguajes libres de contexto. \
- Decidir si $L_1 sect L_2 = emptyset$, con $L_1, L_2$ lenguajes libres de contexto. \

== Preguntas<demo_intersección_leng_regular_y_leng_libre_de_contexto>

1. Sea $L$ un lenguaje. Si todas las palabras de $L$ validan el lema de pumping para lenguajes libres de contexto, ¿podemos concluir que $L$ es un lenguaje libre contexto? \ No, no vale la $implicaVuelta2$.

2. Sea $L$ un lenguaje regular. Demostrar que todas las palabras de $L$ validan el lema de pumping para lenguajes libres de contexto. \ Basta tomar $z = s = lambda$ en la descomposición de la palabra.
3. Mostrar que $L = \{a^p : p "es número primo"$ no es libre de contexto. \
   Ayuda: asumir $L$ es libre de contexto, y $n$ es la longitud dada por el lema de pumping. Sea $m$ el primer primo mayor o igual que $n$, considerar $alpha = a^m$ y bombear $m + 1$ veces. \
   Elegís $alpha = a^m "tal que" m "es el primer primo mayor o igual que n"$, sin perdida de generalidad, podemos decir que la cadena $alpha$ se puede descomponer como $alpha = r x y z s$, tal que \
   $r = a^b, x = a^c, y = a^d, z = a^e, s = a^f$, donde \
   $b, c, d, e, f >= 0$, \
   $f = m-b-c-d-e$, \
   $c + d + e <= n$, \
   $c + e >= 1$ y \
   $b + c + d + e + f = m$. \

   Luego, tomando $i = m +1$ nos queda: $alpha_(i = m + 1) = a^((c+e+1)m)$, que no pertenece a $L$ pues $(c+e+1)m$ no es primo. \
   #QED
4. Demostrar que un lenguaje regular intersección un lenguaje libre de contexto es libre de contexto. \
   Ayuda: definir el autómata de pila que lo reconoce. \
   Sea $M_R = AF(Q_R, Sigma, delta_R, q_0_R, F_R)$ un autómata finito determinístico que reconoce $L_R$ regular y $M_L = AP(Q_L, Sigma, Gamma, delta_L, q_0_L, Z_0, F_L)$ un autómata de pila que reconoce $L_L$ libre de contexto. \
   Defino $M = AP(Q_R times Q_L, Sigma, Gamma, delta, (q_0_R, q_0_L), Z_0, F_R times F_L)$, donde $delta$ se define como \
    $delta((q_R, q_L), a, Z) = {((delta_(R)(q_R, a), p), gamma) | (p, gamma) in delta_(L)(q_L, a, Z)}$. \
    Basta probar que $x in LenguajeDe(M) sii x in LenguajeDe(M_R) and LenguajeDe(M_L)$, donde $x in SigmaEstrella$. \
    #align(center)[
      $x in LenguajeDe(M) &sii delta((q_0_R, q_0_L), Z_0, x) in F \
      &sii ((delta_(R)(q_0_R, x),  p), gamma) in F_R times F_L, "con" (p, gamma) in delta_(L)(q_0_L, x, Z_0) \
      &sii delta_(R)(q_0_R, x) in F_R and (p, gamma) in F_L, "con" (p, gamma) in delta_(L)(q_0_L, x, Z_0) \
      &sii delta_(R)(q_0_R, x) in F_R and delta_(L)(q_0_L, x, Z_0) sect F_L != emptyset \
      &sii x in LenguajeDe(M_R) and x in LenguajeDe(M_L).$
    ]
    #QED
5. Demostrar que hay lenguajes que pueden ser reconocidos con un autómata con dos pilas pero no pueden ser reconocidos con un autómata con una sola pila. \
   $L = {a^n b^n c^n | n >= 0}$ es reconocido por un autómata con dos pilas, pero no por uno con una sola pila. \
   #text(comment)[¡También te lo acepta un autómata de cola!, pero, bueno, es porque un autómata de cola es equivalente a una máquina de Turing...]

= Configuraciones ciclantes, lenguajes libres de contexto determinísticos y gramáticas LR

== Configuraciones ciclantes

Es posible que un autómata de pila determinístico realice una cantidad infinita de λ-movimientos desde alguna configuración, es decir, movimientos que no leen de la cinta de entrada. \

*Definición de configuración ciclante:* \

Sea $P = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$ un autómata de pila determinístico. \
Una configuración $(q, w, alpha)$, con $|alpha| >= 1$, cicla si \
#align(center)[
  $(q, w, alpha) sequent (p_1, w, beta_1) sequent (p_2, w, beta_2) sequent ...$ \
]
con $|beta_i| >= |alpha|$ para todo $i$. \

Así, una configuración cicla si $P$ realiza un número infinito de movimientos sin leer ningún símbolo de la entrada y el tamaño de la pila es de tamaño mayor o igual que $|alpha|$. \
La pila puede crecer indefinidamente o ciclar entre distintas cadenas. \

== Lenguajes libres de contexto determinísticos

Un lenguaje $L$ es *libre de contexto determinístico* si existe un *autómata de pila determinístico* $M$ tal que $L = LenguajeDe(M)$. \
#text(comment)[Se reconocen en tiempo lineal, algoritmo CYK.] \
//Son exactamente los lenguajes $LR(k)$ para $k >= 1$. \

*Un lenguaje libre de contexto determinístico implica que admiten una gramática no ambigua.* \

*Ejemplos:* \
- $L = {a^n b^n}$ \
- $L = {a^i b^i a^j : i, j >= 1}$ \

*Contraejemplos:* \
- $L_0 = {a^i b^i c^i : i >= 1}$ no es libre de contexto (lema de pumping) \
- $L_1 = {a^i b^j a^j : i != j}$ no es libre de contexto (lema de Ogden) \
- $L_2 = {a^i b^j a^j : i != j} union {a^i b^i a^j : i != j}$ no es libre de contexto \
- $L_3 = {a^i b^i a^j : i, j >= 1} union {a^i b^j a^j : i, j >= 1}$ es libre de contexto no determinístico \

== APD sin ciclos

Para todo *autómata de pila determinístico* $P$ *existe otro* autómata de pila determinístico $P'$ tal que $LenguajeDe(P) = LenguajeDe(P')$ y $P'$ *no tiene configuraciones ciclantes*. \

== Lema

Sea $P = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$ un autómata de pila determinístico. \

Si $(q, w, A) sequent^n (q', lambda, lambda)$ entonces para toda $A in Gamma$ y $alpha in Gamma^*$, $(q, w, A alpha) sequent^n (q', lambda, alpha)$. \

#QED

== Detección de configuraciones ciclantes

Dado un autómata de pila *determinístico* $P = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$, la función de transición es una función parcial $delta : Q times (Sigma union {lambda}) times Gamma to Q times Gamma^*$. \

La máxima cantidad de transiciones que $P$ puede hacer sin leer de la entrada y sin repetir el estado ni el tope de la pila es $|Q| times |Gamma|$. \

En cada transición se escriben en la pila a lo sumo $cal(l)$ símbolos de $Gamma$. \

La cantidad máxima de configuraciones distintas sin leer la entrada contando la pila entera es: \
#align(center)[
  $|Q| times "(la cantidad de posibles pilas de longitud hasta" |Q|cal(l)|Gamma|)$ \

  $|Q| sum^(|Q|cal(l)|Gamma|)_(i=0) |Gamma|^i =_"progresión geométrica" |Q| frac(|Gamma|^(|Q|cal(l)|Gamma|+1)-1, |Gamma|-1)$. \
]

Este es el máximo de transiciones-λ que $P$ puede hacer sin ciclar. \

== APD que leen toda la entrada

Un APD $P = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$ es continuo si para toda $w in Sigma^*$ existe $p in Q$ y $gamma in Gamma^*$ tales que $(q_0, w, Z_0) sequent^* (p, lambda, gamma)$. \
Es decir, APD $P$ es continuo si lee toda la cadena de entrada. \

=== Lema

Para todo APD, existe otro equivalente y continuo. \

==== Demostración del lema por algoritmo

#link("https://www-2.dc.uba.ar/staff/becher/Aho-Ullman-Parsing-V1.pdf")[*Algoritmo 2.16, Aho Ullman, vol. 1, pág 187.*] \

Sea $P = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$ un APD. Construimos un APD $P'$ equivalente y continuo. \

Sean,
#align(center)[
  $C_1 &= {(q, Z) | (q, lambda, Z) "es una configuración ciclante y no existe" \ &g in F "para ningún" alpha in Gamma^* "tal que" (q, lambda, Z) sequent^* (g, lambda, alpha)}, \
  C_2 &= {(q, Z) | (q, lambda, Z) "es una configuración ciclante y hay un" \ &g in F " y " alpha in Gamma^* "tales que" (q, lambda, Z) sequent^* (g, lambda, alpha)}.$ \
]

Supongamos $P$ siempre tiene una próxima transición. \
Sea $P' = AP(Q union {f, t}, Sigma, Gamma, delta_0, q_0, Z_0, F union {f})$ donde $f$ y $t$ son nuevos. \
La función de transición $delta' : Q times (Sigma union {lambda}) times Gamma to Q times Gamma^*$ se define así: \
- Para todo $(q, Z) in.not (C_1 union C_2)$, $delta'(q, lambda, Z) = delta(q, lambda, Z)$. \
- Para todo $(q, Z) in C_1$, $delta'(q, lambda, Z) = (t, Z)$. \
- Para todo $(q, Z) in C_2$, $delta'(q, lambda, Z) = (f, Z)$. \
- Para todo $a in Sigma$ y $Z in Gamma$, $delta'(f, a, Z) = (t, Z)$ y $delta'(t, a, Z) = (t, Z)$. \

#QED

== Propiedades de clausura lenguajes libres de contexto determinísticos

*Están clausurados por:*

=== El complemento de un lenguaje libre de contexto determinístico es otro lenguaje libre de contexto determinístico

Sea $P = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$ un APD continuo. \
Definimos $P' = AP(Q', Sigma, Gamma, delta', q'_0, Z_0, F')$ un APD donde \
#align(center)[
  $Q' = {[q,k] : q in Q and k = 0,1,2}$ \
]
El propósito de $k$ es indicar si entre transiciones con consumo de entrada en $P$ pasó o no por un estado final. \

#align(center)[
  $q'_0 = cases([q_0, 0] &"si" q_0 in.not F, [q_0, 1] &"si" q_0 in F)$ \
  $F' = {[q, 2] : q in Q}$ \
]

La semántica es la siguiente: \
- $[q, 0]$ indica que $P$ no pasó por $F$. \
- $[q, 1]$ indica que $P$ sí pasó por $F$. \
- $[q, 2]$ indica que $P$ no pasó por $F$ y $P$ va a seguir leyendo. \

Para todo $q in Q$, $[q, 2]$ es el estado final al que llega $P'$ antes de que $P$ lea un nuevo símbolo. \

*Esta parte falta completarla.* \

=== Intersección con lenguejes regulares<demo_intersección_leng_regular_y_leng_libre_de_contexto_determinístico>
Es lo mismo que lo hecho en la pregunta 4 de la #underline[@demo_intersección_leng_regular_y_leng_libre_de_contexto[sección]], solamente faltaría demostrar que si el lenguaje $L$ libre de contexto es determinístico, entonces $L sect R$ también lo es. \

A nivel intuitivo, $R$ regular se lo puede asumir generado por un autómata finito determinístico sin pérdida de generalidad, pues un AFD es equivalente a un AFND. \
Luego, con la definición de $delta$ dada en la demostración de la pregunta 4 en la #underline[@demo_intersección_leng_regular_y_leng_libre_de_contexto[sección]], para cada $delta((q_R, q_L), a, Z)$ va a existir una única tupla $((p_R, p_L), gamma)$ tal que $p_R in F_R$ y $(p_L, gamma) in F_L$. \

=== Concatenación de un lenguaje libre de contexto determinístico _(primero)_ con un lenguaje regular _(segundo)_

La demostración sale pensando en la concatenación de ambos autómatas. \

=== Prefijos
Si $pre(L)$ es el subconjunto de todas las cadenas de $L$, las cuales tienen un prefijo que también pertenece a $L$. \
Entonces, el subconjunto de cadenas #text(comment)[_(lenguaje)_] $pre(L)$ es libre de contexto determinístico. \

=== Mínimo
Si $min(L)$ es el subconjunto de todas las cadenas de $L$, las cuales no tienen un prefijo que también pertenece a $L$. \
Entonces, el subconjunto de cadenas #text(comment)[_(lenguaje)_] $min(L)$ es libre de contexto determinístico. \

=== Máximo
Si $max(L)$ es el subconjunto de todas las cadenas de $L$, las cuales no son prefijo de una cadena más larga que ellas en $L$. \
Entonces, el subconjunto de cadenas #text(comment)[_(lenguaje)_] $max(L)$ es libre de contexto determinístico. \

*No están clausurados por:*
- Intersección
- Unión
- Reversa
- Concatenación
- Clausura de Kleene

== Decisión sobre lenguajes libres de contexto determinísticos

*Hay algoritmos para:*

- ¿$L = emptyset$? \ #text(comment)[Mismo caso que el no determinístico.]
- ¿$L$ es finito? \ #text(comment)[Pumping.]
- ¿$L$ es infinito? \ #text(comment)[Pumping.]
- ¿$L$ es cofinito? \ #text(comment)[Pumping sobre $L^c$.]
- ¿$L = SigmaEstrella$? \ #text(comment)[Equivalente a preguntarse ¿$L^c = emptyset$?]
- ¿$L_1 = L_2$? \ #text(comment)[Sí, por Géraud Sénizergues, premio Gödel 2002.]
- ¿$omega in L$? \ #text(comment)[Sí, en tiempo lineal en la longitud de $omega$ (algoritmo LR).]
- ¿$L$ es regular? \ #text(comment)[Sí.] \ #text(comment)[Steans, R. (1967). A Regularity Test for Pushdown Machines. Information and Control, 11, 323-340.]
- ¿$L = R$?
- ¿$L subset.eq R$?

*No hay algoritmos para:*
- ¿$L_1 subset.eq L_2$?
- ¿$L_1 sect L_2 = emptyset$?

== Gramáticas LR

*Esta parte falta completarla.* \

== Autómatas de pila enriquecidos (autómata contador)

También llamados autómatas con un contador o autómatas contadores. \
Son autómatas de pila, donde el alfabeto de pila tiene exactamente dos símbolos: $Z_0$ y $I$. \
En cada transición pueden revisar si el contador es $Z_0$ o no, y pueden incrementarlo o decrementarlo. \

Los lenguajes reconocidos por autómatas contadores incluyen a todos los lenguajes regulares pero son un subconjunto propio de los lenguajes reconocidos por autómatas de pila. \

- Los autómatas de pila de dos vías (moverse izq./der. en la cadena de entrada $alpha$), tienen una pila, control finito y una cinta cuya cabeza puede moverse en ambas direcciones, tienen menos poder computacional que una máquina de Turing. \
- Los autómatas con dos o más pilas son equivalentes a una máquina de Turing. \
- Los autómatas con tres contadores son equivalentes a una máquina de Turing. \
- Los autómatas con dos contadores son equivalentes a una máquina de Turing. \
- Los autómatas con una cola son equivalentes a autómatas con dos pilas, por lo tanto, equivalentes a una máquina de Turing. \

= Máquinas de Turing, funciones parcialmente computables y conjuntos computablemente enumerables

== Máquinas de Turing

Una máquina de Turing es una tupla $cal(M) = MT(Q, Sigma, delta, q_0, q_f)$ donde \
- $Q$ es un conjunto finito de estados. \
- $Sigma$ es un conjunto finito de símbolos de la cinta, con $B in Sigma$. \
- $delta subset.eq Q times Sigma times Q times (Sigma union {L, R})$ es la tabla de instrucciones, finita. \
- $q_0 in Q$ es el estado inicial. \
- $q_f in Q$ es el estado final. \

Si no hay restricciones sobre $delta$ decimos que $cal(M)$ es una máquina de Turing no determinística. \

Si no hay dos instrucciones en $delta$ que empiezan con las mismas primeras dos coordenadas, decimos que $cal(M)$ es una máquina de Turing determinística. \

Por lo tanto, $delta : Q times Sigma to Q times (Sigma union {L, R})$. \

*Existe en la literatura un abundante número de definiciones alternativas, pero todas ellas tienen el mismo poder computacional.* \

La tupla $(q, s, a, q') in delta$ se interpreta como: \
#align(center)[
  Si la máquina está en el estado $q$ leyendo en la cinta el símbolo $s$, entonces escribe/realiza la acción $a$ y pasa al estado $q'$. \
]

=== Configuración instantánea de una máquina de Turing
$cal(M)$ está en estado $q$ con la cabeza en el símbolo más a la izquierda de $alpha_2$. \
Una configuración/descripción instantánea de $cal(M)$ es $alpha_1 q alpha_2$, donde $alpha_1, alpha_2 in SigmaEstrella$ y $q in Q$. \

Notar que $alpha_1$ y $alpha_2$ pueden tener blancos adentro. \

=== Transición entre configuraciones instantáneas $sequent$
Supongamos $X_1 ... X_(i-1) q X_i ... X_n$ es una configuración instantánea de $cal(M)$. \

Si $(p, Y) in delta(q, X_i)$ con $Y in.not {L, R}$, entonces para $i>1$,
#align(center)[
  $X_1 ... X_(i-1) q X_i ... X_n sequent_(cal(M)) X_1 ... X_(i-1) p Y ... X_n$ \
]

Si $(p, L) in delta(q, X_i)$, entonces para $i>1$,
#align(center)[
  $X_1 ... X_(i-1) q X_i ... X_n sequent_(cal(M)) X_1 ... X_(i-2) p X_(i-1) X_i ..., X_n$ \
]

Si $(p, R) in delta(q, X_i)$, entonces para $i>1$,
#align(center)[
  $X_1 ... X_(i-1) q X_i ... X_n sequent_(cal(M)) X_1 ... X_(i-1) X_i p X_(i+1) ... X_n$ \
]

$cal(M)$ se detiene en una configuración instantánea cuando no hay un próximo movimiento, porque $delta$no tiene una instrucción para eso. \

=== Lenguaje aceptado por una máquina de Turing
El lenguaje aceptado por una máquina de Turing $cal(M) = MT(Q, Sigma, delta, q_0, q_f)$ es el conjunto de palabras $w in (Sigma without {B})^*$ que causan que $cal(M)$ llegue a un estado final,
#align(center)[
  $LenguajeDe(cal(M)) = {w in (Sigma without {B})^* : q_0 w sequent^* alpha q_f beta, "donde" alpha, beta in SigmaEstrella}$ \
]

== Formas de usar una máquina de Turing
1. Aceptadoras de subconjuntos computablemente enumerables en $(Sigma without {B})^*$. \
2. Funciones $f : NN to NN$ Turing computables. \
3. Enumeradoras de conjuntos computablemente enumerables en $(Sigma without {B})^*$. \


== Lenguajes computablemente enumerables
Un lenguaje $L subset.eq (Sigma without {B})^*$ es computablemente enumerable (abreviado, c.e.) si existe una máquina de Turing $cal(M)$ tal que $LenguajeDe(cal(M)) = L$. \

Equivalentemente, \
si $L subset.eq (Sigma without {B})^*$ representa el dominio de $f : NN to NN$ parcial computable, es decir, si hay una máquina de Turing $cal(M)$ que computa $f$. \

Equivalentemente, \
si $L subset.eq (Sigma without {B})^*$ es el lenguaje generado por una máquina de Turing, $GeneradoPor(cal(M)) = L$. \

== Para toda máquina de Turing $cal(M_1)$ multicinta, existe una máquina de Turing $cal(M_2)$ de una sola cinta tal que $LenguajeDe(cal(M_1)) = LenguajeDe(cal(M_2))$

*Esta parte falta completarla.* \

== Para toda máquina de Turing $cal(M_1)$ no determinística, existe una máquina de Turing $cal(M_2)$ determinística tal que $LenguajeDe(cal(M_1)) = LenguajeDe(cal(M_2))$

*Esta parte falta completarla.* \

= Cheat sheet

== General
- Un AFND es equivalente a un AFD.
- Los APND por pila vacía y por estado final tienen el mismo poder expresivo.
- Un APD por estado final tiene un mayor poder expresivo que un APD por pila vacía.
- Los APD por pila vacía aceptan solamente lenguajes libres de prefijos.
- Todo lenguaje finito es regular.
- Cuando hablamos del $n$ del lema de pumping, podemos decir que:
  - Si es un lenguaje regular, $n = "cantidad de estados de un AFD que genera" L$.
  - Si es un lenguaje libre de contexto, $n = a^(|V_N| + 1)$ donde,
    - $a = "la cantidad máxima de hijos que puede tener un nodo"$.
    - $V_N = "conjunto de no terminales activos"$.

== Lenguajes regulares

=== Hay algoritmos para decidir si:
Sea $L$ un lenguaje regular y $w in L$ una cadena.
- $w in L$
  - _Complejidad $O(n)$ si es determinístico, donde n es la longitud de la cadena._
  - _Si no es determinístico, se puede convertir a determinístico y luego aplicar el algoritmo. El costo de determinizar es exponencial en la cantidad de estado del autómata._
- $L != emptyset$
  - Basta ver si existe cadena de longitud $< n$ _($n$ del lema de pumping)_.
  - Basta buscar los no terminales activos de la gramática que genera el lenguaje regular, luego ver si $S$ pertenece al conjunto de los no terminales activos $(in S)$.
- $L$ es infinito
  - Basta ver si existe alguna cadena con longitud entre $n$ y $2n-1$ _($n$ del lema de pumping)_.
- $L = emptyset$
  - Basta ver si *no existe ninguna* cadena de longitud $< n$ _($n$ del lema de pumping)_.
  - Basta buscar los no terminales activos de la gramática que genera el lenguaje regular, luego ver si $S$ *no* pertenece al conjunto de los no terminales activos $(in.not S)$.
- $L$ es finito
  - Basta ver si *no existe ninguna* cadena con longitud entre $n$ y $2n-1$ _($n$ del lema de pumping)_.

En la teórica vimos métodos efectivos para pasar de:
- *Autómata finito* a *gramática regular*.
- *Gramática regular* a *autómata finito*.
- *Autómata finito* a *expresión regular*.
- *Expresión regular* a *autómata finito*.

Por lo que, las 3 representaciones de lenguajes regulares son equivalentes.

=== Están cerrados por:
- Unión
- Intersección
- Complemento
- Concatenación 
  - _$arrow.t$ Todo esto lo hace un álgebra de Boole._
- Reversa
- Diferencia $to L_1 - L_2 = L_1 sect ComplementoConjunto(L_2)$
- Diferencia simétrica $to L_1 xor L_2 = (L_1 - L_2) union (L_2 - L_1)$
- Clausura de Kleene

== Lenguajes libres de contexto (no determinísticos)

=== Hay algoritmos para decidir si:
Sea $L$ un lenguaje libre de contexto y $w in L$ una cadena.
- $w in L$
  - _Complejidad $O(n^3)$ por el algoritmo CYK, donde n es la longitud de la cadena._
- Obtener los no terminales activos de la gramática que genera el lenguaje libre de contexto.
  - _Ver algoritmo en el apunte._
- $L != emptyset$
  - _$arrow.t$ Explicación en $*$._
- $L$ es infinito
  - _$arrow.t$ Explicación en $*$._
- $L = emptyset$
  - _$arrow.t$ Explicación en $*$._
- $L$ es finito 
  - _$arrow.t *$ Son los mismos que para lenguajes regulares, solo que el $n$ del lema de pumping es el $n$ lema de pumping para lenguajes libres de contexto._

En la teórica vimos métodos efectivos para pasar de:
- *Autómata finito* a *gramática regular*.
- *Gramática regular* a *autómata finito*.

=== No hay algoritmos para decidir si:
Sean $L, L_1, L_2$ lenguajes libres de contexto
- $L$ es regular
- Una gramática $G$ que genera $L$ es ambigua
- $L = Sigma^*$
- $L_1 subset.eq L_2$ es libre de contexto
- $L_1 sect L_2$ es libre de contexto

=== Están cerrados por:
- Unión
- Intersección con un lenguaje regular
- Concatenación
- Reversa
- Clausura de Kleene

=== No están cerrados por:
- Intersección
- Complemento
- Diferencia

== Lenguajes libres de contexto (determinísticos)

=== Hay algoritmos para decidir si:
Sean $L, L_1, L_2$ lenguajes libres de contexto, $R$ un lenguaje regular y $w in L$ una cadena.
- $w in L$
  - _Complejidad $O(n)$ por el algoritmo CYK, donde n es la longitud de la cadena._
- Obtener los no terminales activos de la gramática que genera el lenguaje libre de contexto.
  - _Ver algoritmo en el apunte._
- $L != emptyset$
  - _$arrow.t$ Idem no determinísticos._
- $L$ es infinito
  - _$arrow.t$ Idem no determinísticos._
- $L = emptyset$
  - _$arrow.t$ Idem no determinísticos._
- $L$ es finito
  - _$arrow.t$ Idem no determinísticos._
- $L$ es cofinito
- $L = Sigma^*$
- $L_1 = L_2$
- $L$ es regular
- $L = R$
- $L subset.eq R$

=== No hay algoritmos para decidir si:
Sean $L_1, L_2$ lenguajes libres de contexto.
- $L_1 subset.eq L_2$
- $L_1 sect L_2 = emptyset$

=== Están cerrados por:
- Complemento
- Intersección con un lenguaje regular
- $LR$
  - Concatenación de un lenguaje $L$ libre de contexto con un lenguaje regular $R$.
- $pre(L)$
  - Prefijos de un lenguaje $L$ libre de contexto.
- $min(L)$
- $max(L)$

=== No están cerrados por:
- Unión
- Intersección
- Concatenación
- Reversa
- Clausura de Kleene

= Ejercicios de práctica para el final

== Demostración de que la complejidad de Kolmogorov no es cumputable
Se define la complejidad de Kolmogorov como $K : SigmaEstrella to NN$ tal que \
#align(center)[
  $K(s) = min{|p| : Psi^(1)(p) = s}$. \
]
Es decir, $K(s)$ es igual al programa $p$ más chico\* que "imprime" #text(comment)[-para nosotros, devuelve-] $s$. \
#text(comment)[\* entendido como más chico, cantidad de bits que ocupa el código del programa.] \

Evidentemente hay una especie de \"cota superior\" para $K(s)$,
```c
printf("%s", s);
```
es el programa *trivial* más chico que imprime $s$. \

Asumamos que es computable, por lo que puedo usarla en un programa. Basándonos en la tesis de Church, puedo usar un programa en C tranquilamente tal que \
#text(comment)[$arrow.t$ Todo para no hacerlo en $S$++, el rey de la vagancia.]

```c
// Es un pseudocódigo, puede contener errores.
int main() {
  const int C = 10000; // Constante C arbitraria, tal que sea mayor a la "longitud" de este programa.
  char *s = "";

  while (K(s) <= C) {
    // Donde next(s) es una función que devuelve la siguiente cadena en el alfabeto Sigma, por ejemplo, en un alfabeto de la "a" a la "z", todas minúsculas.
    *s = next(s);
    // Si *s == "", entonces next(s) == "a", si *s == "a", entonces next(s) == "b", ..., si *s == "z", entonces next(s) == "aa"...
  }

  printf("%s", s);
  return 0;
}
```

Tenemos asumido que $K(s)$ es computable. \
Analicemos el comportamiento del $"while"$ en $"main"$. \
Este programa va probando todas las cadenas posibles de $SigmaEstrella$, posiblemente en cierto orden lexicográfico, hasta encontrar una cadena $s$ tal que $K(s) > C$. \
Cuando encuentra una cadena $s$ tal que $K(s) > C$, imprime $s$ y termina. \
#text(comment)[Notar que esa cadena $s$ existe, pues siempre va a existir un string $s$ tal que $K(s) > C$. Pensar en un string "archivo" aleatorio muy, muy, muy, grande.] \
Sin embargo, la existencia de esa cadena $s$ tal que $K(s) > C$, es un absurdo. Pues el programa $"main"$ es un programa que imprime $s$ y tiene complejidad de Kolmogorov $<= C$. \
Luego, la complejidad de Kolmogorov no es computable. \

#QED

== Ejercicio 1
Dar un algoritmo que decida si dos expresiones regulares denotan el mismo lenguaje. \
Justificar la correctitud.

Sean $E_1$ y $E_2$ las dos expresiones regulares. \
1. Utilizando el algoritmo visto en la práctica, puedo *construir los autómatas finitos* $M_1$ y $M_2$ que reconocen los lenguajes denotados por $E_1$ y $E_2$ respectivamente.
2. Luego, puedo *determinizar* ambos autómatas, tal como vimos en la teórica #text(comment)[(usando el método de la construcción de subconjuntos)], obteniendo $M'_1$ y $M'_2$.
3. Ahora, puedo *minimizar* ambos autómatas determinísticos, obteniendo $M''_1$ y $M''_2$. \
   #text(comment)[Me estoy valiendo de minimización, que no vimos en la materia, pero sabemos que el lenguaje generado por dos autómatas finitos es el mismo si y solo si los autómatas finitos minimizados son iguales.]
4. Finalmente, puedo *comparar los autómatas finitos minimizados* $M''_1$ y $M''_2$ para determinar si los lenguajes generados por $E_1$ y $E_2$ son iguales. \

De manera alternativa propongo, \
Si tenés un ER, la podés pasar a AFD; luego, de AFD podés pasarlo a AP determinístico, por lo que es, en particular, un lenguaje libre de contexto determinístico; y en una diapositiva se mencionó que hay un algoritmo hecho por Géraud Sénizergues para saber si dos lenguajes libres de contexto determinísticos son iguales. \

También, \
Podemos usar la *diferencia simétrica*, pues.

*Diferencia simétrica* $to L_1 xor L_2 = (L_1 - L_2) union (L_2 - L_1)$. \
Donde la *diferencia* $L_1 - L_2$ se puede obtener como $L_1 sect ComplementoConjunto(L_2)$. \

Luego, $E_1 = E_2 sii LenguajeDe(E_1) xor LenguajeDe(E_2) = emptyset$. \

== Ejercicio 2
Dar dos algoritmos distintos para determinar si el lenguaje aceptado por un autómata finito dado es el conjunto de todas las cadenas del alfabeto. Justificar cada uno.

Me piden dar dos algoritmos distintos para saber si el lenguaje aceptado por un autómata finito es $SigmaEstrella$. \

Sea $M = AF(Q, Sigma, delta, q_0, F)$ un autómata finito. \

Preguntarme si $LenguajeDe(M) = SigmaEstrella$ es equivalente a preguntarme si $LenguajeDe(M^c) = emptyset$. \
Luego, basta ver que $LenguajeDe(M^c) = emptyset$. \
Sé que los autómatas finitos son cerrados por complemento. \
#text(comment)[Si $M$ no era AFD completo, lo hago AFD completo usando el método de construcción de subconjuntos y agrego los estados "fantasmas" que sean necesarios.] \

=== Algoritmo 1
Por el *lema de pumping*, sé que $LenguajeDe(M^c) = emptyset sii$ el no existe cadena de longitud menor que $n$ que pertenezca a $LenguajeDe(M^c)$. \
Esto es computable #text(comment)[(particularmente r.p.)] pues es un existe acotado: palabras de longitud $n$ con los símbolos del alfabeto finito, a su vez, saber si una palabra pertenece a un lenguaje regular es computable #text(comment)[(en tiempo lineal)]. \

=== Algoritmo 2
Como todo lenguaje aceptado por autómata finito, tiene una gramática regular que lo genera, puedo *construir la gramática regular* $G^c$ que genera $LenguajeDe(M^c)$. \
Luego, puedo obtener el conjunto de no terminales activos de $G^c$ con la función $"ObtenerNoTerminalesActivos"(G^c)$ y ver si $S$ peternece al conjunto de terminales activos. \
- Si $S$ es un terminal activo #text(comment)[$(S in "ObtenerNoTerminalesActivos"(G^c))$], entonces $LenguajeDe(G^c) != emptyset$. \
- Si $S$ no es un terminal activo #text(comment)[$(S in.not "ObtenerNoTerminalesActivos"(G^c))$], entonces $LenguajeDe(G^c) = emptyset$. \

== Ejercicio 3
Dar un algoritmo que determine si un lenguaje regular dado es infinito. Justificar.

Sé, por el *lema de pumping*, que $L$ es infinito $sii$ existe una palabra de longitud entre $n$ y $2n - 1$ que pertenece a $L$. \
Sé que este algoritmo es computable pues es un existe acotado y saber si una palabra pertenece a un lenguaje regular es computable. \

== Ejercicio 4
¿Cuántos autómatas finitos deterministas con dos estados pueden construirse sobre el alfabeto ${0, 1}$?

Tomo la definición de AFD como $M = AF(Q, Sigma, delta, q_0, F)$. Esto quiere decir que,
- Debe haber al menos un estado inicial.
- El conjunto de estados finales puede ser vacío.

Tengo limitada la cantidad de estados a $2$ y me dan el alfabeto $Sigma$. \
Significa que puedo cambiar 3 cosas de mi autómata finito determinístico: \
1. El estado inicial.
2. El conjunto de estados finales.
3. La función de transición.

Para el estado inicial puedo elegir cualquiera de los dos estados de dos maneras.
- Es inicial.
- No es inicial.
#text(comment)[Lo que sí, elegir uno me deja fijado el otro.] \

Para el conjunto de estados finales puedo elegir cualquier subconjunto del conjunto $Q$, por lo que hay $|PartesDe(Q)| = 2^2 = 4$ posibles conjuntos de estados finales. \
Sé que la función de transición va de $Q times Sigma to Q$. $|Q| = 2, |Sigma| = 2$. Por lo que el número de transiciones posibles es $2^(2 times 2) = 2^4 = 16$. \

Luego, el número total de autómatas finitos determinísticos con dos estados es $2 times 4 times 16 = 128$. \

== Ejercicio 5
Sean $L_1$ y $L_2$ lenguajes regulares. Hacer un AFD que reconoce el producto cartesiano de $L_1$ y $L_2$.

Es la concatenación pero ligeramente modificada.

Pongo \"$($\" antes de arrancar el autómata que reconoce $L_1 L_2$, luego, en lugar de las transiciones $lambda$ para pasar del autómata que reconoce $L_1$ al que reconoce $L_2$, pongo una transición con el símbolo \"$,$\" para finalmente poner una transición que lee \"$)$\", de todos los estados finales del autómata que reconoce $L_2$, a un estado final nuevo $q_f$, que será el único estado final del autómata. \

== Ejercicio 6
Determinar verdadero o falso y justificar.
1. Para cada AF hay infinitos AFD que reconocen el mismo lenguaje \ *Verdadero* \ Siempre puedo agregar estados inalcanzables.
2. Si $L$ es libre de contexto, todo subconjunto de $L$ es libre de contexto \ *Falso* \ Contraejemplo: $L = {a^n b^n c^m | n, m >= 0}$, un subconjunto es $L' = {a^n b^n c^n | n >= 0}$, que no es libre de contexto.
3. El lema de pumping para lenguajes libres de contexto implica que si un lenguaje se puede bombear entonces es regular o libre de contexto. \ *Falso* \ Libre de contexto implica que se cumple el lema de pumping, no vale la vuelta.
4. Los autómatas finitos determinísticos reconocen una cadena de longitud $n$ en exactamente $n$ transiciones. \ *Verdadero* \ No hay transiciones $lambda$ y es determinístico, en cada transición se lee un símbolo, no me puedo quedar "boludeando" en el autómata, siempre "avanzo".
5. Los autómatas de pila determinísticos reconocen una cadena de longitud $n$ en exactamente $n$ transiciones. \ *Falso* \ Puedo tener configuraciones ciclantes.
6. Sea $M$ un AFD y sea $M^R$ el autómata que resulta de revertir función de transición. $LenguajeDe(M) sect LenguajeDe(M^R)$ es regular. \ *Verdadero* \ $M^R$ sigue siendo un autómata finito, por lo que $LenguajeDe(M^R)$ es regular. Luego, la intersección de dos lenguajes regulares es regular.

== Ejercicio 7
Dado APD $P = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$, dar un algoritmo que decida si $LenguajeDe(P) = SigmaEstrella$. Justificar la correctitud.

Dado que el autómata de pila es determinístico, puedo construir un autómata de pila que acepte el complemento de $LenguajeDe(P)$, $P^c$. \
#text(comment)[Los lenguajes libres de contexto determinísticos están cerrados por complemento, *solamente los determinísticos*.] \

Asumo construido $P^c$ igual a la demostración vista en clase, que se puede construir, tal como se hace en la demostración. \
Luego, puedo determinar si $LenguajeDe(P^c) = emptyset$. \

Para ello basta con ver:
- si $S$ no es un terminal activo de la gramática regular que genera $LenguajeDe(P^c)$, que la puedo generar pues todo autómata de pila tiene una gramática que genera el lenguaje que acepta el autómata.
- o, sea $n$ la constante del lema de pumping, si no existe una palabra de longitud menor que $n$ que pertenezca a $LenguajeDe(P^c)$. \

== Ejercicio 8
Determinar verdadero o falso y justificar. Es decidible que:
1. La intersección de dos conjuntos c.e. es un conjunto c.e. \ *Verdadero* \ Sí, pues el predicado característico de la intersección de dos conjuntos c.e. es computable, agarrás ambos predicados y usás el $and$ r.p.
2. Si HALT fuera computable entonces la complejidad de Kolmogorov $K : NN to NN$, $K(s) = min{|p| : Psi^(1)(p) = s}$, sería computable. \ *Verdadero* \ Como los programas son infinitos numerables, y sabemos que hay una \"cota trivial superior\" #text(comment)[$arrow.l$ printf(...)] para la complejidad de Kolmogorov, podríamos enumerar todos los programas para luego con HALT ver si terminan, de más chico a más grande. \ #text(comment)[Es decir, programa $0$ es la instrucción _pass_, ...] \ Si no termina lo salteamos, si termina lo simulamos con la máquina de Turing universal y vemos si devuelve $s$. Si devuelve $s$, devolvemos $|p|$, si no devuelve $s$, seguimos con el siguiente programa. \ A su vez, como estamos recorriendo los programas, tal que $|p_i| < |p_j| "con" i < j$, si en algún momento llegamos a un $p_i$ tal que $|p_i| > |"printf"(...)|$, terminamos y devolvemos $|"printf"(...)|$.
3. La pertenencia de una palabra a un lenguaje computable es computable. \ *Verdadero* \ Si el lenguaje es computable, entonces el predicado característico es computable.
4. Clausura de Kleene de un lenguaje c.e. es c.e. \ *Verdadero* \ Sea $L$ un lenguaje c.e., sé que su predicado característico es parcialmente computable, es decir, tengo una máquina de Turing que acepta cadenas de $L$. Luego, puedo construir una máquina de Turing que acepte $L^n$, para todo $n$. Es como la "concatenación" de máquinas de Turing.
5. Clausura de Kleene de lenguaje computable es computable \ *Verdadero* \ Idem arriba pero con lenguajes computables.
6. La reversa de un lenguaje computable es computable. \ *Verdadero* \ La reversa es r.p. \ Tengo el predicado característico de $L$, computable. Luego $L^r$ tiene como predicado característico $p_(L)("reversa"(s))$.
7. La reversa de un lenguaje c.e es c.e. \ *Verdadero* \ Idem arriba, pero reemplazando computable por c.e.
8. Todo conjunto infinito c.e. tiene un subconjunto infinito computable \ *Verdadero* \ Sabemos que el conjunto c.e. es infinito, por lo que es $!= emptyset$. \ Luego, sé que existe una función $f$ r.p. que lo numera, es decir, $L = {f(0), f(1), f(2), ...}$. \ Defino un subconjunto $D$, tal que $D = {f(i) : f(i) succ f(j) "para todo" j < i}$, en otras palabras, $f(i) in D$ si es más larga que todas las palabras que aparecieron antes que $f(i)$ en la enumeración. \ #align(center)[#text(comment)[$"Ejemplo, si" L = {a a, a a a, 0, a, b b, b b b , 11, c c c c, 2 2 2, ...}, \ D = {a a, a a a, b b b, c c c c, ...}.$]] Falta probar que es infinito y computable. \ Digo que es infinito, pues si no lo fuese, significaría que hay una palabra en $D$ con longitud $>=$ a todas las demás. Sea $f(m)$ el último elemento de $D$ -de mayor longitud-, como $f(m)$ fue el "último record", para todo $i > m$ se tiene $f(i) succ f(m)$. \ Esto implica que las palabras de $L$ están acotadas por $|f(m)|$, lo que es un absurdo pues $L$ es infinito. \ Para probar que es computable, queremos mostrar que existe un algoritmo que, dada una palabra $w$, decide (en tiempo finito) si $w in D$. \ #align(center)[Arrancamos con $i = 0$. \ Obtenemos $f(i)$. \ Vemos si $w = f(i)$. \ En caso afirmativo, devolvemos *1*. \ En caso negativo, separamos en dos casos: \ Si $|w| < |f(i)|$, devolvemos *0*. \ Si $|w| > |f(i)|$, incrementamos $i$ y volvemos a empezar.]

== Ejercicio 9
Determinar verdadero o falso y justificar:
1. Toda función total de $NN$ en $NN$ es computable. \ *Falso* \ Hay funciones totales de $NN to NN$ que no son computables. El conjunto de funciones computables es numerable, mientras que el cardinal del conjunto que tiene todas las funciones totales de $NN to NN$ es infinito no numerable (argumento diagonal de Cantor).
2. El conjunto de funciones parcialmente computables de $NN$ en $NN$ es c.e. \ *Verdadero* \ Las funciones parcialmente computables sabemos que son infinitas numerables, pues cada función parcialmente computable puede ser representada por un programa en $S$++, condificado con números naturales. \ Estos programas en $S$++ tienen una biyección con los números naturales. \ #text(comment)[Vimos en clase que la función biyectiva que codifica los programas en $S$++ como números naturales es computable.] \ Por lo tanto, el conjunto de funciones parcialmente computables es c.e., ya que existe un procedimiento efectivo #text(comment)[_(la función biyectiva)_] para listar todos los programas en $S$++ que las representan. \ \ Estoy utilizando la siguiente definición conjunto computablemente enumerable: 
 - _"In computability theory, a set $S$ of natural numbers is called computably enumerable (c.e.), recursively enumerable (r.e.), semidecidable, partially decidable, listable, provable or Turing-recognizable if:"_
  - _"There is an algorithm that enumerates the members of $S$. That means that its output is a list of all the members of $S: s_1, s_2, s_3, ...$. If $S$ is infinite, this algorithm will run forever, but each element of $S$ will be returned after a finite amount of time. Note that these elements do not have to be listed in a particular way, say from smallest to largest."_
  Extraida de #link("https://en.wikipedia.org/wiki/Computably_enumerable_set")[Wikipedia: Computably enumerable set]

== Ejercicio 10
Sea APD $P = (Q, Sigma, delta, Gamma, q_0, F)$ y AP $S = (Q', Sigma', delta', Gamma, q_0', F')$. \
Determinar verdadero o falso y justificar. \
1. $LenguajeDe(P) union LenguajeDe(S)$ es libre de contexto. \ *Verdadero* \ La unión de dos lenguajes libres de contexto es libre de contexto.
2. $LenguajeDe(P) sect LenguajeDe(S)$ es libre de contexto. \ *Falso* \ La intersección de dos lenguajes libres de contexto no necesariamente es libre de contexto.
3. Sea $G = Gramática(V_N, V_T, P, S)$ una gramática libre de contexto que no es recursiva a izquierda. \ Entonces, si $w in LenguajeDe(G)$, su árbol de derivación tiene altura menor que $(|w| + 1)(|V_N| + 1)$. \ *Verdadero* \ Llamemos $n = |w|$ y $k = |V_N|$. \ Consideremos el árbol de derivación más a la izquierda /*de altura mínima*/ para $S =>^h_L w$. \ Sea $n_0$ un nodo hoja del árbol de derivación con etiqueta $a in V_T^*$. \ Supongamos que hay un camino de longitud mayor o igual que $(n + 1)(k + 1)$ nodos de la raíz a la hoja $n_0$. Es decir, \ Sea $S = X_0 frown X_1 frown ... frown X_((n + 1)(k + 1)) = a$, el camino desde la raíz a $n_0$. \ Separemoslos en $n+1$ segmentos de $k+2$ nodos, \ \ #align(center)[ $S = X_0 frown ... frown X_(k+1) \ X_(k+1) frown ... frown X_(2(k+1)) \ ... \ X_(n(k+1)) frown ... frown X_((n+1)(k+1)) = a.$] Consideremos los $n+1$ subárboles de derivación, con $alpha, beta in (V_N union V_T)^*$ apropiados. \ \ #align(center)[ $X_0 =>^(k+1)_L alpha_1 X_(k+1) beta_1 \ X_(k+1) =>^(k+1)_L alpha_2 X_(2(k+1)) beta_2 \ ... \ X_(n(k+1)) =>^(k+1)_L alpha_(n+1) X_((n+1)(k+1)) beta_(n+1).$ ] Es imposible que todas las derivaciones aporten uno o más terminales a $w$, pues $|w| = n$, y acá hay $n+1$ derivaciones. Luego, al menos una de estas derivaciones aporta $lambda$ a $w$. \ \ Supongamos que fuese la $i$-ésima derivación. Eso implica que $alpha_i, beta_i in V_N^*$ pues no pueden aportar terminales a $w$. \ En particular, tanto $alpha_i$ como $beta_i$ aportan $lambda$, pues sabemos que #align(center)[$X_(i(k+1)) =>_L X_(i+1(k+1)) =>^*_L alpha_(n+1) a beta_(n+1).$] Y como en cada derivación hay $k+1$ pasos de derivación pero solamente $|V_N| = k$ no terminales, en algún momento de la $i$-ésima derivación, se llegó a algo de la pinta $X_i =>^+_L X_i$. \ Sin embargo, habíamos dicho que $G$ era no recursiva a izquierda, por lo que llegamos a un absurdo. \ Luego, la altura del árbol de derivación es menor que $(n + 1)(k + 1)$, pues no puede haber un camino de longitud mayor o igual que $(n + 1)(k + 1)$ nodos de la raíz a una hoja.

4. Para todo AP hay un APD que reconoce el mismo lenguaje. \ *Falso* \ No tienen el mismo poder expresivo.
5. Clausura de Kleene de un lenguaje regular es regular. \ *Verdadero* \ Me puedo armar el autómata finito. De hecho, creo que es un ejercicio de la práctica.
6. Clausura de Kleene de un lenguaje libre de contexto determinístico es libre de contexto determinístico. \ *Falso* \ Vimos en la teórica que los lenguajes libres de contexto determinísticos no están cerrados por la clausura de Kleene.
7. Clausura de Kleene de un lenguaje libre de contexto es libre de contexto. \ *Verdadero* \ Me puedo armar el autómata de pila.
8. Es decidible si dos AFs reconocen el mismo lenguaje. \ *Verdadero* \ Se puede pasar de AF a ER con el algoritmo que vimos en la práctica, luego, sale de inmediato por el ejercicio 1.

== Ejercicio 11
=== Definición de codeterminismo
Un autómata finito se dice que es *codeterminístico* si, para cualquier par de estados $p$ y $q$ en el autómata, y para cualquier símbolo $a$ en el alfabeto, no existe ningún estado $r$ tal que sea alcanzable desde $p$ con la entrada $a$ como desde $q$ con la misma entrada $a$ (es decir, no existe $r$ tal que $r in delta(p, a)$ y $r in delta(q, a)$). \

Es decir, no puede haber un estado que sea alcanzable por dos estados distintos, leyendo el mismo símbolo del alfabeto.

\

Dar un algoritmo que codeterminice un automata finito.

Intuitivamente lo que queremos es, dado un autómata con transiciones de esta pinta:

#align(center)[
  #cetz.canvas({
    import cetz.draw: circle, line, content
    import draw: state, transition

    state((0, 2), "q", label: $q$)
    state((0, 0), "q'", label: $q'$)

    state((6, 1), "qf", label: $q_f$, final:true)

    transition("q", "qf", label: $a$, curve: .75)
    transition("q'", "qf", label: $a$, curve: -.75)
  })
  _Puedo asumir, sin pérdida de generalidad, que hay un único estado final._
]

Pasarlo a algo de esta pinta:

#align(center)[
  #cetz.canvas({
    import cetz.draw: circle, line, content
    import draw: state, transition

    state((0, 0), "q", label: $q$)

    state((6, 0), "qf", label: $q_f$, final: true)

    transition("q", "qf", label: $a$, curve: .5)
  })
]

Pero fijate esto, tomamos el reverso del autómata del primer dibujo, nos queda algo interesante:
#align(center)[
  #cetz.canvas({
    import cetz.draw: circle, line, content
    import draw: state, transition

    state((0, 2), "q", label: $q$)
    state((0, 0), "q'", label: $q'$)

    state((6, 1), "qf", label: $q_f$)

    transition("qf", "q'", label: rotate(180deg)[$a$], curve: .75)
    transition("qf", "q", label: $a$, curve: -.75)
  })
]

Lo que hizo que nos quede un autómata finito totalmente no determinístico. \

Es por ello que propongo la siguiente idea para codeterminizar un autómata finito:
- Hacemos el reverso del autómata finito.
- Determinizamos el autómata finito resultante.
- Tomamos otra vez el reverso del autómata finito.

De esta manera, vamos a quitar las transiciones del estilo $delta(q_f, a) = {q, q'}$ del autómata finito reverso, lo que implica que sacamos las transiciones del estilo $delta(q, a) = {q_f}$ y $delta(q', a) = {q_f}$ del autómata finito original. \

Esta idea está basada en el #link("https://en.wikipedia.org/wiki/DFA_minimization#Brzozowski's_algorithm")[algoritmo de Brzozowski] para minimizar un autómata finito. \
La única diferencia entre su algoritmo y este, es que el determiniza otra vez el autómata finito.

== Ejercicio 12
Dado un autómata finito determinístico $A = AF(Q_A, Sigma, delta_A, q_0, F_A)$ y dado un autómata de pila determinístico $P = AP(Q_P, Sigma, Gamma, delta_P, p_0, Z, F_P)$, dar un algoritmo que decida si el lenguaje $L(A) sect L(P)$ es finito. Justificar la correctitud.

Hecho con demostración en la #underline[@demo_intersección_leng_regular_y_leng_libre_de_contexto_determinístico[sección]].

== Ejercicio 13
Sea $Sigma$ un alfabeto. Dada una gramática libre de contexto $G = Gramática(V_N, V_T, P, S)$ que no es recursiva a izquierda y una palabra $w in V_T^*$, dar un algoritmo que explore los árboles de derivación de $G$ y para determinar si $w in LenguajeDe(G)$ o no. Justificar la correctitud.

Se podría hacer BFS (o DFS) en todos los árboles de derivación, tipo fuerza bruta, tal que si la altura del árbol se pasa de $(|w| + 1)(|V_N| + 1)$ y no generó la cadena $w$ aún, entonces rechazamos (backtracking en caso de DFS). \
La respuesta del algoritmo sería el $or$ de todas las respuestas de los árboles de derivación. \
Notar que aunque los árboles de derivación sean infinitos, estos crecen verticalmente, no horizontalmente _(las producciones son finitas)_, por lo que el algoritmo termina _(por nuestra cota de la altura)_. \

El algorito es correcto porque es un algoritmo de bruteforce.

== Ejercicio 14
Determinar verdadero o falso y justificar:
1. Si un lenguaje es reconocible por un autómata contador entonces el lenguaje complemento también. \ *Falso* \ Contraejemplo: $L = {a^n b^n c | n >= 0}$ es reconocible por un autómata contador, pero $a^(n+2) b^(n+2) c^(n+2) in.not L$ no es reconocible por un autómata contador (ni siquiera es libre de contexto).
2. Si dos lenguajes $L_A$ y $L_B$ son reconocibles por autómatas contadores entonces el lenguaje de su unión $L_A union L_B$ también. \ *Verdadero* \ Tengo dos autómatas contadores, $M_A$ y $M_B$, que generan $L_A$ y $L_B$ respectivamente. Puedo armarme un autómata contador $M$ que reconozca $L_A union L_B$ de la siguiente manera: \
  - Los estados son $Q = Q_A union Q_B union {q_0}$. \
  - $q_0$ estado inicial. \
  - Agrego transiciones $lambda$ de $q_0$ a los estados iniciales de $M_A$ y $M_B$, $q_0_A$ y $q_0_B$ respectivamente, sin tocar la pila.
  - El alfabeto de la pila es $Z_0, I$.
  - Mantengo los estados finales de $M_A$ y $M_B$ como estados finales de $M$. \
  - El alfabeto $Sigma$ asumo que es el mismo para $M_A$ y $M_B$, por lo que es el mismo para $M$.
  Faltaría probar que $LenguajeDe(M) = L_A union L_B$, aunque se puede notar por construcción.
3. Si dos lenguajes $L_A$ y $L_B$ reconocibles por un autómatas contadores, entonces el lenguaje de su intersección $L_A sect L_B$ también. \ *Falso* \ Contraejemplo: $L_A = {a^n b^n c^m | n, m >= 0}$ y $L_B = {a^n b^m c^m | n, m >= 0}$ son reconocibles por autómatas contadores, pero $L_A sect L_B = {a^n b^n c^n | n >= 0}$ no es reconocible por un autómata contador.
Ayuda: Considerar cómo se demuestran y cómo se refutan las propiedades de clausura de los lenguajes libres de contexto.

Notar que el item 1. del complemento se puede justificar tambien como: \
Supongamos que $ComplementoConjunto(L)$ es reconocible por un autómata contador. \
Entonces, $L_1 sect L_2 = ComplementoConjunto(ComplementoConjunto(L_1) union ComplementoConjunto(L_2))$ sería reconocible por un autómata contador, lo que es un absurdo. \

== Ejercicio 15
Dado $R$ un lenguaje regular y dado $L$ un lenguaje libre de contexto determinístico, dar un algoritmo que decide si $L = R$.

Respuesta en el Hopcroft 1976, teorema 10.6.

== Ejercicio 16
Dado $R$ un lenguaje regular y dado $L$ un lenguaje libre de contexto determinístico, ¿es decidible si $R subset.eq L$?

Respuesta en el Hopcroft 1976, teorema 10.6.

== Ejercicio 17
Un autómata de cola es un autómata posiblemente no determinístico similar a un autómata de pila pero tiene una cola en vez de una pila. Formalmente un autómata de cola es $M = AP(Q, Sigma, Gamma, delta, q_0, Z_0, F)$ donde:
- $Q$ es el conjunto de estados
- $Sigma$ es el alfabeto de la cinta entrada
- $Gamma$ el el alfabeto de cola
- $delta : Q times Sigma union {lambda} times Gamma union {lambda} to PartesDe(Q times Gamma union lambda)$ es la función de transición
- $q_0 in Q$ es el estado incial.
- $Z_0 in Gamma$ es el símbolo inicial de la cola.
- $F subset.eq Q$ es el conjunto de estados finales.

Por ejemplo $delta(q_1, a, b) = {(q_2, c),(q_3, d)}$ dice que el estado $q_1$ si lee $a$ de la entrada y $b$ está primero en la cola, entonces $b$ sale de la cola, el autómata pasa al estado $q_2$, y pone $c$ a lo último en la cola.

Demostrar que los autómatas de cola tienen mayor poder expresivo que los autómatas de pila. \
Ayuda: dar un lenguaje que no es libre de contexto.

Una forma es demostrar que un autómata de pila puede simular una máquina de Turing, asumiendo que la máquina de Turing tiene mayor poder expresivo que un autómata de pila. \

Otra forma menos complicada es la siguiente: \
Basta con dar un autómata de cola que reconozca un lenguaje que no sea libre de contexto. \
Por ejemplo, el lenguaje $L = {w w | w in SigmaEstrella}$ no es libre de contexto, se puede demostrar utilizando el lema de pumping. \

Sea $Sigma = {a, b}$, el autómata de cola por cola vacía $M$ que reconoce $L$ es el que se muestra en el dibujo. \

#align(center)[
  #cetz.canvas({
    import cetz.draw: circle, line, content
    import draw: state, transition

    state((0, 0), "q0", label: $q_0$, initial: "")
    state((6, 0), "qf", label: $q_f$)

    transition("q0", "q0", label: $a, lambda | A #h(.75cm) b, lambda | B$, curve: 1, anchor: (bottom))
    transition("q0", "qf", label: $lambda, Z_0 | lambda$, curve: .5)
    transition("qf", "qf", label: $a, A | lambda #h(.75cm) b, B | lambda$, curve: 1, anchor: (bottom))
  })
]

== Ejercicio 18
Demostrar que:
1. Todos los lenguajes reconocibles por autómatas finitos son reconocibles por autómatas contadores. \ Es definirte un autómata contador a partir del autómata finito, donde no se use la pila. \ ¿Demostración? en la pregunta 2 de la #underline[@demo_af_contenido_en_ap[sección]].
2. No todos los lenguajes reconocibles por un autómata contador son reconocibles por un autómata finito. \ $L = {a^n b^n | n >= 0}$ es reconocible por un autómata contador, pero no es reconocible por un autómata finito. Debería dar el autómata contador que reconoce $L$ y demostrar por el lema de pumping que no es reconocible por un lenguaje regular.

/*
== Extra
#text(comment)[Final tomado el 9 de diciembre de 2024.]

Un autómata finito $M = AF(Q, Sigma, delta, q_0, F)$ es codeterminístico si su función de transición $delta : Q times Sigma to PartesDe(Q)$ satisface que para todo par de estado $p, q$, y para todo $a in Sigma$, no hay $r$ tal que $r in delta(p, a)$ y $r in delta(q, a)$, y $F$ consiste en un único estado final. 

=== Ejercicio 1
Sea $M = AF(Q, Sigma, delta, q_0, F)$ un autómata finito determinístico. Dar un autómata finito $N$ codeterminístico de modo ta que $M$ y $N$ aceptan el mismo lenguaje, y demostrarlo.
Ayuda: Probar que AF $M$ es codeterminístico si y solo si $M^("reverso")$ es determinístico.

Vayamos primero con la ayuda. Quiero ver que $M$ es codeterminístico si y solo si $M^("reverso")$ es determinístico. \
Más formalmente, sea $M = AF(Q, Sigma, delta, q_0, F)$ un autómata finito determinístico. \ Quiero probar que,

#align(center)[
  $forall p,q,r in Q, p != q, forall a in Sigma, \
  (
  r in.not delta(p, a) and r in.not delta(q, a) and |F| = 1 
  sii 
  p,q in.not delta^("reverso")(r, a) and |delta^("reverso")(r, a)| = 1
  )
  $]

Donde $delta^("reverso")(q, a) = {p : q in delta(p, a)}$, la función de transición del autómata $M^("reverso")$.

- $(implica2)$: \
  Supongamos que $M$ es codeterminístico, veamos que su reverso es determinístico. \
  #align(center)[Es decir, se cumple que \ $r in.not delta(p, a) and r in.not delta(q, a) and |F| = 1$.]
  Ahora, la función de transición de revertir un autómata es $delta^("reverso")(q, a) = {p : q in delta(p, a)}$. \
  Luego, como $r in.not delta(p, a) and r in.not delta(q, a)$ vale que $p,q in.not delta^("reverso")(r, a)$. \
  A su vez, $|delta^("reverso")(r,a)| = 1$ pues para cada $a in Sigma$ le llegaba una única transición _#text(comment)[(pues $M$ es codeterminístico)]_.

=== Ejercicio 2
Sea $S subset.eq NN$ un conjunto computablemente enumerable. \
Sea $g : NN to NN$ una función que enumera $S$ y sea $T = {g(i) : "para todo" j < i, g(j) < g(i)}$. \
Demostrar que $T$ es un conjunto computable.
Ayuda: Caso $g$ tiene dominio finito, y caso $g$ tiene dominio infinito.
*/